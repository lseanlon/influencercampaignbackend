Sean’s Supports:
0. Configuration info
 
http://ifluence.asia/ApiDoc/index.html#api-v1-FormService

1. campaigns: provide api endpoint to get: completed campaigns, draft campaigns and running campaigns
 
http://ifluence.asia/ApiDoc/index.html#api-v1_CampaignRunning-CampaignRunning_get

2. credit: give an api to get credit information (such as amount of credit)
http://ifluence.asia/ApiDoc/index.html#api-v1_UserProfile-UserProfile_get

3. campaign details: api to get status of a campaign (like as requested, ..)
http://ifluence.asia/ApiDoc/index.html#api-v1_CampaignRunning-CampaignRunning_get

4. for billing history: give an api (filter by date and by campaign detail)
*dont have it

5. list of KOLS: api to accept/reject functions, api to show follower size, engagement rate (see screenshot)
*dont have it

6. when reject, we must send reason to Influencer, give api to post reason message
*dont have it

7. filter list of KOLS: api to get category group, filter by group
*dont have it

8. for create campaign, - add a parameter for KOI requirement end date / - add new parameter for KOI Group of campaign, its an array
* need clarification

9. api to “save as draft”when create a campaign
* can utilize exisgin structure

10.  when click on accept KOL, it’ll show chart box => give api to get data in chart boxes
* dont have it need transformation

Influencer login
1. api to filter campaign by categories( such as Fashion / Travel …)
http://ifluence.asia/ApiDoc/index.html#api-v1_Campaign-Campaign_get

2. campaign list page: api to get favorite campaigns, my campaigns, *discovery campaigns
http://ifluence.asia/ApiDoc/index.html#api-v1_Bookmark-Bookmark_get


3. in chart boxes screen, give api to get reject’s reason

4. api notification to applied/running campaigns when campaign has been updated
http://ifluence.asia/ApiDoc/index.html#api-v1_NotificationTransaction-NotificationTransaction_add


Notification
- define kind of notifications (have new campaign, campaign updated…)
http://ifluence.asia/ApiDoc/index.html#api-v1_Notification-Notification_get

*cannot cover all - cover some first
- Review campaign for both influencer and merchant
http://ifluence.asia/ApiDoc/index.html#api-v1_Review-Review_get