<?php

function connectDB()
{ 
    $db = connect_db();
    return $db;
}

function crudDB($statement)
{
    $db = connectDB();
    if ($db->query($statement) === TRUE) {
        return $db;
    } else {
        // echo " Error: " . $sql . "<br>" . $conn->error;
        return false;
    }
    
}
function queryDB($statement)
{
    $db   = connectDB();
    $rs   = $db->query($statement);
    $data = $rs->fetch_all(MYSQLI_ASSOC);
    return $data;
}
//Safety get value
function getKeyVal($objArr, $keyId)
{
    if (!is_null($objArr) && array_key_exists($keyId, $objArr)) {
        return $objArr->$keyId;
    } else {
        return "";
    }
    
}
function getJsonRequest($app)
{
    return json_decode($app->request->getBody());
}
function getJsonResponse($app, $jsonData)
{
    $jsonType = "application/json;charset=utf-8";
    header("Content-Type:  " . $jsonType);  
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Headers: X-Requested-With');
    header('Access-Control-Allow-Headers: Content-Type');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
    header('Access-Control-Max-Age: 86400');
    $response                 = $app->response();
    $response['Content-Type'] = $jsonType;
    
    $response->headers->set('Content-Type', $jsonType);
    $response->body(json_encode($jsonData, JSON_UNESCAPED_UNICODE));
    //alternate / basic way return json
    //echo json_encode($jsonData)
    //WILL CAUSE 404 -NOT FOUND
    //exit;
}
   
 
?>