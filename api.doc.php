<?php
date_default_timezone_set("Asia/Kuala_Lumpur");
// BATCHGROUP group
//  v1Batch/ExpiredCampaign/secret-key
//  
$app->group('/v1Batch', function() use ($app)
{
/**
 * @api {get} /v1Batch/ExpiredCampaign/:secretKey Batchjob Cleanup expired campaign
 * @apiName ExpiredCampaign
 * @apiGroup v1Batch
 *
 *
 * @apiSuccess {String} updCampaignStatus Upd Campaign Status.
 * @apiSuccess {String} currentDate current Date.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "updCampaignStatus": "1",
 *       "currentDate": "2010-12-31"
 *     } 
 * @apiError GeneralException Something went wrong.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error": ""
 *     }
 */
 

/**
 * @api {get} /v1Batch/CampaignRunningStatus/:secretKey Batchjob Cleanup Running Campaign
 * @apiName CampaignRunningStatus
 * @apiGroup v1Batch
 *
 *
 * @apiSuccess {String} updCampaignStatus Upd Campaign Status.
 * @apiSuccess {String} currentDate current Date.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "approUPD": "1",
 *       "nopayUPD": "1",
 *       "progrUPD": "1",
 *       "complUPD": "1" 
 *     } 
 * @apiError GeneralException Something went wrong.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error": ""
 *     }
 */  



/**
 * @api {get} /v1Batch/DashboardPopulation/:secretKey Batchjob Cleanup Running Campaign
 * @apiName DashboardPopulation
 * @apiGroup v1Batch
 *
 *
 * @apiSuccess {String} UserInfluence item 
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "UserInfluence": {
 *      
 *              TotalAmountCollected
*               TotalRunningCampaign
*               FacebookTotalCampaign    
*               FacebookTotalPostLikes   
*               FacebookTotalPostComment 
*               FacebookTotalPostShares  
*               FacebookTotalPostReactions
 *       }
 *     } 
 * @apiError GeneralException The id of the User was not found.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error": ""
 *     }
 */   



/**
 * @api {get} /v1Batch/FacebookUserInfluence/:secretKey Facebook User influence
 * @apiName FacebookUserInfluence
 * @apiGroup v1Batch
 *
 *
 * @apiSuccess {String} updCampaignStatus Upd Campaign Status.
 * @apiSuccess {String} currentDate current Date.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true
 *     } 
 * @apiError 500 Something went wrong.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error": ""
 *     }
 */  
 
/**
 * @api {get} /v1/BrainTreePaymentOnboard/:step/:keyData Do brain tree registration
 * @apiName BrainTreePaymentOnboard - [not finalized]
 * @apiGroup v1
 *
 *
 * @apiSuccess {String} updCampaignStatus Upd Campaign Status.
 * @apiSuccess {String} currentDate current Date.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "status": true
 *     } 
 * @apiError 500 Something went wrong.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error": ""
 *     }
 */  
  

/**
 * @api {get} /v1/PreLogin/ Determine if user already logged in
 * @apiName PreLogin
 * @apiGroup v1
 *
 *
 * @apiSuccess {String} userInfo userInfo item.
 * @apiSuccess {String} userType inf/merchant.
 * @apiSuccess {String} isLoggedOut true/false.
 * @apiSuccess {String} redirect any redirect flag.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *       "userInfo": {userInfo },
 *       "userType": "influencer/merchant",
 *       "isLoggedOut": true/false,
 *       "redirect": "redirect-flag",
 *     } 
 * @apiError 500 Something went wrong.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

   
/**
 * @api {post} /v1/Login/ Login user
 * @apiName Login
 * @apiGroup v1
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
 * @apiParam {LoginType} The Login type - facebook/gmail/normal.
 * @apiParam {Password} User password
 * @apiParam {UserName} User name
 * 
 * 
 * @apiSuccess {String} userInfo userInfo item.
 * @apiSuccess {String} userType inf/merchant.
 * @apiSuccess {String} isLoggedOut true/false.
 * @apiSuccess {String} redirect any redirect flag.
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *{"trxStatus":"SUCCESS","userType":"MERCHANT","userRec":{"Id":"17","UserLoginName":"xgeartech","GoogleLoginName":"","FbLoginName":"","Password":":]","Name":"X Gear Tech","Email":"xgeartech@xgeartechh.comm","Mobile":"+60102264984","Dob":"","Profession":"","Gender":"","Designation":"","CompanyName":"xgear tech Sdn Bhd","CompanyNature":"Tech","CompanyNo":"553-L","CompanyLocation":"xgear tech","Languange":"","Location":"cheras malaysia","Country":"","Summary":"xgear tech is tech accessory","PreferredBankNo":"","PreferredBankName":"","PreferredAccNo":"","PreferredAccName":"","UserTypeId":"2","AccountTypeId":"1","PhotoId":"0","IsFeatureUnlockedInfluencer":"1","FailedLoginAttempt":"000","IsTempBlocked":"0","IsActive":"1","IsLoggedOut":"000","LastFailedLogin":"2017-03-05 16:05:31","LastSuccessfulLogin":"2017-09-16 11:38:22","VirtualAmount":null,"VirtualAmountCheckoutDate":null,"SessionKey":"4064fe4bed3537942c459358fef97e13","CreatedBy":"17","CreatedDate":"2017-09-16 07:29:38","UserType":{"Id":"2","Code":"MERCHANT","Name":"Merchant"},"UserTag":[{"UserId":"17","InterestTagId":"6","IsInfluencer":"0"},{"UserId":"17","InterestTagId":"17","IsInfluencer":"0"}],"UserInfluence":{"UserId":"17","IsBanned":"0","IsFbConnected":null,"IsFbPage":null,"IsInstagramConnected":null,"IsGoogleConnected":null,"IsTwitterConnected":null,"IsVineConnected":null,"TotalSuccessfulCampaign":"0","TotalFailedCampaign":"0","TotalRunningCampaign":null,"TotalAmountCollected":null,"TotalFriendFb":null,"TotalLikesFb":null,"FacebookTotalCampaign":null,"FacebookTotalPostLikes":null,"FacebookTotalPostComment":null,"FacebookTotalPostShares":null,"FacebookTotalPostReactions":null,"InstagramTotalMedia":"0","InstagramTotalSubscribes":"0","TotalFollowerFb":"0","TotalFollowerInstagram":"0","TotalFollowerTwitter":"0","TotalFollowerVine":"0","PageAuthority":"0.00","PageRank":"0.00","ReplyRatio":"0.00","AverageShared":"0.00","CreatedBy":"APIv1","CreatedDate":"2017-03-05 14:25:52"},"UserPayment":{"UserId":"17","PaymentAccNo":"5555555555554444","PaymentHolderName":"xgear tech","PaymentExpiryDate":"022020","PaymentSecurityCode":"321","PaymentIssuerName":"Maybank","PaymentIssuerCategory":"Master","PaymentType":"","CreatedBy":"APIv1","CreatedDate":"2017-03-05 14:25:52"},"UserUsage":{"Id":"1","UserId":"17","Price":"3","RemainingAllowedCampaign":"8","RemainingInfluencer":"5","RemainingPosting":"1","MaxCostRemaining":"9985299.00","CreatedBy":"17","CreatedDate":"2017-07-30 13:21:43"},"UserPhoto":[]},"redirect":"LOGIN-MERCHANT"}  
 *
 * curl 'http://ifluence.asia/API/v1/Login' -H 'Pragma: no-cache' -H 'Origin: http://ifluence.asia' -H 'Accept-Encoding: gzip, deflate' -H 'INFL-AUTH: ocfUP2l5WMytJc4+ciMlnACBHUdjbJU3HyiDAU7IClY=' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept-Language: en-US,en;q=0.8' -H 'Accept: application/json; data=verbose' -H 'Cache-Control: no-cache' -H 'Referer: http://ifluence.asia/' -H 'Proxy-Connection: keep-alive' -H 'INFL-TIMESTAMP: 2017-09-16 11:38:21' --data-binary '{"LoginType":"NORMAL","UserName":"xgeartech","Password":"BdtFtxDicnhUeHtIeBF1xJPfDLxyf0St+Mpx9sCRH94="}' --compressed
 * @apiError 500 Something went wrong.
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

   
/**
 * @api {put} /v1/Login/ Logout user
 * @apiName Logout
 * @apiGroup v1
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 * @apiSuccess {String} status isLoggedOut status
 * @apiSuccess {String} redirect any redirect flag.
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {"status":true,"redirect":"LOGOUT"}  
  
 *
 *  curl 'http://ifluence.asia/API/v1/Login' -X PUT -H 'INFL-KEY: 4064fe4bed3537942c459358fef97e13' -H 'Pragma: no-cache' -H 'Origin: http://ifluence.asia' -H 'Accept-Encoding: gzip, deflate' -H 'INFL-AUTH: ocfUP2l5WMytJc4+ciMlnACBHUdjbJU3HyiDAU7IClY=' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept-Language: en-US,en;q=0.8' -H 'Accept: application/json; data=verbose' -H 'Cache-Control: no-cache' -H 'Referer: http://ifluence.asia/' -H 'Proxy-Connection: keep-alive' -H 'INFL-TIMESTAMP: 2017-09-16 11:38:21' --data-binary '{}' --compressed
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     

/**
 * @api {get} /v1/ResetPassword/:step/:keyData Reset Password user
 * @apiName ResetPassword
 * @apiGroup v1
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
 * 
 * 
 * @apiSuccess {String} status userInfo item.
 * @apiSuccess {String} message inf/merchant.
 * 
 *
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
/**
 * @api {get} /v1/FormService Form list of values to populate on screen
 * @apiName FormService
 * @apiGroup v1
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
 * 
 * 
 * @apiSuccess {array} interestTag list of intersest tags.[{"Id":"1","Code":"P10","Name":"REGULAR ACCOUNT BASIC","SubscriptionName":"REGULAR ACCOUNT BASIC","SubscriptionInfo":"JUST REGULAR ACCOUNT","Price":"10000","MaxAllowedMonth":"32","MaxAllowedCampaign":"100","MaxInfluencer":"100","MaxPosting":"100","MaxCost":"10000.00","CreatedBy":"API","CreatedDate":"2016-06-10 12:12:12.000"} ]
 * @apiSuccess {array} userType list of user types.[{"Id":"1","Code":"INFLUENCER","Name":"Influencer"},{"Id":"2","Code":"MERCHANT","Name":"Merchant"},{"Id":"3","Code":"ADMIN","Name":"ADMIN"},{"Id":"4","Code":"TOPUP","Name":"TOPUP"},{"Id":"5","Code":"ESCROW","Name":"ESCROW"}]
 * @apiSuccess {array} accountType list of account type. [{"Id":"1","Code":"P10","Name":"REGULAR ACCOUNT BASIC","SubscriptionName":"REGULAR ACCOUNT BASIC","SubscriptionInfo":"JUST REGULAR ACCOUNT","Price":"10000","MaxAllowedMonth":"32","MaxAllowedCampaign":"100","MaxInfluencer":"100","MaxPosting":"100","MaxCost":"10000.00","CreatedBy":"API","CreatedDate":"2016-06-10 12:12:12.000"}]
 * @apiSuccess {array} campaignStatus list of campaign currency.[{"Id":"15","StatusCode":"AVAIL","Description":"AVAIL"} ]
 * @apiSuccess {array} virtualCurrency list of virtual currency.[{"Id":"1","Description":"usd","Unit":"1.00","Cost":"1.00","DaysAgo":"-110","CreatedBy":"69","CreatedDate":"2017-10-12 00:00:50.000"}]
 * @apiSuccess {array} notification list of notifications.[{Id , IsRead ,FromUserId ,ToUserName,  ToLocation, FromUserName,FromLocation, ToUserId  , NotificationId  , NotificationCode  ,    FollowUpId  ,   Description  ,    CreatedBy   ,    CreatedDate       }]
 *
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   
  
 
/**
 * @api {get} /v1/SendMail Form list of values to populate on screen
 * @apiName SendMail
 * @apiGroup v1
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
 * 
 * 
 * @apiSuccess {String} recipientEmail email targett
 * @apiSuccess {String} recipientName person name
 * @apiSuccess {String} subject subject name
 * @apiSuccess {String} bodyHtml body html content
 * @apiSuccess {String} bodyText body text content
 * 
 *
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   
  
     


 
/**
 * @api {get} /v1/Photo Form list of values to populate on screen
 * @apiName Photo
 * @apiGroup v1
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * @apiParam {userPhotos[]} DataSource
 * @apiParam {userPhotos[]} FileName
 * @apiParam {userPhotos[]} FileType
 
 *
* @apiParamExample
*  [{DataSource,FileName,FileType}]
 * 
 * @apiSuccess {Photo[]} List of photo information
 *
 *
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   
  
      
 


/**
 * @api {get} /v1/DashboardCampaign/ Get CampaignRunning list
 * @apiName DashboardCampaign-get
 * @apiGroup v1-DashboardCampaign
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *   
 * 
 * 
 * @apiSuccess {CampaignRunning[]}  CampaignRunning List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *  TotalFacebookCampaign    ,  AvgTotalPostComment   ,   AvgTotalPostReaction   ,   AvgTotalPostShare   , AvgTotalLike    
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
//
/**
 * @api {post} /v1/CampaignRunningList/ Get CampaignRunning list
 * @apiName CampaignRunning-get
 * @apiGroup v1-CampaignRunning
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter[{"CampaignRunningGroupId" : "1" ,"StatusCodeList":"APPRO,PENDING","StatusIdList":"11,12", "Id":"2","Name":"Cooling Fan Portable Desk","Description":"Allows you to cool your laptop and work anywhere.","Price":"12000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"12600.00","PhotoIdList":"1","TagIdList":"","IsActive":"1","FromDate":"2017-03-06 00:00:00","ToDate":"2017-05-05 00:00:00","DaysLeft":"-60","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-03-05 14:48:47", ,"PhotoList":[{"id":"1","Url":"uploads\/2017-03-05\/\/xgear3DOTjpg-148869652758bbb4cf778a62017-03-05.jpeg","Blob":"-","CreatedBy":"APIv1","CreatedDate":"2017-03-05 14:48:47"}],"FacebookStat":{"TotalPostComment":0,"TotalLike":0,"TotalPostShare":0}},{"Id":"3","Name":"Campaign2","Description":"Campaign2 is about campaign2 details","Price":"2000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"2100.00","PhotoIdList":"","TagIdList":"","IsActive":"1","FromDate":"2017-07-31 00:00:00","ToDate":"2017-08-31 00:00:00","DaysLeft":"-31","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-07-30 13:21:43", ,"PhotoList":[],"FacebookStat":{"TotalPostComment":0,"TotalLike":0,"TotalPostShare":0}}] 
 * 
 * 
 * @apiSuccess {CampaignRunning[]}  CampaignRunning List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[{"Id":"2","CampaignRunningGroupId" : "1" ,"Name":"Cooling Fan Portable Desk","Description":"Allows you to cool your laptop and work anywhere.","Price":"12000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"12600.00","PhotoIdList":"1","TagIdList":"","IsActive":"1","FromDate":"2017-03-06 00:00:00","ToDate":"2017-05-05 00:00:00","DaysLeft":"-60","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-03-05 14:48:47","Running":[{"Id":"1","CampaignId":"2","InfluencerId":"18","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"19","CampaignPaymentId":"0","CreatedBy":"17","CreatedDate":"2017-05-02 17:33:14"},{"Id":"2","CampaignId":"2","InfluencerId":"2","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"18","CampaignPaymentId":"0","CreatedBy":"2","CreatedDate":"2017-08-01 08:28:28"}],"PhotoList":[{"id":"1","Url":"uploads\/2017-03-05\/\/xgear3DOTjpg-148869652758bbb4cf778a62017-03-05.jpeg","Blob":"-","CreatedBy":"APIv1","CreatedDate":"2017-03-05 14:48:47"}],"FacebookStat":{"TotalPostComment":0,"TotalLike":0,"TotalPostShare":0}},{"Id":"3","Name":"Campaign2","Description":"Campaign2 is about campaign2 details","Price":"2000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"2100.00","PhotoIdList":"","TagIdList":"","IsActive":"1","FromDate":"2017-07-31 00:00:00","ToDate":"2017-08-31 00:00:00","DaysLeft":"-31","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-07-30 13:21:43","Running":[{"Id":"3","CampaignId":"3","InfluencerId":"2","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"17","CampaignPaymentId":"0","CreatedBy":"2","CreatedDate":"2017-08-12 11:07:17"}],"PhotoList":[],"FacebookStat":{"TotalPostComment":0,"TotalLike":0,"TotalPostShare":0}}] 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

    

/**
 * @api {Post} /v1/CampaignRunning/ Add CampaignRunning list
 * @apiName CampaignRunning-Post
 * @apiGroup v1-CampaignRunning
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {"Id":"2","CampaignRunningGroupId" : "1" , "Name":"Cooling Fan Portable Desk","Description":"Allows you to cool your laptop and work anywhere.","Price":"12000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"12600.00","PhotoIdList":"1","TagIdList":"","IsActive":"1","FromDate":"2017-03-06 00:00:00","ToDate":"2017-05-05 00:00:00","DaysLeft":"-60","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-03-05 14:48:47","Running":[{"Id":"1","CampaignId":"2","InfluencerId":"18","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"19","CampaignPaymentId":"0","CreatedBy":"17","CreatedDate":"2017-05-02 17:33:14"},
 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
/**
 * @api {put} /v1/CampaignRunning/:CampaignRunningId Edit CampaignRunning list
 * @apiName CampaignRunning-put
 * @apiGroup v1-CampaignRunning
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {"Id":"2","CampaignRunningGroupId" : "1" , "Name":"Cooling Fan Portable Desk","Description":"Allows you to cool your laptop and work anywhere.","Price":"12000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"12600.00","PhotoIdList":"1","TagIdList":"","IsActive":"1","FromDate":"2017-03-06 00:00:00","ToDate":"2017-05-05 00:00:00","DaysLeft":"-60","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-03-05 14:48:47","Running":[{"Id":"1","CampaignId":"2","InfluencerId":"18","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"19","CampaignPaymentId":"0","CreatedBy":"17","CreatedDate":"2017-05-02 17:33:14"}
 * 
 * 
 * @apiSuccess {result}  Status of update 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
     
/**
 * @api {delete} /v1/CampaignRunning/:CampaignRunningId Delete CampaignRunning list
 * @apiName CampaignRunning-delete
 * @apiGroup v1-CampaignRunning
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {     Id   , CampaignRunningGroupId  ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }
 * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 


/**
 * @api {post} /v1/CampaignListing/ Get Campaign list
 * @apiName Campaign-get
 * @apiGroup v1-Campaign
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{"TagIdList" :"1,2,3" TotalCostMin":"", "TotalCostMax":"",  MinFromDate":"","MaxFromDate":"","FromDate":"","MinToDate":"","MaxToDate":"","ToDate":"","Name":"","CreatedBy":"54"}
 * 
 * 
 * @apiSuccess {Campaign[]}  Campaign List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[{"Id":"2","Name":"Cooling Fan Portable Desk","Description":"Allows you to cool your laptop and work anywhere.","Price":"12000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"12600.00","PhotoIdList":"1","TagIdList":"","IsActive":"1","FromDate":"2017-03-06 00:00:00","ToDate":"2017-05-05 00:00:00","DaysLeft":"-60","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-03-05 14:48:47","Running":[{"Id":"1","CampaignId":"2","InfluencerId":"18","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"19","CampaignPaymentId":"0","CreatedBy":"17","CreatedDate":"2017-05-02 17:33:14"},{"Id":"2","CampaignId":"2","InfluencerId":"2","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"18","CampaignPaymentId":"0","CreatedBy":"2","CreatedDate":"2017-08-01 08:28:28"}],"PhotoList":[{"id":"1","Url":"uploads\/2017-03-05\/\/xgear3DOTjpg-148869652758bbb4cf778a62017-03-05.jpeg","Blob":"-","CreatedBy":"APIv1","CreatedDate":"2017-03-05 14:48:47"}],"FacebookStat":{"TotalPostComment":0,"TotalLike":0,"TotalPostShare":0}},{"Id":"3","Name":"Campaign2","Description":"Campaign2 is about campaign2 details","Price":"2000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"2100.00","PhotoIdList":"","TagIdList":"","IsActive":"1","FromDate":"2017-07-31 00:00:00","ToDate":"2017-08-31 00:00:00","DaysLeft":"-31","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-07-30 13:21:43","Running":[{"Id":"3","CampaignId":"3","InfluencerId":"2","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"17","CampaignPaymentId":"0","CreatedBy":"2","CreatedDate":"2017-08-12 11:07:17"}],"PhotoList":[],"FacebookStat":{"TotalPostComment":0,"TotalLike":0,"TotalPostShare":0}}] 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  


/**
 * @api {Post} /v1/Campaign/ Add Campaign list
 * @apiName Campaign-Post
 * @apiGroup v1-Campaign
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
*{"Name":"campaign name","Description":"descr","Price":"123","NoOfInfluencer":2,"NoOfPosting":2,"Charges1":0,"Charges1Desc":"gst","Charges2":0,"Charges2Desc":null,"Charges3":0,"Charges3Desc":null,"TotalCost":129.15,"PhotoIdList":[],"IsActive":1,"FromDate":"2018-01-30","ToDate":"2018-01-31","InterestTagList":"5,7,8,17,18","UserPhotos":[{"FileSize":141.751,"FileName":"14895616_1156969354371528_964206555_oDOTjpg","DataSource":"'data:image/jpeg;base64,9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGE4MjBkMDAwMDNhMzgwMDAwOWE2NjAwMDAwYTY3MDAwMGVmNjcwMDAwYTE5NDAwMDA2YmRkMDAwMGF'","FileType":".jpeg"}]} 

 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
/**
 * @api {put} /v1/Campaign/:CampaignId Edit Campaign list
 * @apiName Campaign-put
 * @apiGroup v1-Campaign
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {"Id":"2","Name":"Cooling Fan Portable Desk","Description":"Allows you to cool your laptop and work anywhere.","Price":"12000.00","NoOfInfluencer":"5","NoOfPosting":"5","Charges1":"0.00","Charges1Desc":"gst","Charges2":"0.00","Charges2Desc":"-","Charges3":"0.00","Charges3Desc":"-","TotalCost":"12600.00","PhotoIdList":"1","TagIdList":"","IsActive":"1","FromDate":"2017-03-06 00:00:00","ToDate":"2017-05-05 00:00:00","DaysLeft":"-60","CreatedBy":"17","PromoTitle":"X Gear Tech","PromoCaption":null,"PromoDescription":null,"CreatedDate":"2017-03-05 14:48:47","Running":[{"Id":"1","CampaignId":"2","InfluencerId":"18","AgreedTotalCost":"3.00","AgreedNoOfPosting":"1","StatusId":"19","CampaignPaymentId":"0","CreatedBy":"17","CreatedDate":"2017-05-02 17:33:14"}
 * 
 * 
 * @apiSuccess {result}  Status of update 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
     
/**
 * @api {delete} /v1/Campaign/:CampaignId Delete Campaign list
 * @apiName Campaign-delete
 * @apiGroup v1-Campaign
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* *{"Name":"campaign name","Description":"descr","Price":"123","NoOfInfluencer":2,"NoOfPosting":2,"Charges1":0,"Charges1Desc":"gst","Charges2":0,"Charges2Desc":null,"Charges3":0,"Charges3Desc":null,"TotalCost":129.15,"PhotoIdList":[],"IsActive":1,"FromDate":"2018-01-30","ToDate":"2018-01-31","InterestTagList":"5,7,8,17,18","UserPhotos":[{"FileSize":141.751,"FileName":"14895616_1156969354371528_964206555_oDOTjpg","DataSource":"'data:image/jpeg;base64,9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGE4MjBkMDAwMDNhMzgwMDAwOWE2NjAwMDAwYTY3MDAwMGVmNjcwMDAwYTE5NDAwMDA2YmRkMDAwMGF'","FileType":".jpeg"}]}  * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 

 

/**
 * @api {Post} /v1/CampaignRunningHistory/ Get campaign history list
 * @apiName CampaignRunningHistory-Post
 * @apiGroup v1-CampaignRunningHistory
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {    `Id`  ,      `InfluencerId`     `CampaignRunningId`   },
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[  {    `Id`  ,      `InfluencerId`    ,   `PostTitle`  ,  `PostLink`    ,      `PostContent`    ,   `PostPhotoIdList`    ,    `CampaignRunningId`  ,   `InstagramPostId`  }]
 * 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
  
/**
 * @api {Post} /v1/MerchantDashboard/:FeatureType get MerchantDashboard  
 * @apiName MerchantDashboard-Post
 * @apiGroup v1-MerchantDashboard
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {     CampaingId,InfluencerId,MerhantId,MinDate,MaxDate },
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *{   "TotalSpend" ,  "TotalPost" ,    "TotalLikesFav" ,         "TotalShares" ,        "CampaignDatas"  }
 * 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */     

/**
 * @api {Post} /v1/InfluencerListing/ Get InfluencerListing list
 * @apiName InfluencerListing-Post
 * @apiGroup v1-InfluencerListing
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {     CampaingId,InfluencerId,MerhantId,MinDate,MaxDate },
 * 
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK 
* [{"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}]
 * 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */     
    
     
 


/**
 * @api {get} /v1/ReadChat/ Get Chat list
 * @apiName Chat-get
 * @apiGroup v1-Chat
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{   `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`         }
 * 
 * 
 * @apiSuccess {Chat[]}  Chat List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ {    `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`      } ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

/**
 * @api {Post} /v1/Chat/ Add Chat list
 * @apiName Chat-Post
 * @apiGroup v1-Chat
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {     `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`      }
 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   
     
/**
 * @api {delete} /v1/Chat/:ChatId Delete Chat list
 * @apiName Chat-delete
 * @apiGroup v1-Chat
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {      `Id`  ,`CampaignRunningId` ,`SenderId`  , `ReceiverId`  , `Message`  ,`CreatedBy`   ,    `CreatedDate`      }
 * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
//trx
/**
 * @api {get} /v1/ReviewListing/ Get Review list
 * @apiName Review-get
 * @apiGroup v1-Review
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{    `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`         }
 * 
 * 
 * @apiSuccess {Review[]}  Review List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ {    `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`     } ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

/**
 * @api {Post} /v1/Review/ Add Review list
 * @apiName Review-Post
 * @apiGroup v1-Review
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {     `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`     }
 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   
     
/**
 * @api {delete} /v1/Review/:ReviewId Delete Review list
 * @apiName Review-delete
 * @apiGroup v1-Review
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {      `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`   }
 * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 


//trx
//
/**
 * @api {get} /v1/CampaignRunningGroupListing/ Get CampaignRunningGroup list
 * @apiName CampaignRunningGroup-get
 * @apiGroup v1-CampaignRunningGroup
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{   Id   ,CampaignRunningId    ,GroupDescription  ,GroupName CreatedDate   }
 * 
 * 
 * @apiSuccess {CampaignRunningGroup[]}  CampaignRunningGroup List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ {   Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName     CreatedDate     } ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

/**
 * @api {Post} /v1/CampaignRunningGroup/ Add CampaignRunningGroup list
 * @apiName CampaignRunningGroup-Post
 * @apiGroup v1-CampaignRunningGroup
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {   Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate     }
 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
/**
 * @api {put} /v1/CampaignRunningGroup/:CampaignRunningGroupId Edit CampaignRunningGroup list
 * @apiName CampaignRunningGroup-put
 * @apiGroup v1-CampaignRunningGroup
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {  Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate     }
 * 
 * 
 * @apiSuccess {result}  Status of update 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
     
/**
 * @api {delete} /v1/CampaignRunningGroup/:CampaignRunningGroupId Delete CampaignRunningGroup list
 * @apiName CampaignRunningGroup-delete
 * @apiGroup v1-CampaignRunningGroup
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {     Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate   }
 * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 


//trx
//
/**
 * @api {get} /v1/CampaignRunningMilestoneListing/ Get CampaignRunningMilestone list
 * @apiName CampaignRunningMilestone-get
 * @apiGroup v1-CampaignRunningMilestone
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }
 * 
 * 
 * @apiSuccess {CampaignRunningMilestone[]}  CampaignRunningMilestone List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ {   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     } ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

/**
 * @api {Post} /v1/CampaignRunningMilestone/ Add CampaignRunningMilestone list
 * @apiName CampaignRunningMilestone-Post
 * @apiGroup v1-CampaignRunningMilestone
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     }
 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
/**
 * @api {put} /v1/CampaignRunningMilestone/:CampaignRunningMilestoneId Edit CampaignRunningMilestone list
 * @apiName CampaignRunningMilestone-put
 * @apiGroup v1-CampaignRunningMilestone
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {  Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     }
 * 
 * 
 * @apiSuccess {result}  Status of update 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
     
/**
 * @api {delete} /v1/CampaignRunningMilestone/:CampaignRunningMilestoneId Delete CampaignRunningMilestone list
 * @apiName CampaignRunningMilestone-delete
 * @apiGroup v1-CampaignRunningMilestone
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {     Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }
 * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 

/**
 * @api {get} /v1/BookmarkListing/ Get Bookmark list
 * @apiName Bookmark-get
 * @apiGroup v1-Bookmark
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{   Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate }
 * 
 * 
 * @apiSuccess {Bookmark[]}  Bookmark List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ {    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   } ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

/**
 * @api {Post} /v1/Bookmark/ Add Bookmark list
 * @apiName Bookmark-Post
 * @apiGroup v1-Bookmark
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   }
 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
/**
 * @api {put} /v1/Bookmark/:BookmarkId Edit Bookmark list
 * @apiName Bookmark-put
 * @apiGroup v1-Bookmark
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   }
 * 
 * 
 * @apiSuccess {result}  Status of update 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
     
/**
 * @api {delete} /v1/Bookmark/:BookmarkId Delete Bookmark list
 * @apiName Bookmark-delete
 * @apiGroup v1-Bookmark
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* {   Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate  }
 * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
      


 //vct
 

/**
 * @api {get} /v1/VirtualCurrency/ Get VirtualCurrency list
 * @apiName VirtualCurrency-get
 * @apiGroup v1-VirtualCurrency
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }
 * 
 * 
 * @apiSuccess {VirtualCurrency[]}  VirtualCurrency List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ { Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate   } ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

/**
 * @api {Post} /v1/VirtualCurrency/ Add VirtualCurrency list
 * @apiName VirtualCurrency-Post
 * @apiGroup v1-VirtualCurrency
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* { Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }
 * 
 * 
 * @apiSuccess {dbGeneratedId}  Generated id inserted record into db 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
/**
 * @api {put} /v1/VirtualCurrency/:virtualCurrencyId Edit VirtualCurrency list
 * @apiName VirtualCurrency-put
 * @apiGroup v1-VirtualCurrency
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* { Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }
 * 
 * 
 * @apiSuccess {result}  Status of update 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
     
/**
 * @api {delete} /v1/VirtualCurrency/:virtualCurrencyId Delete VirtualCurrency list
 * @apiName VirtualCurrency-delete
 * @apiGroup v1-VirtualCurrency
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* { Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }
 * 
 * 
 * @apiSuccess {result}  Status of delete 
 * 
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
     
 
 
/**
 * @api {get} /v1/JobSuggestion/ Get JobSuggestion list
 * @apiName JobSuggestion-get
 * @apiGroup v1-JobSuggestion
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
* Filter{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }
 * 
 * 
 * @apiSuccess {JobSuggestion[]}  Suggestion List  
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ { Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate } ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
  
    

/**
 * @api {post} /v1/JobSuggestion/ Add JobSuggestion  
 * @apiName JobSuggestion-add
 * @apiGroup v1-JobSuggestion
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
*  { Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }
 * 
 * 
 * @apiSuccess {JobSuggestionId} Generated id inserted into db
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   


/**
 * @api {put} /v1/JobSuggestion/:JobSuggestionId Edit JobSuggestion  
 * @apiName JobSuggestion-edit
 * @apiGroup v1-JobSuggestion
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
*  { Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }
 * 
 * 
 * @apiSuccess {result} The result status of insert
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   

/**
 * @api {delete} /v1/JobSuggestion/:JobSuggestionId Delete JobSuggestion  
 * @apiName JobSuggestion-delete
 * @apiGroup v1-JobSuggestion
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *   
* @apiParamExample
*  { Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }
 * 
 * 
 * @apiSuccess {result} The result status of delete
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */   
 
 


/**
 * @api {get} /v1/Notification/ Get Notification list
 * @apiName Notification-get
 * @apiGroup v1-Notification
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {Description} Notification Description.
 * @apiParam {Code} Notification Code 
 * 
 * 
 * @apiSuccess {Notification[]} NotificationList List of notification items
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *[ {"description":22,"code":14 }   ]
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 
  

/**
 * @api {post} /v1/Notification/ Create new Notification
 * @apiName Notification-add
 * @apiGroup v1-Notification
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {Description} Notification Description.
 * @apiParam {Code} Notification Code 
 * 
 * 
 * @apiSuccess {String} NotificationId  The new id agenerated. 
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
 

/**
 * @api {put} /v1/Notification/ Edit current Notification
 * @apiName Notification-edit
 * @apiGroup v1-Notification
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {Description} Notification Description.
 * @apiParam {Code} Notification Code 
 * 
 * 
 * @apiSuccess {String} result the update status
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  


/**
 * @api {delete} /v1/Notification/ Delete current Notification
 * @apiName Notification-edit
 * @apiGroup v1-Notification
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {Description} Notification Description.
 * @apiParam {Code} Notification Code 
 * 
 * 
 * @apiSuccess {String} result the update status
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

 



/**
 * @api {get} /v1/NotificationTransaction/ Read NotificationTransaction
 * @apiName NotificationTransaction-get
 * @apiGroup v1-NotificationTransaction
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {NotificationId} NotificationId
 * @apiParam {NotificationCode} NotificationCode
 * @apiParam {IsRead} IsRead  
 * @apiParam {FromUserId} FromUserId 
 * @apiParam {ToUserId} ToUserId  
 * @apiParam {FollowUpId} FollowUpId  
 * @apiParam {Description} Description 
 * 
 * 
 * 
 * @apiSuccess {String} NotificationTransactionId  The new id agenerated. 
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
/**
 * @api {post} /v1/NotificationTransactionList/ Read NotificatioTransaction
 * @apiName NotificationTransaction-getlist
 * @apiGroup v1-NotificationTransaction
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {NotificationId} NotificationId
 * @apiParam {NotificationCode} NotificationCode
 * @apiParam {IsRead} IsRead
 * @apiParam {FromUserId} FromUserId
 * @apiParam {ToUserId} ToUserId
 * @apiParam {FollowUpId} FollowUpId
 * @apiParam {Description} Description
 * 
 * 
 * 
 * @apiSuccess {String} NotificationTransactionId  The new id agenerated. 
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

/**
 * @api {post} /v1/NotificationTransaction/ add new NotificatioTransaction
 * @apiName NotificationTransaction-add
 * @apiGroup v1-NotificationTransaction
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {NotificationId} NotificationId  
 * @apiParam {NotificationCode}  NotificationCode
 * @apiParam {IsRead} IsRead
 * @apiParam {FromUserId} FromUserId
 * @apiParam {ToUserId} ToUserId
 * @apiParam {FollowUpId} FollowUpId
 * @apiParam {Description} Description
 * 
 * 
 * 
 * @apiSuccess {String} NotificationTransactionId  The new id agenerated. 
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

/**
 * @api {put} /v1/NotificationTransaction/:NotificationTransactionId Update current NotificatioTransaction
 * @apiName NotificationTransaction-edit
 * @apiGroup v1-NotificationTransaction
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {NotificationId} Notification id.
 * @apiParam {NotificationCode} Notification Code
 * @apiParam {IsRead} Is Read Message?
 * @apiParam {FromUserId} From User Id
 * @apiParam {ToUserId} ToUserId
 * @apiParam {FollowUpId} FollowUpId
 * @apiParam {Description} Description
 * 
 * 
 * 
 * @apiSuccess {String} result  The status. 
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

/**
 * @api {delete} /v1/NotificationTransaction/:NotificationTransactionId Delete current NotificatioTransaction
 * @apiName NotificationTransaction-delete
 * @apiGroup v1-NotificationTransaction
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 
 * @apiParam {NotificationId} NotificationId
 * @apiParam {NotificationCode} NotificationCode
 * @apiParam {IsRead} IsRead
 * @apiParam {FromUserId} FromUserId
 * @apiParam {ToUserId} ToUserId
 * @apiParam {FollowUpId} FollowUpId
 * @apiParam {Description} Description
 * 
 * 
 * 
 * @apiSuccess {String} result The status. 
 *
 * 
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

/**
 * @api {post} /v1/UserProfile/ Create new user
 * @apiName UserProfile-add
 * @apiGroup v1-UserProfile
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
* @apiParamExample
* {"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}
Name
 * 
 * 
 * @apiSuccess {String} userId  userId.
 * @apiSuccess {String} userTagId userTagId.
 * @apiSuccess {String} userInfluenceId userInfluenceId.
 * @apiSuccess {String} userUsageId userUsageId.
 * @apiSuccess {String} userPaymentId userPaymentId.
 * @apiSuccess {String} status status.
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *{"userId":22,"userTagId":14,"userInfluenceId":7,"userUsageId":6,"userPaymentId":7,
 *"success_msg":"Successfuly registered you as user!","status":true}   
 *
 * curl 'http://ifluence.asia/API/v1/UserProfile' -H 'INFL-KEY: 4064fe4bed3537942c459358fef97e13' -H 'Pragma: no-cache' -H 'Cookie: SessionKey=%224064fe4bed3537942c459358fef97e13%22' -H 'Origin: http://ifluence.asia' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json; data=verbose' -H 'Cache-Control: no-cache' -H 'Referer: http://ifluence.asia/' -H 'Proxy-Connection: keep-alive' --data-binary '{"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}' --compressed
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

/**
 * @api {put} /v1/UserProfile/:userId Update current user
 * @apiName UserProfile-upd
 * @apiGroup v1-UserProfile
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
* @apiParamExample
* {"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}
Name
 * 
 * 
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  

    
/**
 * @api {delete} /v1/UserProfile/:userId Delete current user
 * @apiName UserProfile-del
 * @apiGroup v1-UserProfile
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
* @apiParamExample
* {"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}
Name
 * 
 * 
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {"status":true}
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  



    
/**
 * @api {get} /v1/AllUserProfile Get all current user profile listing
 * @apiName Userprofile-All
 * @apiGroup v1-Userprofile
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 *
* @apiParamExample filter
* {"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}
Name
 * 
 * 
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 * {"Id":null,"UserLoginName":"seanlonn","GoogleLoginName":"","FbLoginName":"","Password":"abc123","Name":"eweawr","Email":"abc@abc.com","Mobile":"dfas","Dob":"2017-09-22","Profession":"","Gender":"","Designation":"","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","UserTypeId":"1","AccountTypeId":"1","PhotoId":null,"UserPhoto":null,"UserPayment":{"PaymentAccNo":"","PaymentHolderName":"","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"","PaymentIssuerCategory":"","PaymentType":""},"UserInfluence":{"IsBanned":false,"TotalSuccessfulCampaign":0,"TotalFailedCampaign":0,"TotalFollowerFb":0,"TotalFollowerInstagram":0,"TotalFollowerTwitter":0,"TotalFollowerVine":0,"PageAuthority":0,"PageRank":0,"ReplyRatio":0,"AverageShared":0},"ConfirmPassword":"abc123","Summary":"raedfs","PreferredAccNo":"fsda","Languange":"fa","Location":"adfs","Country":"fdas","PreferredBankName":"dfasdsfdsfsda","PreferredAccName":"fdas","LoginType":"NORMAL","UserTag":"4,13"}
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
    

    
/**
 * @api {get} /v1/UserProfile/:userId Get a certain current user profile details
 * @apiName UserProfile-get
 * @apiGroup v1-UserProfile
 *
 *
 * @apiHeader (Request) {String} INFL-AUTH Authorization value.
 * @apiHeader (Request) {String} INFL-TIMESTAMP Authorization Timestamp value.
 *
 * 
 * @apiHeader (Response) {String} Infl-Key The responded session key 4064fe4bed3537942c459358fef97e13 
 *
 * 
 * 
 * 
 *
 * 
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *  {"Id":"53","UserLoginName":"CandyYuen","GoogleLoginName":"","FbLoginName":"","Password":"abc#123","Name":"Candy Yuen Ka Mang","Email":"candy.yuen@gmail.comm","Mobile":"+6012312321","Dob":"10\/25\/1988","Profession":"actress","Gender":"M","Designation":"Hk artist","CompanyName":"","CompanyNature":"","CompanyNo":"","CompanyLocation":"","Languange":"Hong Kong","Location":"Subang","Country":"malaysia","Summary":"about me","PreferredBankNo":"","PreferredBankName":"Maybank","PreferredAccNo":"723123232","PreferredAccName":"Candy Yuen Ki Man","UserTypeId":"1","AccountTypeId":"1","PhotoId":"121","IsFeatureUnlockedInfluencer":"0","FailedLoginAttempt":"0","IsTempBlocked":"0","IsActive":"1","IsLoggedOut":"0","LastFailedLogin":"2017-02-19 18:24:34.000","LastSuccessfulLogin":"2018-01-30 23:17:43.000","VirtualAmount":null,"VirtualAmountCheckoutDate":"2016-11-19 00:00:00.000","SessionKey":"bb242c67c4efb8030a495c9121c5275d","CreatedBy":"53","CreatedDate":"2018-01-30 23:17:43.000","UserType":{"Id":"1","Code":"INFLUENCER","Name":"Influencer"},"UserTag":[{"UserId":"53","InterestTagId":"17","IsInfluencer":"0"},{"UserId":"53","InterestTagId":"7","IsInfluencer":"0"},{"UserId":"53","InterestTagId":"6","IsInfluencer":"0"}],"UserInfluence":{"UserId":"53","IsBanned":"0","IsFbConnected":null,"IsFbPage":null,"IsInstagramConnected":null,"IsGoogleConnected":null,"IsTwitterConnected":null,"IsVineConnected":null,"TotalSuccessfulCampaign":"0","TotalFailedCampaign":"0","TotalRunningCampaign":null,"TotalAmountCollected":null,"TotalFriendFb":null,"TotalLikesFb":null,"FacebookTotalCampaign":null,"FacebookTotalPostLikes":null,"FacebookTotalPostComment":null,"FacebookTotalPostShares":null,"FacebookTotalPostReactions":null,"TotalFollowerFb":"0","TotalFollowerInstagram":"0","TotalFollowerTwitter":"0","TotalFollowerVine":"0","PageAuthority":"0.00","PageRank":"0.00","ReplyRatio":"0.00","AverageShared":"0.00","CreatedBy":"53","CreatedDate":"2017-02-19 18:29:03.000"},"UserPayment":{"UserId":"53","PaymentAccNo":"72132132","PaymentHolderName":"Candy Yuen Ki Man","PaymentExpiryDate":"","PaymentSecurityCode":"","PaymentIssuerName":"Maybank","PaymentIssuerCategory":"","PaymentType":"","CreatedBy":"53","CreatedDate":"2017-02-19 18:29:03"},"UserUsage":{"Id":"48","UserId":"53","Price":"3","RemainingAllowedCampaign":"10","RemainingInfluencer":"10","RemainingPosting":"11","MaxCostRemaining":"10.00","CreatedBy":"53","CreatedDate":"2017-02-19 18:29:03.000"},"UserPhoto":[{"id":"121","Url":"uploads\/2016-10-16\/\/profile9DOTjpg-14765836345802e0d2cf8922016-10-16.jpeg","Blob":"-","CreatedBy":"APIv1","CreatedDate":"2016-10-16 10:07:14.000"}]} 
 *
 * @apiErrorExample Error-Response:
 *     HTTP/1.1 500  
 *     {
 *       "error_id": ""
 *       "error_msg": ""
 *     }
 */  
  

?>