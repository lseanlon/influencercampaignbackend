<?php
date_default_timezone_set("Asia/Kuala_Lumpur");
// BATCHGROUP group
//  v1Batch/ExpiredCampaign/secret-key


 


$app->group('/v1Batch', function() use ($app)
{
//#FORM SERVICE      
    $app->get('/ExpiredCampaign/:secretKey', function($secretKey) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/Batch.php';
        performCleanExpiredCampaign($app,$secretKey);
    });

}); 

//  v1Batch/CampaignRunningStatus/secret-key
$app->group('/v1Batch', function() use ($app)
{
//#FORM SERVICE      
    $app->get('/CampaignRunningStatus/:secretKey', function($secretKey) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        require_once 'CoreFunction/Batch.php'; 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        performCleanCampaignRunningStatus($app,$secretKey);
    });

});

//  v1Batch/DashboardPopulation/secret-key
$app->group('/v1Batch', function() use ($app)
{
//#FORM SERVICE      
    $app->get('/DashboardPopulation/:secretKey', function($secretKey) use ($app)
    { 
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        require_once 'CoreFunction/Batch.php';
        performDashboardPopulation( $app,$secretKey);



    });

});



//  v1Batch/FacebookUserInfluence/secret-key
$app->group('/v1Batch', function() use ($app)
{
//#FORM SERVICE      
    $app->get('/FacebookUserInfluence/:secretKey', function($secretKey) use ($app)
    {

        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';
        require_once 'CoreFunction/Authorization.php'; 
        require_once 'CoreFunction/Login.php'; 
        require_once 'CoreFunction/Social/Facebook.php';
        require_once  'lib/FacebookPhpSdk/src/Facebook/autoload.php';
        require_once 'CoreFunction/Batch.php';
        performFetchFacebookUserInfluence($app,$secretKey);
    });

});

// API group
$app->group('/v1', function() use ($app)
{

    //#reset password SERVICE      
    $app->get('/BrainTreePaymentOnboard/:step/:keyData', function($step,$keyData) use ($app)
    { 
        require_once 'apiFunctionsV1.php';
        require_once 'lib/BrainTree/Braintree.php';   
        require_once 'CoreFunction/BrainTreePayment.php';

  
        // require_once 'lib/BrainTree/Braintree/Configuration.php';  
        Braintree\Configuration::environment('sandbox');
        Braintree\Configuration::merchantId('fs957t29pnwxjhpr');
        Braintree\Configuration::publicKey('3sdw5pwbgd82swww');
        Braintree\Configuration::privateKey('1f29900bcabf9f05ed59390afb8afa21');

 


        performOnboard($app,$step,$keyData);  

     });

//PreLogin - determine if user already logged in
    $app->post('/PreLogin', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'CoreFunction/UserProfile.php';
        require_once 'CoreFunction/SessionHistory.php';
        require_once 'CoreFunction/Authorization.php'; 
        require_once 'CoreFunction/Social/Facebook.php';
        require_once  'lib/FacebookPhpSdk/src/Facebook/autoload.php';
        require_once 'CoreFunction/Login.php'; 
        performPreLogin($app);
    });
//Login 
    $app->post('/Login', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'CoreFunction/UserProfile.php';
        require_once 'CoreFunction/Authorization.php'; 
        require_once 'CoreFunction/SessionHistory.php';
        require_once 'CoreFunction/Authorization.php'; 
        require_once 'CoreFunction/Social/Facebook.php';
        require_once 'CoreFunction/Social/Google.php';
        require_once  'lib/FacebookPhpSdk/src/Facebook/autoload.php';
        require_once 'CoreFunction/Login.php'; 
        performCommonLogin($app);
    });

//Logout    
    $app->put('/Login', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'CoreFunction/UserProfile.php';
        require_once 'CoreFunction/SessionHistory.php'; 
        require_once 'CoreFunction/Authorization.php'; 
        require_once 'CoreFunction/Social/Facebook.php';
        require_once 'CoreFunction/Logout.php'; 
        require_once  'lib/FacebookPhpSdk/src/Facebook/autoload.php';
        performCommonLogout($app);
    });

//#reset password SERVICE      
    $app->get('/ResetPassword/:step/:keyData', function($step,$keyData) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'CoreFunction/UserProfile.php';
        require_once 'CoreFunction/SessionHistory.php'; 
        require_once 'CoreFunction/Authorization.php';  
        require_once 'templates/resetpassword.php';  
        performResetPassword($app,$step,$keyData); 
     
    });
//#FORM SERVICE      
    $app->get('/FormService', function() use ($app)
    {
        require_once 'apiFunctionsV1.php'; 
        require_once 'CoreFunction/VirtualCurrency.php'; 
        require_once 'CoreFunction/Notification.php'; 
        getFormService($app);
    });
    
    
//#SEND EMAIL
    $app->get('/SendMail', function() use ($app)
    {
        
        require_once 'apiFunctionsV1.php';
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        $emailArr = array(
            "recipientEmail" => "lseanlon@gmail.com",
            "recipientName" => "sean lon",
            "subject" => "php mailer test",
            "bodyHtml" => "php mailer body html",
            "bodyText" => "php mailer body bodyText"
        );
        
        sendEMailMessage($emailArr);
    });


//#PHOTO SERVICE        
    $app->post('/Photo', function() use ($app)
    {
        require_once 'apiFunctionsV1.php'; 
        uploadMultiPhoto( $app)  ; 
    });




//#CAMPAIGN SERVICE     
 
    //get Campaign via post --> with filters   
    $app->post('/CampaignListing', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        getCampaignListing($app);
    });

    //add Campaign  via post
    $app->post('/Campaign', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        addCampaignRec($app);
    });
    
    //edit Campaign  via post
    $app->put('/Campaign/:campaignId', function($campaignId) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        editCampaignRec($app, $campaignId);
    });
    
    //remove Campaign  via delete
    $app->delete('/Campaign/:campaignId', function($campaignId) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        deleteCampaignRec($app, $campaignId); 
    });


         //get Dashboard Campaig  via post
    $app->post('/DashboardCampaign', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        getDashboardByInfluencer( $app)  ; 
    });




//#CAMPAIGNRUNNING   SERVICE      
//
     //get Dashboard Campaig  via post
    $app->post('/DashboardCampaign', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        getDashboardByInfluencer( $app)  ; 
    });
 
    $app->post('/CampaignRunningHistory', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        getCampaignRunningHistory( $app)  ; 
    });
     //get Campaign Running  via post
    $app->post('/CampaignRunningList', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        getCampaignRunningByInfluencer( $app)  ; 
    });

    // update campaign post by influencer
    $app->post('/CampaignRunning', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        addEditCampaignRunningRec($app);
    });    

     //when user do posting
    $app->put('/CampaignRunning', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        updateCampaignPostageByInfluencer($app);
    });
        $app->delete('/CampaignRunning/:campaignId', function($CampaignRunningId) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        deleteCampaigRunningnRec($app, $CampaignRunningId); 
    });
//#MERCHANT FEATURE SERVICE      
    $app->post('/MerchantFeature/:featureType', function($featureType) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/MerchantFeature.php';
        activatMerchantFeature($app,$featureType);
    }); 

    $app->post('/MerchantDashboard', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/CampaignRunning.php';
        require_once 'CoreFunction/MerchantFeature.php';
        getMerchantDashboard($app ); 

    }); 
    $app->post('/InfluencerListing', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Campaign.php';
        require_once 'CoreFunction/MerchantFeature.php';
        getInfluencerListing($app );
    }); 
    
//#Chat SERVICE     
//CRUD : chat
 //read
    $app->post('/ReadChat', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Chat.php';  
   
        getChat($app    );
    });  

//add
  $app->post('/Chat', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php'; 
        require_once 'CoreFunction/Chat.php';   

        addChat($app);
    });

// delete
    $app->delete('/Chat/:chatId', function($chatId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Chat.php';  
        deleteChat($app, $chatId); 
    });

//#REVIEW SERVICE      
 //read 
    $app->post('/ReviewListing', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Review.php';  
        getReviewListing($app );
    });  

    //add  
    $app->post('/Review', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Review.php';
        addReview($app);
    });
     
    //delete  
    $app->delete('/Review/:reviewId', function($reviewId) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Review.php';
        deleteReview($app, $reviewId); 
    });


//#MILESTONE SERVICE      
 //read 
    $app->post('/CampaignRunningMilestoneListing', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningMilestone.php';  
        getCampaignRunningMilestoneListing($app );
    });  

    //add  
    $app->post('/CampaignRunningMilestone', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningMilestone.php';
        addCampaignRunningMilestone($app);
    });
     



    //edit  
    $app->put('/CampaignRunningMilestone/:milestoneId', function($milestoneId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningMilestone.php'; 

        editCampaignRunningMilestone($app, $milestoneId);
    });



    //delete  
    $app->delete('/CampaignRunningMilestone/:milestoneId', function($milestoneId) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningMilestone.php';
        deleteCampaignRunningMilestone($app, $milestoneId); 
    });

 


//#GROUP SERVICE      
 //read 
    $app->post('/CampaignRunningGroupListing', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningGroup.php';  
        getCampaignRunningGroupListing($app );
    });  

    //add  
    $app->post('/CampaignRunningGroup', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningGroup.php';
        addCampaignRunningGroup($app);
    });
      


    //edit  
    $app->put('/CampaignRunningGroup/:groupId', function($groupId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningGroup.php'; 

        editCampaignRunningGroup($app, $groupId);
    });



    //delete  
    $app->delete('/CampaignRunningGroup/:groupId', function($groupId) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/CampaignRunningGroup.php';
        deleteCampaignRunningGroup($app, $groupId); 
    });


//#Bookmark SERVICE      
 //read 
    $app->post('/BookmarkListing', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Bookmark.php';  
        getBookmarkListing($app );
    });  

    //add  
    $app->post('/Bookmark', function() use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Bookmark.php';
        addBookmark($app);
    });
     



    //edit  
    $app->put('/Bookmark/:milestoneId', function($bookmarkId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Bookmark.php'; 

        editBookmark($app, $bookmarkId);
    });



    //delete  
    $app->delete('/Bookmark/:bookmarkId', function($bookmarkId) use ($app)
    {
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Bookmark.php';
        deleteBookmark($app, $bookmarkId); 
    });


//#VIRTUAL CURRENCY SERVICE     
//CRUD : VIRTUAL CURRENCY
 //read
    $app->get('/VirtualCurrency', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php'; 
   
        getVirtualCurrency($app    );
    });   
//add
  $app->post('/VirtualCurrency', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php';
 

        addVirtualCurrency($app);
    });


//edit  
    $app->put('/VirtualCurrency/:virtualCurrencyId', function($virtualCurrencyId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php'; 


        editVirtualCurrency($app, $virtualCurrencyId);
    });

// delete
    $app->delete('/VirtualCurrency/:virtualCurrencyId', function($virtualCurrencyId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php'; 
          


        deleteVirtualCurrency($app, $virtualCurrencyId); 
    });


//CRUD : VIRTUAL CURRENCY TRANSACTION
//get,add,edit,delete
//
 
 //read
   $app->get('/VirtualCurrencyTransaction', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrencyTransaction.php'; 
   
        getVirtualCurrencyTransaction($app    );
    });  
  
    


//add
  $app->post('/VirtualCurrencyTransaction', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrencyTransaction.php';
 

        addVirtualCurrencyTransaction($app);
    });


//edit  
    $app->put('/VirtualCurrencyTransaction/:virtualCurrencyTransactionId', function($virtualCurrencyTransactionId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrencyTransaction.php'; 


        editVirtualCurrencyTransaction($app, $virtualCurrencyTransactionId);
    });

// delete
    $app->delete('/VirtualCurrencyTransaction/:virtualCurrencyTransactionId', function($virtualCurrencyTransactionId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrencyTransaction.php';  


        deleteVirtualCurrencyTransaction($app, $virtualCurrencyTransactionId); 
    });
 

//#VIRTUAL CURRENCY SERVICE     
//CRUD : VIRTUAL CURRENCY
 //read
    $app->get('/VirtualCurrency', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php'; 
   
        getVirtualCurrency($app    );
    });   
//add
  $app->post('/VirtualCurrency', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php';
 

        addVirtualCurrency($app);
    });


//edit  
    $app->put('/VirtualCurrency/:virtualCurrencyId', function($virtualCurrencyId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php'; 


        editVirtualCurrency($app, $virtualCurrencyId);
    });

// delete
    $app->delete('/VirtualCurrency/:virtualCurrencyId', function($virtualCurrencyId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/VirtualCurrency.php'; 
          


        deleteVirtualCurrency($app, $virtualCurrencyId); 
    });



//CRUD :  TRANSACTION 
 
 //read
   $app->get('/JobSuggestion', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/JobSuggestion.php'; 
   
        getJobSuggestion($app    );
    });  
  
    
//Job suggestion 
//add
  $app->post('/JobSuggestion', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/JobSuggestion.php';
 

        addJobSuggestion($app);
    });


//edit  
    $app->put('/JobSuggestion/:JobSuggestionId', function($JobSuggestionId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/JobSuggestion.php'; 


        editJobSuggestion($app, $JobSuggestionId);
    });

// delete
    $app->delete('/JobSuggestion/:JobSuggestionId', function($JobSuggestionId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/JobSuggestion.php';  


        deleteJobSuggestion($app, $JobSuggestionId); 
    });


//CRUD :  TRANSACTION 
 
 //read
   $app->get('/Notification', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Notification.php'; 
   
        getNotification($app    );
    });  
  
    


//add
  $app->post('/Notification', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Notification.php';
 

        addNotification($app);
    });


//edit  
    $app->put('/Notification/:NotificationId', function($NotificationId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Notification.php'; 


        editNotification($app, $NotificationId);
    });

// delete
    $app->delete('/Notification/:NotificationId', function($NotificationId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/Notification.php';  


        deleteNotification($app, $NotificationId); 
    });

 //read
   $app->get('/NotificationTransaction', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/NotificationTransaction.php'; 
   
        getNotificationTransaction($app    );
    });  
  
    

   $app->post('/NotificationTransactionList', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/NotificationTransaction.php'; 
   
        getNotificationTransaction($app    );
    });  
  

//add
  $app->post('/NotificationTransaction', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/NotificationTransaction.php';
 

        addNotificationTransaction($app);
    });


//edit  
    $app->put('/NotificationTransaction/:NotificationTransactionId', function($NotificationTransactionId) use ($app)
    { 
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/NotificationTransaction.php'; 


        editNotificationTransaction($app, $NotificationTransactionId);
    });

// delete
    $app->delete('/NotificationTransaction/:NotificationTransactionId', function($NotificationTransactionId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/NotificationTransaction.php';  


        deleteNotificationTransaction($app, $NotificationTransactionId); 
    });
 

//#USER PROFILE SERVICE     
//    //get all  UserProfile   for admin
    $app->get('/AllUserProfile/', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';


        //ensure only return this to admin
        $userRec = getUserSessionInfo($app) ;  
        if(empty( $userRec) ||  empty($userRec["UserType"]["Name"] ) || strtoupper($userRec["UserType"]["Name"] )!="ADMIN" ){

             return processErrorMessage($app, "Service denied ", "001");
        }

        getAllUserProfile($app    );
    });  


    //get UserProfile  via post
    $app->get('/UserProfile/:userId', function($userId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';
        getUserProfile($app, $userId);
    });    
    $app->get('/UserProfile/', function( ) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';
        getCurrentUserProfile($app );
    });
    //add UserProfile  via post
    $app->post('/UserProfile', function() use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';
        addUserProfile($app);
    });
    
    //edit UserProfile  via post
    $app->put('/UserProfile/:userId', function($userId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';
        editUserProfile($app, $userId);
    });
    
    //remove UserProfile  via delete
    $app->delete('/UserProfile/:userId', function($userId) use ($app)
    {
        require_once 'lib/PhpMailer/PHPMailerAutoload.php';
        require_once 'CoreFunction/SendEmailV1.php';
        require_once 'apiFunctionsV1.php';
        require_once 'CoreFunction/UserProfile.php';
        deleteUserProfile($app, $userId); 
    });
    
    
    
});  

?>