<?php

function connectDB()
{
    require_once 'lib/mysql.php';
    $db = connect_db();
    return $db;
}
// Function to get the client IP address
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}


function crudDB($statement)
{
    $db = connectDB();
    if ($db->query($statement) === TRUE) {
        return $db;
    } else {
        // echo " Error: " . $sql . "<br>" . $conn->error;
        return false;
    }
    
}
function queryDB($statement)
{
    $db   = connectDB();
    $rs   = $db->query($statement);
    $data = $rs->fetch_all(MYSQLI_ASSOC);
    return $data;
}
//Safety get value
function getKeyVal($objArr, $keyId)
{
    if (!is_null($objArr) && array_key_exists($keyId, $objArr)) { 
        if (!empty( $objArr->$keyId )  ) {
          return $objArr->$keyId; 
        }
        else{
               // if (!empty( $objArr[$keyId] )  ) { 
               //      return $objArr[$keyId] ;
               // }

        }


    } else {

        return "";
    }
    
}


function checkLegitTimestamp($app,$timestamp ){ 
        // $currentDate = date('Y-m-d H:i:s', strtotime('-11 hours'));  
        $currentDate = date('Y-m-d H:i:s', strtotime('+10 minutes'));  
        $currentTime = strtotime( $currentDate  );     
        $headerTime = strtotime( $timestamp );     
        if($headerTime >  $currentTime){ 
          return processIllegalAccess($app, "ILLEGAL ENTRY ACCESS - EXPIRED", "100");
        } 
}


function checkUserSession($app,$SessionKey ){ 
    if(empty( $SessionKey)){ 
          return processIllegalAccess($app, "ILLEGAL ACCESS - NO ACTIVE SESSION", "100");
    }
    $filter = new StdClass;
    // var_dump($SessionKey  )  ;
    $userInfo= getCommonUserProfile($filter, null,  $SessionKey); 

    // var_dump($userInfo  )  ;
    if(empty($userInfo ) ){ 
        return processIllegalAccess($app, "INVALID USER SESSION ", "100");
    }
    return $userInfo ;
}


function getHmacValue( $rawValueMessage   ){   
    $hashedValueMessge = hash_hmac('sha256', $rawValueMessage, 'secret', true);
    $hashedValueMessge = base64_encode($hashedValueMessge); 
    return  $hashedValueMessge;

}


function isHashValueEqual($app,$rawValueMessage , $AuthCode){  
    $hashedValueMessge =  getHmacValue( $rawValueMessage   ) ;
    // var_export($hashedValueMessge );
    // var_export($AuthCode );

    return  isComparedEqual( $hashedValueMessge,$AuthCode)  ;
}
function checkHashEqual($app,$rawValueMessage , $AuthCode){   
    if(!isHashValueEqual($app,$rawValueMessage , $AuthCode)){ 
      return processIllegalAccess($app, "ILLEGAL ENTRY ACCESS - NO AUTH", "100");
    }       

}
function processPreLoginHeaderRequest($app){
 

        $Timestamp = $app->request->headers->get('INFL-TIMESTAMP');    
        $AuthCode = $app->request->headers->get('INFL-AUTH');     

        //CHECK N THROW ILLEGAL ACCESS  
        checkLegitTimestamp($app, $Timestamp ); 


        // $reqParam = getJsonRequest($app);   
        // $UserName = getKeyVal($reqParam, "UserName");     
        // $Password = getKeyVal($reqParam, "Password");     
        
        // // check if auth code valid [hmac256 of sess+timetamp=auth]   
        // //retrieve user access object and return
        // $rawValueMessage= $UserName . "" .$Timestamp . "" .   $Password ;
        // checkHashEqual($app,$rawValueMessage , $AuthCode) ;
 

}
function getUserSessionInfo( $app){

        $SessionKey = $app->request->headers->get('INFL-KEY');     
        return checkUserSession($app, $SessionKey );
}

function processPostLoginHeaderRequest($app){

        $Timestamp = $app->request->headers->get('INFL-TIMESTAMP');   
        $SessionKey = $app->request->headers->get('INFL-KEY');     
        $AuthCode = $app->request->headers->get('INFL-AUTH');    

        //CHECK N THROW ILLEGAL ACCESS
        // check if invalid timestamp[cannot be more than 10 minutes now]      
        checkLegitTimestamp($app, $Timestamp ); 
        
        // check if auth code valid [hmac256 of sess+timetamp=auth]    
        $rawValueMessage= $SessionKey . "" .$Timestamp; 
        checkHashEqual($app,$rawValueMessage , $AuthCode) ;


        // check if invalid session key[not found in db]  
        $userInfo = checkUserSession($app,$SessionKey ); 

        return $userInfo;
}

function processCommonErrorMessage($app,$msg ,$errId, $errorCode) { 
  
    $errorJSON = array(
                'error_id' => empty( $errId )  ? "ERROR" :$errId ,
                'error_msg'  => $msg  
            );
     $jsonType = "application/json;charset=utf-8";
     
        header("Content-Type:  " .  $jsonType);
        $response = $app->response();
        $response['Content-Type'] = $jsonType;
        $response->status($errorCode);
        
        $response->body(json_encode($errorJSON));    

     $app->response()->status($errorCode);
     $app->halt($errorCode, json_encode($errorJSON)); 
    
}
function processErrorMessage($app,$msg ,$errId) { 
   processCommonErrorMessage($app,$msg ,$errId, 500);
    
}
function processIllegalAccess($app,$msg ,$errId) { 
   processCommonErrorMessage($app,$msg ,$errId, 401); 
}
function processLogoutMessage($app,$msg ,$errId) { 
   processCommonErrorMessage($app,$msg ,$errId, 401); 
}
function getJsonRequest($app)
{
    if(empty($app->request)){
        return null;
    }
    return json_decode($app->request->getBody());
}
 


function getSessionJsonResponse($app, $jsonData, $sessionKey)
{
 
    $response = $app->response();
    $response->headers->set('INFL-KEY', $sessionKey); 
    getJsonResponse($app, $jsonData);

}

function getJsonResponse($app, $jsonData)
{
    $jsonType = "application/json;charset=utf-8";
    header("Content-Type:  " . $jsonType);
    
    // header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Headers: X-Requested-With');
    header('Access-Control-Allow-Headers: Content-Type');
    header('Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT');
    header('Access-Control-Max-Age: 86400');
    $response = $app->response();
    $response['Content-Type'] = $jsonType;
    
    $response->headers->set('Content-Type', $jsonType);
    $response->body(json_encode($jsonData, JSON_UNESCAPED_UNICODE));
    //alternate / basic way return json
    //echo json_encode($jsonData)
    //WILL CAUSE 404 -NOT FOUND
    //exit;
}
 
function getJoiningListing($app)
{
    
    
    $reqParam = getJsonRequest($app);
    
    $sqlStatement = ' SELECT j.Id  ,
                        j.ActivityId  , 
                         j.isSpeaker,
                        a.Name  ,
                        a.Description ,
                        a.isPrivate ,
                        a.joinersLimit  ,
                        a.FromPeriod,
                        a.ToPeriod,a.price,
                        a.currencyId, cr.name as currencyName, cr.countryId as currencyCountryId,
                        a.countryId,c.shortName as countryShortName,c.name as countryName, c.isBanned as countryIsBanned,
                        a.ActivityType as activityId ,  
                        t.name as activityDesc ,   
                        a.Address  ,
                        a.LatLoc ,
                        a.LngLoc ,
                        a.createdBy as userCreatedId,
                        a.createdDate ,
                        j.JoinerId as joinerId,  j.isApproved,
                        user.JoinerFbUsername  as joinerFbUsername,   
                        user.JoinerImageUrl   as joinerImageUrl, 
                        user.Name  as  joinerName, 
                        user.Qualification as  joinerQualification  ,
                        user.bankName as  joinerBankName  ,
                        user.accNo as  joinerAccNo  ,
                        user.accName as  joinerAccName  ,
                        FROM Joining j, ActivityType t, Joiner user, Activity a ,Country c, Currency cr ';
    
    // $joinStatement=" 
    // LEFT OUTER JOIN Photo photo ON a.photoid = photo.Id 
    // LEFT OUTER JOIN Comment comment ON a.commentid = comment.Id   
    //  ";
    $whereStatement = " where 1=1 and j.ActivityId=a.Id and j.joinerId=user.Id  and a.ActivityType=t.Id  and a.currencyId=cr.Id and a.countryId=c.Id  ";
    
    $orderStatement   = "  ORDER BY a.FromPeriod ASC  ";
    $filterActivityId = getKeyVal($reqParam, "activityId");
    if (!empty($filterActivityId)) {
        $whereStatement = $whereStatement . " and j.ActivityId = '" . $filterActivityId . "'";
    }
    
    $filterjoinerId = getKeyVal($reqParam, "joinerId");
    if (!empty($filterjoinerId)) {
        $whereStatement = $whereStatement . " and j.JoinerId = '" . $filterjoinerId . "'";
    }
    
    //MONTH FILTER
    $filterFromPeriod = getKeyVal($reqParam, "fromPeriod");
    $filterToPeriod   = getKeyVal($reqParam, "toPeriod");
    $month            = getMonth();
    $filterMonth      = getKeyVal($reqParam, "month");
    if (!empty($filterMonth)) {
        $valueRangeList = explode("|", $month[$filterMonth]);
        if (!empty($valueRangeList[0])) {
            $filterFromPeriod = $valueRangeList[0];
            $filterToPeriod   = $valueRangeList[1];
        }
    }
    
    if (!empty($filterFromPeriod)) {
        $whereStatement = $whereStatement . " and a.fromPeriod >='" . $filterFromPeriod . "'";
    }
    
    if (!empty($filterToPeriod)) {
        $whereStatement = $whereStatement . " and a.toPeriod < '" . $filterToPeriod . "'";
    }
    
    
    
    $filterName = getKeyVal($reqParam, "name");
    if (!empty($filterName)) {
        $whereStatement = $whereStatement . " and a.name like '%" . $filterName . "%'";
    }

    $filterisApproved = getKeyVal($reqParam, "isApproved");
    if (!empty($filterisApproved)) {
        $whereStatement = $whereStatement . " and j.isApproved ='" . $filterisApproved . "'";
    }
    
    $filterDescription = getKeyVal($reqParam, "description");
    if (!empty($filterDescription)) {
        $whereStatement = $whereStatement . " and a.description like '%" . $filterDescription . "%'";
    }
    $filterAddress = getKeyVal($reqParam, "address");
    if (!empty($filterAddress)) {
        $whereStatement = $whereStatement . " and a.address like '%" . $filterAddress . "%'";
    }

    $filterpriceMin = getKeyVal($reqParam, "priceMin");
    if (!empty($filterpriceMin)) {
        $whereStatement = $whereStatement . " and cast( a.price  as decimal)  >= cast( '" . $filterpriceMin . "' as decimal)  ";
    }
      $filterpriceMax = getKeyVal($reqParam, "priceMax");
    if (!empty($filterpriceMax)) {
        $whereStatement = $whereStatement . " and  cast( a.price  as decimal)  <=  cast( '" . $filterpriceMax . "'  as decimal)  ";
    }

    
    // $filterpriceMin = getKeyVal($reqParam, "priceMin");
    // if (!empty($filterpriceMin)) {
    //     $whereStatement = $whereStatement . " and a.priceMin >= " . $filterpriceMin . " ";
    // }
    //   $filterpriceMax = getKeyVal($reqParam, "priceMax");
    // if (!empty($filterpriceMax)) {
    //     $whereStatement = $whereStatement . " and a.priceMax =<" . $filterpriceMax . " ";
    // }
    
    $filtercurrencyId = getKeyVal($reqParam, "currencyId");
    if (!empty($filtercurrencyId)) {
        $whereStatement = $whereStatement . " and a.currencyId like '%" . $filtercurrencyId . "%'";
    }
    
    $filtercountryId = getKeyVal($reqParam, "countryId");
    if (!empty($filtercountryId)) {
        $whereStatement = $whereStatement . " and a.countryId like '%" . $filtercountryId . "%'";
    }
    
    
    $filterCreatedDate = getKeyVal($reqParam, "createdDate");
    if (!empty($filterCreatedDate)) {
        $whereStatement = $whereStatement . " and a.CreatedDate   ='" . $filterCreatedDate . "'";
    }
    $filterCreatedBy = getKeyVal($reqParam, "createdBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . " and a.CreatedBy = '%" . $filterCreatedBy . "%'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . $orderStatement . " ; ";
    $data         = queryDB($sqlStatement);
    getJsonResponse($app, $data);
    
}

function isUserInfluencer($userId )
{

    $sqlStatement   = " SELECT  count(user.id)  
                        FROM User user, UserType type 
                        where UPPER(type.Code) 
                            like '%INFLUENCER%'
                            and user.id='".$userid."'";
      $data = queryDB($sqlStatement);
      return !empty($data[0]) && intval($data[0])>=1 ; 
}


function base64_to_jpeg( $directoryPath, $base64_string, $output_file)
{
    
    //  $filename_path = md5(time().uniqid()).".jpg";  
    // $output_file="uploads/".$filename_path;
    
    $directoryPath = dirname($output_file);
    if (!is_dir($directoryPath))
    {
        mkdir($directoryPath, 0755, true);
    }


    $ifp = fopen($output_file, "w+");
    
    $data = explode(',', $base64_string);
    
    fwrite($ifp, base64_decode($data[1]));
    fclose($ifp);
    
    return $output_file;
}

function uploadMultiPhoto( $app)
{ 
    $reqParam = getJsonRequest($app) ; 
    $UserPhotos = $reqParam; 
    checkEmpty($app,"UserPhotos", $UserPhotos) ;
  
    $PhotoIdList=[]; 
    foreach ($UserPhotos as $key => $value) { 
        if (!empty($value)) {
            $photoId=addPhoto( $value);
            if(!empty($photoId)){
             array_push($PhotoIdList,$photoId); 
            }
        } 
    } 
   

    $PhotoIdList =  implode(",",$PhotoIdList);   
    getJsonResponse($app,  getUserPhotoList(  $PhotoIdList ) ); 
  
}


function addPhoto( $reqParam)
{ 

    $DataSource = getKeyVal($reqParam, "DataSource");
    $FileName = getKeyVal($reqParam, "FileName");
    $FileType  = getKeyVal($reqParam, "FileType");
    $directoryPath="uploads/" .date("Y-m-d") . "/" ;
    $filePath   = $directoryPath. "/". $FileName ."-" . (time() . uniqid()) . date("Y-m-d") . $FileType;
    $fileResult = array(
        "path" => $filePath
    );
    base64_to_jpeg( $directoryPath, $DataSource, $filePath);
    

    $sqlStatement = "INSERT INTO  Photo ( 
                        `Url` ,
                        `Blob` , 
                        `createdDate` ,
                        `createdBy`  )  ";
    
    $valueStatement = "VALUES ("; 
    $url            = $filePath;

    if (!empty($url)) {
        $valueStatement = $valueStatement . "'" . $url . "',";
    } else{
          $valueStatement = $valueStatement . "'-',";
    }
    
   $DataSource="";
    if (!empty($DataSource)) {
        $valueStatement = $valueStatement . "" . $DataSource . ",";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    }
   
    //createddate
    $valueStatement = $valueStatement .  "'".date('Y-m-d H:i:s')."', ";    

    $createdBy = "APIv1";
    if (!empty($createdBy)) {
        $valueStatement = $valueStatement . "'" . $createdBy . "' ";
    }
   
    $valueStatement = $valueStatement . " )";
    $data           = crudDB($sqlStatement . $valueStatement); 
    return $data->insert_id;  
}

function getInterestTag(  $interestTagId )
{ 
 
    $sqlStatement   = 'SELECT Id,Name,Description,IsActive ,CreatedBy,CreatedDate FROM InterestTag';
    $whereStatement = " where 1=1 " ;
    if (!empty($interestTagId)) {
        $whereStatement = $whereStatement . " and id = '" . $interestTagId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}
function getUserPhoto(  $photoId )
{ 

    $sqlStatement   = 'SELECT   `id`   ,
                            `Url`   ,
                            `Blob`  ,  
                            `CreatedBy`   ,   
                            `CreatedDate`    
                       FROM Photo';
    $whereStatement = " where 1=1 " ;

    if (!empty($photoId)) {
        $whereStatement = $whereStatement . " and id = '" . $photoId . "'";
    }
    else{ 
        $whereStatement = $whereStatement . " and id = '-999'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}

function getUserPhotoList(  $photoIdList )
{ 

    $sqlStatement   = 'SELECT   `Id`   ,
                                `Url`   , 
                                `CreatedBy`   ,   
                                `CreatedDate`    
                       FROM Photo';
    $whereStatement = " where 1=1 " ;
 
     if (!empty($photoIdList)) {
      $isList = strpos($photoIdList,",");
      if($isList){
              $photoIdList= explode( ",",  $photoIdList  );   
                $whereStatement = $whereStatement . " AND ( ";
                foreach ($photoIdList as $Key => $photoValue) {  
                    if(!empty( $photoValue )){ 
                        if($Key == 0 ){ 
                          $whereStatement = $whereStatement . "   Id = '" . $photoValue . "' ";
                        }else{
                         $whereStatement = $whereStatement . " OR Id = '" . $photoValue . "' ";
                        }
                         
                   } 
                }
              $whereStatement = $whereStatement . "  ) " ;
      }
      else{ 
            $whereStatement = $whereStatement . " AND Id = '" . $photoIdList . "'";
      } 
    }
 
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}

function getUserType(  $userTypeId )
{ 
 
$sqlStatement   = 'SELECT Id,Code,Name   FROM UserType';
$whereStatement = " where 1=1 " ;
    if (!empty($userTypeId)) {
        $whereStatement = $whereStatement . " and id = '" . $userTypeId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}

function getCampaignStatusByCode(  $statCode )
{ 
  
    $sqlStatement   = 'SELECT Id,StatusCode,  Description FROM  CampaignStatus ';
    $whereStatement = " where 1=1 " ;
    if (!empty($statCode)) {
        $whereStatement = $whereStatement . " and StatusCode = '" . $statCode . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data[0];
}

function getCampaignStatus(  $accTypeId )
{ 
  
    $sqlStatement   = 'SELECT Id,StatusCode,  Description FROM  CampaignStatus ';
    $whereStatement = " where 1=1 " ;
    if (!empty($accTypeId)) {
        $whereStatement = $whereStatement . " and id = '" . $accTypeId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}
 

function getAccountType(  $accTypeId )
{  
    $sqlStatement   = 'SELECT Id,Code,Name,SubscriptionName,
    SubscriptionInfo ,Price,
    MaxAllowedMonth,MaxAllowedCampaign,MaxInfluencer,MaxPosting,MaxCost,
    CreatedBy,CreatedDate 
    FROM AccountType';
    $whereStatement = " where 1=1 " ;
    if (!empty($accTypeId)) {
        $whereStatement = $whereStatement . " and id = '" . $accTypeId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}

function getFormService($app ){
    $reqParam = getJsonRequest($app);
    $list=null; 

    $list['interestTag']=  getInterestTag(null ); 
    $list['userType']=  getUserType(null ); 
    $list['accountType']=  getAccountType(null ); 
    $list['campaignStatus']=  getCampaignStatus(null ); 
    $list['virtualCurrency']=  getCommonVirtualCurrency(null ); 
    $list['notification']=  getCommonNotification(null ); 
    return  getJsonResponse($app, $list);    
}


function checkEmpty($app,$fldName, $val){
    if(empty($val) ){ 
       processErrorMessage($app, $fldName. " is mandatory", "000");
        return true;
    }

        return false;
}

function getUserFilter($reqParam )
{
      

    $sqlStatement = 'SELECT
                         `Id`,
                         `UserLoginName`,
                        `GoogleLoginName`,
                        `FbLoginName`,
                        `Password`,
                        `Name`,
                        `Email`,
                        `Mobile`,
                        `Dob`,
                        `Profession`,
                        `Gender`,
                        `Designation`,
                        `CompanyName`,
                        `CompanyNature`,
                        `CompanyNo`,
                        `CompanyLocation`, 
                        `Languange`   ,
                        `Location`   ,
                        `Country`   ,
                        `Summary`   ,
                        `PreferredBankNo`   ,
                        `PreferredBankName`   ,
                        `PreferredAccNo`   ,
                        `PreferredAccName`   , 
                        `UserTypeId`,
                        `AccountTypeId`,
                        `PhotoId`, 
                        `IsFeatureUnlockedInfluencer`   ,
                        `FailedLoginAttempt`   ,
                        `IsTempBlocked`  ,
                        `IsActive`  ,
                        `IsLoggedOut`  ,
                        `LastFailedLogin`     , 
                        `LastSuccessfulLogin`     , 
                        `VirtualAmount`     , 
                        `VirtualAmountCheckoutDate`     , 
                        `SessionKey`   , 
                        `CreatedBy`     ,
                        `CreatedDate`    
                    FROM User';
    
    $whereStatement = " where 1=1 ";
    $limitStatement = "   ";
    
    $SessionKey= getKeyVal($reqParam, "SessionKey");
    if (!empty($SessionKey)) {
        $whereStatement = $whereStatement . " and SessionKey = '" . $SessionKey . "'"; 
    }
    
    $UserId= getKeyVal($reqParam, "UserId");
    if (!empty($UserId)) {
        $whereStatement = $whereStatement . " and ( id = '" . $UserId . "'";
        $whereStatement = $whereStatement . " or fbLoginName = '" . $UserId . "'";
        $whereStatement = $whereStatement . " or userLoginName = '" . $UserId . "'";
        $whereStatement = $whereStatement . " or googleLoginName = '" . $UserId . "' )";
    }

    $AccountTypeId= getKeyVal($reqParam, "AccountTypeId");
    if (!empty($AccountTypeId)) { 
        $whereStatement = $whereStatement . " and AccountTypeId = '" . $AccountTypeId . "'"; 
    }
    $UserTypeId= getKeyVal($reqParam, "UserTypeId");
    if (!empty($UserTypeId)) { 
        $whereStatement = $whereStatement . " and UserTypeId = '" . $UserTypeId . "'"; 
    }

   $Email= getKeyVal($reqParam, "Email");
    if (!empty($Email)) { 
        $whereStatement = $whereStatement . " and Email = '" . $Email . "'"; 
    }

   $IsFeatureUnlockedInfluencer= getKeyVal($reqParam, "IsFeatureUnlockedInfluencer");
    if (!empty($IsFeatureUnlockedInfluencer) && intval($IsFeatureUnlockedInfluencer) >0) {  
         $limitStatement = "   limit 100  ";
    }else{ 
          $limitStatement = "   limit 5 ";
    } 
    

    $sqlStatement = $sqlStatement . $whereStatement .      $limitStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
     //var_dump($data ) ; 
}

function getUser( $reqParam ,  $userId,$sessionKey)
{
     
        $filterUser = new StdClass;
        $filterUser->SessionKey =$sessionKey;   
        $filterUser->UserId =$userId;   
        // var_dump( $filterUser) 
        return getUserFilter($filterUser  );
     
}
function getUserTag($reqParam, $userId)
{
     
    $sqlStatement = 'SELECT
                         `UserId` ,
                        `InterestTagId`  , 
                        `IsInfluencer`  
                    FROM UserTag';
    
    $whereStatement = " where 1=1 ";
    
    
    if (!empty($userId)) {
        $whereStatement = $whereStatement . " and userId = '" . $userId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}


function getUserInfluence($reqParam, $userId)
{


     
    $sqlStatement = 'SELECT
                         `UserId` ,
                        `IsBanned`  , 
                        `IsFbConnected`  , 
                        `IsFbPage`  , 
                        `IsInstagramConnected`  , 
                        `IsGoogleConnected`  , 
                        `IsTwitterConnected`  , 
                        `IsVineConnected`  , 
                        `TotalSuccessfulCampaign`   ,
                        `TotalFailedCampaign`   , 
                        `TotalRunningCampaign`  ,  
                        `TotalAmountCollected`  , 
                        `TotalFriendFb`   , 
                        `TotalLikesFb`   ,  

                        `FacebookTotalCampaign`   ,  
                        `FacebookTotalPostLikes`   , 
                        `FacebookTotalPostComment`   , 
                        `FacebookTotalPostShares`   , 
                        `FacebookTotalPostReactions`   ,  

                        `InstagramTotalMedia`   ,  
                        `InstagramTotalSubscribes`   , 
 
                        `TotalFollowerFb`   , 
                        `TotalFollowerInstagram` , 
                        `TotalFollowerTwitter`  ,   
                        `TotalFollowerVine`  ,  
                        `PageAuthority`   ,  
                        `PageRank`   ,  
                        `ReplyRatio`   ,  
                        `AverageShared`   ,  
                        `CreatedBy`   ,   
                        `CreatedDate`     
                    FROM UserInfluence';
    
    $whereStatement = " where 1=1 ";
    
    
    if (!empty($userId)) {
        $whereStatement = $whereStatement . " and userId = '" . $userId . "'";
    }
    
    $MaxCreatedDate= getKeyVal($reqParam, "MaxCreatedDate");
    if (!empty($MaxCreatedDate)) {
        $whereStatement = $whereStatement . " and CreatedDate <= '" . $MaxCreatedDate . "'"; 
    }

    $MinCreatedDate= getKeyVal($reqParam, "MinCreatedDate");
    if (!empty($MinCreatedDate)) {
        $whereStatement = $whereStatement . " and CreatedDate >= '" . $MinCreatedDate . "'"; 
    }

    $CreatedBy= getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $whereStatement = $whereStatement . " and CreatedBy = '" . $CreatedBy . "'"; 
    }

    $IsFbConnected= getKeyVal($reqParam, "IsFbConnected");
    if (!empty($IsFbConnected)) {
        $whereStatement = $whereStatement . " and IsFbConnected = '" . $IsFbConnected . "'"; 
    }
    $IsInstagramConnected= getKeyVal($reqParam, "IsInstagramConnected");
    if (!empty($IsInstagramConnected)) {
        $whereStatement = $whereStatement . " and IsInstagramConnected = '" . $IsInstagramConnected . "'"; 
    }
    $IsGoogleConnected= getKeyVal($reqParam, "IsGoogleConnected");
    if (!empty($IsGoogleConnected)) {
        $whereStatement = $whereStatement . " and IsGoogleConnected = '" . $IsGoogleConnected . "'"; 
    }
    $IsTwitterConnected= getKeyVal($reqParam, "IsTwitterConnected");
    if (!empty($IsTwitterConnected)) {
        $whereStatement = $whereStatement . " and IsTwitterConnected = '" . $IsTwitterConnected . "'"; 
    }
    $IsVineConnected= getKeyVal($reqParam, "IsVineConnected");
    if (!empty($IsVineConnected)) {
        $whereStatement = $whereStatement . " and IsVineConnected = '" . $IsVineConnected . "'"; 
    }

    $IsBanned= getKeyVal($reqParam, "IsBanned");
    if (!empty($IsBanned)) {
        $whereStatement = $whereStatement . " and IsBanned = '" . $IsBanned . "'"; 
    }

    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
    // var_export(  ($sqlStatement) );
}
function getUserPayment($reqParam, $userId)
{
     
    $sqlStatement = 'SELECT
                        `UserId`  , 
                        `PaymentAccNo`   ,
                        `PaymentHolderName`   , 
                        `PaymentExpiryDate`   , 
                        `PaymentSecurityCode` , 
                        `PaymentIssuerName`  ,   
                        `PaymentIssuerCategory`  ,  
                        `PaymentType`   ,  
                        `CreatedBy`   ,   
                        `CreatedDate`  
            FROM UserPayment';
    
    $whereStatement = " where 1=1 ";
    
    
    if (!empty($userId)) {
        $whereStatement = $whereStatement . " and UserId = '" . $userId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}



function getUserUsage($reqParam, $userId)
{
     
    $sqlStatement = 'SELECT 
                        `Id`  , 
                        `UserId` , 
                        `Price` ,  
                        `RemainingAllowedCampaign`  , 
                        `RemainingInfluencer`  , 
                        `RemainingPosting`  , 
                        `MaxCostRemaining`  ,  
                        `CreatedBy`   ,   
                        `CreatedDate`    
            FROM UserUsage';
    
    $whereStatement = " where 1=1 ";
    
    
    if (!empty($userId)) {
        $whereStatement = $whereStatement . " and UserId = '" . $userId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}



function sendEmailSuccessfulActivation($email, $name ,$username)
{    
        
    
        $bodyText="
             Hi $name ,\n We have successfull activated the account for your username: $username  linked to this email: $Email.\n
              If you didnt register or expect any of this, please ignore otherwise and report to us immediately.
 
        ";
        $bodyHtml="
            <p>
             Hi $name ,<br/> We have successfull activated the account for your username:$username  linked to this email: $Email.</p>
             <p> If you didnt register or expect any of this, please ignore otherwise and report to us immediately..</p>

        ";
         $emailArr = array(
            "recipientEmail" =>   $email,
            "recipientName" =>   $name,
            "subject" => "Influencer Account Activation",
            "bodyHtml" => $bodyHtml,
            "bodyText" => $bodyText
        );
  
        $emailResult= sendEMailMessage($emailArr); 

        $result = array(  
        "success_msg" => "Successfuly activated you as user!",
        "status" => true
    );
    
    
    return $result ;

}

function editUserCommon($reqParam, $userId,$userPhotoId) 
{  

    $sqlStatement   = " Update  User  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $userId . "' ;"; 

    $UserLoginName = getKeyVal($reqParam, "UserLoginName");
    if (!empty($UserLoginName)) {
        $setStatement = $setStatement . " UserLoginName='" . $UserLoginName . "' ,";
    }
    $GoogleLoginName = getKeyVal($reqParam, "GoogleLoginName");
    if (!empty($GoogleLoginName)) {
        $setStatement = $setStatement . " GoogleLoginName='" . $GoogleLoginName . "' ,";
    }
    $FbLoginName = getKeyVal($reqParam, "FbLoginName");
    if (!empty($FbLoginName)) {
        $setStatement = $setStatement . " FbLoginName='" . $FbLoginName . "' ,";
    }
    $LoginType = getKeyVal($reqParam, "LoginType");
    if (!empty($LoginType)) {
        $setStatement = $setStatement . " LoginType='" . $LoginType . "' ,";
    }
    $Password = getKeyVal($reqParam, "Password");
    if (!empty($Password)) {
        $setStatement = $setStatement . " Password='" . $Password . "' ,";
    }
    $Name = getKeyVal($reqParam, "Name");
    if (!empty($Name)) {
        $setStatement = $setStatement . " Name='" . $Name . "' ,";
    }
    $Email = getKeyVal($reqParam, "Email");
    if (!empty($Email)) {
        $setStatement = $setStatement . " Email='" . $Email . "' ,";
    }
    $Mobile = getKeyVal($reqParam, "Mobile");
    if (!empty($Mobile)) {
        $setStatement = $setStatement . " Mobile='" . $Mobile . "' ,";
    }
    $Dob = getKeyVal($reqParam, "Dob");
    if (!empty($Dob)) {
        $setStatement = $setStatement . " Dob='" . $Dob . "' ,";
    }
    $Profession = getKeyVal($reqParam, "Profession");
    if (!empty($Profession)) {
        $setStatement = $setStatement . " Profession='" . $Profession . "' ,";
    }
    $Gender = getKeyVal($reqParam, "Gender");
    if (!empty($Gender)) {
        $setStatement = $setStatement . " Gender='" . $Gender . "' ,";
    }
    $CompanyName = getKeyVal($reqParam, "CompanyName");
    if (!empty($CompanyName)) {
        $setStatement = $setStatement . " CompanyName='" . $CompanyName . "' ,";
    }
    $Designation = getKeyVal($reqParam, "Designation");
    if (!empty($Designation)) {
        $setStatement = $setStatement . " Designation='" . $Designation . "' ,";
    }
    $CompanyNature = getKeyVal($reqParam, "CompanyNature");
    if (!empty($CompanyNature)) {
        $setStatement = $setStatement . " CompanyNature='" . $CompanyNature . "' ,";
    }
    $CompanyNo = getKeyVal($reqParam, "CompanyNo");
    if (!empty($CompanyNo)) {
        $setStatement = $setStatement . " CompanyNo='" . $CompanyNo . "' ,";
    }
    $CompanyLocation = getKeyVal($reqParam, "CompanyLocation");
    if (!empty($CompanyLocation)) {
        $setStatement = $setStatement . " CompanyLocation='" . $CompanyLocation . "' ,";
    }
    $VirtualAmount = getKeyVal($reqParam, "VirtualAmount");
    if (!empty($VirtualAmount)) {
        $setStatement = $setStatement . " VirtualAmount='" . $VirtualAmount . "' ,";
    }
    $VirtualAmountCheckoutDate = getKeyVal($reqParam, "VirtualAmountCheckoutDate");
    if (!empty($VirtualAmountCheckoutDate)) {
        $setStatement = $setStatement . " VirtualAmountCheckoutDate='" . $VirtualAmountCheckoutDate . "' ,";
    } 
 
    $Languange = getKeyVal($reqParam, "Languange");
    if (!empty($Languange)) {
        $setStatement = $setStatement . " Languange='" . $Languange . "' ,";
    }
 
    $Location = getKeyVal($reqParam, "Location");
    if (!empty($Location)) {
        $setStatement = $setStatement . " Location='" . $Location . "' ,";
    }
    
    $Country = getKeyVal($reqParam, "Country");
    if (!empty($Country)) {
        $setStatement = $setStatement . " Country='" . $Country . "' ,";
    }
    $Summary = getKeyVal($reqParam, "Summary");
    if (!empty($Summary)) {
        $setStatement = $setStatement . " Summary='" . $Summary . "' ,";
    } 
    $PreferredBankNo = getKeyVal($reqParam, "PreferredBankNo");
    if (!empty($PreferredBankNo)) {
        $setStatement = $setStatement . " PreferredBankNo='" . $PreferredBankNo . "' ,";
    }  
    $PreferredBankName = getKeyVal($reqParam, "PreferredBankName");
    if (!empty($PreferredBankName)) {
        $setStatement = $setStatement . " PreferredBankName='" . $PreferredBankName . "' ,";
    }   
    $PreferredAccNo = getKeyVal($reqParam, "PreferredAccNo");
    if (!empty($PreferredAccNo)) {
        $setStatement = $setStatement . " PreferredAccNo='" . $PreferredAccNo . "' ,";
    }   
    $PreferredAccName = getKeyVal($reqParam, "PreferredAccName");
    if (!empty($PreferredAccName)) {
        $setStatement = $setStatement . " PreferredAccName='" . $PreferredAccName . "' ,";
    }   
 

    $FailedLoginAttempt = getKeyVal($reqParam, "FailedLoginAttempt");
    if (!empty($FailedLoginAttempt)) {
        $setStatement = $setStatement . " FailedLoginAttempt='" . $FailedLoginAttempt . "' ,";
    }
 
    $IsLoggedOut = getKeyVal($reqParam, "IsLoggedOut");
    if (!empty($IsLoggedOut)) {
        $setStatement = $setStatement . " IsLoggedOut='" . $IsLoggedOut . "' ,";
    }
    $IsFeatureUnlockedInfluencer = getKeyVal($reqParam, "IsFeatureUnlockedInfluencer");
    if (!empty($IsFeatureUnlockedInfluencer)) {
        $setStatement = $setStatement . " IsFeatureUnlockedInfluencer='" . $IsFeatureUnlockedInfluencer . "' ,";
    }
     $IsActive = getKeyVal($reqParam, "IsActive");
    if (!empty($IsActive)) {
        $setStatement = $setStatement . " IsActive='" . $IsActive . "' ,";

            if (intval($IsActive)>0) { 
                    //get user info 
                     $filterParam = new StdClass;
                     $filterParam ->UserId = $userId;
                     $userRec = getUserFilter($filterParam ) ;
                     $userRec =  $userRec[0];
                    //send email to notify activation - if only has real change
                    if (!empty($userRec) && $IsActive!=$userRec["IsActive"]) {
                         sendEmailSuccessfulActivation($userRec["Email"], $userRec["Name"] ,$userRec["UserLoginName"]);
                    } 
                  
            } 

    }
    $IsTempBlocked = getKeyVal($reqParam, "IsTempBlocked");
    if (!empty($IsTempBlocked)) {
        $setStatement = $setStatement . " IsTempBlocked='" . $IsTempBlocked . "' ,";
    }
    $LastFailedLogin = getKeyVal($reqParam, "LastFailedLogin");
    if (!empty($LastFailedLogin)) {
        $setStatement = $setStatement . " LastFailedLogin='" . $LastFailedLogin . "' ,";
    }
    $LastSuccessfulLogin = getKeyVal($reqParam, "LastSuccessfulLogin");
    if (!empty($LastSuccessfulLogin)) {
        $setStatement = $setStatement . " LastSuccessfulLogin='" . $LastSuccessfulLogin . "' ,";
    }
    $SessionKey = getKeyVal($reqParam, "SessionKey");
    if (!empty($SessionKey)) {
        $setStatement = $setStatement . " SessionKey='" . $SessionKey . "' ,";
    }
    $UserTypeId = getKeyVal($reqParam, "UserTypeId");
    if (!empty($UserTypeId)) {
        $setStatement = $setStatement . " UserTypeId='" . $UserTypeId . "' ,";
    }
    $AccountTypeId = getKeyVal($reqParam, "AccountTypeId");
    if (!empty($AccountTypeId)) {
        $setStatement = $setStatement . " AccountTypeId='" . $AccountTypeId . "' ,";
    }
    $PhotoId = $userPhotoId;
    if (!empty($PhotoId)) {
        $setStatement = $setStatement . " PhotoId='" . $PhotoId . "' ,";
    }
    $CreatedBy = $userId;
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate ";
    return crudDB($sqlStatement . $setStatement . $whereStatement); 

   //  var_export(  $sqlStatement . $setStatement . $whereStatement); 

}

function editUserUsageCommon($reqParam, $userId)
{  

    if( empty( $reqParam)){
            return false;
    } 
    $sqlStatement   = " Update  UserUsage ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE UserId='" . $userId . "' ;";
 
 
    $Price = getKeyVal($reqParam, "Price");
    if (!empty($Price)) {
        $setStatement = $setStatement . " Price='" . $Price . "' ,";
    } 
    $RemainingAllowedCampaign = getKeyVal($reqParam, "RemainingAllowedCampaign");
    if (!empty($RemainingAllowedCampaign)) {
        $setStatement = $setStatement . " RemainingAllowedCampaign='" . $RemainingAllowedCampaign . "' ,";
    }
    
    $MaxCostRemaining = getKeyVal($reqParam, "MaxCostRemaining");
    if (!empty($MaxCostRemaining)) {
        $setStatement = $setStatement . " MaxCostRemaining='" . $MaxCostRemaining . "' ,";
    }
    
    $MaxCostRemaining = getKeyVal($reqParam, "MaxCostRemaining");
    if (!empty($MaxCostRemaining)) {
        $setStatement = $setStatement . " MaxCostRemaining='" . $MaxCostRemaining . "' ,";
    }
    $RemainingPosting = getKeyVal($reqParam, "RemainingPosting");
    if (!empty($RemainingPosting)) {
        $setStatement = $setStatement . " RemainingPosting='" . $RemainingPosting . "' ,";
    }
    
    $RemainingInfluencer = getKeyVal($reqParam, "RemainingInfluencer");
    if (!empty($RemainingInfluencer)) {
        $setStatement = $setStatement . " RemainingInfluencer='" . $RemainingInfluencer . "' ,";
    }
      
    $CreatedBy = $userId;
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate= " . $CreatedDate . "  ,";
    }
    
    
    $setStatement = $setStatement . " CreatedBy=CreatedBy ";

     return crudDB($sqlStatement . $setStatement . $whereStatement); 
    
}
 
function isComparedEqual($str1,$str2){ 
    return ( strcasecmp(  ($str1) ,  $str2)==0);
}



function getMonthDiff($date1,$date2){  

    $ts1 = strtotime($date1);
    $ts2 = strtotime($date2);

    $year1 = date('Y', $ts1);
    $year2 = date('Y', $ts2);

    $month1 = date('m', $ts1);
    $month2 = date('m', $ts2);

    $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

    return $diff   ;
}

function getCommonUserProfile($reqParam, $userId,$sessionKey){
 
    $userRec                     = getUser($reqParam, $userId,$sessionKey);
    $userRec                     =  $userRec[0];
     
    if(empty($userRec  )){
        return null;
    }
    //GET USER type  
     $userRec["UserType"]       = getUserType( $userRec["UserTypeId"] )[0];
    //GET USER TAG  
    $userRec["UserTag"]       = getUserTag($reqParam, $userRec['Id']); 
    //GET USER INFLUENCE  
    $userRec["UserInfluence"] = getUserInfluence($reqParam, $userRec['Id'])[0];
    //GET USER PAYMENT
    $userRec["UserPayment"]   = getUserPayment($reqParam, $userRec['Id'])[0];
    //GET USER USAGE
    $userRec["UserUsage"]   = getUserUsage($app, $userRec['Id'])[0]; 
    //GET USER FOTO 
    $userRec["UserPhoto"]     = getUserPhoto($userRec['PhotoId']);
    
    return  $userRec;
}




?>