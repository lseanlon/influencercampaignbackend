<?php 
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

// //import api log  writer
 require('apiLogConfig.php');
$app = new \Slim\Slim(  array( 
    'cookies.encrypt' => true,
    'cookies.secret_key' => 'YOUR_SECRET_KEY',
    'cookies.cipher' => MCRYPT_RIJNDAEL_256,
    'cookies.cipher_mode' => MCRYPT_MODE_CBC,
	'debug' => true, 'log.enabled' => true , 'log.level' => \Slim\Log::DEBUG , 'log.writer' => $logWriter) );  
 
// $app = new \Slim\Slim();
 
//import api logging features
 require('apiLogFunctions.php');

require_once 'lib/CorsSlim/CorsSlim.php'; 
$corsOptions = array(
    "origin" => "*", "allowCredentials" => true,
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE' )
); 
$app->add(new \CorsSlim\CorsSlim($corsOptions));

// $app->map('/:x+', function($x) {
//     http_response_code(200);
// })->via('OPTIONS');


 


//import api functions 
$GLOBALS['pathapi'] = "/v1"; 
require_once 'lib/mysql.php';
require_once 'apiV1.php' ;
require_once 'apiV2.php' ;
$app->run();


?> 