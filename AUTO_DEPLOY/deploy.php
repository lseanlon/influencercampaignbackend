<?php  


$web_root_dir = '/home/seanlohc/public_html/Influencer/API';
$log_dir = '/home/seanlohc/public_html/Influencer/API/AUTO_DEPLOY/logs';

$web_root_dir = '/home/curatedbpc/public_html/API';
$log_dir = '/home/curatedbpc/public_html/API/AUTO_DEPLOY/logs';


// Full path to git binary is required if git is not in your PHP user's path. Otherwise just use 'git'.
$git_bin_path = 'git';

$update = true;

// Parse data from Bitbucket hook payload
$payload = json_decode($_POST['payload']);
 
if ($update) {
  // Do a git checkout to the web root
  exec('cd ' . $web_root_dir . ' && ' . $git_bin_path  . ' fetch');
  exec(  $git_bin_path  . ' reset --hard HEAD '); 
  exec(  $git_bin_path  . ' pull ');

  // Log the deployment
  $commit_hash = shell_exec('cd ' . $log_dir . ' && ' . $git_bin_path  . ' rev-parse --short HEAD');
  file_put_contents('deploy.log', date('m/d/Y h:i:s a') . " Deployed branch: " .  $branch . " Commit: " . $commit_hash . "\n", FILE_APPEND);
}
?>