define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./apidoc/main.js",
    "group": "D__workspace_influencer_influencercampaignbackend_apidoc_main_js",
    "groupTitle": "D__workspace_influencer_influencercampaignbackend_apidoc_main_js",
    "name": ""
  },
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./node_modules/apidoc/template/main.js",
    "group": "D__workspace_influencer_influencercampaignbackend_node_modules_apidoc_template_main_js",
    "groupTitle": "D__workspace_influencer_influencercampaignbackend_node_modules_apidoc_template_main_js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Read data of a User",
    "version": "0.3.0",
    "name": "GetUser",
    "group": "User",
    "permission": [
      {
        "name": "admin",
        "title": "Admin access rights needed.",
        "description": "<p>Optionally you can write here further Informations about the permission.</p> <p>An &quot;apiDefinePermission&quot;-block can have an &quot;apiVersion&quot;, so you can attach the block to a specific version.</p>"
      }
    ],
    "description": "<p>Compare Verison 0.3.0 with 0.2.0 and you will see the green markers with new items in version 0.3.0 and red markers with removed items since 0.2.0.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost/user/4711",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "registered",
            "description": "<p>Registration Date.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "name",
            "description": "<p>Fullname of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "String[]",
            "optional": false,
            "field": "nicknames",
            "description": "<p>List of Users nicknames (Array of Strings).</p>"
          },
          {
            "group": "Success 200",
            "type": "Object",
            "optional": false,
            "field": "profile",
            "description": "<p>Profile data (example for an Object)</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "profile.age",
            "description": "<p>Users age.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "profile.image",
            "description": "<p>Avatar-Image.</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "options",
            "description": "<p>List of Users options (Array of Objects).</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "options.name",
            "description": "<p>Option Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "options.value",
            "description": "<p>Option Value.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated Admins can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": "HTTP/1.1 401 Not Authenticated\n{\n  \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "./node_modules/apidoc/example/example.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Read data of a User",
    "version": "0.2.0",
    "name": "GetUser",
    "group": "User",
    "permission": [
      {
        "name": "admin",
        "title": "This title is visible in version 0.1.0 and 0.2.0",
        "description": ""
      }
    ],
    "description": "<p>Here you can describe the function. Multilines are possible.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "name",
            "description": "<p>Fullname of the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      }
    },
    "filename": "./node_modules/apidoc/example/_apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/user/:id",
    "title": "Read data of a User",
    "version": "0.1.0",
    "name": "GetUser",
    "group": "User",
    "permission": [
      {
        "name": "admin",
        "title": "This title is visible in version 0.1.0 and 0.2.0",
        "description": ""
      }
    ],
    "description": "<p>Here you can describe the function. Multilines are possible.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "name",
            "description": "<p>Fullname of the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The error description text in version 0.1.0.</p>"
          }
        ]
      }
    },
    "filename": "./node_modules/apidoc/example/_apidoc.js",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Create a new User",
    "version": "0.3.0",
    "name": "PostUser",
    "group": "User",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>In this case &quot;apiErrorStructure&quot; is defined and used. Define blocks with params that will be used in several functions, so you dont have to rewrite them.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the User.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>The new Users-ID.</p>"
          }
        ]
      }
    },
    "filename": "./node_modules/apidoc/example/example.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated Admins can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNameTooShort",
            "description": "<p>Minimum of 5 characters required.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"error\": \"UserNameTooShort\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "post",
    "url": "/user",
    "title": "Create a User",
    "version": "0.2.0",
    "name": "PostUser",
    "group": "User",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>In this case &quot;apiErrorStructure&quot; is defined and used. Define blocks with params that will be used in several functions, so you dont have to rewrite them.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the User.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>The Users-ID.</p>"
          }
        ]
      }
    },
    "filename": "./node_modules/apidoc/example/_apidoc.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated Admins can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNameTooShort",
            "description": "<p>Minimum of 5 characters required.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"error\": \"UserNameTooShort\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "put",
    "url": "/user/:id",
    "title": "Change a User",
    "version": "0.3.0",
    "name": "PutUser",
    "group": "User",
    "permission": [
      {
        "name": "none"
      }
    ],
    "description": "<p>This function has same errors like POST /user, but errors not defined again, they were included with &quot;apiErrorStructure&quot;</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>Name of the User.</p>"
          }
        ]
      }
    },
    "filename": "./node_modules/apidoc/example/example.js",
    "groupTitle": "User",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated Admins can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNameTooShort",
            "description": "<p>Minimum of 5 characters required.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": "HTTP/1.1 400 Bad Request\n{\n  \"error\": \"UserNameTooShort\"\n}",
          "type": "json"
        }
      ]
    }
  },
  {
    "type": "get",
    "url": "/v1Batch/CampaignRunningStatus/:secretKey",
    "title": "Batchjob Cleanup Running Campaign",
    "name": "CampaignRunningStatus",
    "group": "v1Batch",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updCampaignStatus",
            "description": "<p>Upd Campaign Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currentDate",
            "description": "<p>current Date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"approUPD\": \"1\",\n  \"nopayUPD\": \"1\",\n  \"progrUPD\": \"1\",\n  \"complUPD\": \"1\" \n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "GeneralException",
            "description": "<p>Something went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1Batch"
  },
  {
    "type": "get",
    "url": "/v1Batch/DashboardPopulation/:secretKey",
    "title": "Batchjob Cleanup Running Campaign",
    "name": "DashboardPopulation",
    "group": "v1Batch",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "UserInfluence",
            "description": "<p>item</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"UserInfluence\": {\n \n         TotalAmountCollected\n          TotalRunningCampaign\n          FacebookTotalCampaign    \n          FacebookTotalPostLikes   \n          FacebookTotalPostComment \n          FacebookTotalPostShares  \n          FacebookTotalPostReactions\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "GeneralException",
            "description": "<p>The id of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1Batch"
  },
  {
    "type": "get",
    "url": "/v1Batch/ExpiredCampaign/:secretKey",
    "title": "Batchjob Cleanup expired campaign",
    "name": "ExpiredCampaign",
    "group": "v1Batch",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updCampaignStatus",
            "description": "<p>Upd Campaign Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currentDate",
            "description": "<p>current Date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"updCampaignStatus\": \"1\",\n  \"currentDate\": \"2010-12-31\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "GeneralException",
            "description": "<p>Something went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1Batch"
  },
  {
    "type": "get",
    "url": "/v1Batch/FacebookUserInfluence/:secretKey",
    "title": "Facebook User influence",
    "name": "FacebookUserInfluence",
    "group": "v1Batch",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updCampaignStatus",
            "description": "<p>Upd Campaign Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currentDate",
            "description": "<p>current Date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "500",
            "description": "<p>Something went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1Batch"
  },
  {
    "type": "get",
    "url": "/v1/BrainTreePaymentOnboard/:step/:keyData",
    "title": "Do brain tree registration",
    "name": "BrainTreePaymentOnboard____not_finalized_",
    "group": "v1",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "updCampaignStatus",
            "description": "<p>Upd Campaign Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "currentDate",
            "description": "<p>current Date.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"status\": true\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "500",
            "description": "<p>Something went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "get",
    "url": "/v1/FormService",
    "title": "Form list of values to populate on screen",
    "name": "FormService",
    "group": "v1",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "interestTag",
            "description": "<p>list of intersest tags.[{&quot;Id&quot;:&quot;1&quot;,&quot;Code&quot;:&quot;P10&quot;,&quot;Name&quot;:&quot;REGULAR ACCOUNT BASIC&quot;,&quot;SubscriptionName&quot;:&quot;REGULAR ACCOUNT BASIC&quot;,&quot;SubscriptionInfo&quot;:&quot;JUST REGULAR ACCOUNT&quot;,&quot;Price&quot;:&quot;10000&quot;,&quot;MaxAllowedMonth&quot;:&quot;32&quot;,&quot;MaxAllowedCampaign&quot;:&quot;100&quot;,&quot;MaxInfluencer&quot;:&quot;100&quot;,&quot;MaxPosting&quot;:&quot;100&quot;,&quot;MaxCost&quot;:&quot;10000.00&quot;,&quot;CreatedBy&quot;:&quot;API&quot;,&quot;CreatedDate&quot;:&quot;2016-06-10 12:12:12.000&quot;} ]</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "userType",
            "description": "<p>list of user types.[{&quot;Id&quot;:&quot;1&quot;,&quot;Code&quot;:&quot;INFLUENCER&quot;,&quot;Name&quot;:&quot;Influencer&quot;},{&quot;Id&quot;:&quot;2&quot;,&quot;Code&quot;:&quot;MERCHANT&quot;,&quot;Name&quot;:&quot;Merchant&quot;},{&quot;Id&quot;:&quot;3&quot;,&quot;Code&quot;:&quot;ADMIN&quot;,&quot;Name&quot;:&quot;ADMIN&quot;},{&quot;Id&quot;:&quot;4&quot;,&quot;Code&quot;:&quot;TOPUP&quot;,&quot;Name&quot;:&quot;TOPUP&quot;},{&quot;Id&quot;:&quot;5&quot;,&quot;Code&quot;:&quot;ESCROW&quot;,&quot;Name&quot;:&quot;ESCROW&quot;}]</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "accountType",
            "description": "<p>list of account type. [{&quot;Id&quot;:&quot;1&quot;,&quot;Code&quot;:&quot;P10&quot;,&quot;Name&quot;:&quot;REGULAR ACCOUNT BASIC&quot;,&quot;SubscriptionName&quot;:&quot;REGULAR ACCOUNT BASIC&quot;,&quot;SubscriptionInfo&quot;:&quot;JUST REGULAR ACCOUNT&quot;,&quot;Price&quot;:&quot;10000&quot;,&quot;MaxAllowedMonth&quot;:&quot;32&quot;,&quot;MaxAllowedCampaign&quot;:&quot;100&quot;,&quot;MaxInfluencer&quot;:&quot;100&quot;,&quot;MaxPosting&quot;:&quot;100&quot;,&quot;MaxCost&quot;:&quot;10000.00&quot;,&quot;CreatedBy&quot;:&quot;API&quot;,&quot;CreatedDate&quot;:&quot;2016-06-10 12:12:12.000&quot;}]</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "campaignStatus",
            "description": "<p>list of campaign currency.[{&quot;Id&quot;:&quot;15&quot;,&quot;StatusCode&quot;:&quot;AVAIL&quot;,&quot;Description&quot;:&quot;AVAIL&quot;} ]</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "virtualCurrency",
            "description": "<p>list of virtual currency.[{&quot;Id&quot;:&quot;1&quot;,&quot;Description&quot;:&quot;usd&quot;,&quot;Unit&quot;:&quot;1.00&quot;,&quot;Cost&quot;:&quot;1.00&quot;,&quot;DaysAgo&quot;:&quot;-110&quot;,&quot;CreatedBy&quot;:&quot;69&quot;,&quot;CreatedDate&quot;:&quot;2017-10-12 00:00:50.000&quot;}]</p>"
          },
          {
            "group": "Success 200",
            "type": "array",
            "optional": false,
            "field": "notification",
            "description": "<p>list of notifications.[{Id , IsRead ,FromUserId ,ToUserName,  ToLocation, FromUserName,FromLocation, ToUserId  , NotificationId  , NotificationCode  ,    FollowUpId  ,   Description  ,    CreatedBy   ,    CreatedDate       }]</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "post",
    "url": "/v1/Login/",
    "title": "Login user",
    "name": "Login",
    "group": "v1",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "LoginType",
            "optional": false,
            "field": "The",
            "description": "<p>Login type - facebook/gmail/normal.</p>"
          },
          {
            "group": "Parameter",
            "type": "Password",
            "optional": false,
            "field": "User",
            "description": "<p>password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userInfo",
            "description": "<p>userInfo item.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userType",
            "description": "<p>inf/merchant.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "isLoggedOut",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "redirect",
            "description": "<p>any redirect flag.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\"trxStatus\":\"SUCCESS\",\"userType\":\"MERCHANT\",\"userRec\":{\"Id\":\"17\",\"UserLoginName\":\"xgeartech\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\":]\",\"Name\":\"X Gear Tech\",\"Email\":\"xgeartech@xgeartechh.comm\",\"Mobile\":\"+60102264984\",\"Dob\":\"\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"xgear tech Sdn Bhd\",\"CompanyNature\":\"Tech\",\"CompanyNo\":\"553-L\",\"CompanyLocation\":\"xgear tech\",\"Languange\":\"\",\"Location\":\"cheras malaysia\",\"Country\":\"\",\"Summary\":\"xgear tech is tech accessory\",\"PreferredBankNo\":\"\",\"PreferredBankName\":\"\",\"PreferredAccNo\":\"\",\"PreferredAccName\":\"\",\"UserTypeId\":\"2\",\"AccountTypeId\":\"1\",\"PhotoId\":\"0\",\"IsFeatureUnlockedInfluencer\":\"1\",\"FailedLoginAttempt\":\"000\",\"IsTempBlocked\":\"0\",\"IsActive\":\"1\",\"IsLoggedOut\":\"000\",\"LastFailedLogin\":\"2017-03-05 16:05:31\",\"LastSuccessfulLogin\":\"2017-09-16 11:38:22\",\"VirtualAmount\":null,\"VirtualAmountCheckoutDate\":null,\"SessionKey\":\"4064fe4bed3537942c459358fef97e13\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-09-16 07:29:38\",\"UserType\":{\"Id\":\"2\",\"Code\":\"MERCHANT\",\"Name\":\"Merchant\"},\"UserTag\":[{\"UserId\":\"17\",\"InterestTagId\":\"6\",\"IsInfluencer\":\"0\"},{\"UserId\":\"17\",\"InterestTagId\":\"17\",\"IsInfluencer\":\"0\"}],\"UserInfluence\":{\"UserId\":\"17\",\"IsBanned\":\"0\",\"IsFbConnected\":null,\"IsFbPage\":null,\"IsInstagramConnected\":null,\"IsGoogleConnected\":null,\"IsTwitterConnected\":null,\"IsVineConnected\":null,\"TotalSuccessfulCampaign\":\"0\",\"TotalFailedCampaign\":\"0\",\"TotalRunningCampaign\":null,\"TotalAmountCollected\":null,\"TotalFriendFb\":null,\"TotalLikesFb\":null,\"FacebookTotalCampaign\":null,\"FacebookTotalPostLikes\":null,\"FacebookTotalPostComment\":null,\"FacebookTotalPostShares\":null,\"FacebookTotalPostReactions\":null,\"InstagramTotalMedia\":\"0\",\"InstagramTotalSubscribes\":\"0\",\"TotalFollowerFb\":\"0\",\"TotalFollowerInstagram\":\"0\",\"TotalFollowerTwitter\":\"0\",\"TotalFollowerVine\":\"0\",\"PageAuthority\":\"0.00\",\"PageRank\":\"0.00\",\"ReplyRatio\":\"0.00\",\"AverageShared\":\"0.00\",\"CreatedBy\":\"APIv1\",\"CreatedDate\":\"2017-03-05 14:25:52\"},\"UserPayment\":{\"UserId\":\"17\",\"PaymentAccNo\":\"5555555555554444\",\"PaymentHolderName\":\"xgear tech\",\"PaymentExpiryDate\":\"022020\",\"PaymentSecurityCode\":\"321\",\"PaymentIssuerName\":\"Maybank\",\"PaymentIssuerCategory\":\"Master\",\"PaymentType\":\"\",\"CreatedBy\":\"APIv1\",\"CreatedDate\":\"2017-03-05 14:25:52\"},\"UserUsage\":{\"Id\":\"1\",\"UserId\":\"17\",\"Price\":\"3\",\"RemainingAllowedCampaign\":\"8\",\"RemainingInfluencer\":\"5\",\"RemainingPosting\":\"1\",\"MaxCostRemaining\":\"9985299.00\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-07-30 13:21:43\"},\"UserPhoto\":[]},\"redirect\":\"LOGIN-MERCHANT\"}  \n\ncurl 'http://ifluence.asia/API/v1/Login' -H 'Pragma: no-cache' -H 'Origin: http://ifluence.asia' -H 'Accept-Encoding: gzip, deflate' -H 'INFL-AUTH: ocfUP2l5WMytJc4+ciMlnACBHUdjbJU3HyiDAU7IClY=' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept-Language: en-US,en;q=0.8' -H 'Accept: application/json; data=verbose' -H 'Cache-Control: no-cache' -H 'Referer: http://ifluence.asia/' -H 'Proxy-Connection: keep-alive' -H 'INFL-TIMESTAMP: 2017-09-16 11:38:21' --data-binary '{\"LoginType\":\"NORMAL\",\"UserName\":\"xgeartech\",\"Password\":\"BdtFtxDicnhUeHtIeBF1xJPfDLxyf0St+Mpx9sCRH94=\"}' --compressed",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "500",
            "description": "<p>Something went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "put",
    "url": "/v1/Login/",
    "title": "Logout user",
    "name": "Logout",
    "group": "v1",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>isLoggedOut status</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "redirect",
            "description": "<p>any redirect flag.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\"status\":true,\"redirect\":\"LOGOUT\"}  \n\n curl 'http://ifluence.asia/API/v1/Login' -X PUT -H 'INFL-KEY: 4064fe4bed3537942c459358fef97e13' -H 'Pragma: no-cache' -H 'Origin: http://ifluence.asia' -H 'Accept-Encoding: gzip, deflate' -H 'INFL-AUTH: ocfUP2l5WMytJc4+ciMlnACBHUdjbJU3HyiDAU7IClY=' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept-Language: en-US,en;q=0.8' -H 'Accept: application/json; data=verbose' -H 'Cache-Control: no-cache' -H 'Referer: http://ifluence.asia/' -H 'Proxy-Connection: keep-alive' -H 'INFL-TIMESTAMP: 2017-09-16 11:38:21' --data-binary '{}' --compressed",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "get",
    "url": "/v1/Photo",
    "title": "Form list of values to populate on screen",
    "name": "Photo",
    "group": "v1",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "userPhotos[]",
            "optional": false,
            "field": "DataSource",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "userPhotos[]",
            "optional": false,
            "field": "FileName",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "userPhotos[]",
            "optional": false,
            "field": "FileType",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "[{DataSource,FileName,FileType}]",
          "content": "[{DataSource,FileName,FileType}]",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Photo[]",
            "optional": false,
            "field": "List",
            "description": "<p>of photo information</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "get",
    "url": "/v1/PreLogin/",
    "title": "Determine if user already logged in",
    "name": "PreLogin",
    "group": "v1",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userInfo",
            "description": "<p>userInfo item.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userType",
            "description": "<p>inf/merchant.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "isLoggedOut",
            "description": "<p>true/false.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "redirect",
            "description": "<p>any redirect flag.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"userInfo\": {userInfo },\n  \"userType\": \"influencer/merchant\",\n  \"isLoggedOut\": true/false,\n  \"redirect\": \"redirect-flag\",\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "500",
            "description": "<p>Something went wrong.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "get",
    "url": "/v1/ResetPassword/:step/:keyData",
    "title": "Reset Password user",
    "name": "ResetPassword",
    "group": "v1",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>userInfo item.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>inf/merchant.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "get",
    "url": "/v1/SendMail",
    "title": "Form list of values to populate on screen",
    "name": "SendMail",
    "group": "v1",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "recipientEmail",
            "description": "<p>email targett</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "recipientName",
            "description": "<p>person name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "subject",
            "description": "<p>subject name</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "bodyHtml",
            "description": "<p>body html content</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "bodyText",
            "description": "<p>body text content</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1"
  },
  {
    "type": "Post",
    "url": "/v1/Bookmark/",
    "title": "Add Bookmark list",
    "name": "Bookmark_Post",
    "group": "v1_Bookmark",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   }",
          "content": "{    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Bookmark"
  },
  {
    "type": "delete",
    "url": "/v1/Bookmark/:BookmarkId",
    "title": "Delete Bookmark list",
    "name": "Bookmark_delete",
    "group": "v1_Bookmark",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{   Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate  }",
          "content": "{   Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate  }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Bookmark"
  },
  {
    "type": "get",
    "url": "/v1/BookmarkListing/",
    "title": "Get Bookmark list",
    "name": "Bookmark_get",
    "group": "v1_Bookmark",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{   Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate }",
          "content": "Filter{   Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Bookmark[]",
            "optional": false,
            "field": "Bookmark",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ {    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Bookmark"
  },
  {
    "type": "put",
    "url": "/v1/Bookmark/:BookmarkId",
    "title": "Edit Bookmark list",
    "name": "Bookmark_put",
    "group": "v1_Bookmark",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   }",
          "content": "{    Id   , InfluencerId    ,   MerchantId  ,  CampaignId   ,  Priority   ,  CreatedBy    ,   BookmarkDate    ,     Name    ,    Description    ,    PromoTitle    ,    PromoCaption    ,    PromoDescription    ,    Price    ,    NoOfInfluencer    ,    NoOfPosting    ,    Charges1    ,    Charges1Desc    ,    TotalCost    ,    FromDate    ,    ToDate    ,     IsActive    ,     CreatedDate   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of update</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Bookmark"
  },
  {
    "type": "Post",
    "url": "/v1/Campaign/",
    "title": "Add Campaign list",
    "name": "Campaign_Post",
    "group": "v1_Campaign",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{\"Name\":\"campaign name\",\"Description\":\"descr\",\"Price\":\"123\",\"NoOfInfluencer\":2,\"NoOfPosting\":2,\"Charges1\":0,\"Charges1Desc\":\"gst\",\"Charges2\":0,\"Charges2Desc\":null,\"Charges3\":0,\"Charges3Desc\":null,\"TotalCost\":129.15,\"PhotoIdList\":[],\"IsActive\":1,\"FromDate\":\"2018-01-30\",\"ToDate\":\"2018-01-31\",\"InterestTagList\":\"5,7,8,17,18\",\"UserPhotos\":[{\"FileSize\":141.751,\"FileName\":\"14895616_1156969354371528_964206555_oDOTjpg\",\"DataSource\":\"'data:image/jpeg;base64,9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGE4MjBkMDAwMDNhMzgwMDAwOWE2NjAwMDAwYTY3MDAwMGVmNjcwMDAwYTE5NDAwMDA2YmRkMDAwMGF'\",\"FileType\":\".jpeg\"}]}",
          "content": "{\"Name\":\"campaign name\",\"Description\":\"descr\",\"Price\":\"123\",\"NoOfInfluencer\":2,\"NoOfPosting\":2,\"Charges1\":0,\"Charges1Desc\":\"gst\",\"Charges2\":0,\"Charges2Desc\":null,\"Charges3\":0,\"Charges3Desc\":null,\"TotalCost\":129.15,\"PhotoIdList\":[],\"IsActive\":1,\"FromDate\":\"2018-01-30\",\"ToDate\":\"2018-01-31\",\"InterestTagList\":\"5,7,8,17,18\",\"UserPhotos\":[{\"FileSize\":141.751,\"FileName\":\"14895616_1156969354371528_964206555_oDOTjpg\",\"DataSource\":\"'data:image/jpeg;base64,9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGE4MjBkMDAwMDNhMzgwMDAwOWE2NjAwMDAwYTY3MDAwMGVmNjcwMDAwYTE5NDAwMDA2YmRkMDAwMGF'\",\"FileType\":\".jpeg\"}]}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Campaign"
  },
  {
    "type": "delete",
    "url": "/v1/Campaign/:CampaignId",
    "title": "Delete Campaign list",
    "name": "Campaign_delete",
    "group": "v1_Campaign",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "*{\"Name\":\"campaign name\",\"Description\":\"descr\",\"Price\":\"123\",\"NoOfInfluencer\":2,\"NoOfPosting\":2,\"Charges1\":0,\"Charges1Desc\":\"gst\",\"Charges2\":0,\"Charges2Desc\":null,\"Charges3\":0,\"Charges3Desc\":null,\"TotalCost\":129.15,\"PhotoIdList\":[],\"IsActive\":1,\"FromDate\":\"2018-01-30\",\"ToDate\":\"2018-01-31\",\"InterestTagList\":\"5,7,8,17,18\",\"UserPhotos\":[{\"FileSize\":141.751,\"FileName\":\"14895616_1156969354371528_964206555_oDOTjpg\",\"DataSource\":\"'data:image/jpeg;base64,9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGE4MjBkMDAwMDNhMzgwMDAwOWE2NjAwMDAwYTY3MDAwMGVmNjcwMDAwYTE5NDAwMDA2YmRkMDAwMGF'\",\"FileType\":\".jpeg\"}]}  *",
          "content": "*{\"Name\":\"campaign name\",\"Description\":\"descr\",\"Price\":\"123\",\"NoOfInfluencer\":2,\"NoOfPosting\":2,\"Charges1\":0,\"Charges1Desc\":\"gst\",\"Charges2\":0,\"Charges2Desc\":null,\"Charges3\":0,\"Charges3Desc\":null,\"TotalCost\":129.15,\"PhotoIdList\":[],\"IsActive\":1,\"FromDate\":\"2018-01-30\",\"ToDate\":\"2018-01-31\",\"InterestTagList\":\"5,7,8,17,18\",\"UserPhotos\":[{\"FileSize\":141.751,\"FileName\":\"14895616_1156969354371528_964206555_oDOTjpg\",\"DataSource\":\"'data:image/jpeg;base64,9j/4AAQSkZJRgABAgAAAQABAAD/7QCEUGhvdG9zaG9wIDMuMAA4QklNBAQAAAAAAGccAigAYkZCTUQwMTAwMGE4MjBkMDAwMDNhMzgwMDAwOWE2NjAwMDAwYTY3MDAwMGVmNjcwMDAwYTE5NDAwMDA2YmRkMDAwMGF'\",\"FileType\":\".jpeg\"}]}  *",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Campaign"
  },
  {
    "type": "post",
    "url": "/v1/CampaignListing/",
    "title": "Get Campaign list",
    "name": "Campaign_get",
    "group": "v1_Campaign",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{\"TagIdList\" :\"1,2,3\" TotalCostMin\":\"\", \"TotalCostMax\":\"\",  MinFromDate\":\"\",\"MaxFromDate\":\"\",\"FromDate\":\"\",\"MinToDate\":\"\",\"MaxToDate\":\"\",\"ToDate\":\"\",\"Name\":\"\",\"CreatedBy\":\"54\"}",
          "content": "Filter{\"TagIdList\" :\"1,2,3\" TotalCostMin\":\"\", \"TotalCostMax\":\"\",  MinFromDate\":\"\",\"MaxFromDate\":\"\",\"FromDate\":\"\",\"MinToDate\":\"\",\"MaxToDate\":\"\",\"ToDate\":\"\",\"Name\":\"\",\"CreatedBy\":\"54\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Campaign[]",
            "optional": false,
            "field": "Campaign",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[{\"Id\":\"2\",\"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"},{\"Id\":\"2\",\"CampaignId\":\"2\",\"InfluencerId\":\"2\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"18\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"2\",\"CreatedDate\":\"2017-08-01 08:28:28\"}],\"PhotoList\":[{\"id\":\"1\",\"Url\":\"uploads\\/2017-03-05\\/\\/xgear3DOTjpg-148869652758bbb4cf778a62017-03-05.jpeg\",\"Blob\":\"-\",\"CreatedBy\":\"APIv1\",\"CreatedDate\":\"2017-03-05 14:48:47\"}],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}},{\"Id\":\"3\",\"Name\":\"Campaign2\",\"Description\":\"Campaign2 is about campaign2 details\",\"Price\":\"2000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"2100.00\",\"PhotoIdList\":\"\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-07-31 00:00:00\",\"ToDate\":\"2017-08-31 00:00:00\",\"DaysLeft\":\"-31\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-07-30 13:21:43\",\"Running\":[{\"Id\":\"3\",\"CampaignId\":\"3\",\"InfluencerId\":\"2\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"17\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"2\",\"CreatedDate\":\"2017-08-12 11:07:17\"}],\"PhotoList\":[],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Campaign"
  },
  {
    "type": "put",
    "url": "/v1/Campaign/:CampaignId",
    "title": "Edit Campaign list",
    "name": "Campaign_put",
    "group": "v1_Campaign",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{\"Id\":\"2\",\"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"}",
          "content": "{\"Id\":\"2\",\"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of update</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Campaign"
  },
  {
    "type": "Post",
    "url": "/v1/CampaignRunning/",
    "title": "Add CampaignRunning list",
    "name": "CampaignRunning_Post",
    "group": "v1_CampaignRunning",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{\"Id\":\"2\",\"CampaignRunningGroupId\" : \"1\" , \"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"},",
          "content": "{\"Id\":\"2\",\"CampaignRunningGroupId\" : \"1\" , \"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"},",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunning"
  },
  {
    "type": "delete",
    "url": "/v1/CampaignRunning/:CampaignRunningId",
    "title": "Delete CampaignRunning list",
    "name": "CampaignRunning_delete",
    "group": "v1_CampaignRunning",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{     Id   , CampaignRunningGroupId  ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }",
          "content": "{     Id   , CampaignRunningGroupId  ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunning"
  },
  {
    "type": "post",
    "url": "/v1/CampaignRunningList/",
    "title": "Get CampaignRunning list",
    "name": "CampaignRunning_get",
    "group": "v1_CampaignRunning",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter[{\"CampaignRunningGroupId\" : \"1\" ,\"StatusCodeList\":\"APPRO,PENDING\",\"StatusIdList\":\"11,12\", \"Id\":\"2\",\"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\", ,\"PhotoList\":[{\"id\":\"1\",\"Url\":\"uploads\\/2017-03-05\\/\\/xgear3DOTjpg-148869652758bbb4cf778a62017-03-05.jpeg\",\"Blob\":\"-\",\"CreatedBy\":\"APIv1\",\"CreatedDate\":\"2017-03-05 14:48:47\"}],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}},{\"Id\":\"3\",\"Name\":\"Campaign2\",\"Description\":\"Campaign2 is about campaign2 details\",\"Price\":\"2000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"2100.00\",\"PhotoIdList\":\"\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-07-31 00:00:00\",\"ToDate\":\"2017-08-31 00:00:00\",\"DaysLeft\":\"-31\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-07-30 13:21:43\", ,\"PhotoList\":[],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}}]",
          "content": "Filter[{\"CampaignRunningGroupId\" : \"1\" ,\"StatusCodeList\":\"APPRO,PENDING\",\"StatusIdList\":\"11,12\", \"Id\":\"2\",\"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\", ,\"PhotoList\":[{\"id\":\"1\",\"Url\":\"uploads\\/2017-03-05\\/\\/xgear3DOTjpg-148869652758bbb4cf778a62017-03-05.jpeg\",\"Blob\":\"-\",\"CreatedBy\":\"APIv1\",\"CreatedDate\":\"2017-03-05 14:48:47\"}],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}},{\"Id\":\"3\",\"Name\":\"Campaign2\",\"Description\":\"Campaign2 is about campaign2 details\",\"Price\":\"2000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"2100.00\",\"PhotoIdList\":\"\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-07-31 00:00:00\",\"ToDate\":\"2017-08-31 00:00:00\",\"DaysLeft\":\"-31\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-07-30 13:21:43\", ,\"PhotoList\":[],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}}]",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "CampaignRunning[]",
            "optional": false,
            "field": "CampaignRunning",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[{\"Id\":\"2\",\"CampaignRunningGroupId\" : \"1\" ,\"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"},{\"Id\":\"2\",\"CampaignId\":\"2\",\"InfluencerId\":\"2\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"18\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"2\",\"CreatedDate\":\"2017-08-01 08:28:28\"}],\"PhotoList\":[{\"id\":\"1\",\"Url\":\"uploads\\/2017-03-05\\/\\/xgear3DOTjpg-148869652758bbb4cf778a62017-03-05.jpeg\",\"Blob\":\"-\",\"CreatedBy\":\"APIv1\",\"CreatedDate\":\"2017-03-05 14:48:47\"}],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}},{\"Id\":\"3\",\"Name\":\"Campaign2\",\"Description\":\"Campaign2 is about campaign2 details\",\"Price\":\"2000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"2100.00\",\"PhotoIdList\":\"\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-07-31 00:00:00\",\"ToDate\":\"2017-08-31 00:00:00\",\"DaysLeft\":\"-31\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-07-30 13:21:43\",\"Running\":[{\"Id\":\"3\",\"CampaignId\":\"3\",\"InfluencerId\":\"2\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"17\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"2\",\"CreatedDate\":\"2017-08-12 11:07:17\"}],\"PhotoList\":[],\"FacebookStat\":{\"TotalPostComment\":0,\"TotalLike\":0,\"TotalPostShare\":0}}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunning"
  },
  {
    "type": "put",
    "url": "/v1/CampaignRunning/:CampaignRunningId",
    "title": "Edit CampaignRunning list",
    "name": "CampaignRunning_put",
    "group": "v1_CampaignRunning",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{\"Id\":\"2\",\"CampaignRunningGroupId\" : \"1\" , \"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"}",
          "content": "{\"Id\":\"2\",\"CampaignRunningGroupId\" : \"1\" , \"Name\":\"Cooling Fan Portable Desk\",\"Description\":\"Allows you to cool your laptop and work anywhere.\",\"Price\":\"12000.00\",\"NoOfInfluencer\":\"5\",\"NoOfPosting\":\"5\",\"Charges1\":\"0.00\",\"Charges1Desc\":\"gst\",\"Charges2\":\"0.00\",\"Charges2Desc\":\"-\",\"Charges3\":\"0.00\",\"Charges3Desc\":\"-\",\"TotalCost\":\"12600.00\",\"PhotoIdList\":\"1\",\"TagIdList\":\"\",\"IsActive\":\"1\",\"FromDate\":\"2017-03-06 00:00:00\",\"ToDate\":\"2017-05-05 00:00:00\",\"DaysLeft\":\"-60\",\"CreatedBy\":\"17\",\"PromoTitle\":\"X Gear Tech\",\"PromoCaption\":null,\"PromoDescription\":null,\"CreatedDate\":\"2017-03-05 14:48:47\",\"Running\":[{\"Id\":\"1\",\"CampaignId\":\"2\",\"InfluencerId\":\"18\",\"AgreedTotalCost\":\"3.00\",\"AgreedNoOfPosting\":\"1\",\"StatusId\":\"19\",\"CampaignPaymentId\":\"0\",\"CreatedBy\":\"17\",\"CreatedDate\":\"2017-05-02 17:33:14\"}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of update</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunning"
  },
  {
    "type": "Post",
    "url": "/v1/CampaignRunningGroup/",
    "title": "Add CampaignRunningGroup list",
    "name": "CampaignRunningGroup_Post",
    "group": "v1_CampaignRunningGroup",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{   Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate     }",
          "content": "{   Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate     }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningGroup"
  },
  {
    "type": "delete",
    "url": "/v1/CampaignRunningGroup/:CampaignRunningGroupId",
    "title": "Delete CampaignRunningGroup list",
    "name": "CampaignRunningGroup_delete",
    "group": "v1_CampaignRunningGroup",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{     Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate   }",
          "content": "{     Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningGroup"
  },
  {
    "type": "get",
    "url": "/v1/CampaignRunningGroupListing/",
    "title": "Get CampaignRunningGroup list",
    "name": "CampaignRunningGroup_get",
    "group": "v1_CampaignRunningGroup",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{   Id   ,CampaignRunningId    ,GroupDescription  ,GroupName CreatedDate   }",
          "content": "Filter{   Id   ,CampaignRunningId    ,GroupDescription  ,GroupName CreatedDate   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "CampaignRunningGroup[]",
            "optional": false,
            "field": "CampaignRunningGroup",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ {   Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName     CreatedDate     } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningGroup"
  },
  {
    "type": "put",
    "url": "/v1/CampaignRunningGroup/:CampaignRunningGroupId",
    "title": "Edit CampaignRunningGroup list",
    "name": "CampaignRunningGroup_put",
    "group": "v1_CampaignRunningGroup",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{  Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate     }",
          "content": "{  Id   ,CampaignRunningId    ,GroupDescription  ,GroupQuestion   ,  GroupName   , CreatedDate     }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of update</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningGroup"
  },
  {
    "type": "Post",
    "url": "/v1/CampaignRunningHistory/",
    "title": "Get campaign history list",
    "name": "CampaignRunningHistory_Post",
    "group": "v1_CampaignRunningHistory",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{    `Id`  ,      `InfluencerId`     `CampaignRunningId`   },",
          "content": "{    `Id`  ,      `InfluencerId`     `CampaignRunningId`   },",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[  {    `Id`  ,      `InfluencerId`    ,   `PostTitle`  ,  `PostLink`    ,      `PostContent`    ,   `PostPhotoIdList`    ,    `CampaignRunningId`  ,   `InstagramPostId`  }]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningHistory"
  },
  {
    "type": "Post",
    "url": "/v1/CampaignRunningMilestone/",
    "title": "Add CampaignRunningMilestone list",
    "name": "CampaignRunningMilestone_Post",
    "group": "v1_CampaignRunningMilestone",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     }",
          "content": "{   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningMilestone"
  },
  {
    "type": "delete",
    "url": "/v1/CampaignRunningMilestone/:CampaignRunningMilestoneId",
    "title": "Delete CampaignRunningMilestone list",
    "name": "CampaignRunningMilestone_delete",
    "group": "v1_CampaignRunningMilestone",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{     Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }",
          "content": "{     Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningMilestone"
  },
  {
    "type": "get",
    "url": "/v1/CampaignRunningMilestoneListing/",
    "title": "Get CampaignRunningMilestone list",
    "name": "CampaignRunningMilestone_get",
    "group": "v1_CampaignRunningMilestone",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }",
          "content": "Filter{   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "CampaignRunningMilestone[]",
            "optional": false,
            "field": "CampaignRunningMilestone",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ {   Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningMilestone"
  },
  {
    "type": "put",
    "url": "/v1/CampaignRunningMilestone/:CampaignRunningMilestoneId",
    "title": "Edit CampaignRunningMilestone list",
    "name": "CampaignRunningMilestone_put",
    "group": "v1_CampaignRunningMilestone",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{  Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     }",
          "content": "{  Id   ,CampaignRunningId    ,MilestoneDescription  ,MilestoneQuestion   ,  MilestoneAnswers   ,    Price   ,  Funded    ,   Paid    , CreatedDate     }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of update</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_CampaignRunningMilestone"
  },
  {
    "type": "Post",
    "url": "/v1/Chat/",
    "title": "Add Chat list",
    "name": "Chat_Post",
    "group": "v1_Chat",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{     `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`      }",
          "content": "{     `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`      }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Chat"
  },
  {
    "type": "delete",
    "url": "/v1/Chat/:ChatId",
    "title": "Delete Chat list",
    "name": "Chat_delete",
    "group": "v1_Chat",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{      `Id`  ,`CampaignRunningId` ,`SenderId`  , `ReceiverId`  , `Message`  ,`CreatedBy`   ,    `CreatedDate`      }",
          "content": "{      `Id`  ,`CampaignRunningId` ,`SenderId`  , `ReceiverId`  , `Message`  ,`CreatedBy`   ,    `CreatedDate`      }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Chat"
  },
  {
    "type": "get",
    "url": "/v1/ReadChat/",
    "title": "Get Chat list",
    "name": "Chat_get",
    "group": "v1_Chat",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{   `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`         }",
          "content": "Filter{   `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`         }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Chat[]",
            "optional": false,
            "field": "Chat",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ {    `Id`  ,`CampaignRunningId` ,    `SenderId`  , `ReceiverId`  , `Message`  ,       `CreatedBy`   ,    `CreatedDate`      } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Chat"
  },
  {
    "type": "get",
    "url": "/v1/DashboardCampaign/",
    "title": "Get CampaignRunning list",
    "name": "DashboardCampaign_get",
    "group": "v1_DashboardCampaign",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "CampaignRunning[]",
            "optional": false,
            "field": "CampaignRunning",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\nTotalFacebookCampaign    ,  AvgTotalPostComment   ,   AvgTotalPostReaction   ,   AvgTotalPostShare   , AvgTotalLike",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_DashboardCampaign"
  },
  {
    "type": "Post",
    "url": "/v1/InfluencerListing/",
    "title": "Get InfluencerListing list",
    "name": "InfluencerListing_Post",
    "group": "v1_InfluencerListing",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{     CampaingId,InfluencerId,MerhantId,MinDate,MaxDate },",
          "content": "{     CampaingId,InfluencerId,MerhantId,MinDate,MaxDate },",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK \n[{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_InfluencerListing"
  },
  {
    "type": "post",
    "url": "/v1/JobSuggestion/",
    "title": "Add JobSuggestion",
    "name": "JobSuggestion_add",
    "group": "v1_JobSuggestion",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "content": "{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JobSuggestionId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_JobSuggestion"
  },
  {
    "type": "delete",
    "url": "/v1/JobSuggestion/:JobSuggestionId",
    "title": "Delete JobSuggestion",
    "name": "JobSuggestion_delete",
    "group": "v1_JobSuggestion",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "content": "{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "The",
            "description": "<p>result status of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_JobSuggestion"
  },
  {
    "type": "put",
    "url": "/v1/JobSuggestion/:JobSuggestionId",
    "title": "Edit JobSuggestion",
    "name": "JobSuggestion_edit",
    "group": "v1_JobSuggestion",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "content": "{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "The",
            "description": "<p>result status of insert</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_JobSuggestion"
  },
  {
    "type": "get",
    "url": "/v1/JobSuggestion/",
    "title": "Get JobSuggestion list",
    "name": "JobSuggestion_get",
    "group": "v1_JobSuggestion",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "content": "Filter{ Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JobSuggestion[]",
            "optional": false,
            "field": "Suggestion",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ { Id  ,  CampaignId ,  NotificationId  , ToUserId  , FromUserId  ,     Description  ,    CreatedBy   ,    CreatedDate     ,   ToUserName    , ToLocation    , FromUserName    , FromLocation    CampaignName  , CampaignDescription  , CampaignTitle    CampaignIsActive  , CampaignFromDate , CampaignToDate  , CampaignClosingDate } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_JobSuggestion"
  },
  {
    "type": "Post",
    "url": "/v1/MerchantDashboard/:FeatureType",
    "title": "get MerchantDashboard",
    "name": "MerchantDashboard_Post",
    "group": "v1_MerchantDashboard",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{     CampaingId,InfluencerId,MerhantId,MinDate,MaxDate },",
          "content": "{     CampaingId,InfluencerId,MerhantId,MinDate,MaxDate },",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{   \"TotalSpend\" ,  \"TotalPost\" ,    \"TotalLikesFav\" ,         \"TotalShares\" ,        \"CampaignDatas\"  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_MerchantDashboard"
  },
  {
    "type": "post",
    "url": "/v1/Notification/",
    "title": "Create new Notification",
    "name": "Notification_add",
    "group": "v1_Notification",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Notification",
            "description": "<p>Description.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "NotificationId",
            "description": "<p>The new id agenerated.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Notification"
  },
  {
    "type": "put",
    "url": "/v1/Notification/",
    "title": "Edit current Notification",
    "name": "Notification_edit",
    "group": "v1_Notification",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Notification",
            "description": "<p>Description.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>the update status</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Notification"
  },
  {
    "type": "delete",
    "url": "/v1/Notification/",
    "title": "Delete current Notification",
    "name": "Notification_edit",
    "group": "v1_Notification",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Notification",
            "description": "<p>Description.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>the update status</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Notification"
  },
  {
    "type": "get",
    "url": "/v1/Notification/",
    "title": "Get Notification list",
    "name": "Notification_get",
    "group": "v1_Notification",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Notification",
            "description": "<p>Description.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Notification[]",
            "optional": false,
            "field": "NotificationList",
            "description": "<p>List of notification items</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ {\"description\":22,\"code\":14 }   ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Notification"
  },
  {
    "type": "post",
    "url": "/v1/NotificationTransaction/",
    "title": "add new NotificatioTransaction",
    "name": "NotificationTransaction_add",
    "group": "v1_NotificationTransaction",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "NotificationId",
            "optional": false,
            "field": "NotificationId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "NotificationCode",
            "optional": false,
            "field": "NotificationCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "IsRead",
            "optional": false,
            "field": "IsRead",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FromUserId",
            "optional": false,
            "field": "FromUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "ToUserId",
            "optional": false,
            "field": "ToUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FollowUpId",
            "optional": false,
            "field": "FollowUpId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Description",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "NotificationTransactionId",
            "description": "<p>The new id agenerated.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_NotificationTransaction"
  },
  {
    "type": "delete",
    "url": "/v1/NotificationTransaction/:NotificationTransactionId",
    "title": "Delete current NotificatioTransaction",
    "name": "NotificationTransaction_delete",
    "group": "v1_NotificationTransaction",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "NotificationId",
            "optional": false,
            "field": "NotificationId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "NotificationCode",
            "optional": false,
            "field": "NotificationCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "IsRead",
            "optional": false,
            "field": "IsRead",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FromUserId",
            "optional": false,
            "field": "FromUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "ToUserId",
            "optional": false,
            "field": "ToUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FollowUpId",
            "optional": false,
            "field": "FollowUpId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Description",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>The status.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_NotificationTransaction"
  },
  {
    "type": "put",
    "url": "/v1/NotificationTransaction/:NotificationTransactionId",
    "title": "Update current NotificatioTransaction",
    "name": "NotificationTransaction_edit",
    "group": "v1_NotificationTransaction",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "NotificationId",
            "optional": false,
            "field": "Notification",
            "description": "<p>id.</p>"
          },
          {
            "group": "Parameter",
            "type": "IsRead",
            "optional": false,
            "field": "Is",
            "description": "<p>Read Message?</p>"
          },
          {
            "group": "Parameter",
            "type": "FromUserId",
            "optional": false,
            "field": "From",
            "description": "<p>User Id</p>"
          },
          {
            "group": "Parameter",
            "type": "ToUserId",
            "optional": false,
            "field": "ToUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FollowUpId",
            "optional": false,
            "field": "FollowUpId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Description",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "result",
            "description": "<p>The status.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_NotificationTransaction"
  },
  {
    "type": "get",
    "url": "/v1/NotificationTransaction/",
    "title": "Read NotificationTransaction",
    "name": "NotificationTransaction_get",
    "group": "v1_NotificationTransaction",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "NotificationId",
            "optional": false,
            "field": "NotificationId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "NotificationCode",
            "optional": false,
            "field": "NotificationCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "IsRead",
            "optional": false,
            "field": "IsRead",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FromUserId",
            "optional": false,
            "field": "FromUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "ToUserId",
            "optional": false,
            "field": "ToUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FollowUpId",
            "optional": false,
            "field": "FollowUpId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Description",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "NotificationTransactionId",
            "description": "<p>The new id agenerated.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_NotificationTransaction"
  },
  {
    "type": "post",
    "url": "/v1/NotificationTransactionList/",
    "title": "Read NotificatioTransaction",
    "name": "NotificationTransaction_getlist",
    "group": "v1_NotificationTransaction",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "NotificationId",
            "optional": false,
            "field": "NotificationId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "NotificationCode",
            "optional": false,
            "field": "NotificationCode",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "IsRead",
            "optional": false,
            "field": "IsRead",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FromUserId",
            "optional": false,
            "field": "FromUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "ToUserId",
            "optional": false,
            "field": "ToUserId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "FollowUpId",
            "optional": false,
            "field": "FollowUpId",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "Description",
            "optional": false,
            "field": "Description",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "NotificationTransactionId",
            "description": "<p>The new id agenerated.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_NotificationTransaction"
  },
  {
    "type": "Post",
    "url": "/v1/Review/",
    "title": "Add Review list",
    "name": "Review_Post",
    "group": "v1_Review",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{     `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`     }",
          "content": "{     `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`     }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Review"
  },
  {
    "type": "delete",
    "url": "/v1/Review/:ReviewId",
    "title": "Delete Review list",
    "name": "Review_delete",
    "group": "v1_Review",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{      `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`   }",
          "content": "{      `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`   }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Review"
  },
  {
    "type": "get",
    "url": "/v1/ReviewListing/",
    "title": "Get Review list",
    "name": "Review_get",
    "group": "v1_Review",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{    `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`         }",
          "content": "Filter{    `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`         }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Review[]",
            "optional": false,
            "field": "Review",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ {    `FromId` ,`ToId`  ,`Name`  ,`Description`  ,`DescriptionLong`  ,`CreatedBy`   ,`CreatedDate`     } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Review"
  },
  {
    "type": "post",
    "url": "/v1/UserProfile/",
    "title": "Create new user",
    "name": "UserProfile_add",
    "group": "v1_UserProfile",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}",
          "content": "{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}\nName",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userId",
            "description": "<p>userId.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userTagId",
            "description": "<p>userTagId.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userInfluenceId",
            "description": "<p>userInfluenceId.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userUsageId",
            "description": "<p>userUsageId.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "userPaymentId",
            "description": "<p>userPaymentId.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>status.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\"userId\":22,\"userTagId\":14,\"userInfluenceId\":7,\"userUsageId\":6,\"userPaymentId\":7,\n\"success_msg\":\"Successfuly registered you as user!\",\"status\":true}   \n\ncurl 'http://ifluence.asia/API/v1/UserProfile' -H 'INFL-KEY: 4064fe4bed3537942c459358fef97e13' -H 'Pragma: no-cache' -H 'Cookie: SessionKey=%224064fe4bed3537942c459358fef97e13%22' -H 'Origin: http://ifluence.asia' -H 'Accept-Encoding: gzip, deflate' -H 'Accept-Language: en-US,en;q=0.8' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json; data=verbose' -H 'Cache-Control: no-cache' -H 'Referer: http://ifluence.asia/' -H 'Proxy-Connection: keep-alive' --data-binary '{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}' --compressed",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_UserProfile"
  },
  {
    "type": "delete",
    "url": "/v1/UserProfile/:userId",
    "title": "Delete current user",
    "name": "UserProfile_del",
    "group": "v1_UserProfile",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}",
          "content": "{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}\nName",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\"status\":true}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_UserProfile"
  },
  {
    "type": "get",
    "url": "/v1/UserProfile/:userId",
    "title": "Get a certain current user profile details",
    "name": "UserProfile_get",
    "group": "v1_UserProfile",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "   HTTP/1.1 200 OK\n{\"Id\":\"53\",\"UserLoginName\":\"CandyYuen\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc#123\",\"Name\":\"Candy Yuen Ka Mang\",\"Email\":\"candy.yuen@gmail.comm\",\"Mobile\":\"+6012312321\",\"Dob\":\"10\\/25\\/1988\",\"Profession\":\"actress\",\"Gender\":\"M\",\"Designation\":\"Hk artist\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"Languange\":\"Hong Kong\",\"Location\":\"Subang\",\"Country\":\"malaysia\",\"Summary\":\"about me\",\"PreferredBankNo\":\"\",\"PreferredBankName\":\"Maybank\",\"PreferredAccNo\":\"723123232\",\"PreferredAccName\":\"Candy Yuen Ki Man\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":\"121\",\"IsFeatureUnlockedInfluencer\":\"0\",\"FailedLoginAttempt\":\"0\",\"IsTempBlocked\":\"0\",\"IsActive\":\"1\",\"IsLoggedOut\":\"0\",\"LastFailedLogin\":\"2017-02-19 18:24:34.000\",\"LastSuccessfulLogin\":\"2018-01-30 23:17:43.000\",\"VirtualAmount\":null,\"VirtualAmountCheckoutDate\":\"2016-11-19 00:00:00.000\",\"SessionKey\":\"bb242c67c4efb8030a495c9121c5275d\",\"CreatedBy\":\"53\",\"CreatedDate\":\"2018-01-30 23:17:43.000\",\"UserType\":{\"Id\":\"1\",\"Code\":\"INFLUENCER\",\"Name\":\"Influencer\"},\"UserTag\":[{\"UserId\":\"53\",\"InterestTagId\":\"17\",\"IsInfluencer\":\"0\"},{\"UserId\":\"53\",\"InterestTagId\":\"7\",\"IsInfluencer\":\"0\"},{\"UserId\":\"53\",\"InterestTagId\":\"6\",\"IsInfluencer\":\"0\"}],\"UserInfluence\":{\"UserId\":\"53\",\"IsBanned\":\"0\",\"IsFbConnected\":null,\"IsFbPage\":null,\"IsInstagramConnected\":null,\"IsGoogleConnected\":null,\"IsTwitterConnected\":null,\"IsVineConnected\":null,\"TotalSuccessfulCampaign\":\"0\",\"TotalFailedCampaign\":\"0\",\"TotalRunningCampaign\":null,\"TotalAmountCollected\":null,\"TotalFriendFb\":null,\"TotalLikesFb\":null,\"FacebookTotalCampaign\":null,\"FacebookTotalPostLikes\":null,\"FacebookTotalPostComment\":null,\"FacebookTotalPostShares\":null,\"FacebookTotalPostReactions\":null,\"TotalFollowerFb\":\"0\",\"TotalFollowerInstagram\":\"0\",\"TotalFollowerTwitter\":\"0\",\"TotalFollowerVine\":\"0\",\"PageAuthority\":\"0.00\",\"PageRank\":\"0.00\",\"ReplyRatio\":\"0.00\",\"AverageShared\":\"0.00\",\"CreatedBy\":\"53\",\"CreatedDate\":\"2017-02-19 18:29:03.000\"},\"UserPayment\":{\"UserId\":\"53\",\"PaymentAccNo\":\"72132132\",\"PaymentHolderName\":\"Candy Yuen Ki Man\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"Maybank\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\",\"CreatedBy\":\"53\",\"CreatedDate\":\"2017-02-19 18:29:03\"},\"UserUsage\":{\"Id\":\"48\",\"UserId\":\"53\",\"Price\":\"3\",\"RemainingAllowedCampaign\":\"10\",\"RemainingInfluencer\":\"10\",\"RemainingPosting\":\"11\",\"MaxCostRemaining\":\"10.00\",\"CreatedBy\":\"53\",\"CreatedDate\":\"2017-02-19 18:29:03.000\"},\"UserPhoto\":[{\"id\":\"121\",\"Url\":\"uploads\\/2016-10-16\\/\\/profile9DOTjpg-14765836345802e0d2cf8922016-10-16.jpeg\",\"Blob\":\"-\",\"CreatedBy\":\"APIv1\",\"CreatedDate\":\"2016-10-16 10:07:14.000\"}]}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_UserProfile"
  },
  {
    "type": "put",
    "url": "/v1/UserProfile/:userId",
    "title": "Update current user",
    "name": "UserProfile_upd",
    "group": "v1_UserProfile",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}",
          "content": "{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}\nName",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_UserProfile"
  },
  {
    "type": "get",
    "url": "/v1/AllUserProfile",
    "title": "Get all current user profile listing",
    "name": "Userprofile_All",
    "group": "v1_Userprofile",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "filter",
          "content": "{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}\nName",
          "type": "json"
        }
      ]
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\"Id\":null,\"UserLoginName\":\"seanlonn\",\"GoogleLoginName\":\"\",\"FbLoginName\":\"\",\"Password\":\"abc123\",\"Name\":\"eweawr\",\"Email\":\"abc@abc.com\",\"Mobile\":\"dfas\",\"Dob\":\"2017-09-22\",\"Profession\":\"\",\"Gender\":\"\",\"Designation\":\"\",\"CompanyName\":\"\",\"CompanyNature\":\"\",\"CompanyNo\":\"\",\"CompanyLocation\":\"\",\"UserTypeId\":\"1\",\"AccountTypeId\":\"1\",\"PhotoId\":null,\"UserPhoto\":null,\"UserPayment\":{\"PaymentAccNo\":\"\",\"PaymentHolderName\":\"\",\"PaymentExpiryDate\":\"\",\"PaymentSecurityCode\":\"\",\"PaymentIssuerName\":\"\",\"PaymentIssuerCategory\":\"\",\"PaymentType\":\"\"},\"UserInfluence\":{\"IsBanned\":false,\"TotalSuccessfulCampaign\":0,\"TotalFailedCampaign\":0,\"TotalFollowerFb\":0,\"TotalFollowerInstagram\":0,\"TotalFollowerTwitter\":0,\"TotalFollowerVine\":0,\"PageAuthority\":0,\"PageRank\":0,\"ReplyRatio\":0,\"AverageShared\":0},\"ConfirmPassword\":\"abc123\",\"Summary\":\"raedfs\",\"PreferredAccNo\":\"fsda\",\"Languange\":\"fa\",\"Location\":\"adfs\",\"Country\":\"fdas\",\"PreferredBankName\":\"dfasdsfdsfsda\",\"PreferredAccName\":\"fdas\",\"LoginType\":\"NORMAL\",\"UserTag\":\"4,13\"}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_Userprofile"
  },
  {
    "type": "Post",
    "url": "/v1/VirtualCurrency/",
    "title": "Add VirtualCurrency list",
    "name": "VirtualCurrency_Post",
    "group": "v1_VirtualCurrency",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "content": "{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "dbGeneratedId",
            "optional": false,
            "field": "Generated",
            "description": "<p>id inserted record into db</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_VirtualCurrency"
  },
  {
    "type": "delete",
    "url": "/v1/VirtualCurrency/:virtualCurrencyId",
    "title": "Delete VirtualCurrency list",
    "name": "VirtualCurrency_delete",
    "group": "v1_VirtualCurrency",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "content": "{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of delete</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_VirtualCurrency"
  },
  {
    "type": "get",
    "url": "/v1/VirtualCurrency/",
    "title": "Get VirtualCurrency list",
    "name": "VirtualCurrency_get",
    "group": "v1_VirtualCurrency",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "Filter{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "content": "Filter{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "VirtualCurrency[]",
            "optional": false,
            "field": "VirtualCurrency",
            "description": "<p>List</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n[ { Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate   } ]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_VirtualCurrency"
  },
  {
    "type": "put",
    "url": "/v1/VirtualCurrency/:virtualCurrencyId",
    "title": "Edit VirtualCurrency list",
    "name": "VirtualCurrency_put",
    "group": "v1_VirtualCurrency",
    "header": {
      "fields": {
        "Request": [
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-AUTH",
            "description": "<p>Authorization value.</p>"
          },
          {
            "group": "Request",
            "type": "String",
            "optional": false,
            "field": "INFL-TIMESTAMP",
            "description": "<p>Authorization Timestamp value.</p>"
          }
        ],
        "Response": [
          {
            "group": "Response",
            "type": "String",
            "optional": false,
            "field": "Infl-Key",
            "description": "<p>The responded session key 4064fe4bed3537942c459358fef97e13</p>"
          }
        ]
      }
    },
    "parameter": {
      "examples": [
        {
          "title": "{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "content": "{ Id  ,  Description ,  Unit  , Cost  , DaysAgo  ,     CreatedBy  ,    CreatedDate    }",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "result",
            "optional": false,
            "field": "Status",
            "description": "<p>of update</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500  \n{\n  \"error_id\": \"\"\n  \"error_msg\": \"\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.doc.php",
    "groupTitle": "v1_VirtualCurrency"
  }
] });
