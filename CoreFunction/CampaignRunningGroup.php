<?php 
//#entry point  
function getCampaignRunningGroupListing($app  )
{
  
    $reqParam = getJsonRequest($app); 
   // $userInfo = getUserSessionInfo($app) ;    
    getJsonResponse($app, getCampaignRunningGroup($reqParam )  ); 
}
 

function getCampaignRunningGroup($reqParam ){ 
 
    $sqlStatement   = 'SELECT 
                          Id   , 
                          CampaignRunningId    ,   
                          GroupName  ,   
                          GroupDescription   , 
                          InfluencerId   ,   
                          CreatedBy    ,         
                          CreatedDate        
    FROM CampaignRunningGroup   ';
    $whereStatement = " where 1=1   ";
  

    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and  Id = '" . $Id . "'";
    }  
   
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $whereStatement = $whereStatement . "and  CampaignRunningId = '" . $CampaignRunningId . "'";
    }
    $GroupName = getKeyVal($reqParam, "GroupName");
    if (!empty($GroupName)) {
        $whereStatement = $whereStatement . "and  GroupName like '%" . $GroupName . "%'";
    }   
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . "and  InfluencerId like '%" . $InfluencerId . "%'";
    }   

    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . "and  CreatedBy like '%" . $filterCreatedBy . "%'";
    }
 
    $CreatedDate = getKeyVal($reqParam, "CreatedDate");
    if (!empty($CreatedDate)) {
        $whereStatement = $whereStatement . "and  CreatedDate  = '" . $CreatedDate . "'";
    }  
   
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
   return  queryDB($sqlStatement); 

    
}
 


//#entry point 
function  editCampaignRunningGroup($app , $campaignRunningGroupId    ){
  
    $reqParam = getJsonRequest($app);  
    // $userInfo = getUserSessionInfo($app) ; 
 
     
    checkEmpty($app,"campaignRunningGroupId", $campaignRunningGroupId) ; 
    
    // $CampaignRunningGroupId  = getKeyVal($reqParam, "CampaignRunningGroupId");   
    // checkEmpty($app,"CampaignRunningGroupId", $CampaignRunningGroupId) ; 
    
    // $InfluencerId  = getKeyVal($reqParam, "InfluencerId");   
    // checkEmpty($app,"InfluencerId", $InfluencerId) ; 

    $sqlStatement   = " Update  CampaignRunningGroup  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $campaignRunningGroupId . "' ;"; 
     
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $setStatement = $setStatement . " CampaignRunningId='" . $CampaignRunningId . "' ,";
    }

 
    $GroupName = getKeyVal($reqParam, "GroupName");
    if (!empty($GroupName)) {
        $setStatement = $setStatement . " GroupName='" . $GroupName . "' ,";
    } 
   
    $GroupDescription = getKeyVal($reqParam, "GroupDescription");
    if (!empty($GroupDescription)) {
        $setStatement = $setStatement . " GroupDescription='" . $GroupDescription . "' ,";
    } 
    
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $setStatement = $setStatement . " InfluencerId='" . $InfluencerId . "' ,";
    }  
    $CreatedBy = "*";
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
     
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "campaignRunningGroupId" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

 
//#entry point 
function  deleteCampaignRunningGroup($app, $CampaignRunningGroupId) 
{

    $reqParam = getJsonRequest($app);  
    // $userInfo = getUserSessionInfo($app) ;    

    checkEmpty($app,"CampaignRunningGroupId", $CampaignRunningGroupId) ;  

    //delete virtualCurrency record
    $sqlStatement = "DELETE FROM CampaignRunningGroup WHERE Id='" . $CampaignRunningGroupId . "'";
    $dataResult = crudDB($sqlStatement);   
    $result = array(  "status" => true,  );


 
    getJsonResponse($app, $result); 
}
 
//#entry point  
function addCampaignRunningGroup($app  ){
    $reqParam = getJsonRequest($app) ; 

 
    $CampaignRunningId  = getKeyVal($reqParam, "CampaignRunningId");  
    $GroupName  = getKeyVal($reqParam, "GroupName");  
    $GroupDescription  = getKeyVal($reqParam, "GroupDescription");  
    $InfluencerId  = getKeyVal($reqParam, "InfluencerId");   
 
    // echo var_dump($CampaignRunningId);
    // return ;
    // checkEmpty($app,"CampaignRunningId", $CampaignRunningId) ;
    // checkEmpty($app,"Group Name", $GroupName) ;  
    // checkEmpty($app,"Group Description", $GroupDescription) ;
    // checkEmpty($app,"InfluencerId", $InfluencerId) ;

 
 
  
    $sqlStatement   = "INSERT INTO  CampaignRunningGroup (    
                        `CampaignRunningId` ,  
                        `GroupName`  ,   
                        `GroupDescription`  , 
                        `InfluencerId`  ,   
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
    
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $valueStatement = $valueStatement . "'" . $CampaignRunningId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $GroupName = getKeyVal($reqParam, "GroupName");
    if (!empty($GroupName)) {
        $valueStatement = $valueStatement . "'" . $GroupName . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    $GroupDescription = getKeyVal($reqParam, "GroupDescription");
    if (!empty($GroupDescription)) {
        $valueStatement = $valueStatement . "'" . $GroupDescription . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $valueStatement = $valueStatement . "'" . $InfluencerId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
 

 
//createdby
    $CreatedBy = "*";
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "CampaignRunningGroupId" =>$mysqli->insert_id  );
    getJsonResponse($app,$result); 

}
 


 
 
 
 
?>