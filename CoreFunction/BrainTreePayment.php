<?php 

function performOnboard( $app, $step, $keydata)
{

     $infAccountParams = [
      'individual' => [
        'firstName' => 'Jane',
        'lastName' => 'Doe',
        'email' => 'jane@14ladders.com',
        'phone' => '5553334444',
        'dateOfBirth' => '1981-11-19',
        'ssn' => '456-45-4567',
        'address' => [
          'streetAddress' => '111 Main St',
          'locality' => 'Chicago',
          'region' => 'IL',
          'postalCode' => '60622'
        ]
      ], 
      'funding' => [
        'descriptor' => 'iFlunce Payment',
        'destination' => Braintree_MerchantAccount::FUNDING_DESTINATION_BANK, 
        'accountNumber' => '7012342070',
        'routingNumber' => '026001287'
      ],


      'tosAccepted' => true,
      'masterMerchantAccountId' => "ifluencetest" 

      // CIBBMYKLXXX

    ];
    var_dump($infAccountParams  );
    echo "<p><br></p>";

    $result = Braintree_MerchantAccount::create($infAccountParams); 
    var_dump($result  );



} 

 


function addPasswordReset($reqParam, $userId)
{

    if(empty( $reqParam)){ 
         $reqParam = new StdClass;
    }
          
 
    $sqlStatement = "INSERT INTO  PasswordReset (  
                        `UserId` , 
                        `Password` ,  
                        `ActivationCode`  , 
                        `Status`  ,   
                        `CreatedBy`   ,   
                        `CreatedDate`    
                    )  ";
    
    $valueStatement = "VALUES (";
    if (!empty($userId)) {
        $valueStatement = $valueStatement . "'" . $userId . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $Password = getKeyVal($reqParam, "Password");
    if (!empty($Password)) {
        $valueStatement = $valueStatement . "'" . $Password . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    $ActivationCode = getKeyVal($reqParam, "ActivationCode");
    if (!empty($ActivationCode)) {
        $valueStatement = $valueStatement . "'" . $ActivationCode . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Status = getKeyVal($reqParam, "Status");
    if (!empty($Status)) {
        $valueStatement = $valueStatement . "'" . $Status . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    } 
    
    //createdby
    $valueStatement = $valueStatement . "'APIv1' , ";
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";
    
    $mysqli = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
}
 
function editPasswordReset($reqParam ,$activationCode)
{

    if( empty( $reqParam)){
            return false;
    } 
    $sqlStatement   = " Update  PasswordReset ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE activationCode='" . $activationCode . "' ;";
 
 
    $UserId = getKeyVal($reqParam, "UserId");
    if (!empty($UserId)) {
        $setStatement = $setStatement . " UserId='" . $UserId . "' ,";
    } 

    $Password = getKeyVal($reqParam, "Password");
    if (!empty($Password)) {
        $setStatement = $setStatement . " Password='" . $Password . "' ,";
    } 

    $Status = getKeyVal($reqParam, "Status");
    if (!empty($Status)) {
        $setStatement = $setStatement . " Status='" . $Status . "' ,";
    }  
    
    $CreatedBy = "'APIv1'";
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy= " . $CreatedBy . "  ,";
    }
    
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate= " . $CreatedDate . "  ,";
    }
    
    
    $setStatement = $setStatement . " CreatedBy=CreatedBy ";

     return crudDB($sqlStatement . $setStatement . $whereStatement); 
     // var_export(  $sqlStatement . $setStatement . $whereStatement);
 }

 
function getPasswordReset($reqParam )
{
 

    $sqlStatement = 'SELECT 
                        `Id`  , 
                        `UserId` , 
                        `Password` ,  
                        `ActivationCode`  , 
                        `Status`  ,   
                        `CreatedBy`   ,   
                        `CreatedDate`    
            FROM PasswordReset';
    
    $whereStatement = " where 1=1 ";

    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and  Id='" . $Id . "'  ";
    }
    $ActivationCode = getKeyVal($reqParam, "ActivationCode");
    if (!empty($ActivationCode)) {
        $whereStatement = $whereStatement . " and   ActivationCode='" . $ActivationCode . "'  ";
    }
    
    $UserId = getKeyVal($reqParam, "UserId");
    if (!empty($UserId)) {
        $whereStatement = $whereStatement . "  and   UserId='" . $UserId . "'  ";
    }

    $Status = getKeyVal($reqParam, "Status");
    if (!empty($Status)) {
        $whereStatement = $whereStatement . " and Status='" . $Status . "'  ";
    }
 
    $sqlStatement = $sqlStatement . $whereStatement . " and CreatedDate=CreatedDate   ; "; 
    $data         = queryDB($sqlStatement);
    return $data;
   // var_export(  $sqlStatement);
}
function random_password( $length = 8 ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}
//#ENTRY
 function  performResetPassword($app,$step,$keyData){

    //get step
    if(empty( $step) ||  empty( $keyData)){ 
        return processErrorMessage($app, " access does not exist. ", "001");
    }
    //
    //step1 
    //  determine keydata is username, find from db username exist
    //  if not exist-->throw error ,500 :no user found, check spelling
    //  if exist add PasswordReset 
    //  generate activationCode and random new password, 
    //  send to email to activate, email receives a link with activationcode
    //  
    if(isComparedEqual( "1",$step)){ 
        if( is_int(   $keyData ) ){  
            return processErrorMessage($app, " User not found. Please make sure you key in correctly. ", "001");
        }

         $resultList = getUser($reqParam, $keyData, null); 
         if(empty($resultList ) ||empty($resultList[0] ) ){

            return processErrorMessage($app, " User not found. Please make sure you key in correctly. ", "001");
         }
        $userId=$resultList[0]["Id"] ;
        $generatedKey = sha1(mt_rand(10000,99999).time().$userId);

        $addParam = new StdClass;
        $addParam->UserId = $resultList[0]["Id"] ;       
        $addParam->Password = random_password(   8 ) ;       
        $addParam->ActivationCode = $generatedKey ;       
        $addParam->Status ="NEW" ;            


        $resetId = addPasswordReset($addParam, $resultList[0]["Id"]);

        $Email = $resultList[0]["Email"] ;  
        $Name = $resultList[0]["Name"] ;  
        $Link =$_SERVER['HTTP_HOST']."/Influencer/API/v1/ResetPassword/2/".$addParam->ActivationCode."";
        $bodyText="
             Hi $Name ,\n Please press the link below to activate and reset your password.\n\n
             Your new password is  \n\n           $addParam->Password\n\n
              Please click on this link to activate.  $Link \n\n 
              If you didnt request for this, please ignore otherwise and report to us immediately.

        ";
        $bodyHtml="
            <p>Hi $Name , <br/> Please press the link below to activate and reset your password.</p>
            <p>Your new password is  </p>          <p>$addParam->Password</p>
            <p> Please click on this link to activate. <a href='$Link'>$Link</a></p> 
            <p>
              If you didnt request for this, please ignore otherwise and report to us immediately.</p>

        ";
         $emailArr = array(
            "recipientEmail" =>   $Email,
            "recipientName" =>   $Name,
            "subject" => "Influencer Password Reset",
            "bodyHtml" => $bodyHtml,
            "bodyText" => $bodyText
        );
 
        $emailResult= sendEMailMessage($emailArr); 

          $result = array( 
            "status" => "true",
            "message" => "An email with reset instruction has been sent to user, $keyData . Please check your email." 
        );

       getJsonResponse($app,$result);  
        $app->halt("200", json_encode($result));
    }
    //step2
    //  determine if keydata/activationCode [opened from email] exist in  password reset table
    //  if not exist -->throw error 500 : invalid page, go back home page
    //  if exist , update password history status, update user table with new password
    //  respond with login redirect.

    if(isComparedEqual( "2",$step)){ 

        $getParam = new StdClass;
        $getParam->Status ="NEW";       
        $getParam->ActivationCode = $keyData;      
        $resultList = getPasswordReset($getParam ) ; 


       if(empty($resultList ) ||empty($resultList[0] ) ){ 
            return processErrorMessage($app, " Activation key not found. Please make sure you key in correctly. We advise copy-pasting from the email. ", "001");
        }

        
        $userId=$resultList[0]["UserId"] ;
        $password=$resultList[0]["Password"] ;
        $activationCode=$resultList[0]["ActivationCode"] ;
        $editParam = new StdClass;
        $editParam->Status ="CLOSED";         
        editPasswordReset($editParam ,$activationCode) ; 


        $editParam = new StdClass; 
        $editParam->Password =$password;     
        $editParam->FailedLoginAttempt ="00";   
        $editParam->IsTempBlocked ="00";   
        $editParam->IsLoggedOut ="1";    
    
        editUserCommon($editParam, $userId,null); 

        $result = array( 
            "status" => "true",
            "redirect" =>"home"
        ); 
 
        return ;
    }
   

 } 


 
?>