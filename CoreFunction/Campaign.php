<?php 

  


function getCampaignFacebook(  $campaignId ,$reqParam)
{ 

    $sqlStatement   = 'SELECT   `id`   ,
                            `CampaignId`   ,
                            `UserId`   ,
                            `PostedId`   ,
                            `PostContent`  ,   
                            `PostedDate`     ,
                            `TotalPostComment` ,
                            `TotalPostReaction` ,
                            `TotalLike` ,
                            `TotalPostShare` ,
                            `ProductPageId`   ,
                            `TotalProductPageShare` ,
                            `TotalProductPageFollow`   , 
                            `CreatedBy`  ,   
                            `CreatedDate`     
                       FROM CampaignFacebook';
    $whereStatement = " where 1=1 " ;

    if (!empty($campaignId)) {
        $whereStatement = $whereStatement . " and campaignId = '" . $campaignId . "'";
    }
    else{ 
        $whereStatement = $whereStatement . " and campaignId = '-999'";
    }
    


    $filterMinDate = getKeyVal($reqParam, "MinDate");
    if (!empty($filterMinDate)) {
        $whereStatement = $whereStatement . " and  Date >= '" . $filterMinDate . "'";
    }


    $filterMaxDate = getKeyVal($reqParam, "MaxDate");
    if (!empty($filterMaxDate)) {
        $whereStatement = $whereStatement . " and Date <= '" . $filterMaxDate . "'";
    }


    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data;
}



 

function getCampaign($reqParam ){ 

    $sqlStatement   = 'SELECT 
                        `Id`  , 
                        `Name`   , 
                        `Description` ,  
                        `Price`  , 
                        `NoOfInfluencer`  , 
                        `NoOfPosting`  , 
                        `Charges1`  ,
                        `Charges1Desc`   ,
                        `Charges2`  ,
                        `Charges2Desc`   ,
                        `Charges3`  ,
                        `Charges3Desc`   , 
                        `TotalCost`  ,     
                        `PhotoIdList`   , 
                        `TagIdList`   , 
                        `IsActive`    ,     
                        `FromDate`      , 
                        `ToDate`      , 
                         DATEDIFF(FromDate ,ToDate  ) AS DaysLeft,
                        `CreatedBy`   ,   
                        `PromoTitle`   ,   
                        `PromoCaption`   ,   
                        `PromoDescription`   ,    
                        `CreatedDate`         

    FROM Campaign ';
    $whereStatement = " where 1=1 ";
 
    $CampaignList= getKeyVal($reqParam, "CampaignList");
    if (!empty($CampaignList)) {
    $limit = count($CampaignList);
      foreach ($CampaignList as $keyy => $valuee) {  
                if(!empty( $valuee )){ 
                        if($keyy == 0 ){ 
                          $whereStatement = $whereStatement . " and (   Id = '" . $valuee . "' ";
                        }else{
                         $whereStatement = $whereStatement . " OR  Id = '" . $valuee . "' ";
                        }

                        if($keyy == intval($limit) - 1 ){  
                          $whereStatement = $whereStatement . "  ) ";
                        }
                         
               } 
        }
    }


   $TagIdList= getKeyVal($reqParam, "TagIdList");  
     if (!empty($TagIdList)) {
      $isList = strpos($TagIdList,",");
      if($isList){
              $TagIdList= explode( ",",  $TagIdList  );   
                $whereStatement = $whereStatement . " AND ( ";
                foreach ($TagIdList as $statusKey => $statusValue) {  
                    if(!empty( $statusValue )){ 
                        if($statusKey == 0 ){ 
                          $whereStatement = $whereStatement . " TagIdList like '%" . $statusValue . "%' ";
                        }else{
                         $whereStatement = $whereStatement . " OR TagIdList like '%" . $statusValue . "%' ";
                        }
                         
                   } 
                }
              $whereStatement = $whereStatement . "  ) " ;
      }
      else{ 
            $whereStatement = $whereStatement . " AND TagIdList like '%" . $TagIdList . "%' ";
      } 

    }

    $filterId = getKeyVal($reqParam, "Id");
    if (!empty($filterId)) {
        $whereStatement = $whereStatement . " and Id = '" . $filterId . "'";
    }
   
    $filterName = getKeyVal($reqParam, "Name");
    if (!empty($filterName)) {
        $whereStatement = $whereStatement . " and Name like '%" . $filterName . "%'";
    }
    $filterDescription = getKeyVal($reqParam, "Description");
    if (!empty($filterDescription)) {
        $whereStatement = $whereStatement . " and Description like '%" . $filterDescription . "%'";
    } 
// Price
    $filterPriceMin = getKeyVal($reqParam, "PriceMin");
    if (!empty($filterPriceMin)) {
        $whereStatement = $whereStatement . " and cast( a.Price  as decimal)  >= cast( '" . $filterPriceMin . "' as decimal)  ";
    }
      $filterPriceMax = getKeyVal($reqParam, "PriceMax");
    if (!empty($filterPriceMax)) {
        $whereStatement = $whereStatement . " and  cast( a.Price  as decimal)  <=  cast( '" . $filterpriceMax . "'  as decimal)  ";
    }
// NoOfPosting
    $filterNoOfPostingMin = getKeyVal($reqParam, "NoOfPostingMin");
    if (!empty($filterNoOfPostingMin)) {
        $whereStatement = $whereStatement . " and cast( a.NoOfPosting  as decimal)  >= cast( '" . $filterNoOfPostingMin . "' as decimal)  ";
    }
      $filterNoOfPostingMax = getKeyVal($reqParam, "NoOfPostingMax");
    if (!empty($filterNoOfPostingMax)) {
        $whereStatement = $whereStatement . " and  cast( a.NoOfPosting  as decimal)  <=  cast( '" . $filterNoOfPostingMax . "'  as decimal)  ";
    }

// NoOfInfluencer
    $filterNoOfInfluencerMin = getKeyVal($reqParam, "NoOfInfluencerMin");
    if (!empty($filterNoOfInfluencerMin)) {
        $whereStatement = $whereStatement . " and cast( a.NoOfInfluencer  as decimal)  >= cast( '" . $filterNoOfInfluencerMin . "' as decimal)  ";
    }
      $filterNoOfInfluencerMax = getKeyVal($reqParam, "NoOfInfluencerMax");
    if (!empty($filterNoOfInfluencerMax)) {
        $whereStatement = $whereStatement . " and  cast( a.NoOfInfluencer  as decimal)  <=  cast( '" . $filterNoOfInfluencerMax . "'  as decimal)  ";
    }
// TotalCost
    $filterTotalCostMin = getKeyVal($reqParam, "TotalCostMin");
    if (!empty($filterTotalCostMin)) {
        $whereStatement = $whereStatement . " and cast( a.TotalCost  as decimal)  >= cast( '" . $filterTotalCostMin . "' as decimal)  ";
    }
      $filterTotalCostMax = getKeyVal($reqParam, "TotalCostMax");
    if (!empty($filterTotalCostMax)) {
        $whereStatement = $whereStatement . " and  cast( a.TotalCost  as decimal)  <=  cast( '" . $filterTotalCostMax . "'  as decimal)  ";
    } 
 
    $filterMinFromDate = getKeyVal($reqParam, "MinFromDate");
    if (!empty($filterMinFromDate)) {
        $whereStatement = $whereStatement . " and  FromDate >= '" . $filterMinFromDate . "'";
    }
 
    $filterMaxFromDate = getKeyVal($reqParam, "MaxFromDate");
    if (!empty($filterMaxFromDate)) {
        $whereStatement = $whereStatement . " and FromDate <= '" . $filterMaxFromDate . "'";
    }
    $filterFromDate = getKeyVal($reqParam, "FromDate");
    if (!empty($filterFromDate)) {
        $whereStatement = $whereStatement . " and FromDate = '" . $filterFromDate . "'";
    }

    $filterMinToDate = getKeyVal($reqParam, "MinToDate");
    if (!empty($filterMinToDate)) {
        $whereStatement = $whereStatement . " and  ToDate >= '" . $filterMinToDate . "'";
    }
    $filterMaxToDate = getKeyVal($reqParam, "MaxToDate");
    if (!empty($filterMaxToDate)) {
        $whereStatement = $whereStatement . " and  ToDate <= '" . $filterMaxToDate . "'";
    }
    $filterToDate = getKeyVal($reqParam, "ToDate");
    if (!empty($filterToDate)) {
        $whereStatement = $whereStatement . " and ToDate = '" . $filterToDate . "'";
    }
    $filterIsActive = getKeyVal($reqParam, "IsActive");
    if (!empty($filterIsActive)) {
        $whereStatement = $whereStatement . " and IsActive = '" . $filterIsActive . "'";
    }

    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . " and CreatedBy like '%" . $filterCreatedBy . "%'";
    }

    $filterPromoTitle = getKeyVal($reqParam, "PromoTitle");
    if (!empty($filterPromoTitle)) {
        $whereStatement = $whereStatement . " and PromoTitle  = '" . $filterPromoTitle . "'";
    }
    $filterCreatedDate = getKeyVal($reqParam, "CreatedDate");
    if (!empty($filterCreatedDate)) {
        $whereStatement = $whereStatement . " and CreatedDate  = '" . $filterCreatedDate . "'";
    }
    //CurrentTimestamp must be >= fromDate && <=ToDate
    $currentDate= date('Y-m-d H:i:s', strtotime('-0 months'));
    // $whereStatement = $whereStatement . " and FromDate   <= '" .  $currentDate   . "'";
    // $whereStatement = $whereStatement . " and ToDate   >= '" .  $currentDate   . "'"; 
   
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
   return  queryDB($sqlStatement); 
    
}

function getCampaignFacebookSquashed($rec , $filterCampaignStat ){

           //Fetch fb stat  - then aggregate them
                $fbStatList=getCampaignFacebook(  $rec["Id"], $filterCampaignStat );

                   $rec['FacebookStat'] =[]; $totalComment= 0 ; $totalLike=0; $totalShare=0;
                    foreach ($fbStatList as $fbRecKey => $fbRecValue) {  
                        if(!empty( $fbRecValue)){ 
                          $totalComment+=floatval( $fbRecValue["TotalPostComment"]);
                          $totalLike+=floatval($fbRecValue["TotalLike"]);
                          $totalShare+=floatval($fbRecValue["TotalPostShare"]);
                        } 
                    }

                $rec['FacebookStat']["TotalPostComment"]=$totalComment;
                $rec['FacebookStat']["TotalLike"]=$totalLike;
                $rec['FacebookStat']["TotalPostShare"]=$totalShare;

    return $rec;
}
function getCampaignDataListing($reqParam){

    $listCampaign =getCampaign($reqParam ) ;  
    $NoExtraData = getKeyVal($reqParam, "NoExtraData");
    
    foreach ($listCampaign as $key => &$rec) {  
        if( !empty($rec ) ){
 

            if(empty($NoExtraData)){

                //fetch campaign running 
                $filter = new StdClass; 
                $filter->CampaignId =  $rec["Id"] ;  
                $rec["Running"]=getCampaignRunning(  $filter ); 
               }

                //Fetch foto attachment
                $PhotoIdList= explode( ",",  $rec["PhotoIdList"]  );   
                
                $rec['PhotoList'] =[];
                foreach ($PhotoIdList as $photoKey => $photoValue) { 
                    $photoRec=getUserPhoto(  $photoValue )[0];
                    if(!empty( $photoRec)){   array_push( $rec['PhotoList'] ,  $photoRec);}

                }

         


 

            $filterCampaignStat = new StdClass;
            $filterCampaignStat->MinDate = null;   
            $filterCampaignStat->MaxDate = null; 
            $rec = getCampaignFacebookSquashed($rec , $filterCampaignStat);


        }  

    }
    return $listCampaign ;
}

function getCampaignListing($app){
 
    $reqParam = getJsonRequest($app); 
    return getJsonResponse($app, getCampaignDataListing($reqParam) ); 
}

function addCampaign($app,  $userInfo ){
    $reqParam = getJsonRequest($app) ;
   
 
    $UserPhotoList  = getKeyVal($reqParam, "UserPhotos");
    $TagIdList  = getKeyVal($reqParam, "TagIds");
    $PhotoIdList=[];  
    if(!empty($UserPhotoList)){
        foreach ($UserPhotoList as $key => $value) { 
            if (!empty($value)) {
                $photoId=addPhoto( $value);
                if(!empty($photoId)){
                 array_push($PhotoIdList,$photoId);
                
                }
            } 
        } 
    }

    $PhotoIdList =  implode(",",$PhotoIdList);

    $sqlStatement   = "INSERT INTO  Campaign (  
                `Name`,
                `Description`,
                `Price`,
                `NoOfInfluencer` , 
                `NoOfPosting` , 
                `Charges1`  ,
                `Charges1Desc`   ,
                `Charges2`  ,
                `Charges2Desc`   ,
                `Charges3`  ,
                `Charges3Desc`   , 
                `TotalCost`  ,     
                `PhotoIdList`   ,  
                `TagIdList`   ,  
                `IsActive`    ,   
                `FromDate`      , 
                `ToDate`      , 
                `CreatedBy`   ,   
                `PromoTitle`   ,   
                `CreatedDate`  
            )  ";
    $valueStatement = "VALUES (";
    $Name  = getKeyVal($reqParam, "Name");
    if (!empty($Name)) {
        $valueStatement = $valueStatement . "'" . $Name . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $valueStatement = $valueStatement . "'" . $Description . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    $Price = getKeyVal($reqParam, "Price");
    if (!empty($Price)) {
        $valueStatement = $valueStatement . "'" . $Price . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $NoOfInfluencer = getKeyVal($reqParam, "NoOfInfluencer");
    if (!empty($NoOfInfluencer)) {
        $valueStatement = $valueStatement . "'" . $NoOfInfluencer . "',";
    }else{ 
         $valueStatement = $valueStatement . "'0',";
    }
    
    $NoOfPosting = getKeyVal($reqParam, "NoOfPosting");
    if (!empty($NoOfPosting)) {
        $valueStatement = $valueStatement . "'" . $NoOfPosting . "',";
    }
  else{ 
         $valueStatement = $valueStatement . "'0',";
    }
      
    $Charges1 = getKeyVal($reqParam, "Charges1");
    if (!empty($Charges1)) {
        $valueStatement = $valueStatement . "'" . $Charges1 . "',";
    }else{ 
         $valueStatement = $valueStatement . "'0',";
    }
    $Charges1Desc = getKeyVal($reqParam, "Charges1Desc");
    if (!empty($Charges1Desc)) {
        $valueStatement = $valueStatement . "'" . $Charges1Desc . "',";
    }else{ 
         $valueStatement = $valueStatement . "'-',";
    }

    $Charges2 = getKeyVal($reqParam, "Charges2");
    if (!empty($Charges2)) {
        $valueStatement = $valueStatement . "'" . $Charges2 . "',";
    }else{ 
         $valueStatement = $valueStatement . "'0',";
    }
    $Charges2Desc = getKeyVal($reqParam, "Charges2Desc");
    if (!empty($Charges2Desc)) {
        $valueStatement = $valueStatement . "'" . $Charges2Desc . "',";
    }else{ 
         $valueStatement = $valueStatement . "'-',";
    }
    $Charges3 = getKeyVal($reqParam, "Charges3");
    if (!empty($Charges3)) {
        $valueStatement = $valueStatement . "'" . $Charges3 . "',";
    }else{ 
         $valueStatement = $valueStatement . "'0',";
    }
    
    $Charges3Desc = getKeyVal($reqParam, "Charges3Desc");
    if (!empty($Charges3Desc)) {
        $valueStatement = $valueStatement . "'" . $Charges3Desc . "',";
    }else{ 
         $valueStatement = $valueStatement . "'-',";
    }
    $TotalCost = getKeyVal($reqParam, "TotalCost");
    if (!empty($TotalCost)) {
        $valueStatement = $valueStatement . "'" . $TotalCost . "',";
    }else{ 
         $valueStatement = $valueStatement . "'0',";
    } 
     if (!empty($PhotoIdList)) {
        $valueStatement = $valueStatement . "'" . $PhotoIdList . "',";
    }else{ 
         $valueStatement = $valueStatement . "'',";
    } 
    if (!empty($TagIdList)) {
        $valueStatement = $valueStatement . "'" . $TagIdList . "',";
    }else{ 
         $valueStatement = $valueStatement . "'',";
    } 
        
    $IsActive = getKeyVal($reqParam, "IsActive");
    if (!empty($IsActive)) {
        $valueStatement = $valueStatement . "'" . $IsActive . "',";
    }else{

        $valueStatement = $valueStatement . "'1',";
    }
                  
    $FromDate = getKeyVal($reqParam, "FromDate");
    if (!empty($FromDate)) {
        $valueStatement = $valueStatement . "'" . $FromDate . "',";
    } 
    $ToDate = getKeyVal($reqParam, "ToDate");
    if (!empty($ToDate)) {
        $valueStatement = $valueStatement . "'" . $ToDate . "',";
    }

    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        //createdby
        $valueStatement = $valueStatement . "'APIv1' ,";
    }

    $BrandName = $userInfo['Name'];
    if (!empty($BrandName)) {
        $valueStatement = $valueStatement . "'" . $BrandName . "',";
    }
    else{ 
        //BrandName
        $valueStatement = $valueStatement . "'BrandName' ,";
    }
    
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli         = crudDB($sqlStatement . $valueStatement);
    if( $mysqli ){ 
    // successful post-->update usage,statusId is always REQ/  
    //UPDATE USAGE
    $NoOfInfluencer  = getKeyVal($reqParam, "NoOfInfluencer");
    $NoOfPosting  = getKeyVal($reqParam, "NoOfPosting");
    $TotalCost  = getKeyVal($reqParam, "TotalCost");  

 
    
    $userusage = $userInfo["UserUsage"]  ;
    //hydrate because can be empty 
    $userInfo["UserUsage"] = getUserUsage($reqParam,  $userInfo['Id'] );
    $userusage =$userInfo["UserUsage"][0];  

  
     $userusage["RemainingAllowedCampaign"]  = intval( $userusage["RemainingAllowedCampaign"]  ) - 1;
     $userusage["RemainingInfluencer"]  = intval($userusage["RemainingInfluencer"]  ) - intval($NoOfInfluencer);

     $userusage["RemainingPosting"]  =intval($userusage["RemainingPosting"]   )    - intval( $NoOfPosting );

     $userusage["MaxCostRemaining"]  = floatval($userusage["MaxCostRemaining"]   )  - floatval($TotalCost); 
     

     $userUsage =   json_decode(  json_encode(   $userusage ) ,FALSE); 
 
      // var_dump(   getKeyVal($userUsage, "MaxCostRemaining")      ) ; 
      editUserUsageCommon( $userUsage,  $userInfo["Id"]);


       return $mysqli->insert_id;
    }
    else{
        return false;
    } 
}


function isValidCampaign($app,  $userInfo  ){


    $reqParam = getJsonRequest($app); 
  
// check if user is influencer. got right to post caampagin
// check user usage against noposting , infleuncer and TotalCost

// check fromdate and todate. max is 3 months for now.
//  -current date <=fromDate --> error :campaign not started yet
//  - currentdate >= toDate--> errro : campaign expired already
//  - toDate- fromDate> 3months>error : 
 
 //ensure not empty 
    $Name  = getKeyVal($reqParam, "Name");
    $Description  = getKeyVal($reqParam, "Description");
    $NoOfInfluencer  = getKeyVal($reqParam, "NoOfInfluencer");
    $NoOfPosting  = getKeyVal($reqParam, "NoOfPosting");
    $TotalCost  = getKeyVal($reqParam, "TotalCost");  
    $currentDate= date('Y-m-d H:i:s', strtotime('-0 months'));   
    $FromDate  = getKeyVal($reqParam, "FromDate");
    $ToDate  = getKeyVal($reqParam, "ToDate");

    checkEmpty($app,"Name", $Name) ;
    checkEmpty($app,"Description", $Description) ;
    checkEmpty($app,"NoOfInfluencer", $NoOfInfluencer) ;
    checkEmpty($app,"NoOfPosting", $NoOfPosting) ;
    checkEmpty($app,"TotalCost", $TotalCost) ;
    checkEmpty($app,"FromDate", $FromDate) ;
    checkEmpty($app,"ToDate", $ToDate)  ;

  // $userInfo = json_encode($userInfo, true);
 
 // ensure user is merchant or brands USERTYPE --> allowed to post
    if(!isComparedEqual($userInfo["UserType"]["Code"],"MERCHANT")  ){ 
        return processErrorMessage($app, "POSTING:NO-AUTH:NOT USER MERCHANT", "002");
    } 

   //ensure user has QUOTA to post   
   if(  ($userInfo["UserUsage"]["RemainingAllowedCampaign"]  )<1  ){ 
        return processErrorMessage($app, "POSTING:OVER-QUOTA:RemainingAllowedCampaign", "002");
    } 
   if( intval($userInfo["UserUsage"]["RemainingInfluencer"]  )<  intval( $NoOfInfluencer ) ){ 
        return processErrorMessage($app, "POSTING:OVER-QUOTA:RemainingInfluencer", "002");
    }
   if(  intval($userInfo["UserUsage"]["RemainingPosting"]  )<= $NoOfPosting  ){ 
        return processErrorMessage($app, "POSTING:OVER-QUOTA:RemainingPosting", "002");
    }
   if(  intval($userInfo["UserUsage"]["MaxCostRemaining"]  )<= $TotalCost  ){ 
        return processErrorMessage($app, "POSTING:OVER-QUOTA:MaxCostRemaining", "002");
    }

   //ensure user DATE VALIDITY :   MaxAllowedMonth GAP  
   if( $FromDate<= $currentDate  ){ 
        return processErrorMessage($app, "POSTING:DATE:FromDate must be today onwards", "002");
    } 
   if( $ToDate<= $currentDate  ){ 
        return processErrorMessage($app, "POSTING:DATE:ToDate must be today onwards", "002");
    }
   //ensure user DATE VALIDITY :   MaxAllowedMonth GAP   
   // // 
   if( getMonthDiff($FromDate,$ToDate) <0   ){ 
    return processErrorMessage($app, "POSTING:DATE:Duration of month between FromDate - Todate < 0", "002");
    }    
    // if( getMonthDiff($FromDate,$ToDate) > intval(  ($userInfo["UserType"]["MaxAllowedMonth"] ) ) ){ 
    //  return processErrorMessage($app, "POSTING:DATE:Duration of Date more than MaxAllowedMonth Gap", "002");
    // }

    return true;
}


function addCampaignRec($app ){
  
    $reqParam = getJsonRequest($app); 
    $userInfo = getUserSessionInfo($app) ;  

    //check duplocate
    $filterCampaign = new StdClass;
    $filterCampaign->Name = getKeyVal($reqParam, "Name");
    $filterCampaign->Description =getKeyVal($reqParam, "Description");  

    $data =getCampaign($filterCampaign ) ;  
    

    if( !empty($data[0] ) ){ 
        processErrorMessage($app, "DUPLICATE", "001"); 
    } 
 
    //check legit parameter
      if (isValidCampaign($app,  $userInfo )){

           $campaignId= addCampaign($app,  $userInfo ); 
            $result = array( 
                "campaignId" => $campaignId 
            );
              getJsonResponse($app,$result); 
      }
    
   


}

function editCampaignValue ($reqParam , $campaignId    ){ 

    $sqlStatement   = " Update  Campaign  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $campaignId . "' ;"; 
    
  

 
    $Charges1 = getKeyVal($reqParam, "Charges1");
    if (!empty($Charges1)) {
        $setStatement = $setStatement . " Charges1='" . $Charges1 . "' ,";
    }
    $Charges1Desc = getKeyVal($reqParam, "Charges1Desc");
    if (!empty($Charges1Desc)) {
        $setStatement = $setStatement . " Charges1Desc='" . $Charges1Desc . "' ,";
    }

     $Charges2 = getKeyVal($reqParam, "Charges2");
    if (!empty($Charges2)) {
        $setStatement = $setStatement . " Charges2='" . $Charges2 . "' ,";
    }
    $Charges2Desc = getKeyVal($reqParam, "Charges2Desc");
    if (!empty($Charges2Desc)) {
        $setStatement = $setStatement . " Charges2Desc='" . $Charges2Desc . "' ,";
    }
    $Charges3 = getKeyVal($reqParam, "Charges3");
    if (!empty($Charges3)) {
        $setStatement = $setStatement . " Charges3='" . $Charges3 . "' ,";
    }
    $Charges3Desc = getKeyVal($reqParam, "Charges3Desc");
    if (!empty($Charges3Desc)) {
        $setStatement = $setStatement . " Charges3Desc='" . $Charges3Desc . "' ,";
    } 
   
    $NoOfInfluencer = getKeyVal($reqParam, "NoOfInfluencer");
    if (!empty($NoOfInfluencer)) {
        $setStatement = $setStatement . " NoOfInfluencer='" . $NoOfInfluencer . "' ,";
    } 
    $NoOfPosting = getKeyVal($reqParam, "NoOfPosting");
    if (!empty($NoOfPosting)) {
        $setStatement = $setStatement . " NoOfPosting='" . $NoOfPosting . "' ,";
    } 
    $PhotoIdList = getKeyVal($reqParam, "PhotoIdList");
    if (!empty($PhotoIdList)) {
        $setStatement = $setStatement . " PhotoIdList='" . $PhotoIdList . "' ,";
    } 
   
    $TagIdList = getKeyVal($reqParam, "TagIdList");
    if (!empty($TagIdList)) {
        $setStatement = $setStatement . " TagIdList='" . $TagIdList . "' ,";
    } 
   
      $Price = getKeyVal($reqParam, "Price");
    if (!empty($Price)) {
        $setStatement = $setStatement . " Price='" . $Price . "' ,";
    }
    $TotalCost = getKeyVal($reqParam, "TotalCost");
    if (!empty($TotalCost)) {
        $setStatement = $setStatement . " TotalCost='" . $TotalCost . "' ,";
    } 
    $IsActive = getKeyVal($reqParam, "IsActive");
    if (!empty($IsActive)) {
        $setStatement = $setStatement . " IsActive='" . $IsActive . "' ,";
    }else { 
        $setStatement = $setStatement . " IsActive='0' ,"; 
    } 

    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate ";
    return crudDB($sqlStatement . $setStatement . $whereStatement); 
    // var_dump(  $sqlStatement . $setStatement . $whereStatement); 
 }

function editCampaign ($app , $campaignId, $userInfo    ){
     $reqParam       = getJsonRequest($app);
    $sqlStatement   = " Update  Campaign  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $campaignId . "' ;";

  
    
    $Name = getKeyVal($reqParam, "Name");
    if (!empty($Name)) {
        $setStatement = $setStatement . " Name='" . $Name . "' ,";
    }
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $setStatement = $setStatement . " Description='" . $Description . "' ,";
    }
 
 
     $NoOfInfluencer = getKeyVal($reqParam, "NoOfInfluencer");
    if (!empty($NoOfInfluencer)) {
        $setStatement = $setStatement . " NoOfInfluencer='" . $NoOfInfluencer . "' ,";
    } 
    $NoOfPosting = getKeyVal($reqParam, "NoOfPosting");
    if (!empty($NoOfPosting)) {
        $setStatement = $setStatement . " NoOfPosting='" . $NoOfPosting . "' ,";
    } 
    $PhotoIdList = getKeyVal($reqParam, "PhotoIdList");
    if (!empty($PhotoIdList)) {
        $setStatement = $setStatement . " PhotoIdList='" . $PhotoIdList . "' ,";
    } 
     $Price = getKeyVal($reqParam, "Price");
    if (!empty($Price)) {
        $setStatement = $setStatement . " Price='" . $Price . "' ,";
    }
    $TotalCost = getKeyVal($reqParam, "TotalCost");
    if (!empty($TotalCost)) {
        $setStatement = $setStatement . " TotalCost='" . $TotalCost . "' ,";
    } 
    $TagIdList = getKeyVal($reqParam, "TagIdList");
    if (!empty($TagIdList)) {
        $setStatement = $setStatement . " TagIdList='" . $TagIdList . "' ,";
    } 
   
    $IsActive = getKeyVal($reqParam, "IsActive");
    if (!empty($IsActive)) {
        $setStatement = $setStatement . " IsActive='" . $IsActive . "' ,";
    }else{

        $setStatement = $setStatement . " IsActive='0' ,";
    }
    $FromDate = getKeyVal($reqParam, "FromDate");
    if (!empty($FromDate)) {
        $setStatement = $setStatement . " FromDate='" . $FromDate . "' ,";
    }
    $ToDate = getKeyVal($reqParam, "ToDate");
    if (!empty($ToDate)) {
        $setStatement = $setStatement . " ToDate='" . $ToDate . "' ,";
    }  
    $CreatedBy = $userInfo["Id"];
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate ";
    return crudDB($sqlStatement . $setStatement . $whereStatement); 
 }

function editCampaignRec($app , $campaignId    ){
  
    $reqParam = getJsonRequest($app); 

    $userInfo = getUserSessionInfo($app) ;  
 
      // if (isValidCampaign($app,  $userInfo )){ }

        $reqParam       = getJsonRequest($app);
        $Name  = getKeyVal($reqParam, "Name");
        $Description  = getKeyVal($reqParam, "Description"); 
        $FromDate  = getKeyVal($reqParam, "FromDate");
        $ToDate  = getKeyVal($reqParam, "ToDate");

        checkEmpty($app,"Name", $Name) ;
        checkEmpty($app,"Description", $Description) ;
        checkEmpty($app,"FromDate", $FromDate) ;
        checkEmpty($app,"ToDate", $ToDate) ;

        $campaignId= editCampaign($app, $campaignId , $userInfo ); 
        $result = array( 
        "campaignId" => $campaignId 
        );
        getJsonResponse($app,$result); 
     
}
       
function  deleteCampaignRec($app, $campaignId) 
{

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;  


    //get row of campaign 
    $filterCampaign = new StdClass;
    $filterCampaign->Id =$campaignId; 
    $campaignData = getCampaign($filterCampaign ) ; 
    $campaignData =$campaignData[0];  

    //get UserUsage
    $usageData = getUserUsage($reqParam,  $userInfo["Id"]);
    $usageData =$usageData[0]; 

    // add campaign data back into user usage
    $updUserUsage = new StdClass;
    $updUserUsage->RemainingAllowedCampaign = intval( $usageData["RemainingAllowedCampaign"]) +1 ; 
    $updUserUsage->RemainingInfluencer =intval($usageData["RemainingInfluencer"]) + intval( $campaignData["NoOfInfluencer"]) ; 
    $updUserUsage->RemainingPosting=intval($usageData["RemainingPosting"]) + intval( $campaignData["NoOfPosting"]) ; 
    $updUserUsage->MaxCostRemaining   =floatval($usageData["MaxCostRemaining"]) + floatval( $campaignData["TotalCost"]) ; 


    //update UserUsage      
    editUserUsageCommon($updUserUsage,   $userInfo["Id"]);
   

    //delete campaign record
    $sqlStatement      = "DELETE FROM Campaign WHERE Id='" . $campaignId . "'";
    $dataUser          = crudDB($sqlStatement);  

    //delete campaign running record
    $sqlStatement      = "DELETE FROM CampaignRunning WHERE CampaignId='" . $campaignId . "'";
    $dataUser          = crudDB($sqlStatement); 
    $result            = array(
        "status" => true,
    );


 
    getJsonResponse($app, $result); 
}
?>