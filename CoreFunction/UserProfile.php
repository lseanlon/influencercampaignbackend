<?php
/* 
    Entry profile 
*/
 
function getUserProfile($app, $userId)
{ 
    //$userInfo = getUserSessionInfo($app) ;
  
    $reqParam = getJsonRequest($app); 
    $reqParam->IsTempBlocked="0";
    $userRec= getCommonUserProfile($reqParam, $userId,null);
    getJsonResponse($app, $userRec ); 
}


function getAllUserProfile($app ){
  
    $reqParam = getJsonRequest($app); 

     $filterParam = new StdClass;
    $filterParam->IsFeatureUnlockedInfluencer= "1";
    $userRec  = getUserFilter($filterParam );  
    getJsonResponse($app, $userRec ); 
}


function getCurrentUserProfile($app )
{
    
    $userRec = getUserSessionInfo($app) ;  
    getJsonResponse($app, $userRec ); 
}


function addUser($reqParam)
{
    if(empty( $reqParam)){ 
         $reqParam = new StdClass;
    }
    
    $photoId   = 0;
    $UserPhoto = getKeyVal($reqParam, "UserPhoto");
    if (!empty($UserPhoto)) {
        $photoId = addPhoto($UserPhoto); 
    }
    

    $sqlStatement   = "INSERT INTO  User (  
                `UserLoginName`,
                `GoogleLoginName`,
                `FbLoginName`,   
                `failedLoginAttempt`  ,
                `isTempBlocked`   ,
                `isActive`   ,
                `lastFailedLogin`     , 
                `lastSuccessfulLogin`   ,  
                `VirtualAmount`     , 
                `VirtualAmountCheckoutDate`     , 
                `SessionKey`, 
                `LoginType`, 
                `Languange`   ,
                `Location`   ,
                `Country`   ,
                `Summary`   ,
                `PreferredBankNo`   ,   
                `PreferredBankName`   ,   
                `PreferredAccNo`   ,   
                `PreferredAccName`   ,   
                `Password`,
                `Name`,
                `Email`,
                `Mobile`,
                `Dob`,
                `Profession`,
                `Gender`,
                `Designation`,
                `CompanyName`,
                `CompanyNature`,
                `CompanyNo`,
                `CompanyLocation`,
                `UserTypeId`,
                `AccountTypeId`,
                `PhotoId`, 
                `IsFeatureUnlockedInfluencer`,  
                `CreatedBy`     ,
                `CreatedDate`   
            )  ";


    $valueStatement = "VALUES (";
    $UserLoginName  = getKeyVal($reqParam, "UserLoginName");
    if (!empty($UserLoginName)) {
        $valueStatement = $valueStatement . "'" . $UserLoginName . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $GoogleLoginName = getKeyVal($reqParam, "GoogleLoginName");
    if (!empty($GoogleLoginName)) {
        $valueStatement = $valueStatement . "'" . $GoogleLoginName . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $FbLoginName = getKeyVal($reqParam, "FbLoginName");
    if (!empty($FbLoginName)) {
        $valueStatement = $valueStatement . "'" . $FbLoginName . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    } 
    $failedLoginAttempt = getKeyVal($reqParam, "failedLoginAttempt");
    if (!empty($failedLoginAttempt)) {
        $valueStatement = $valueStatement . "'" . $failedLoginAttempt . "',";
    }else{ 
        $valueStatement = $valueStatement . "0,";
    }
    $isTempBlocked = getKeyVal($reqParam, "isTempBlocked");
    if (!empty($isTempBlocked)) {
        $valueStatement = $valueStatement . "'" . $isTempBlocked . "',";
    }else{ 
        $valueStatement = $valueStatement . "'0',";
    }

    $isActive = "00";
    if (!empty($isActive)) {
        $valueStatement = $valueStatement . "'" . $isActive . "',";
    }else{ 
        $valueStatement = $valueStatement . "'0',";
    }

    $lastFailedLogin = getKeyVal($reqParam, "lastFailedLogin");
    if (!empty($lastFailedLogin)) {
        $valueStatement = $valueStatement . "'" . $lastFailedLogin . "',";
    }else{ 
        $valueStatement = $valueStatement . "NULL,";
    }
    $lastSuccessfulLogin = getKeyVal($reqParam, "lastSuccessfulLogin");
    if (!empty($lastSuccessfulLogin)) {
        $valueStatement = $valueStatement . "'" . $lastSuccessfulLogin . "',";
    }else{ 
        $valueStatement = $valueStatement . "NULL,";
    }

    $VirtualAmount = getKeyVal($reqParam, "VirtualAmount");
    if (!empty($VirtualAmount)) {
        $valueStatement = $valueStatement . "'" . $VirtualAmount . "',";
    }else{ 
        $valueStatement = $valueStatement . "NULL,";
    }
    $VirtualAmountCheckoutDate = getKeyVal($reqParam, "VirtualAmountCheckoutDate");
    if (!empty($VirtualAmountCheckoutDate)) {
        $valueStatement = $valueStatement . "'" . $VirtualAmountCheckoutDate . "',";
    }else{ 
        $valueStatement = $valueStatement . "NULL,";
    }

    $SessionKey = getKeyVal($reqParam, "SessionKey");
    if (!empty($SessionKey)) {
        $valueStatement = $valueStatement . "'" . $SessionKey . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    $LoginType = getKeyVal($reqParam, "LoginType");
    if (!empty($LoginType)) {
        $valueStatement = $valueStatement . "'" . $LoginType . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Languange = getKeyVal($reqParam, "Languange");
    if (!empty($Languange)) {
        $valueStatement = $valueStatement . "'" . $Languange . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    

    $Location = getKeyVal($reqParam, "Location");
    if (!empty($Location)) {
        $valueStatement = $valueStatement . "'" . $Location . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    

    $Country = getKeyVal($reqParam, "Country");
    if (!empty($Country)) {
        $valueStatement = $valueStatement . "'" . $Country . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    

    $Summary = getKeyVal($reqParam, "Summary");
    if (!empty($Summary)) {
        $valueStatement = $valueStatement . "'" . $Summary . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    

    $PreferredBankNo = getKeyVal($reqParam, "PreferredBankNo");
    if (!empty($PreferredBankNo)) {
        $valueStatement = $valueStatement . "'" . $PreferredBankNo . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    

    $PreferredBankName = getKeyVal($reqParam, "PreferredBankName");
    if (!empty($PreferredBankName)) {
        $valueStatement = $valueStatement . "'" . $PreferredBankName . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    

    $PreferredAccNo = getKeyVal($reqParam, "PreferredAccNo");
    if (!empty($PreferredAccNo)) {
        $valueStatement = $valueStatement . "'" . $PreferredAccNo . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    

    $PreferredAccName = getKeyVal($reqParam, "PreferredAccName");
    if (!empty($PreferredAccName)) {
        $valueStatement = $valueStatement . "'" . $PreferredAccName . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
  
 
    $Password = getKeyVal($reqParam, "Password");
    if (!empty($Password)) {
        $valueStatement = $valueStatement . "'" . $Password . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
     
    $Name = getKeyVal($reqParam, "Name");
    if (!empty($Name)) {
        $valueStatement = $valueStatement . "'" . $Name . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Email = getKeyVal($reqParam, "Email");
    if (!empty($Email)) {
        $valueStatement = $valueStatement . "'" . $Email . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Mobile = getKeyVal($reqParam, "Mobile");
    if (!empty($Mobile)) {
        $valueStatement = $valueStatement . "'" . $Mobile . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    
    
    $Dob = getKeyVal($reqParam, "Dob");
    if (!empty($Dob)) {
        $valueStatement = $valueStatement . "'" . $Dob . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Profession = getKeyVal($reqParam, "Profession");
    if (!empty($Profession)) {
        $valueStatement = $valueStatement . "'" . $Profession . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Gender = getKeyVal($reqParam, "Gender");
    if (!empty($Gender)) {
        $valueStatement = $valueStatement . "'" . $Gender . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Designation = getKeyVal($reqParam, "Designation");
    if (!empty($Designation)) {
        $valueStatement = $valueStatement . "'" . $Designation . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $CompanyName = getKeyVal($reqParam, "CompanyName");
    if (!empty($CompanyName)) {
        $valueStatement = $valueStatement . "'" . $CompanyName . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $CompanyNature = getKeyVal($reqParam, "CompanyNature");
    if (!empty($CompanyNature)) {
        $valueStatement = $valueStatement . "'" . $CompanyNature . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    $CompanyNo = getKeyVal($reqParam, "CompanyNo");
    if (!empty($CompanyNo)) {
        $valueStatement = $valueStatement . "'" . $CompanyNo . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    
    $CompanyLocation = getKeyVal($reqParam, "CompanyLocation");
    if (!empty($CompanyLocation)) {
        $valueStatement = $valueStatement . "'" . $CompanyLocation . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $UserTypeId = getKeyVal($reqParam, "UserTypeId");
    if (!empty($UserTypeId)) {
        $valueStatement = $valueStatement . "'" . $UserTypeId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $AccountTypeId = getKeyVal($reqParam, "AccountTypeId");
    if (!empty($AccountTypeId)) {
        $valueStatement = $valueStatement . "'" . $AccountTypeId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'1',";
    }
    
    $valueStatement = $valueStatement . "'" . $photoId . "', ";

    //IsFeatureUnlockedInfluencer
    $valueStatement = $valueStatement . "'0', ";
     
    
    //createdby
    $valueStatement = $valueStatement . "'APIv1' ,";
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";
    // var_export( $sqlStatement . $valueStatement ); 
    $mysqli         = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
     
}




function addUserTag($reqParam, $userId)
{     

    if(empty( $reqParam )){  
         $reqParam = new StdClass;
    }

    $userTags = getKeyVal($reqParam, "UserTag");  
    
     if(empty( $userTags )){ 
         $userTags = [];
    }
    if(is_string( $userTags )){ 
         $userTags = explode( "," ,  $userTags  );   
    }


     


        $sqlStatement = "INSERT INTO  UserTag ( 
                            `UserId` ,
                            `InterestTagId`  , 
                            `IsInfluencer`    
                        )  ";
        
        
        $valueStatement = "VALUES";
        
        foreach ($userTags as $key => $value) {
            if ($key == 0) {
                $valueStatement = $valueStatement . "  ( '" . $userId . "',";
            } else {
                $valueStatement = $valueStatement . " ,( '" . $userId . "',";
            }
            
            if (!empty($value)) {
                $valueStatement = $valueStatement . " '" . $value . "',";
            }else { 
                $valueStatement = $valueStatement . " '',";
            }
            
            
            $isInfluencer = getKeyVal($reqParam, "isInfluencer");
            if (empty($isInfluencer)) {
                $valueStatement = $valueStatement . "'0' ";
            } else {
                $valueStatement = $valueStatement . "'" . $isInfluencer . "' ";
            }
            
            
            $valueStatement = $valueStatement . " )";
        }
        
        $mysqli = crudDB($sqlStatement . $valueStatement);
        return $mysqli->insert_id;
    
    
}

function addUserInfluence($reqParam, $userId)
{
    
     
    $reqParam = getKeyVal($reqParam, "UserInfluence");
    if(empty( $reqParam)){ 
         $reqParam = new StdClass;
    }

    $sqlStatement = "INSERT INTO  UserInfluence ( 
                        `UserId` ,
                        `IsBanned`  , 
                        `TotalSuccessfulCampaign`   ,
                        `TotalFailedCampaign`   , 
                        `TotalFollowerFb`   , 
                        `TotalFollowerInstagram` , 
                        `TotalFollowerTwitter`  ,   
                        `TotalFollowerVine`  ,  

                        `InstagramTotalMedia`   ,  
                        `InstagramTotalSubscribes`   ,  

                        `PageAuthority`   ,  
                        `PageRank`   ,  
                        `ReplyRatio`   ,  
                        `AverageShared`   ,  
                        `CreatedBy`   ,   
                        `CreatedDate`      
                    )  ";
    
    $valueStatement = "VALUES (";
    if (!empty($userId)) {
        $valueStatement = $valueStatement . "'" . $userId . "',";
    }
    
    $IsBanned = getKeyVal($reqParam, "IsBanned");
    if (!empty($IsBanned)) {
        $valueStatement = $valueStatement . "'" . $IsBanned . "',";
    } else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $TotalSuccessfulCampaign = getKeyVal($reqParam, "TotalSuccessfulCampaign");
    if (!empty($TotalSuccessfulCampaign)) {
        $valueStatement = $valueStatement . "'" . $TotalSuccessfulCampaign . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $TotalFailedCampaign = getKeyVal($reqParam, "TotalFailedCampaign");
    if (!empty($TotalFailedCampaign)) {
        $valueStatement = $valueStatement . "'" . $TotalFailedCampaign . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $TotalFollowerFb = getKeyVal($reqParam, "TotalFollowerFb");
    if (!empty($TotalFollowerFb)) {
        $valueStatement = $valueStatement . "'" . $TotalFollowerFb . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $TotalFollowerInstagram = getKeyVal($reqParam, "TotalFollowerInstagram");
    if (!empty($TotalFollowerInstagram)) {
        $valueStatement = $valueStatement . "'" . $TotalFollowerInstagram . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $TotalFollowerTwitter = getKeyVal($reqParam, "TotalFollowerTwitter");
    if (!empty($TotalFollowerTwitter)) {
        $valueStatement = $valueStatement . "'" . $TotalFollowerTwitter . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $TotalFollowerVine = getKeyVal($reqParam, "TotalFollowerVine");
    if (!empty($TotalFollowerVine)) {
        $valueStatement = $valueStatement . "'" . $TotalFollowerVine . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $InstagramTotalMedia = getKeyVal($reqParam, "InstagramTotalMedia");
    if (!empty($InstagramTotalMedia)) {
        $valueStatement = $valueStatement . "'" . $InstagramTotalMedia . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $InstagramTotalSubscribes = getKeyVal($reqParam, "InstagramTotalSubscribes");
    if (!empty($InstagramTotalSubscribes)) {
        $valueStatement = $valueStatement . "'" . $InstagramTotalSubscribes . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $PageAuthority = getKeyVal($reqParam, "PageAuthority");
    if (!empty($PageAuthority)) {
        $valueStatement = $valueStatement . "'" . $PageAuthority . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $PageRank = getKeyVal($reqParam, "PageRank");
    if (!empty($PageRank)) {
        $valueStatement = $valueStatement . "'" . $PageRank . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $ReplyRatio = getKeyVal($reqParam, "ReplyRatio");
    if (!empty($ReplyRatio)) {
        $valueStatement = $valueStatement . "'" . $ReplyRatio . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    
    $AverageShared = getKeyVal($reqParam, "AverageShared");
    if (!empty($AverageShared)) {
        $valueStatement = $valueStatement . "'" . $AverageShared . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    //createdby
    $valueStatement = $valueStatement . "'APIv1' , ";
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";
    
    $mysqli = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
}


function addUserPayment($reqParam, $userId)
{
     
    $reqParam = getKeyVal($reqParam, "UserPayment");
    if(empty( $reqParam)){ 
         $reqParam = new StdClass;
    }
    
    $sqlStatement = "INSERT INTO  UserPayment ( 
                        `UserId`  , 
                        `PaymentAccNo`   ,
                        `PaymentHolderName`   , 
                        `PaymentExpiryDate`   , 
                        `PaymentSecurityCode` , 
                        `PaymentIssuerName`  ,   
                        `PaymentIssuerCategory`  ,  
                        `PaymentType`   ,  
                        `CreatedBy`   ,   
                        `CreatedDate`       
                    )  ";
    
    $valueStatement = "VALUES (";
    if (!empty($userId)) {
        $valueStatement = $valueStatement . "'" . $userId . "',";
    }else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $PaymentAccNo = getKeyVal($reqParam, "PaymentAccNo");
    if (!empty($PaymentAccNo)) {
        $valueStatement = $valueStatement . "'" . $PaymentAccNo . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    $PaymentHolderName = getKeyVal($reqParam, "PaymentHolderName");
    if (!empty($PaymentHolderName)) {
        $valueStatement = $valueStatement . "'" . $PaymentHolderName . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    $PaymentExpiryDate = getKeyVal($reqParam, "PaymentExpiryDate");
    if (!empty($PaymentExpiryDate)) {
        $valueStatement = $valueStatement . "'" . $PaymentExpiryDate . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    $PaymentSecurityCode = getKeyVal($reqParam, "PaymentSecurityCode");
    if (!empty($PaymentSecurityCode)) {
        $valueStatement = $valueStatement . "'" . $PaymentSecurityCode . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    $PaymentIssuerName = getKeyVal($reqParam, "PaymentIssuerName");
    if (!empty($PaymentIssuerName)) {
        $valueStatement = $valueStatement . "'" . $PaymentIssuerName . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    $PaymentIssuerCategory = getKeyVal($reqParam, "PaymentIssuerCategory");
    if (!empty($PaymentIssuerCategory)) {
        $valueStatement = $valueStatement . "'" . $PaymentIssuerCategory . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    $PaymentType = getKeyVal($reqParam, "PaymentType");
    if (!empty($PaymentType)) {
        $valueStatement = $valueStatement . "'" . $PaymentType . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    //createdby
    $valueStatement = $valueStatement . "'APIv1' , ";
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";
    
    $mysqli = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
}

 

function addUserUsage($reqParam, $userId)
{
    if(empty( $reqParam)){ 
         $reqParam = new StdClass;
    }
    $AccountTypeId = getKeyVal($reqParam, "AccountTypeId"); 
	$accountTypeInfo = getAccountType($AccountTypeId );
	$accountTypeInfo = $accountTypeInfo[0];
    
    $sqlStatement = "INSERT INTO  UserUsage (  
						`UserId`   , 
						`Price` ,  
						`RemainingAllowedCampaign`  , 
						`RemainingInfluencer`  , 
						`RemainingPosting`  , 
						`MaxCostRemaining`  ,  
						`CreatedBy`   ,   
						`CreatedDate`  
                    )  ";
    
    $valueStatement = "VALUES (";
    if (!empty($userId)) {
        $valueStatement = $valueStatement . "'" . $userId . "',";
    }else { 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Price =  $accountTypeInfo["Price"];
    if (!empty($Price)) {
        $valueStatement = $valueStatement . "'" . $Price . "',";
    } else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $MaxAllowedCampaign =  $accountTypeInfo["MaxAllowedCampaign"];
    if (!empty($MaxAllowedCampaign)) {
        $valueStatement = $valueStatement . "'" . $MaxAllowedCampaign . "',";
    } else { 
        $valueStatement = $valueStatement . "'0',";
    }
    $MaxInfluencer =  $accountTypeInfo["MaxInfluencer"];
    if (!empty($MaxInfluencer)) {
        $valueStatement = $valueStatement . "'" . $MaxInfluencer . "',";
    } else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $MaxPosting =  $accountTypeInfo["MaxPosting"];
    if (!empty($MaxPosting)) {
        $valueStatement = $valueStatement . "'" . $MaxPosting . "',";
    } else { 
        $valueStatement = $valueStatement . "'0',";
    }
    
    $MaxCost =  $accountTypeInfo["MaxCost"];
    if (!empty($MaxCost)) {
        $valueStatement = $valueStatement . "'" . $MaxCost . "',";
    }  else { 
        $valueStatement = $valueStatement . "'0',";
    }

    //createdby
    $valueStatement = $valueStatement . "'APIv1' , ";
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";
    
    $mysqli = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id; 

}



function addUserCommon($reqParam)
{     
    $userId          = addUser($reqParam);
    //INSERT USER TAG  
    $userTagId       = addUserTag($reqParam, $userId);
    // //INSERT USER INFLUENCE  
    $userInfluenceId = addUserInfluence($reqParam, $userId);
    // //INSERT USER PAYMENT
    $userPaymentId   = addUserPayment($reqParam, $userId);
    //INSERT DEFAUL USER USAGE
    $userUsageId   = addUserUsage($reqParam, $userId);
    
    
        $Name = getKeyVal($reqParam, "Name");  
        $Email = getKeyVal($reqParam, "Email"); 
        $UserName = getKeyVal($reqParam, "UserLoginName"); 
        if(empty($UserName)){ 
            $UserName = getKeyVal($reqParam, "GoogleLoginName"); 
        }
        if(empty($UserName)){ 
            $UserName = getKeyVal($reqParam, "FbLoginName"); 
        }
    
        $bodyText="
             Hi $Name ,\n We have successfull registered your username: $UserName  with this email: $Email.\n 
              You should be able to gain access for this platform with your acccount once the review process is done. Review process is usually done within 3 working days.\n
              If you didnt register for this, please ignore otherwise and report to us immediately.
 
        ";
        $bodyHtml="
            <p>
             Hi $Name ,<br/> We have successfull registered your username:$UserName  with this email: $Email.</p>
              <p> You should be able to gain access for this platform with your acccount once the review process is done. Review process is usually done within 3 working days.</p>
             <p> If you didnt register for this, please ignore otherwise and report to us immediately..</p>

        ";
         $emailArr = array(
            "recipientEmail" =>   $Email,
            "recipientName" =>   $Name,
            "subject" => "Influencer Account Registration",
            "bodyHtml" => $bodyHtml,
            "bodyText" => $bodyText
        );
  
        $emailResult= sendEMailMessage($emailArr); 

        $result = array( 
        "userId" => $userId,
        "userTagId" => $userTagId,
        "userInfluenceId" => $userInfluenceId,
        "userUsageId" => $userUsageId,
        "userPaymentId" => $userPaymentId,
        "success_msg" => "Successfuly registered you as user!",
        "status" => true
    );
    
    
    return $result ;
}
  
function addUserProfile($app)
{
    
    $reqParam        = getJsonRequest($app);
 

    //check user dont exist
    $UserLoginName   = getKeyVal($reqParam, "UserLoginName");
    $GoogleLoginName = getKeyVal($reqParam, "GoogleLoginName");
    $FbLoginName     = getKeyVal($reqParam, "FbLoginName");
    $Name            = getKeyVal($reqParam, "Name");
    $LoginType       = getKeyVal($reqParam, "LoginType");
    $Email           = getKeyVal($reqParam, "Email");
    $Password        = getKeyVal($reqParam, "Password");
    $UserTypeId      = getKeyVal($reqParam, "UserTypeId");
    $Mobile          = getKeyVal($reqParam, "Mobile");

    if (   empty($LoginType)  || isComparedEqual( $LoginType,"NORMAL")    ) {
     

            checkEmpty($app,"Name", $Name) ;
            checkEmpty($app,"Login Name", $UserLoginName) ;
            checkEmpty($app,"Email", $Email) ;
            checkEmpty($app,"Password", $Password) ;
            checkEmpty($app,"UserTypeId", $UserTypeId) ;
            checkEmpty($app,"Mobile", $Mobile) ;


            //Check duplicate 
            $UserLoginName   = getUser($reqParam, $UserLoginName,null);
            $GoogleLoginName = getUser($reqParam, $GoogleLoginName,null);
            $FbLoginName     = getUser($reqParam, $FbLoginName,null);
            //duplicate - same user type
             if (!empty($UserLoginName) ) {
                return processErrorMessage($app, "DUPLICATE: USER EXISTED.Please choose another username", "001");
            }

            //duplicate - another user type
            if (!empty($UserLoginName) && !empty($GoogleLoginName) && !empty($FbLoginName)) {
                return processErrorMessage($app, "DUPLICATE: USER EXISTED.Please choose another username", "001A");
            }
 
         //duplicate - email existed 
            $filterParam = new StdClass;
            $filterParam->Email=   $Email ;
             $resultEmail = getUserFilter($filterParam  ); 
            if (!empty($resultEmail)  ||  !empty($resultEmail[0]) ) {
             return processErrorMessage($app, "DUPLICATE: USER EMAIL EXISTED.Please choose another email", "001A");
            }
    }
 
    

    $result = addUserCommon($reqParam) ;
    getJsonResponse($app, $result); 
    
}

function editUser($reqParam, $userId,$userPhotoId)
{ 
    return editUserCommon($reqParam, $userId,$userPhotoId) ;

    
}
function editUserPayment($reqParam, $userId)
{
    
     
    $reqParam = getKeyVal($reqParam, "UserPayment");

    if( empty( $reqParam)){
    		return false;
    }
 


    $sqlStatement   = " Update  UserPayment  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE UserId='" . $userId . "' ;";
    
    
    $PaymentAccNo = getKeyVal($reqParam, "PaymentAccNo");
    if (!empty($PaymentAccNo)) {
        $setStatement = $setStatement . " PaymentAccNo='" . $PaymentAccNo . "' ,";
    }
    
    
    $PaymentHolderName = getKeyVal($reqParam, "PaymentHolderName");
    if (!empty($PaymentHolderName)) {
        $setStatement = $setStatement . " PaymentHolderName='" . $PaymentHolderName . "' ,";
    }
    
    $PaymentExpiryDate = getKeyVal($reqParam, "PaymentExpiryDate");
    if (!empty($PaymentExpiryDate)) {
        $setStatement = $setStatement . " PaymentExpiryDate='" . $PaymentExpiryDate . "' ,";
    }
    $PaymentSecurityCode = getKeyVal($reqParam, "PaymentSecurityCode");
    if (!empty($PaymentSecurityCode)) {
        $setStatement = $setStatement . " PaymentSecurityCode='" . $PaymentSecurityCode . "' ,";
    }
    $PaymentIssuerName = getKeyVal($reqParam, "PaymentIssuerName");
    if (!empty($PaymentIssuerName)) {
        $setStatement = $setStatement . " PaymentIssuerName='" . $PaymentIssuerName . "' ,";
    }
    $PaymentIssuerCategory = getKeyVal($reqParam, "PaymentIssuerCategory");
    if (!empty($PaymentIssuerCategory)) {
        $setStatement = $setStatement . " PaymentIssuerCategory='" . $PaymentIssuerCategory . "' ,";
    }
    $PaymentType = getKeyVal($reqParam, "PaymentType");
    if (!empty($PaymentType)) {
        $setStatement = $setStatement . " PaymentType='" . $PaymentType . "' ,";
    }
    $CreatedBy = $userId;
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . "  ,";
    }
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate ";
    return crudDB($sqlStatement . $setStatement . $whereStatement); 
    
}
 
 
function editUserPhoto($reqParam, $userId)
{
      
    $UserPhoto = getKeyVal($reqParam, "UserPhoto");

    if( !empty($UserPhoto)){
   	 return addPhoto( $UserPhoto);
   		
    }
	return false;  
    
    
}
function editUserTag($reqParam, $userId)
{ 
    $userTags = getKeyVal($reqParam, "UserTag");
    if(!empty($userTags)){

	    $sqlStatement      = "DELETE FROM UserTag WHERE UserId='" . $userId . "'";
	    $dataUser          = crudDB($sqlStatement);
		return addUserTag($reqParam, $userId);

    }
    return false;
    
    
}
function editUserInfluence($reqParam, $userId)
{
     
    $reqParam = getKeyVal($reqParam, "UserInfluence");
     
    if( empty($reqParam)){
    	return false;
    }
    $sqlStatement   = " Update  UserInfluence ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE UserId='" . $userId . "' ;";
    
   
    $IsFbConnected = getKeyVal($reqParam, "IsFbConnected");
    if (!empty($IsFbConnected)) {
        $setStatement = $setStatement . " IsFbConnected='" . $IsFbConnected . "' ,";
    } 
    $IsFbPage = getKeyVal($reqParam, "IsFbPage");
    if (!empty($IsFbPage)) {
        $setStatement = $setStatement . " IsFbPage='" . $IsFbPage . "' ,";
    }  

    $InstagramTotalSubscribes = getKeyVal($reqParam, "InstagramTotalSubscribes");
    if (!empty($InstagramTotalSubscribes)) {
        $setStatement = $setStatement . " InstagramTotalSubscribes='" . $InstagramTotalSubscribes . "' ,";
    } 

    $InstagramTotalMedia = getKeyVal($reqParam, "InstagramTotalMedia");
    if (!empty($InstagramTotalMedia)) {
        $setStatement = $setStatement . " InstagramTotalMedia='" . $InstagramTotalMedia . "' ,";
    }  

    $IsInstagramConnected = getKeyVal($reqParam, "IsInstagramConnected");
    if (!empty($IsInstagramConnected)) {
        $setStatement = $setStatement . " IsInstagramConnected='" . $IsInstagramConnected . "' ,";
    } 

    $IsTwitterConnected = getKeyVal($reqParam, "IsTwitterConnected");
    if (!empty($IsTwitterConnected)) {
        $setStatement = $setStatement . " IsTwitterConnected='" . $IsTwitterConnected . "' ,";
    } 
    $IsGoogleConnected = getKeyVal($reqParam, "IsGoogleConnected");
    if (!empty($IsGoogleConnected)) {
        $setStatement = $setStatement . " IsGoogleConnected='" . $IsGoogleConnected . "' ,";
    } 
    $IsVineConnected = getKeyVal($reqParam, "IsVineConnected");
    if (!empty($IsVineConnected)) {
        $setStatement = $setStatement . " IsVineConnected='" . $IsVineConnected . "' ,";
    } 
    $IsBanned = getKeyVal($reqParam, "IsBanned");
    if (!empty($IsBanned)) {
        $setStatement = $setStatement . " IsBanned='" . $IsBanned . "' ,";
    } 
    
    $TotalAmountCollected = getKeyVal($reqParam, "TotalAmountCollected");
    if (!empty($TotalAmountCollected)) {
        $setStatement = $setStatement . " TotalAmountCollected='" . $TotalAmountCollected . "' ,";
    }
    $TotalRunningCampaign = getKeyVal($reqParam, "TotalRunningCampaign");
    if (!empty($TotalRunningCampaign)) {
        $setStatement = $setStatement . " TotalRunningCampaign='" . $TotalRunningCampaign . "' ,";
    }

    $FacebookTotalPostReactions = getKeyVal($reqParam, "FacebookTotalPostReactions");
    if (!empty($FacebookTotalPostReactions)) {
        $setStatement = $setStatement . " FacebookTotalPostReactions='" . $FacebookTotalPostReactions . "' ,";
    }    
    $FacebookTotalPostShares = getKeyVal($reqParam, "FacebookTotalPostShares");
    if (!empty($FacebookTotalPostShares)) {
        $setStatement = $setStatement . " FacebookTotalPostShares='" . $FacebookTotalPostShares . "' ,";
    }    
    $FacebookTotalPostComment = getKeyVal($reqParam, "FacebookTotalPostComment");
    if (!empty($FacebookTotalPostComment)) {
        $setStatement = $setStatement . " FacebookTotalPostComment='" . $FacebookTotalPostComment . "' ,";
    }    
    $FacebookTotalPostLikes = getKeyVal($reqParam, "FacebookTotalPostLikes");
    if (!empty($FacebookTotalPostLikes)) {
        $setStatement = $setStatement . " FacebookTotalPostLikes='" . $FacebookTotalPostLikes . "' ,";
    }    

    $FacebookTotalCampaign = getKeyVal($reqParam, "FacebookTotalCampaign");
    if (!empty($FacebookTotalCampaign)) {
        $setStatement = $setStatement . " FacebookTotalCampaign='" . $FacebookTotalCampaign . "' ,";
    }
    
    $TotalSuccessfulCampaign = getKeyVal($reqParam, "TotalSuccessfulCampaign");
    if (!empty($TotalSuccessfulCampaign)) {
        $setStatement = $setStatement . " TotalSuccessfulCampaign='" . $TotalSuccessfulCampaign . "' ,";
    }
    
    $TotalFailedCampaign = getKeyVal($reqParam, "TotalFailedCampaign");
    if (!empty($TotalFailedCampaign)) {
        $setStatement = $setStatement . " TotalFailedCampaign='" . $TotalFailedCampaign . "' ,";
    }
    
    $TotalFollowerFb = getKeyVal($reqParam, "TotalFollowerFb");
    if (!empty($TotalFollowerFb)) {
        $setStatement = $setStatement . " TotalFollowerFb='" . $TotalFollowerFb . "' ,";
    }

    $TotalLikesFb = getKeyVal($reqParam, "TotalLikesFb");
    if (!empty($TotalLikesFb)) {
        $setStatement = $setStatement . " TotalLikesFb='" . $TotalLikesFb . "' ,";
    }

    $TotalFriendFb = getKeyVal($reqParam, "TotalFriendFb");
    if (!empty($TotalFriendFb)) {
        $setStatement = $setStatement . " TotalFriendFb='" . $TotalFriendFb . "' ,";
    }
    
    $TotalFollowerInstagram = getKeyVal($reqParam, "TotalFollowerInstagram");
    if (!empty($TotalFollowerInstagram)) {
        $setStatement = $setStatement . " TotalFollowerInstagram='" . $TotalFollowerInstagram . "' ,";
    }
    
    $TotalFollowerTwitter = getKeyVal($reqParam, "TotalFollowerTwitter");
    if (!empty($TotalFollowerTwitter)) {
        $setStatement = $setStatement . " TotalFollowerTwitter='" . $TotalFollowerTwitter . "' ,";
    }
     
    $TotalFollowerVine = getKeyVal($reqParam, "TotalFollowerVine");
    if (!empty($TotalFollowerVine)) {
        $setStatement = $setStatement . " TotalFollowerVine='" . $TotalFollowerVine . "' ,";
    }
    
  
 
    $InstagramTotalMedia = getKeyVal($reqParam, "InstagramTotalMedia");
    if (!empty($InstagramTotalMedia)) {
        $setStatement = $setStatement . " InstagramTotalMedia='" . $InstagramTotalMedia . "' ,";
    }
    $InstagramTotalSubscribes = getKeyVal($reqParam, "InstagramTotalSubscribes");
    if (!empty($InstagramTotalSubscribes)) {
        $setStatement = $setStatement . " InstagramTotalSubscribes='" . $InstagramTotalSubscribes . "' ,";
    }
    $TotalFollowerInstagram = getKeyVal($reqParam, "TotalFollowerInstagram");
    if (!empty($TotalFollowerInstagram)) {
        $setStatement = $setStatement . " TotalFollowerInstagram='" . $TotalFollowerInstagram . "' ,";
    }

    $PageAuthority = getKeyVal($reqParam, "PageAuthority");
    if (!empty($PageAuthority)) {
        $setStatement = $setStatement . " PageAuthority='" . $PageAuthority . "' ,";
    }
    
    $PageRank = getKeyVal($reqParam, "PageRank");
    if (!empty($PageRank)) {
        $setStatement = $setStatement . " PageRank='" . $PageRank . "' ,";
    }
    
    $ReplyRatio = getKeyVal($reqParam, "ReplyRatio");
    if (!empty($ReplyRatio)) {
        $setStatement = $setStatement . " ReplyRatio='" . $ReplyRatio . "' ,";
    }
    
    
    $AverageShared = getKeyVal($reqParam, "AverageShared");
    if (!empty($AverageShared)) {
        $setStatement = $setStatement . " AverageShared='" . $AverageShared . "' ,";
    }
    
    $CreatedBy = $userId;
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate= " . $CreatedDate . "  ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate ";
    return crudDB($sqlStatement . $setStatement . $whereStatement); 
    
}
 
function editUserUsage($reqParam, $userId)
{
     
    $reqParam = getKeyVal($reqParam, "UserUsage");

    return editUserUsageCommon($reqParam, $userId); 
}
 
function editUserProfile($app, $userId)
{


    $reqParam = getJsonRequest($app);
    //Edit USER foto  
    $userPhotoId = editUserPhoto($reqParam, $userId); 

    $userResp = editUser($reqParam, $userId,$userPhotoId);  

    //Edit USER TAG  
    $userTagResp       = editUserTag($reqParam, $userId);
    //Edit USER INFLUENCE  
    $userInfluenceResp = editUserInfluence($reqParam, $userId);
    //Edit USER PAYMENT
    $userPaymentResp   = editUserPayment($reqParam, $userId);
    //Edit USER USAGE
    $userUsageResp   = editUserUsage($reqParam, $userId);
    
    $data[0]["status"] = true;
    $data[0]["user"] =  $userResp;
    $data[0]["userTag"] =  $userTagResp;
    $data[0]["userInfluence"] =  $userInfluenceResp;
    $data[0]["userPayment"] =  $userPaymentResp;
    $data[0]["userUsage"] =  $userUsageResp; 
    getJsonResponse($app, $data[0]); 
    
    
}

function deleteUserProfile($app, $userId)
{
    
    //soft delet

    $reqParam = getJsonRequest($app);  

     $filterParam = new StdClass;
    $filterParam->IsTempBlocked= "1";
    $filterParam->Id= $userId;

    $userResp = editUser($filterParam, $userId,null);  
 
 
     $result = array(
        "status" => true,
    );
    getJsonResponse($app, $result ); 
 


    // $sqlStatement      = "DELETE FROM User WHERE Id='" . $userId . "'";
    // $dataUser          = crudDB($sqlStatement);
    // $sqlStatement      = "DELETE FROM UserTag WHERE UserId='" . $userId . "'";
    // $dataUserTag       = crudDB($sqlStatement);
    // $sqlStatement      = "DELETE FROM UserInfluence WHERE UserId='" . $userId . "'";
    // $dataUserInfluence = crudDB($sqlStatement);
    // $sqlStatement      = "DELETE FROM UserPayment WHERE UserId='" . $userId . "'";
    // $dataUserPayment   = crudDB($sqlStatement);
    // $result            = array(
    //     "status" => true,
    // );


    //     // "dataUser" => $dataUser,
    //     // "dataUserTag" => $dataUserTag,
    //     // "dataUserInfluence" => $dataUserInfluence,
    //     // "dataUserPayment" => $dataUserPayment
    // getJsonResponse($app, $result); 
}
    
 
 
?>