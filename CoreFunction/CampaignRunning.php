<?php  


function editCampaignRunning(  $reqParam )
{ 
    
    if( empty( $reqParam)){
            return false;
    }
 


    $CampaignId  = getKeyVal($reqParam, "CampaignId"); 
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    $CampaignRunningGroupId = getKeyVal($reqParam, "CampaignRunningGroupId");
    $sqlStatement   = " Update  CampaignRunning  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE CampaignId='" . $CampaignId . "'  "; 

   if(!empty($InfluencerId)){
        $whereStatement =  $whereStatement. " AND  InfluencerId='" . $InfluencerId . "'  ";
 
    }


    if(!empty($CampaignRunningGroupId)){
        $whereStatement =  $whereStatement. " AND  CampaignRunningGroupId='" . $CampaignRunningGroupId . "'  ";
 
    }
   
  
 
    $AgreedTotalCost = getKeyVal($reqParam, "AgreedTotalCost");
    if (!empty($AgreedTotalCost)) {
        $setStatement = $setStatement . " AgreedTotalCost='" . $AgreedTotalCost . "' ,";
    } 
    $AgreedNoOfPosting = getKeyVal($reqParam, "AgreedNoOfPosting");
    if (!empty($AgreedNoOfPosting)) {
        $setStatement = $setStatement . " AgreedNoOfPosting='" . $AgreedNoOfPosting . "' ,";
    }

    
    $StatusId = getKeyVal($reqParam, "StatusId");
    if (!empty($StatusId)) {
        $setStatement = $setStatement . " StatusId='" . $StatusId . "' ,";
    }
    
     $InfluencerSinglePostCost = getKeyVal($reqParam, "InfluencerSinglePostCost");
    if (!empty($InfluencerSinglePostCost)) {
        $setStatement = $setStatement . " InfluencerSinglePostCost='" . $InfluencerSinglePostCost . "' ,";
    }
     $InfluencerRemainingCost = getKeyVal($reqParam, "InfluencerRemainingCost");
    if (!empty($InfluencerRemainingCost)) {
        $setStatement = $setStatement . " InfluencerRemainingCost='" . $InfluencerRemainingCost . "' ,";
    }
     $InfluencerRemainingPost = getKeyVal($reqParam, "InfluencerRemainingPost");
    if (!empty($InfluencerRemainingPost)) {
        $setStatement = $setStatement . " InfluencerRemainingPost='" . $InfluencerRemainingPost . "' ,";
    } 
 
    $CampaignPaymentId = getKeyVal($reqParam, "CampaignPaymentId");
    if (!empty($CampaignPaymentId)) {
        $setStatement = $setStatement . " CampaignPaymentId='" . $CampaignPaymentId . "' ,";
    } 

    $CreatedBy =  getKeyVal($reqParam, "CreatedBy")   ;
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . "  ,";
    }
    
     $setStatement = $setStatement . " CreatedDate=CreatedDate ";
     // var_dump(  $reqParam);  
     // return false; 
     return crudDB($sqlStatement . $setStatement . $whereStatement);  
}


function addCampaignFacebook(  $reqParam )
{    

    $sqlStatement   = "INSERT INTO  CampaignFacebook (  
                        `CampaignId`    ,  
                        `UserId`    ,  
                        `PostedId`    ,
                        `PostContent`   ,   
                        `PostedDate`      ,
                        `TotalPostComment`  ,
                        `TotalPostReaction`  ,
                        `TotalLike`  ,
                        `TotalPostShare`  ,
                        `ProductPageId`    ,
                        `TotalProductPageShare`  ,
                        `TotalProductPageFollow`  ,
                        `CreatedBy`  ,   
                        `CreatedDate`     
                      )  ";
    $valueStatement = "VALUES (";  

    $CampaignId  = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $valueStatement = $valueStatement . "'" . $CampaignId . "',";
    }
    
    $UserId = getKeyVal($reqParam, "UserId");
    if (!empty($UserId)) {
        $valueStatement = $valueStatement . "'" . $UserId . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    } 

    $PostedId = getKeyVal($reqParam, "PostedId");
    if (!empty($PostedId)) {
        $valueStatement = $valueStatement . "'" . $PostedId . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }       
    $PostContent = getKeyVal($reqParam, "PostContent");
    if (!empty($PostContent)) {
        $valueStatement = $valueStatement . "'" . $PostContent . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }
    $PostedDate = getKeyVal($reqParam, "PostedDate");
    if (!empty($PostedDate)) {
        $valueStatement = $valueStatement . "'" . $PostedDate . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }
     
            

    $TotalPostComment = getKeyVal($reqParam, "TotalPostComment");
    if (!empty($TotalPostComment)) {
        $valueStatement = $valueStatement . "'" . $TotalPostComment . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }

    $TotalPostReaction = getKeyVal($reqParam, "TotalPostReaction");
    if (!empty($TotalPostReaction)) {
        $valueStatement = $valueStatement . "'" . $TotalPostReaction . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }

    $TotalLike = getKeyVal($reqParam, "TotalLike");
    if (!empty($TotalLike)) {
        $valueStatement = $valueStatement . "'" . $TotalLike . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }

    $TotalPostShare = getKeyVal($reqParam, "TotalPostShare");
    if (!empty($TotalPostShare)) {
        $valueStatement = $valueStatement . "'" . $TotalPostShare . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }


    $ProductPageId = getKeyVal($reqParam, "ProductPageId");
    if (!empty($ProductPageId)) {
        $valueStatement = $valueStatement . "'" . $ProductPageId . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'-',";
    }

    $TotalProductPageShare = getKeyVal($reqParam, "TotalProductPageShare");
    if (!empty($TotalProductPageShare)) {
        $valueStatement = $valueStatement . "'" . $TotalProductPageShare . "',";
    }
    else{

        $valueStatement = $valueStatement . "'-',";
    }

     
    $TotalProductPageFollow = getKeyVal($reqParam, "TotalProductPageFollow");
    if (!empty($TotalProductPageFollow)) {
        $valueStatement = $valueStatement . "'" . $TotalProductPageFollow . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }

 

    $CreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    }  

    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )"; 
    $mysqli         = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
}


function getCampaignRunningHistory(  $app )
{ 
 

   $userRec = getUserSessionInfo($app) ;  
    $reqParam = getJsonRequest($app);     
    
    $reqParam ->InfluencerId =   $userRec["Id"]; 
     // $reqParam ->InfluencerId =  "72"; 

    $sqlStatement   = 'SELECT   
                        `Id`  ,  
                        `InfluencerId`    ,   
                        `PostTitle`  ,
                        `PostLink`    ,   
                        `PostContent`    ,  
                        `PostPhotoIdList`    ,       
                        `CampaignRunningId`  ,   
                        `InstagramPostId`  
                      FROM CampaignHistory';

    $whereStatement = " where 1=1 " ;

    
    $Id= getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and Id = '" . $Id . "'";
    }
 
    $InfluencerId= getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and InfluencerId = '" . $InfluencerId . "'";
    }
    $CampaignRunningId= getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $whereStatement = $whereStatement . " and CampaignRunningId = '" . $CampaignRunningId . "'";
    } 
 
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
     $data = queryDB($sqlStatement);  

  return  getJsonResponse($app, $data ); 
}

 


function addCampaignHistory(  $reqParam )
{   
   
    $sqlStatement   = "INSERT INTO  CampaignHistory (  
                        `InfluencerId`   , 
                        `PostTitle`     ,   
                        `PostLink`     ,   
                        `PostContent`    , 
                        `PostPhotoIdList`   ,   
                        `CampaignRunningId`   ,   
                        `FacebookPostId`  ,  
                        `TwitterPostId`  ,  
                        `InstagramPostId`  ,  
                        `VinePostId`  , 
                        `GooglePostId`  ,  
                        `PostCost`  , 
                        `CreatedBy`     , 
                        `CreatedDate`
                      )  ";
    $valueStatement = "VALUES (";  

    $InfluencerId  = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $valueStatement = $valueStatement . "'" . $InfluencerId . "',";
    }
    
    $PostTitle = getKeyVal($reqParam, "PostTitle");
    if (!empty($PostTitle)) {
        $valueStatement = $valueStatement . "'" . $PostTitle . "',";
    }
    else{

        $valueStatement = $valueStatement . "'-',";
    }

    $PostLink = getKeyVal($reqParam, "PostLink");
    if (!empty($PostLink)) {
        $valueStatement = $valueStatement . "'" . $PostLink . "',";
    }
    else{

        $valueStatement = $valueStatement . "'-',";
    }

    $PostContent = getKeyVal($reqParam, "PostContent");
    if (!empty($PostContent)) {
        $valueStatement = $valueStatement . "'" . $PostContent . "',";
    }
    else{

        $valueStatement = $valueStatement . "'-',";
    }

 

    $PostPhotoIdList = getKeyVal($reqParam, "PostPhotoIdList");
    if (!empty($PostPhotoIdList)) {
        $valueStatement = $valueStatement . "'" . $PostPhotoIdList . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }


    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $valueStatement = $valueStatement . "'" . $CampaignRunningId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }   
                   

    $FacebookPostId = getKeyVal($reqParam, "FacebookPostId");
    if (!empty($FacebookPostId)) {
        $valueStatement = $valueStatement . "'" . $FacebookPostId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    }

    $TwitterPostId = getKeyVal($reqParam, "TwitterPostId");
    if (!empty($TwitterPostId)) {
        $valueStatement = $valueStatement . "'" . $TwitterPostId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    }


    $InstagramPostId = getKeyVal($reqParam, "InstagramPostId");
    if (!empty($InstagramPostId)) {
        $valueStatement = $valueStatement . "'" . $InstagramPostId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    }



    $VinePostId = getKeyVal($reqParam, "VinePostId");
    if (!empty($VinePostId)) {
        $valueStatement = $valueStatement . "'" . $VinePostId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    }     

    $GooglePostId = getKeyVal($reqParam, "GooglePostId");
    if (!empty($GooglePostId)) {
        $valueStatement = $valueStatement . "'" . $GooglePostId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    } 
    $PostCost = getKeyVal($reqParam, "PostCost");
    if (!empty($PostCost)) {
        $valueStatement = $valueStatement . "'" . $PostCost . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    } 

    $CreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    } 
    

    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )"; 
    $mysqli         = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
}
 
function addCampaignRunning(  $reqParam )
{  
    $sqlStatement   = "INSERT INTO  CampaignRunning (  
                `CampaignId`,
                `InfluencerId`,
                `CampaignRunningGroupId`,
                `AgreedTotalCost`,
                `AgreedNoOfPosting`,
                `StatusId`,
                `CampaignPaymentId`,
                `InfluencerSinglePostCost`,
                `InfluencerRemainingCost`,
                `InfluencerRemainingPost`, 
                `CreatedBy`,
                `CreatedDate`  
            )  ";
    $valueStatement = "VALUES (";
    $CampaignId  = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $valueStatement = $valueStatement . "'" . $CampaignId . "',";
    }
    
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $valueStatement = $valueStatement . "'" . $InfluencerId . "',";
    }

    $CampaignRunningGroupId = getKeyVal($reqParam, "CampaignRunningGroupId");
    if (!empty($CampaignRunningGroupId)) {
        $valueStatement = $valueStatement . "'" . $CampaignRunningGroupId . "',";
    }


    $AgreedTotalCost = getKeyVal($reqParam, "AgreedTotalCost");
    if (!empty($AgreedTotalCost)) {
        $valueStatement = $valueStatement . "'" . $AgreedTotalCost . "',";
    }else{

        $valueStatement = $valueStatement . "'0',";
    }

    $AgreedNoOfPosting = getKeyVal($reqParam, "AgreedNoOfPosting");
    if (!empty($AgreedNoOfPosting)) {
        $valueStatement = $valueStatement . "'" . $AgreedNoOfPosting . "',";
    }else{

        $valueStatement = $valueStatement . "'0',";
    }

    $StatusId = getKeyVal($reqParam, "StatusId");
    if (!empty($StatusId)) {
        $valueStatement = $valueStatement . "'" . $StatusId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'-',";
    } 
    
    $CampaignPaymentId = getKeyVal($reqParam, "CampaignPaymentId");
    if (!empty($CampaignPaymentId)) {
        $valueStatement = $valueStatement . "'" . $CampaignPaymentId . "',";
    }else{

        $valueStatement = $valueStatement . "'0',";
    } 
     
    $InfluencerSinglePostCost = getKeyVal($reqParam, "InfluencerSinglePostCost");
    if (!empty($InfluencerSinglePostCost)) {
        $valueStatement = $valueStatement . "'" . $InfluencerSinglePostCost . "',";
    }else{

        $valueStatement = $valueStatement . "'0',";
    } 
     
    $InfluencerRemainingCost = getKeyVal($reqParam, "InfluencerRemainingCost");
    if (!empty($InfluencerRemainingCost)) {
        $valueStatement = $valueStatement . "'" . $InfluencerRemainingCost . "',";
    }else{

        $valueStatement = $valueStatement . "'0',";
    } 
    $InfluencerRemainingPost = getKeyVal($reqParam, "InfluencerRemainingPost");
    if (!empty($InfluencerRemainingPost)) {
        $valueStatement = $valueStatement . "'" . $InfluencerRemainingPost . "',";
    }else{

        $valueStatement = $valueStatement . "'0',";
    } 
 


     $CreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    } 
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    } 
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )"; 
    $mysqli         = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
}

 
 
 
// #entry 
function getCampaignRunningByInfluencer( $app)
{   
//INPUT PARAM
// REQUIRED:
// InfluencerId - USERID
// MinDate, MaxDate 
// OPTIONAL:
//  CampaignId, Name, Description

    $userRec = getUserSessionInfo($app) ;  
    $reqParam = getJsonRequest($app);    
    $reqParam ->InfluencerId =   $userRec["Id"]; 
    $listCampaign = getCampaignRunningByInfluencerList(  $reqParam   ) ; 

    getJsonResponse($app, $listCampaign ); 
}
 

// #entry 
function getDashboardByInfluencer(  $app  )
{   
    $userRec = getUserSessionInfo($app) ;  
    $reqParam = getJsonRequest($app);    
    $reqParam ->InfluencerId =   $userRec["Id"]; 
    $dasboardData = getDashboardListByInfluencer(  $reqParam   ) ; 

    getJsonResponse($app, $dasboardData ); 

}

function getDashboardListByInfluencer(  $reqParam  )
{   

 
  //GET TOTAL campaign REC
    $sqlStatement   = "  SELECT COUNT(cr.Id ) AS TotalCampaign, 
                        SUM(cr.AgreedTotalCost) AS TotalAmountCollected    
                        FROM CampaignRunning cr   ";
 
    $whereStatement = "  where 1=1     ";
    $orderStatement   = "    "; 

    $InfluencerId= getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cr.InfluencerId = '" . $InfluencerId . "'";
    }
    
    $CampaignRunningGroupId= getKeyVal($reqParam, "CampaignRunningGroupId");
    if (!empty($CampaignRunningGroupId)) {
        $whereStatement = $whereStatement . " and cr.CampaignRunningGroupId = '" . $CampaignRunningGroupId . "'";
    }
    

    $sqlStatement = $sqlStatement . $whereStatement .   $orderStatement . " ; ";
    $recTotalCampaign = queryDB($sqlStatement);
    // var_export($recTotalCampaign ) ;

  //GET Running campaign REC
    $sqlStatement   = "  SELECT COUNT(cr.Id ) AS TotalRunningCampaign    
                        FROM CampaignRunning cr ,CampaignStatus cs  ";
 
    $whereStatement = "  where 1=1 and cr.StatusId=cs.Id    ";
    $orderStatement   = "     "; 

    $InfluencerId= getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cr.InfluencerId = '" . $InfluencerId . "'";
    }

    $CampaignRunningGroupId= getKeyVal($reqParam, "CampaignRunningGroupId");
    if (!empty($CampaignRunningGroupId)) {
        $whereStatement = $whereStatement . " and cr.CampaignRunningGroupId = '" . $CampaignRunningGroupId . "'";
    }

    $StatusCode= "PROGR" ;
    if (!empty($StatusCode)) {
        $whereStatement = $whereStatement . " and cs.StatusCode = '" . $StatusCode . "'";
    }

    $sqlStatement = $sqlStatement . $whereStatement .   $orderStatement . " ; ";
    $recRunningCampaign = queryDB($sqlStatement);
 
    // var_export($recRunningCampaign ) ;
 
// //GET user influence   REC
    $recUserInfluence = getUserInfluence(null,   $InfluencerId); 
    // var_export($recUserInfluence ) ;

// //GET facebook summary campaign REC  
    $sqlStatement   = "  SELECT COUNT(cf.Id ) AS TotalFacebookCampaign    ,
                           AVG(cf.TotalPostComment ) AS AvgTotalPostComment   , 
                           AVG(cf.TotalPostReaction ) AS AvgTotalPostReaction   , 
                           AVG(cf.TotalPostShare ) AS AvgTotalPostShare   , 
                           AVG(cf.TotalLike ) AS AvgTotalLike    
                        FROM  CampaignRunning cr, CampaignFacebook cf, CampaignStatus cs   ";
  

    $whereStatement = "  where 1=1 and  cr.StatusId = cs.Id   ";
    $orderStatement   = "     "; 

    $UserId=   $InfluencerId ;
    if (!empty($UserId)) {
        $whereStatement = $whereStatement . " and cf.UserId = '" . $UserId . "'";
    } 
 
    $sqlStatement = $sqlStatement . $whereStatement .   $orderStatement . " ; ";
    $recfbCampaign = queryDB($sqlStatement);
    // var_export($recfbCampaign ) ;    
 
    $recTotalCampaign[0] = empty($recTotalCampaign[0]) ?array() : $recTotalCampaign[0];
    $recRunningCampaign[0] = empty($recRunningCampaign[0]) ?array() : $recRunningCampaign[0];
    $recUserInfluence[0] = empty($recUserInfluence[0]) ?array() : $recUserInfluence[0];
    $recfbCampaign[0] = empty($recfbCampaign[0]) ? array() : $recfbCampaign[0];
    return  array_merge($recTotalCampaign[0],$recRunningCampaign[0],$recUserInfluence[0],$recfbCampaign[0] );
  
 
}

function getCampaignRunningByInfluencerList(  $reqParam  )
{   
  
    $sqlStatement   = " SELECT   
                        cr.Id   , 
                        cr.InfluencerId     ,   
                        cr.CampaignRunningGroupId     ,   
                        cr.AgreedTotalCost   ,
                        cr.AgreedNoOfPosting     ,   
                        cr.InfluencerSinglePostCost     , 
                        cr.InfluencerRemainingCost     , 
                        cr.InfluencerRemainingPost     ,     

                        cr.StatusId     ,  
                        cs.StatusCode as StatusCode  ,  
                        cr.CampaignPaymentId     ,       
                        cr.CreatedBy   ,   
                        cr.CreatedDate   ,
                         DATEDIFF( c.ToDate , NOW()  ) AS DaysLeft,
                        cr.CampaignId   as campaignId  ,   
                        c.Id   as CampaignId  ,   
                        c.Name  as CampaignName , 
                        c.Description as CampaignDescription   ,  
                        c.PromoTitle  as CampaignPromoTitle  ,  
                        c.PromoCaption as CampaigPromoCaption   ,   
                        c.PromoDescription as CampaignPromoDescription   ,  
                        c.Price as CampaignPrice  , 
                        c.NoOfInfluencer as CampaignNoOfInfluencer   , 
                        c.NoOfPosting as CampaignNoOfPosting  , 
                        c.Charges1  as CampaignCharges1 ,
                        c.Charges1Desc  as CampaignCharges1Desc  ,
                        c.Charges2  as CampaignCharges2 ,
                        c.Charges2Desc  as CampaignCharges2Desc   ,
                        c.Charges3 as CampaignCharges3  ,
                        c.Charges3Desc as CampaignCharges3Desc  , 
                        c.TotalCost as CampaignTotalCost  ,     
                        c.PhotoIdList as CampaignPhotoIdList   , 
                        c.IsActive   as CampaignIsActive ,    
                        c.FromDate   as CampaignFromDate   , 
                        c.ToDate     as CampaignToDate , 
                        c.CreatedBy as CampaignCreatedBy   
                      FROM CampaignRunning cr , Campaign c  , CampaignStatus cs";
 
    $whereStatement = "  where 1=1 and cr.CampaignId=c.Id  and cr.StatusId=cs.Id     ";
    $orderStatement   = "  ORDER BY c.FromDate ASC  ";
     

    $InfluencerId= getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cr.InfluencerId = '" . $InfluencerId . "'";
    }

    $CampaignRunningGroupId= getKeyVal($reqParam, "CampaignRunningGroupId");
    if (!empty($CampaignRunningGroupId)) {
        $whereStatement = $whereStatement . " and cr.CampaignRunningGroupId = '" . $CampaignRunningGroupId . "'";
    }

    $CampaignId= getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $whereStatement = $whereStatement . " and cr.CampaignId = '" . $CampaignId . "'";
    }


    $StatusCodeList= getKeyVal($reqParam, "StatusCodeList");  
     if (!empty($StatusCodeList)) {
      $isList = strpos($StatusCodeList,",");
      if($isList){
              $StatusCodeList= explode( ",",  $StatusCodeList  );   
                $whereStatement = $whereStatement . " AND ( ";
                foreach ($StatusCodeList as $statusKey => $statusValue) {  
                    if(!empty( $statusValue )){ 
                        if($statusKey == 0 ){ 
                          $whereStatement = $whereStatement . "   cs.StatusCode = '" . $statusValue . "' ";
                        }else{
                         $whereStatement = $whereStatement . " OR cs.StatusCode = '" . $statusValue . "' ";
                        }
                         
                   } 
                }
              $whereStatement = $whereStatement . "  ) " ;
      }
      else{ 
            $whereStatement = $whereStatement . " AND cs.StatusCode = '" . $StatusCodeList . "'";
      } 

    }


    $StatusIdList= getKeyVal($reqParam, "StatusIdList");  
     if (!empty($StatusIdList)) {
      $isList = strpos($StatusIdList,",");
      if($isList){
              $StatusIdList= explode( ",",  $StatusIdList  );   
                $whereStatement = $whereStatement . " AND ( ";
                foreach ($StatusIdList as $statusKey => $statusValue) {  
                    if(!empty( $statusValue )){ 
                        if($statusKey == 0 ){ 
                          $whereStatement = $whereStatement . "   cr.StatusId = '" . $statusValue . "' ";
                        }else{
                         $whereStatement = $whereStatement . " OR cr.StatusId = '" . $statusValue . "' ";
                        }
                         
                   } 
                }
              $whereStatement = $whereStatement . "  ) " ;
      }
      else{ 
            $whereStatement = $whereStatement . " AND cr.StatusId = '" . $StatusIdList . "'";
      } 

    }
 
    $Name= getKeyVal($reqParam, "Name");
    if (!empty($Name)) {
        $whereStatement = $whereStatement . " and c.Name = '" . $Name . "'";
    }

    $Description= getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $whereStatement = $whereStatement . " and c.Description = '" . $Description . "'";
    }   
    $PromoTitle= getKeyVal($reqParam, "PromoTitle");
    if (!empty($PromoTitle)) {
    $whereStatement = $whereStatement . " and c.PromoTitle = '" . $PromoTitle . "'";
    }
    $PromoDescription= getKeyVal($reqParam, "PromoDescription");
    if (!empty($PromoDescription)) {
    $whereStatement = $whereStatement . " and c.PromoDescription = '" . $PromoDescription . "'";
    }

    $PromoCaption= getKeyVal($reqParam, "PromoCaption");
    if (!empty($PromoCaption)) {
        $whereStatement = $whereStatement . " and c.PromoCaption = '" . $PromoCaption . "'";
    }
    $MinPrice= getKeyVal($reqParam, "MinPrice");
    if (!empty($MinPrice)) {  
      $whereStatement = $whereStatement . " and  cast( c.Price  as decimal)  >=  cast( '" . $MinPrice . "'  as decimal)  ";
    }

    $MaxPrice= getKeyVal($reqParam, "MaxPrice");
    if (!empty($MaxPrice)) { 
        $whereStatement = $whereStatement . " and  cast( c.Price  as decimal)  <=  cast( '" . $MaxPrice . "'  as decimal)  ";
    } 

    $MinFromDate= getKeyVal($reqParam, "MinFromDate");
    if (!empty($MinFromDate)) {
        $whereStatement = $whereStatement . " and c.FromDate >= '" . $MinFromDate . "'";
    }
    $MaxFromDate= getKeyVal($reqParam, "MaxFromDate");
    if (!empty($MaxFromDate)) {
        $whereStatement = $whereStatement . " and c.FromDate <= '" . $MaxFromDate . "'";
    }
    $MinToDate= getKeyVal($reqParam, "MinToDate");
    if (!empty($MinToDate)) {
        $whereStatement = $whereStatement . " and c.ToDate >= '" . $MinToDate . "'";
    }
    $MaxToDate= getKeyVal($reqParam, "MaxToDate");
    if (!empty($MaxToDate)) {
        $whereStatement = $whereStatement . " and c.ToDate <= '" . $MaxToDate . "'";
    }


    $MinCreatedDate= getKeyVal($reqParam, "MinCreatedDate");
    if (!empty($MinCreatedDate)) {
        $whereStatement = $whereStatement . " and cr.CreatedDate >= '" . $MinCreatedDate . "'";
    }
    $MaxCreatedDate= getKeyVal($reqParam, "MaxCreatedDate");
    if (!empty($MaxCreatedDate)) {
        $whereStatement = $whereStatement . " and  cr.CreatedDate<= '" . $MaxCreatedDate . "'";
    }



    $CampaignCreatedBy= getKeyVal($reqParam, "CampaignCreatedBy");
    if (!empty($CampaignCreatedBy)) {
        $whereStatement = $whereStatement . " and c.CreatedBy = '" . $CampaignCreatedBy . "'";
    }  

    $sqlStatement = $sqlStatement . $whereStatement .   $orderStatement . " ; ";
    $listCampaign = queryDB($sqlStatement);

 
    foreach ($listCampaign as $key => &$rec) {   
        if( !empty($rec ) ){
        //Fetch foto attachment
            $CampaignPhotoIdList= explode( ",",  $rec["CampaignPhotoIdList"]  );   
            $rec['PhotoList'] =[];
            foreach ($CampaignPhotoIdList as $photoKey => $photoValue) { 
                $photoRec=getUserPhoto(  $photoValue )[0];
                if(!empty( $photoRec)){   array_push( $rec['PhotoList'] ,  $photoRec);}

            }
 

            $filterCampaignStat = new StdClass;

            $filterCampaignStat->MinDate = getKeyVal($reqParam, "MinDate");    
            // $MaxDate = empty(  getKeyVal($reqParam, "MaxDate") )? "'" . date('Y-m-d H:i:s') . "' " : getKeyVal($reqParam, "MaxDate")  ;
            $MaxDate = getKeyVal($reqParam, "MaxDate") ;
            $filterCampaignStat->MaxDate =   $MaxDate ;  
            $rec = getCampaignFacebookSquashed($rec , $filterCampaignStat); 

        }   
    } 
  
return $listCampaign  ; 
 //  return  $sqlStatement; 
}


 

function getCampaignRunningCount(  $reqParam ,$statusCodeList)
{ 
    
 
    $sqlStatement   = 'SELECT COUNT(cr.Id) AS TotalRunning,   
                        SUM(cr.AgreedNoOfPosting) AS TotalAgreedTotalPost  ,
                        SUM(cr.AgreedTotalCost) AS TotalAgreedTotalCost  ,
                        SUM(cr.InfluencerSinglePostCost) AS TotalSinglePostCost   ,
                        SUM(cr.InfluencerRemainingPost) AS TotalInfluencerRemainingPost  ,
                        SUM(cr.InfluencerRemainingCost) AS TotalInfluencerRemainingCost 
                        FROM CampaignRunning cr, CampaignStatus cs '; 
    $whereStatement = " where 1=1 and  cr.StatusId = cs.Id   " ;
 

    $CampaignId= getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $whereStatement = $whereStatement . " and cr.CampaignId = '" . $CampaignId . "'";
    }
    $MerchantId= getKeyVal($reqParam, "MerchantId");
    if (!empty($MerchantId)) {
        $whereStatement = $whereStatement . " and cr.CreatedBy = '" . $MerchantId . "'";
    }
    $InfluencerId= getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cr.InfluencerId = '" . $InfluencerId . "'";
    }
    $CampaignRunningGroupId= getKeyVal($reqParam, "CampaignRunningGroupId");
    if (!empty($CampaignRunningGroupId)) {
        $whereStatement = $whereStatement . " and cr.CampaignRunningGroupId = '" . $CampaignRunningGroupId . "'";
    }
 
    $MaxDate= getKeyVal($reqParam, "MaxDate");
    $MinDate= getKeyVal($reqParam, "MinDate");
    if (!empty($MaxDate) && !empty($MinDate)) {
        $whereStatement = $whereStatement .
         " and (cr.CreatedDate BETWEEN '" . $MinDate . "' AND '".$MaxDate ."' ) ";
    } 

    $limit = count($statusCodeList);
      foreach ($statusCodeList as $statusKey => $statusValue) {  
                if(!empty( $statusValue )){ 
                        if($statusKey == 0 ){ 
                          $whereStatement = $whereStatement . " and (  cs.StatusCode = '" . $statusValue . "' ";
                        }else{
                         $whereStatement = $whereStatement . " OR cs.StatusCode = '" . $statusValue . "' ";
                        }

                        if($statusKey == intval($limit) - 1 ){  
                          $whereStatement = $whereStatement . "  ) ";
                        }
                         
               } 
        }
 
   
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    // var_dump(  $sqlStatement) ; 
    // return false;
    return queryDB($sqlStatement)[0]; 
   
  

}



function getCampaignRunning(  $reqParam )
{ 
    

    $sqlStatement   = 'SELECT   
                        `Id`  , 
                        `CampaignId`    , 
                        `InfluencerId`    ,   
                        `CampaignRunningGroupId`    ,   
                        `AgreedTotalCost`  ,
                        `AgreedNoOfPosting`    ,   
                        `StatusId`    ,  
                        `CampaignPaymentId`    ,       
                        `CreatedBy`  ,   
                        `CreatedDate`  
                      FROM CampaignRunning';

    $whereStatement = " where 1=1 " ;

    
    $Id= getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and Id = '" . $Id . "'";
    }

    $CampaignId= getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $whereStatement = $whereStatement . " and CampaignId = '" . $CampaignId . "'";
    }
    $InfluencerId= getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and InfluencerId = '" . $InfluencerId . "'";
    }
    $CampaignRunningGroupId= getKeyVal($reqParam, "CampaignRunningGroupId");
    if (!empty($CampaignRunningGroupId)) {
        $whereStatement = $whereStatement . " and CampaignRunningGroupId = '" . $CampaignRunningGroupId . "'";
    }
    $CampaignPaymentId= getKeyVal($reqParam, "CampaignPaymentId");
    if (!empty($CampaignPaymentId)) {
        $whereStatement = $whereStatement . " and CampaignPaymentId = '" . $CampaignPaymentId . "'";
    } 


    $filterAgreedNoOfPostingMin = getKeyVal($reqParam, "AgreedNoOfPostingMin");
    if (!empty($filterAgreedNoOfPostingMin)) {
        $whereStatement = $whereStatement . " and cast( a.AgreedNoOfPosting  as decimal)  >= cast( '" . $filterAgreedNoOfPostingMin . "' as decimal)  ";
    }
     $filterAgreedNoOfPostingMax = getKeyVal($reqParam, "AgreedNoOfPostingMax");
    if (!empty($filterAgreedNoOfPostingMax)) {
        $whereStatement = $whereStatement . " and  cast( a.AgreedNoOfPosting  as decimal)  <=  cast( '" . $filterAgreedTotalCostMax . "'  as decimal)  ";
    }

    $filterAgreedTotalCostMin = getKeyVal($reqParam, "AgreedTotalCostMin");
    if (!empty($filterAgreedTotalCostMin)) {
        $whereStatement = $whereStatement . " and cast( a.AgreedTotalCost  as decimal)  >= cast( '" . $filterAgreedTotalCostMin . "' as decimal)  ";
    }
      $filterAgreedTotalCostMax = getKeyVal($reqParam, "AgreedTotalCostMax");
    if (!empty($filterAgreedTotalCostMax)) {
        $whereStatement = $whereStatement . " and  cast( a.AgreedTotalCost  as decimal)  <=  cast( '" . $filterAgreedTotalCostMax . "'  as decimal)  ";
    }

    $StatusId= getKeyVal($reqParam, "StatusId");
    if (!empty($StatusId)) {
        $whereStatement = $whereStatement . " and StatusId = '" . $StatusId . "'";
    }
    $CreatedBy= getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $whereStatement = $whereStatement . " and CreatedBy = '" . $CreatedBy . "'";
    }
    
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);

   

    return $data;
}

 


function getCampaignIdRunningList(  $reqParam )
{ 
    

    $sqlStatement   = 'SELECT   DISTINCT  cr.CampaignId       
                      FROM CampaignRunning cr, CampaignStatus cs'; 

    $whereStatement = " where 1=1 and cr.StatusId = cs.Id" ;

    
    $Id= getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and cr.Id = '" . $Id . "'";
    }

    $CampaignId= getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $whereStatement = $whereStatement . " and cr.CampaignId = '" . $CampaignId . "'";
    }
    $MerchantId= getKeyVal($reqParam, "MerchantId");
    if (!empty($MerchantId)) {
        $whereStatement = $whereStatement . " and cr.CreatedBy = '" . $MerchantId . "'";
    }
    $InfluencerId= getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cr.InfluencerId = '" . $InfluencerId . "'";
    }
    $CampaignRunningGroupId= getKeyVal($reqParam, "CampaignRunningGroupId");
    if (!empty($CampaignRunningGroupId)) {
        $whereStatement = $whereStatement . " and cr.CampaignRunningGroupId = '" . $CampaignRunningGroupId . "'";
    }
    $CampaignPaymentId= getKeyVal($reqParam, "CampaignPaymentId");
    if (!empty($CampaignPaymentId)) {
        $whereStatement = $whereStatement . " and cr.CampaignPaymentId = '" . $CampaignPaymentId . "'";
    } 
 
    $MaxDate= getKeyVal($reqParam, "MaxDate");
    $MinDate= getKeyVal($reqParam, "MinDate");
    if (!empty($MaxDate) && !empty($MinDate)) {
        $whereStatement = $whereStatement .
         " and (cr.CreatedDate BETWEEN '" . $MinDate . "' AND '".$MaxDate ."' ) ";
    } 

    // $StatusCodeList= getKeyVal($reqParam, "StatusCodeList");
    // if (!empty($StatusCodeList)) {
    // $limit = count($StatusCodeList);
    //   foreach ($StatusCodeList as $statusKey => $statusValue) {  
    //             if(!empty( $statusValue )){ 
    //                     if($statusKey == 0 ){ 
    //                       $whereStatement = $whereStatement . " and (   cs.StatusCode = '" . $statusValue . "' ";
    //                     }else{
    //                      $whereStatement = $whereStatement . " OR  cs.StatusCode = '" . $statusValue . "' ";
    //                     }

    //                     if($statusKey == intval($limit) - 1 ){  
    //                       $whereStatement = $whereStatement . "  ) ";
    //                     }
                         
    //            } 
    //     }
    // }
    // $StatusId= getKeyVal($reqParam, "StatusId");
    // if (!empty($StatusId)) {
    //     $whereStatement = $whereStatement . " and StatusId = '" . $StatusId . "'";
    // }

    $filterAgreedNoOfPostingMin = getKeyVal($reqParam, "AgreedNoOfPostingMin");
    if (!empty($filterAgreedNoOfPostingMin)) {
        $whereStatement = $whereStatement . " and cast( a.AgreedNoOfPosting  as decimal)  >= cast( '" . $filterAgreedNoOfPostingMin . "' as decimal)  ";
    }
     $filterAgreedNoOfPostingMax = getKeyVal($reqParam, "AgreedNoOfPostingMax");
    if (!empty($filterAgreedNoOfPostingMax)) {
        $whereStatement = $whereStatement . " and  cast( a.AgreedNoOfPosting  as decimal)  <=  cast( '" . $filterAgreedTotalCostMax . "'  as decimal)  ";
    }

    $filterAgreedTotalCostMin = getKeyVal($reqParam, "AgreedTotalCostMin");
    if (!empty($filterAgreedTotalCostMin)) {
        $whereStatement = $whereStatement . " and cast( a.AgreedTotalCost  as decimal)  >= cast( '" . $filterAgreedTotalCostMin . "' as decimal)  ";
    }
      $filterAgreedTotalCostMax = getKeyVal($reqParam, "AgreedTotalCostMax");
    if (!empty($filterAgreedTotalCostMax)) {
        $whereStatement = $whereStatement . " and  cast( a.AgreedTotalCost  as decimal)  <=  cast( '" . $filterAgreedTotalCostMax . "'  as decimal)  ";
    }

    $CreatedBy= getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $whereStatement = $whereStatement . " and CreatedBy = '" . $CreatedBy . "'";
    }
    
    
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement); 
    $listResult= [];$x = 0; 
    while($x <  count($data)) { 
        $listResult[$x] = $data[$x]["CampaignId"] ;
        $x++;
    }
 
    return $listResult;
}

 


function updateCampaignPostageByInfluencer($app){

    $reqParam = getJsonRequest($app); 
    $userInfo = getUserSessionInfo($app) ;  
   

    $Step = getKeyVal($reqParam, "Step"); 
    $CampaignId = getKeyVal($reqParam, "CampaignId");    
    $StatusTargetId = getKeyVal($reqParam, "StatusTargetId"); 
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId"); 

    $InfluencerId =  $userInfo["Id"];    
    $CreatedBy = $userInfo["Id"];      

    checkEmpty($app,"Step ", $Step ) ; 
    checkEmpty($app,"InfluencerId ", $InfluencerId ) ;  
    checkEmpty($app,"CampaignId ", $CampaignId ) ;  


    $PostTitle = getKeyVal($reqParam, "PostTitle");    
    $PostContent = getKeyVal($reqParam, "PostContent");    
    $PostLink = getKeyVal($reqParam, "PostLink");    
 
 
    $FacebookPostId = getKeyVal($reqParam, "FacebookPostId");     
    $InstagramPostId = getKeyVal($reqParam, "InstagramPostId");     
    $TwitterPostId = getKeyVal($reqParam, "TwitterPostId");    
  

    $filterCampaigRunning = new StdClass; 
    $filterCampaigRunning->InfluencerId =$InfluencerId;  
    $filterCampaigRunning->CampaignId =$CampaignId;     

    $rowRec = getCampaignRunningByInfluencerList(  $filterCampaigRunning  )[0]  ;
 
   //  if(empty($rowRec) ){ 
   //          return processErrorMessage($app, "This record is missing from the db. Please inform admin. Data tampering suspected. ", "010"); 
   //  }
   
        
   //  if( floatval( $rowRec["InfluencerSinglePostCost"])  > floatval( $rowRec["InfluencerRemainingCost"] ) ){
   //       return processErrorMessage($app, "Posting is disallowed.  This post will exceed the budget cost given by merchant..  ", "9999"); 
   //   }


   //  if( intval( $rowRec["InfluencerRemainingPost"])  < intval(  1   ) ){
   //       return processErrorMessage($app, "Posting this is disallowed. You have already completed the     campaign postage", "9999"); 
   //   }

   // if( !isComparedEqual("INF-PROGR" , $rowRec["StatusCode"])){
   //       return processErrorMessage($app, "Posting this is disallowed. This campaign status is not correct", "9999"); 
   //   }

  // if( isComparedEqual( $Step,"2")){

        checkEmpty($app,"PostLink ", $PostLink ) ; 
        checkEmpty($app,"PostTitle ", $PostTitle ) ; 
        checkEmpty($app,"PostContent ", $PostContent ) ;   
        checkEmpty($app,"CampaignRunningId ", $CampaignRunningId ) ;  

        //CampaignRunning table upd
        $updRec =   new StdClass; 

        $updRec->InfluencerId =$InfluencerId;  
        $updRec->CampaignId =$CampaignId;   
        $updRec->StatusId =$StatusTargetId;   
        
        $updRec->InfluencerRemainingPost = intval(  $rowRec["InfluencerRemainingPost"]) - 1;  
        $updRec->InfluencerRemainingPost = empty( $updRec->InfluencerRemainingPost  )? "00": $updRec->InfluencerRemainingPost ;

        $updRec->InfluencerRemainingCost = floatval( $rowRec["InfluencerRemainingCost"] ) - floatval( $rowRec["InfluencerSinglePostCost"]) ;      
        $updRec->InfluencerRemainingCost = empty( $updRec->InfluencerRemainingCost  )? "00": $updRec->InfluencerRemainingCost ; 
        
         editCampaignRunning(  $updRec );



        //CampaignHistory table add   
         $reqParam->InfluencerId = $InfluencerId;  
         $reqParam->CreatedBy = $InfluencerId;  
         $reqParam->UserId = $InfluencerId;   
         $reqParam->PostCost = ( $rowRec["InfluencerSinglePostCost"]) ;
 
         $historyId = addCampaignHistory(  $reqParam )  ; 
         $fbId= "";

        if(!empty(  $FacebookPostId )){ 

         $reqParam->PostedDate = date('Y-m-d H:i:s') ;  
         $reqParam->PostedId = $FacebookPostId;  
        //CampaignFacebook table add
          $fbId= addCampaignFacebook(  $reqParam ); 
        }

        if(!empty(  $InstagramPostId )){ 

        //CampaignFacebook table add
        }

        if(!empty(  $TwitterPostId )){ 

        //CampaignFacebook table add
        }

    // }
    // 
    //get CampaignRunning by influencer and campaignid
    // step 1:
    // if   InfluencerSinglePostCost>= InfluencerRemainingCost --> error
    // if   InfluencerRemainingPost<1   --> error 
    // if  InfluencerRemainingPost <=0 ||InfluencerRemainingCost <=0 ==>    StatusId = 'compl'
    // 
    // step 2:
    // update :InfluencerRemainingCost = InfluencerRemainingCost  -InfluencerSinglePostCost
    // update :InfluencerRemainingPost =  InfluencerRemainingPost - 1 
    // 
    // add new record into campaign history
    // add new record into campaignfacebook, campaigninstagram,campaignpost according


    $result = array( 
        "operation" => $operation,
        "fbId" => $fbId, 
        "historyId" => $historyId  
    );

     getJsonResponse($app,$result);  
}

 

 function addEditCampaignRunningRec($app) 
{
 
    $reqParam = getJsonRequest($app); 
    $userInfo = getUserSessionInfo($app) ;   


    $InfluencerId = getKeyVal($reqParam, "InfluencerId");   
    $CampaignId = getKeyVal($reqParam, "CampaignId");    
    $StatusId =  getKeyVal($reqParam, "StatusId"); 
    $StatusCode =  getKeyVal($reqParam, "StatusCode");    
    $ByInfluencer =  getKeyVal($reqParam, "ByInfluencer");   
    if(!empty( $ByInfluencer )){ 
        $InfluencerId =$userInfo["Id"];
        $reqParam->InfluencerId =$InfluencerId ;  
    }

    checkEmpty($app,"Influencer ", $InfluencerId ) ; 
    checkEmpty($app,"Campaign Id ", $CampaignId ) ;  
    checkEmpty($app,"StatusId", $StatusId) ;
    checkEmpty($app,"StatusCode", $StatusCode) ;

    //retrieve existing campaign info
    $filterCampaign = new StdClass; 
    $filterCampaign->Id =$CampaignId;  
    $campaignRow  =  getCampaign($filterCampaign )[0] ;  
    //Campaign must exist 
    checkEmpty($app," Campaign Data ",  $campaignRow) ; 

 
     //get  campaignrunning table with filter campaignid and influencerid 
    $filter = new StdClass;
    $filter->InfluencerId =$InfluencerId ;   
    $filter->CampaignId = $CampaignId;  
    $resultList=getCampaignRunning(  $filter ); 
    $reqParam->CreatedBy=$userInfo["Id"];

   

    $AgreedTotalCost =  getKeyVal($reqParam, "AgreedTotalCost"); 
    $AgreedNoOfPosting =  getKeyVal($reqParam, "AgreedNoOfPosting"); 

 //--VALIDATIONS--
 /////
    if( !isComparedEqual( $StatusCode,"INF-REQUE")  ){ 


        //not allowed modify initial value
        $AgreedTotalCost =  getKeyVal($reqParam, "AgreedTotalCost");  
        $AgreedNoOfPosting =  getKeyVal($reqParam, "AgreedNoOfPosting");    
        $InfluencerSinglePostCost =  getKeyVal($reqParam, "InfluencerSinglePostCost"); 

  
        $AgreedTotalCost =   0;  
        $AgreedNoOfPosting =  0;  
        $InfluencerSinglePostCost =   0;

    }


 /////
 ///
 //
 /*
  if( !isComparedEqual( $StatusCode,"REQUE")  ){ 


        //not allowed modify initial value
        $AgreedTotalCost =  getKeyVal($reqParam, "AgreedTotalCost");  
        $AgreedNoOfPosting =  getKeyVal($reqParam, "AgreedNoOfPosting");    
        $InfluencerSinglePostCost =  getKeyVal($reqParam, "InfluencerSinglePostCost"); 

        if(!empty($AgreedTotalCost  )|| !empty($AgreedNoOfPosting  ) || !empty($InfluencerSinglePostCost  )    ){

          return processIllegalAccess($app, "ILLEGAL Data Mod - NOT PERMITTED", "100");
        }
 

    }
*/

//     if( isComparedEqual( $StatusCode,"APPRO")  ){  
//             //merchant re-request again
//         if( empty( $resultList)){  
//             return processErrorMessage($app, "This record is missing from the db. Please inform admin. Data tampering suspected. ", "010"); 
//          }  

//         $AgreedTotalCost =  $resultList[0]["AgreedTotalCost"];  
//         $AgreedNoOfPosting =  $resultList[0]["AgreedNoOfPosting"];  

//     }
//     if( isComparedEqual( $StatusCode,"REQUE")  ){ 

//         checkEmpty($app,"Agreed Total Cost ", $AgreedTotalCost ) ;
//         checkEmpty($app,"Agreed Posting No ", $AgreedNoOfPosting ) ;

//          //merchant re-request again
//         if(!empty( $resultList)){  
//             return processErrorMessage($app, "You have already requested this influencer to run your campaign. Please be patient on awaiting this influencer's reply. ", "010"); 
//          }  

 
//         //merchant attempt to post more than campaign limitation price & post..
//          if($campaignRow["NoOfPosting"]<=$AgreedNoOfPosting){
//           return processErrorMessage($app, "Sorry. This setting is against the campaign posting limitation.", "0999");
//          }
//          if($campaignRow["Price"]<=$AgreedTotalCost){
//           return processErrorMessage($app, "Sorry. This setting is against the campaign costing limitation.", "0999");
//          } 
         
//         //populate data
//         $reqParam->InfluencerSinglePostCost=floatval( $AgreedTotalCost /  $AgreedNoOfPosting) ;
//         $reqParam->InfluencerRemainingCost= ( $AgreedTotalCost  ) ;
//         $reqParam->InfluencerRemainingPost= ( $AgreedNoOfPosting  ) ;
//     }

//     //check  capacity limit
//    if( isComparedEqual( $StatusCode,"REQUE")   || isComparedEqual( $StatusCode,"APPRO")  ){  
//     // 'REQUE'  'APPRO'  'NOPAY'  'PROGR'  'COMPL'  'INACT'
//     $filter = new StdClass; 
//     $filter->CampaignId = $CampaignId;  
//     $statusCodeList = array("NOPAY", "PROGR", "COMPL", "INACT");  
//     $campaignRunningSummary=getCampaignRunningCount(  $filter , $statusCodeList); 
    
//     $newtotalInfluencerInCampaign= intval( $campaignRunningSummary["TotalRunning"] ) + 1;
//     $newTotalAgreedTotalCost=floatval(  $AgreedTotalCost) + floatval(  $campaignRunningSummary["TotalAgreedTotalCost"] );
//     $newTotalAgreedTotalPost=intval( $AgreedNoOfPosting) + intval(  $campaignRunningSummary["TotalAgreedTotalPost"]);   
          
//      if($campaignRow["NoOfInfluencer"]< $newtotalInfluencerInCampaign){ 
//         return processErrorMessage($app, "Sorry. This running campaign already has a pool of influencers. This transaction will exceed the max influencer remaining. This means as merchant, you may not assign any more influencer unless you increase this campaign's budget on the influencer no or reduce the allocation. For influencer, you may not be able to approve this until the merchant adjusts this campaign's budget. ", "0999"); 
//      } 

//      if($campaignRow["Price"]<  $newTotalAgreedTotalCost){ 
//         return processErrorMessage($app, "Sorry.This running campaign already reached a certain usage of cost.  This transaction will overload the cost price budget remaining for this campaign. This means that for merchant, please increase the campaign amount budget or reduce the allocation. For influencer, you may not be able to approve this until the merchant adjusts this campaign's budget.", "0999"); 
//      } 

//      if($campaignRow["NoOfPosting"]< $newTotalAgreedTotalPost){ 
//         return processErrorMessage($app, "Sorry.This running campaign already reached a certain usage of postage.This transaction will exceed the posting budget remaining. This means that for merchant, please increase the campaign posting no budget or reduce the allocation. For influencer, you may not be able to approve this until the merchant adjusts this campaign's budget.", "0999"); 
//      } 


//    }
    

 

    //add into campaign running    
        if(empty( $resultList)){

            //if not exist --> add  campaignrunning table with request data
            $operation="ADD";
            $trxStatus=addCampaignRunning(  $reqParam ) ;
        }else{

            //if exist --> update  campaignrunning table with request data
            $operation="UPD"; 
           $trxStatus= editCampaignRunning(  $reqParam ) ;
        }


    //send email notification ?  

    $result = array( 
        "operation" => $operation,
        "trxStatus" => $trxStatus, 
    );

     getJsonResponse($app,$result);  
}


?>