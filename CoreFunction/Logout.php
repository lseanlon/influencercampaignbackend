<?php  
//#Entry
 function performCommonLogout($app) 
{

    //USE COOKIE INFO
    // $serializedUserRec = $app->getCookie('UserData');
    // $LoginType         = $app->getCookie('LoginType');

    // $userRec = unserialize($serializedUserRec);
   
    // //get sessionkey. from sessionkey find user id.
    // $SessionKey = $userRec[ "SessionKey"];    
 
    $userRec = getUserSessionInfo($app) ; 



    $userId =$userRec["Id"];

    $userRecData = new StdClass;
    $userRecData->CreatedBy=$userId;
    $userRecData->CreatedDate=date('Y-m-d H:i:s');
    $userRecData->IsLoggedOut="1";
 
     if(intval($userRec["IsLoggedOut"] ) >=1 ){ 
          return processIllegalAccess($app, "ALREADY LOGGED OUT ", "100");
     }


    // update logout flag
     editUserCommon($userRecData, $userId,null); 


    // update sessionhistory with current data.   
    $userRecData->SessionKey=$SessionKey;
    $userRecData->UserId =$userId;   
    $userRecData->LogoutDate=date('Y-m-d H:i:s'); 
    performSessionHistory(  $userRecData, "LOGOUT");
     // editSessionHistory($userRecData,  $userId );
     
    //Delete cookie
    // $app->deleteCookie('UserData','/Influencer'); 
    // $app->deleteCookie('LoginType','/Influencer'); 
    // $app->deleteCookie('SessionKey','/Influencer');  

    $app->setCookie('UserData', '/', '0 minutes', '/Influencer'); 
    $app->setCookie('SessionKey', '/', '0 minutes', '/Influencer'); 
    $app->setCookie('LoginType', "/", '0 minutes', '/Influencer'); 
 

     $result = array(
        "status" => true ,
        "redirect" => "LOGOUT" 
    );

     return  getJsonResponse($app,$result);  
        
   
 

}
?>