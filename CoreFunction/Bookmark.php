<?php 
//#entry point  
function getBookmarkListing($app  )
{
  
    $reqParam = getJsonRequest($app); 
    $userInfo = getUserSessionInfo($app) ;    
    getJsonResponse($app, getBookmark($reqParam, $userInfo  )  ); 
}
 

function getBookmark($reqParam , $userInfo ){  
 
    $sqlStatement   = 'SELECT 
                          b.Id   , 
                          b.InfluencerId    ,   
                          b.MerchantId  ,  
                          b.CampaignId   ,  
                          b.Priority   ,  
                          b.CreatedBy    ,   
                          b.CreatedDate as BookmarkDate    ,    
                          c.Name    ,   
                          c.Description    ,   
                          c.PromoTitle    ,   
                          c.PromoCaption    ,   
                          c.PromoDescription    ,   
                          c.Price    ,   
                          c.NoOfInfluencer    ,   
                          c.NoOfPosting    ,   
                          c.Charges1    ,   
                          c.Charges1Desc    ,   
                          c.TotalCost    ,   
                          c.FromDate    ,   
                          c.ToDate    ,    
                          c.IsActive    ,    
                          c.CreatedDate        
    FROM Bookmark b , Campaign c ';
    $whereStatement = " where 1=1 and b.CampaignId=c.Id   ";
  
 
    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and  b.Id = '" . $Id . "'";
    }  
   
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . "and  b.InfluencerId = '" . $InfluencerId . "'";
     } 
    $MerchantId = getKeyVal($reqParam, "MerchantId");
    if (!empty($MerchantId)) {
        $whereStatement = $whereStatement . "and  b.MerchantId = '" . $MerchantId . "'";
  }    

   $CampaignId = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $whereStatement = $whereStatement . "and  b.CampaignId = '" . $CampaignId . "'";
   } 

    $Priority = getKeyVal($reqParam, "Priority");
    if (!empty($Priority)) {
        $whereStatement = $whereStatement . "and  b.Priority like '%" . $Priority . "%'";
    }    
    $CreatedBy = getKeyVal($reqParam, "CreatedBy"); 
    $CreatedBy = $userInfo['Id']; 
    if (!empty($CreatedBy)) {
        $whereStatement = $whereStatement . "and  b.CreatedBy like '%" . $CreatedBy . "%'";
    } 
 
    $CreatedDate = getKeyVal($reqParam, "CreatedDate");
    if (!empty($CreatedDate)) {
        $whereStatement = $whereStatement . "and  b.CreatedDate  = '" . $CreatedDate . "'";
    }   


    $paginationLimit = " ";
    $maxRecord = getKeyVal($reqParam, "maxRecord");
    if (!empty($maxRecord)) {
        $paginationLimit = $paginationLimit . " LIMIT " . $maxRecord  ;
    }  
    $recordIndex = getKeyVal($reqParam, "recordIndex");
    if (!empty($recordIndex)) {
        $paginationLimit = $paginationLimit . " OFFSET " . $recordIndex  ;
    }  

    $sqlStatement = $sqlStatement . $whereStatement . $paginationLimit . " ; ";
    return  queryDB($sqlStatement); 
   //var_dump($sqlStatement); 
    
}
 


function getBookmarkBasic($reqParam , $userInfo ){  
 
    $sqlStatement   = 'SELECT 
                          b.Id   , 
                          b.InfluencerId    ,   
                          b.MerchantId  ,  
                          b.CampaignId   ,  
                          b.Priority   ,  
                          b.CreatedBy    ,   
                          b.CreatedDate as BookmarkDate         
                           
    FROM Bookmark b   ';
    $whereStatement = " where 1=1   "; 
 
    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and  b.Id = '" . $Id . "'";
    }  
   
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . "and  b.InfluencerId = '" . $InfluencerId . "'";
     } 
    $MerchantId = getKeyVal($reqParam, "MerchantId");
    if (!empty($MerchantId)) {
        $whereStatement = $whereStatement . "and  b.MerchantId = '" . $MerchantId . "'";
  }    

   $CampaignId = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $whereStatement = $whereStatement . "and  b.CampaignId = '" . $CampaignId . "'";
   } 

    $Priority = getKeyVal($reqParam, "Priority");
    if (!empty($Priority)) {
        $whereStatement = $whereStatement . "and  b.Priority like '%" . $Priority . "%'";
    }    
    $CreatedBy = getKeyVal($reqParam, "CreatedBy"); 
    $CreatedBy = $userInfo['Id']; 
    if (!empty($CreatedBy)) {
        $whereStatement = $whereStatement . "and  b.CreatedBy like '%" . $CreatedBy . "%'";
    } 
 
    $CreatedDate = getKeyVal($reqParam, "CreatedDate");
    if (!empty($CreatedDate)) {
        $whereStatement = $whereStatement . "and  b.CreatedDate  = '" . $CreatedDate . "'";
    }  
   
    $paginationLimit = " ";
    $maxRecord = getKeyVal($reqParam, "maxRecord");
    if (!empty($maxRecord)) {
        $paginationLimit = $paginationLimit . " LIMIT " . $maxRecord  ;
    }  
    $recordIndex = getKeyVal($reqParam, "recordIndex");
    if (!empty($recordIndex)) {
        $paginationLimit = $paginationLimit . " OFFSET " . $recordIndex  ;
    }  

    $sqlStatement = $sqlStatement . $whereStatement . $paginationLimit . " ; "; 
    return  queryDB($sqlStatement); 
   //var_dump($sqlStatement); 
    
}
 


//#entry point 
function  editBookmark($app , $BookmarkId    ){
  
    $reqParam = getJsonRequest($app);   

    $userInfo = getUserSessionInfo($app) ;     

  
    $CampaignId  = getKeyVal($reqParam, "CampaignId");   
    checkEmpty($app,"CampaignId", $CampaignId) ; 

    $sqlStatement   = " Update  Bookmark  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $BookmarkId . "' ;"; 
     
    $CampaignId = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $setStatement = $setStatement . " CampaignId='" . $CampaignId . "' ,";
    } 
 
   
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $setStatement = $setStatement . " InfluencerId='" . $InfluencerId . "' ,";
    } 
    $MerchantId = getKeyVal($reqParam, "MerchantId");
    if (!empty($MerchantId)) {
        $setStatement = $setStatement . " MerchantId='" . $MerchantId . "' ,";
    } 
    $Priority = getKeyVal($reqParam, "Priority");
    if (!empty($Priority)) {
        $setStatement = $setStatement . " Priority='" . $Priority . "' ,";
    }  
    $CreatedBy = "*";
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    } 
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "BookmarkId" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

 
//#entry point 
function  deleteBookmark($app, $BookmarkId) 
{


    $reqParam = getJsonRequest($app);  
     $userInfo = getUserSessionInfo($app) ;    

    checkEmpty($app,"BookmarkId", $BookmarkId) ;  
 
    $sqlStatement = "DELETE FROM Bookmark WHERE Id='" . $BookmarkId . "'";
    $dataResult = crudDB($sqlStatement);   
    $result            = array(
        "status" => true,
    ); 
    getJsonResponse($app, $result); 
}
 
//#entry point  
function addBookmark($app  ){
    $reqParam = getJsonRequest($app) ;  
 
    $userInfo = getUserSessionInfo($app) ;  
    $CampaignId  = getKeyVal($reqParam, "CampaignId");   
    checkEmpty($app,"CampaignId", $CampaignId) ;   

   $filterParam = new StdClass;
   $filterParam->CampaignId =$CampaignId;   

    $result = getBookmarkBasic($filterParam, $userInfo  ) ; 

     if (!empty($result[0]["Id"]) ) {
        return processErrorMessage($app, "DUPLICATE: Bookmark existed. ", "001");
    }


  
    $sqlStatement   = "INSERT INTO  Bookmark (    
                        `InfluencerId` ,  
                        `MerchantId`  , 
                        `CampaignId`  ,   
                        `Priority`  ,   
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
    
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $valueStatement = $valueStatement . "'" . $InfluencerId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $MerchantId = getKeyVal($reqParam, "MerchantId");
    if (!empty($MerchantId)) {
        $valueStatement = $valueStatement . "'" . $MerchantId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    $CampaignId = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $valueStatement = $valueStatement . "'" . $CampaignId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $Priority = getKeyVal($reqParam, "Priority");
    if (!empty($Priority)) {
        $valueStatement = $valueStatement . "'" . $Priority . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
 
 
//createdby
    $CreatedBy = $userInfo['Id']; 
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "BookmarkId" =>$mysqli->insert_id  );
    getJsonResponse($app,$result); 

}
 


 
 
 
 
?>