  
 <?php 

function getCommonConfigFacebook(){

	return  [
	  'app_id' => '1763919787187412',
	  'app_secret' => '43afd8849b1bf24067001fa8ae3e15c2',
	  'default_graph_version' => 'v2.6',
	]; 
} 



function getCommonFacebookInstance(){ 

$fb =null; 
	try {
		//not first time
		$serializedFb = implode("", @file("serializedFb"));
		$fb = unserialize($serializedFb);

	} catch (Exception $e) { 
		// first time
		$fb = new Facebook\Facebook( getCommonConfigFacebook()); 

		$serializedFb = serialize($fb); 
		$path="serializedFb"; 
		$fp = fopen($path, "w");
		fwrite($fp, $serializedFb);
		fclose($fp);
	}
  
  return $fb;
} 

function getProfilePageLikes($app,$fanPageProfileId,$accessToken){
	$fb =  getCommonFacebookInstance() ;
 	$path=  "/" . $fanPageProfileId . "?&fields=fan_count";
	try { 
		$response = $fb->get( $path   , $accessToken );  
	    $decodedBody = $response->getDecodedBody(); // for Array resonse 
        return $decodedBody['summary']['total_count'];  
	} catch(Facebook\Exceptions\FacebookResponseException $e) { 
            // return processErrorMessage($app,'getProfilePageLikes FacebookGraphError: ' . $e->getMessage(), "010");
            return 0;
	} catch(Facebook\Exceptions\FacebookSDKException $e) { 
            return processErrorMessage($app,'getProfilePageLikes FacebookError: ' . $e->getMessage(), "010"); 
	}


     
}


 

function getProfilePageFriends($app,$userProfileId,$accessToken){
	$fb =  getCommonFacebookInstance() ;
	$path =  "/" . $userProfileId . "/friends?fields=id,name";
	// $path =  "/me"  . "/friends"; 
    // var_export(  $path   );

	try { 
		$response = $fb->get($path  , $accessToken );    
	    $decodedBody = $response->getDecodedBody(); // for Array resonse 
     	 return $decodedBody['summary']['total_count'];  
	} catch(Facebook\Exceptions\FacebookResponseException $e) { 
            // return processErrorMessage($app,'getProfilePageFriends FacebookGraphError: ' . $e->getMessage(), "010");
            return 0;
	} catch(Facebook\Exceptions\FacebookSDKException $e) { 
            return processErrorMessage($app,' getProfilePageFriends FacebookError: ' . $e->getMessage(), "010"); 
	} 
     
}


 
function getPostStatus($app,$postId,$type,$accessToken){
	$fb =  getCommonFacebookInstance() ;
	// type=sharedposts/comments/likes
	$path =  "/" . $postId . "/" .$type . "?limit=1&summary=1"  ; 

	try { 
		$response = $fb->get($path  , $accessToken );    
	    $decodedBody = $response->getDecodedBody(); // for Array resonse 
     	 return $decodedBody['summary']['total_count'];  
	} catch(Facebook\Exceptions\FacebookResponseException $e) { 
            // return processErrorMessage($app,'getProfilePageFriends FacebookGraphError: ' . $e->getMessage(), "010");
            return 0;
	} catch(Facebook\Exceptions\FacebookSDKException $e) { 
            return processErrorMessage($app,' getProfilePageFriends FacebookError: ' . $e->getMessage(), "010"); 
	} 
     
}

function updateUserFacebookInfluence($app, $dbUserId ,$userFbId ,$SessionKey ){
 
//FETCH STATS
	$TotalFollowerFb= getProfilePageLikes($app, $userFbId,$SessionKey);
	$TotalFriendFb=  getProfilePageFriends($app,$userFbId,$SessionKey);
	// var_export(    $noFollowers  );
	// var_export(    $noFriends  );
 

     // $TotalPostComments= getPostStatus($app,'1741474879461416','comments',$SessionKey);
     // var_export(    $TotalPostComments  );
     // $TotalPostLikes= getPostStatus($app,'1741474879461416','likes',$SessionKey);
     // var_export(    $TotalPostLikes  );
     // $TotalPostShares= getPostStatus($app,'1741474879461416','sharedposts',$SessionKey);
     // var_export(    $TotalPostShares  );
 

//UPDATE COUNTERS
    $UserInfluence = new StdClass;
    $UserInfluence->TotalFriendFb = $TotalFriendFb;     
    $UserInfluence->TotalFollowerFb = $TotalFollowerFb;         
    $UserInfluence->IsFbConnected = "1"; 

    $reqParam = new StdClass;
    $reqParam->UserInfluence = $UserInfluence;      


	 // var_export(    $reqParam  );
     $response = editUserInfluence($reqParam, $dbUserId);
	 // var_export(    $response  );
     return $response;
 

}
function extendAccessTokenPeriod($app,$fb,$AccessToken   ){

	// Sets the default fallback access token so we don't have to pass it to each request
	$fb->setDefaultAccessToken($AccessToken); 
	 
	 
	 
	$client = $fb->getOAuth2Client();
	try {
	  // Returns a long-lived access token
	  $accessToken = $client->getLongLivedAccessToken($AccessToken );
	  return $accessToken ;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  // There was an error communicating with Graph 
	  return processErrorMessage($app,'FacebookGraphError: ' . $e->getMessage(), "010"); 
	   
	}
	 

 

} 
function checkAccessTokenstatus($app,$reqParam){

	$fb =  getCommonFacebookInstance() ;
	$AccessToken =   getKeyVal($reqParam, "SessionKey");   
	$user =null;
  
	try {
		// Returns a `Facebook\FacebookResponse` object
		$response = $fb->get('/me', $AccessToken );
		$user = $response->getGraphUser();
		$accessToken=  extendAccessTokenPeriod($app,$fb,$AccessToken   );
	    $result = array( 
	        "accessToken" => $accessToken,
	        "userName" =>  $user -> getName(), 
	    );  
 
		return $result;
	} catch(Facebook\Exceptions\FacebookResponseException $e) { 
            return processErrorMessage($app,'FacebookGraphError: ' . $e->getMessage(), "010");
	} catch(Facebook\Exceptions\FacebookSDKException $e) { 
            return processErrorMessage($app,'FacebookError: ' . $e->getMessage(), "010"); 
	}
 
 
} 