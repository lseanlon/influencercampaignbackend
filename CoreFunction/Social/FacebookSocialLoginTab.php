

 <?php 
// require_once(    '/public_html/Influencer/API/lib/FacebookPhpSdk/src/Facebook/autoload.php'); 
$apipath = realpath(__DIR__ . '/../..');
require_once $apipath . '/lib/FacebookPhpSdk/src/Facebook/autoload.php';
$fb = new Facebook\Facebook([
  'app_id' => '1763919787187412',
  'app_secret' => '43afd8849b1bf24067001fa8ae3e15c2',
  'default_graph_version' => 'v2.6',
]);

$helper = $fb->getPageTabHelper();

try {
  $accessToken = $helper->getAccessToken();
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

if (! isset($accessToken)) {
  echo 'No OAuth data could be obtained from the signed request. User has not authorized your app yet.';
  exit;
}

// Logged in
echo '<h3>Page ID</h3>';
var_dump($helper->getPageId());

echo '<h3>User is admin of page</h3>';
var_dump($helper->isAdmin());

echo '<h3>Signed Request</h3>';
var_dump($helper->getSignedRequest());

echo '<h3>Access Token</h3>';
var_dump($accessToken->getValue());