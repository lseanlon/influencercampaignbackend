  
 <?php 

 
function checkGoogleAccessTokenstatus($app, $tokenKey){

	$url  = "https://www.googleapis.com/oauth2/v3/tokeninfo?id_token="  . $tokenKey ; 
	 $ch = curl_init();
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_HEADER, 1); 
    curl_setopt($ch, CURLOPT_URL, $url);

	$response = curl_exec($ch);
 

	// Then, after your curl_exec call:
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$header = substr($response, 0, $header_size);
	$bodyResp = substr($response, $header_size);

  	$bodyResp  = json_decode($bodyResp); 
 
	

     $errorExist =  getKeyVal($bodyResp, "error_description");
     if(!empty( $errorExist)){ 
        return processErrorMessage($app, "Invalid google user login ", "001");
     }

	// var_dump($bodyResp ) ; 
 
	return $bodyResp;
} 