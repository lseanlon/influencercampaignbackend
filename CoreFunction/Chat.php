<?php
 function checkAccessAdmin($userRec  )
{
     //ensure only return this to admin 
    if(empty( $userRec) ||  empty($userRec["UserType"]["Name"] ) || strtoupper($userRec["UserType"]["Name"] )!="ADMIN" ){  
         return processErrorMessage($app, "Service denied ", "001");
    }


}
//#entry point  
function getChat($app  )
{
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;  
 
    getJsonResponse($app, getCommonChat($reqParam )  ); 
}



//#entry point 
function  deleteChat($app, $ChatId) 
{

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;      

    checkEmpty($app,"ChatId", $ChatId) ;  

    //delete Chat record
    $sqlStatement      = "DELETE FROM CampaignRunningChat WHERE Id='" . $ChatId . "'";
    $dataResult      = crudDB($sqlStatement);  
 
    $result            = array(
        "status" => true,
    );


 
    getJsonResponse($app, $result); 
}
 
//#entry point  
function addChat($app  ){
    $reqParam = getJsonRequest($app) ;
   
   $userInfo = getUserSessionInfo($app) ;    
  
    $CampaignRunningId  = getKeyVal($reqParam, "CampaignRunningId"); 
    $SenderId  = getKeyVal($reqParam, "SenderId");
    $ReceiverId  = getKeyVal($reqParam, "ReceiverId");
    $Message  = getKeyVal($reqParam, "Message");
 
    checkEmpty($app,"CampaignRunningId", $CampaignRunningId) ;
    checkEmpty($app,"SenderId", $SenderId) ;
    checkEmpty($app,"ReceiverId", $ReceiverId) ;
    checkEmpty($app,"Message", $Message) ;
 


    $sqlStatement   = "INSERT INTO  CampaignRunningChat (    
                        `CampaignRunningId` ,  
                        `SenderId`  , 
                        `ReceiverId`  ,   
                        `Message`  ,   
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $valueStatement = $valueStatement . "'" . $CampaignRunningId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    $SenderId = getKeyVal($reqParam, "SenderId");
    if (!empty($SenderId)) {
        $valueStatement = $valueStatement . "'" . $SenderId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    $ReceiverId = getKeyVal($reqParam, "ReceiverId");
    if (!empty($ReceiverId)) {
        $valueStatement = $valueStatement . "'" . $ReceiverId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
     }
    $Message = getKeyVal($reqParam, "Message");
    if (!empty($Message)) {
        $valueStatement = $valueStatement . "'" . $Message . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }  

//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "ChatId" =>$mysqli->insert_id  );
    getJsonResponse($app,$result);  

}

function getCommonChat($reqParam ){  
 
 

    $sqlStatement   = 'SELECT 
                        `Id`  ,  
                        `CampaignRunningId` ,  
                        `SenderId`  , 
                        `ReceiverId`  , 
                        `Message`  ,    
                        `CreatedBy`   ,    
                        `CreatedDate`         

    FROM CampaignRunningChat ';
    $whereStatement = " where 1=1 ";
 
     
    $filterId = getKeyVal($reqParam, "Id");
    if (!empty($filterId)) { 
        $whereStatement = $whereStatement . " and Id = '" . $filterId . "'";
    }
   
     
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $whereStatement = $whereStatement . " and CampaignRunningId = '" . $CampaignRunningId . "'";
    }  
 
    $SenderId = getKeyVal($reqParam, "SenderId");
    if (!empty($SenderId)) {
        $whereStatement = $whereStatement . " and SenderId = '" . $SenderId . "'";
    }  
 
    $ReceiverId = getKeyVal($reqParam, "ReceiverId");
    if (!empty($ReceiverId)) {
        $whereStatement = $whereStatement . " and ReceiverId = '" . $ReceiverId . "'";
    }   
     
     
    $Message = getKeyVal($reqParam, "Message");
    if (!empty($Message)) {
        $whereStatement = $whereStatement . " and Message like '%" . $Message . "%'";
    }  
 
      
//CreatedDate
    $filterCreatedDate = getKeyVal($reqParam, "CreatedDate");
    if (!empty($filterCreatedDate)) {
        $whereStatement = $whereStatement . " and CreatedDate like '%" . $filterCreatedDate . "%'";
    }
//createdby
    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy) ) {
        $whereStatement = $whereStatement . " and CreatedBy like '%" . $filterCreatedBy . "%'";
    }
  $orderStatement  = " ORDER BY CreatedDate ASC ";
  $sqlStatement = $sqlStatement . $whereStatement .   $orderStatement . " ; ";
   return  queryDB($sqlStatement); 

     //  var_dump( $sqlStatement); 
}
 
 
 
 
?>