<?php
 


//#entry point  
function getNotification($app  )
{
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ; 
    // checkAccessAdmin($userInfo  ) ;

 
    getJsonResponse($app, getCommonNotification($reqParam )  ); 
}



//#entry point 
function  deleteNotification($app, $NotificationId) 
{

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;    
    // checkAccessAdmin($userInfo  ) ;
 

    checkEmpty($app,"NotificationId", $NotificationId) ;  

    //delete Notification record
    $sqlStatement      = "DELETE FROM Notification WHERE Id='" . $NotificationId . "'";
    $dataResult      = crudDB($sqlStatement);  
 
    $result            = array(
        "status" => true,
    );


 
    getJsonResponse($app, $result); 
}


//#entry point 
function  editNotification($app , $NotificationId    ){
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;
    // checkAccessAdmin($userInfo  ) ;  
 
  
    $Description  = getKeyVal($reqParam, "Description"); 
    $Code  = getKeyVal($reqParam, "Code"); 
 
    checkEmpty($app,"Description", $Description) ;
    checkEmpty($app,"Code", $Code) ; 

    $sqlStatement   = " Update  Notification  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $NotificationId . "' ;";

  
     
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $setStatement = $setStatement . " Description='" . $Description . "' ,";
    }
   
    $Code = getKeyVal($reqParam, "Code");
    if (!empty($Code)) {
        $setStatement = $setStatement . " Code='" . $Code . "' ,";
    }  
    $CreatedBy = $userInfo["Id"];
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "NotificationId" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

//#entry point  
function addNotification($app  ){
    $reqParam = getJsonRequest($app) ;
   
    $userInfo = getUserSessionInfo($app) ;  
    // checkAccessAdmin($userInfo  ) ; 
  
     $result = getCommonNotification(    $reqParam   ) ;  
     if (!empty($result[0]["Id"]) ) {
        return processErrorMessage($app, "DUPLICATE: Notification existed. ", "001");
    }


    $Description  = getKeyVal($reqParam, "Description"); 
    $Code  = getKeyVal($reqParam, "Code"); 
    checkEmpty($app,"Description", $Description) ;
    checkEmpty($app,"Code", $Code) ; 


    $sqlStatement   = "INSERT INTO  Notification (
                        `Code`  ,      
                        `Description` ,  
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
    $Code = getKeyVal($reqParam, "Code");
    if (!empty($Code)) {
        $valueStatement = $valueStatement . "'" . $Code . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $valueStatement = $valueStatement . "'" . $Description . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

 
//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "NotificationId" =>$mysqli->insert_id  );
    getJsonResponse($app,$result); 

}

function getCommonNotification($reqParam ){  
  
    $sqlStatement   = 'SELECT 
                        `Id`  ,  
                        `Code`  ,  
                        `Description` ,  
                        `CreatedBy`   ,    
                        `CreatedDate`         

    FROM Notification ';
    $whereStatement = " where 1=1 ";
 
      
    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and Id = '" . $Id . "'";
    }
   
    $Code = getKeyVal($reqParam, "Code");
    if (!empty($Code)) {
        $whereStatement = $whereStatement . " and Code = '" . $Code . "'";
    }
   
     
    $filterDescription = getKeyVal($reqParam, "Description");
    if (!empty($filterDescription)) {
        $whereStatement = $whereStatement . " and Description like '%" . $filterDescription . "%'";
    }  
 

//createddate
    $filterMinCreatedDate = getKeyVal($reqParam, "MinCreatedDate");
    if (!empty($filterMinCreatedDate)) {
        $whereStatement = $whereStatement . " and  CreatedDate >= '" . $filterMinCreatedDate . "'";
    }
    $filterMaxCreatedDate = getKeyVal($reqParam, "MaxCreatedDate");
    if (!empty($filterMaxCreatedDate)) {
        $whereStatement = $whereStatement . " and  CreatedDate <= '" . $filterMaxCreatedDate . "'";
    } 
//createdby
    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . " and CreatedBy like '%" . $filterCreatedBy . "%'";
    }
 
   
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
   return  queryDB($sqlStatement); 
    
}
 
 
 
 
?>