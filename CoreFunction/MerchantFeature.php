<?php  

// #entry 
function getMerchantDashboard( $app)
{   
//--INPUT PARAM --
// **MerchantId - USERID 
// CampaignId    
// **StatusCodeList  - completed / *progress
// MinDate, MaxDate 
//
//--OUTPUT PARAM--  
// {
//  "TotalSpend" :0.00,
//  "TotalLikesFav" :0,
//  "TotalShares" :0,
//  "CampaignDatas": [] 
// }
    $userRec = getUserSessionInfo($app) ;  
    $reqParam = getJsonRequest($app);    
    $reqParam ->MerchantId =   $userRec["Id"]; 
    $listCampaign = getMerchantDashboardSummary(  $reqParam   ) ; 

   return getJsonResponse($app, $listCampaign ); 
}





function getMerchantDashboardSummary(  $reqParam  )
{    
 
    // 'REQUE'  'APPRO'  'NOPAY'  'PROGR'  'COMPL'  'INACT'  
    //handle default case
    $StatusCodeList = array(  "PROGR", "COMPL" ); 
    $StatusCode = getKeyVal($reqParam, "StatusCodeList");   
    if(!empty($StatusCode)){  
         $StatusCodeList = array(  $StatusCode );  
    }

    $reqParam ->StatusCodeList = $StatusCodeList;
    $campaignRunningSummary=getCampaignRunningCount(  $reqParam , $StatusCodeList); 



    //get list of campaignid. eg[1,2,3]
    $campaignIdList=getCampaignIdRunningList(  $reqParam  ); 
  
    $reqParam->CampaignList =  $campaignIdList;  
    //get each media aggregated statistic  
     $facebookStatistic=getAggregatedFacebookStatistic(  $reqParam  ) ;


    //get amount of total likes fav  - by adding all media stat 
     $TotalLikesFav=floatval($facebookStatistic["TotalLikes"])+ floatval($facebookStatistic["TotalPostReactions"] )+ floatval($facebookStatistic["TotalPostComments"] ); 

    //get amount of total shares- by adding all media stat
     $TotalShares=floatval($facebookStatistic["TotalPostShare"])+ floatval($facebookStatistic["TotalProductPageShares"] )+ floatval($facebookStatistic["TotalProductPageFollows"] );


    //get list of campaign info [{campaininfo1},{campaigninfo2}]
    if( !empty($campaignIdList)){ 
      $filterListing = new StdClass;
      $filterListing->CampaignList =  $campaignIdList;   
      // $filterListing->NoExtraData =  "TRUE";    
      $campaignDataList=getCampaignDataListing(  $filterListing  ); 
    }
 
   $result = array( 
        "TotalSpend" => $campaignRunningSummary["TotalAgreedTotalCost"],
        "TotalPost" => $campaignRunningSummary["TotalAgreedTotalPost"],
        "TotalLikesFav" =>  $TotalLikesFav, 
        "TotalShares" =>  $TotalShares,
        "CampaignDatas" => $campaignDataList  
    );
 
    return $result;
   
}


function getAggregatedFacebookStatistic(  $reqParam  )
{     

    $sqlStatement   = 'SELECT   
                             SUM( `TotalPostComment` ) AS TotalPostComments  ,
                             SUM(`TotalPostReaction` ) AS TotalPostReactions  ,
                             SUM(`TotalLike`) AS TotalLikes   ,
                             SUM(`TotalPostShare`) AS TotalPostShare   , 
                             SUM(`TotalProductPageShare`) AS TotalProductPageShares   ,
                             SUM(`TotalProductPageFollow`) AS TotalProductPageFollows     
                       FROM CampaignFacebook';
    $whereStatement = " where 1=1 " ;

     $CampaignList= getKeyVal($reqParam, "CampaignList");
        if (!empty($CampaignList)) {
        $limit = count($CampaignList);
          foreach ($CampaignList as $keyy => $valuee) {  
                    if(!empty( $valuee )){ 
                            if($keyy == 0 ){ 
                              $whereStatement = $whereStatement . " and (   CampaignId = '" . $valuee . "' ";
                            }else{
                             $whereStatement = $whereStatement . " OR  CampaignId = '" . $valuee . "' ";
                            }

                            if($keyy == intval($limit) - 1 ){  
                              $whereStatement = $whereStatement . "  ) ";
                            }
                             
                   } 
            }
        }
    
 

    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
    $data         = queryDB($sqlStatement);
    return $data[0];
}



function getAggregatedTwitterStatistic(  $reqParam  )
{    
    return null;
}

function getAggregatedInstagramStatistic(  $reqParam  )
{    
    return null;
}

 function getInfluencerListing($app  ) 
{ 
 

    $reqParam = getJsonRequest($app);   
    $userInfo = getUserSessionInfo($app) ;  

    $filterUser = new StdClass;
    $filterUser->IsFeatureUnlockedInfluencer =$userInfo["IsFeatureUnlockedInfluencer"];   
    //Influencer only
    $filterUser->UserTypeId ="1";   
    $influencerList =  getUserFilter($filterUser ) ; 

    $extraFilter = new StdClass;
        //iterate each row, get all dependecies 
        foreach ($influencerList as $key =>&$rec  ) { 
         
            $userId=$rec["Id"]; 
            $rec  = [] ; 
            $rec = getCommonUserProfile($extraFilter, $userId, null) ;  
        }
 
    
    getJsonResponse($app,  $influencerList );   
 

}
        
 function activatMerchantFeature($app,$featureType) 
{ 

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;  

    checkEmpty($app,"featureType", $featureType) ;



    //check feattype CATEGORY
    $INFLUNCER_ACCESS="FULL-INFLUENCER-ACCESS";
    if(isComparedEqual( $INFLUNCER_ACCESS,$featureType)){
        if(isComparedEqual(  $userInfo["IsFeatureUnlockedInfluencer"] , "1")){ 
            return processErrorMessage($app, "Already unlocked : FULL-INFLUENCER-ACCESS", "001"); 
        }

     // get all campaigns by a user
        $filterCampaign = new StdClass;
        $filterCampaign->CreatedBy = $userInfo["Id"] ; 
        $campaignList= getCampaign($filterCampaign );
 
    //iterate each row, check not empty charges,
        $updCampaignStatus=false;
          foreach ($campaignList as $key =>&$rec  ) { 
            $campaignPrice=floatval($rec["Price"] ); 
            $unlockFeeCharge=$campaignPrice * floatval(0.05);  
            $campaignTotalCost=floatval($rec["TotalCost"] ) +  $unlockFeeCharge; 
            $campaignId=$rec["Id"];

            // add new charges into non empty charges, with feature cost.
             if(floatval($rec["Charges1"] )<=0){
                    $rec["Charges1"] =$unlockFeeCharge;
                    $rec["Charges1Desc"] = $INFLUNCER_ACCESS;
             }else  if(floatval($rec["Charges2"] )<=0){
                    $rec["Charges2"] =$unlockFeeCharge;
                    $rec["Charges2Desc"] = $INFLUNCER_ACCESS;

             }else if(floatval($rec["Charges3"] )<=0){
                    $rec["Charges3"] =$unlockFeeCharge;
                    $rec["Charges3Desc"] = $INFLUNCER_ACCESS; 
             }
              $rec["TotalCost"]=$campaignTotalCost;
           
              $rec = json_decode(  json_encode($rec ) ,FALSE); 

               //update each row in db
              $updCampaignStatus= editCampaignValue ($rec , $campaignId    );
        }
 

    }
    
        //Update user feature  flag in db   
        $addUserParam = new StdClass;
        $addUserParam->IsFeatureUnlockedInfluencer ="1" ;  
        $updUserStatus= editUserCommon($addUserParam,  $userInfo["Id"] , null);  
 
          $result = array( 
            "updUserStatus" => $updUserStatus,
            "updCampaignStatus" => $updCampaignStatus,
            "status" => empty($updUser) && empty($updCampaignStatus) ?  false:true
        );

        getJsonResponse($app,$result);  


}
  
?>