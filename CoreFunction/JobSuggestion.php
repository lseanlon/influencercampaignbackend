<?php
 function checkAccessAdmin($userRec  )
{
     //ensure only return this to admin 
    if(empty( $userRec) ||  empty($userRec["UserType"]["Name"] ) || strtoupper($userRec["UserType"]["Name"] )!="ADMIN" ){

         return processErrorMessage($app, "Service denied ", "001");
    }


}
//#entry point  
function getJobSuggestion($app  )
{
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;  
    getJsonResponse($app, getCommonJobSuggestion($reqParam )  ); 
}



//#entry point 
function  deleteJobSuggestion($app, $JobSuggestionId) 
{

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;    
    // checkAccessAdmin($userInfo  ) ;
 

    checkEmpty($app,"JobSuggestionId", $JobSuggestionId) ;  

    //delete campaign record
    $sqlStatement      = "DELETE FROM JobSuggestion WHERE Id='" . $JobSuggestionId . "'";
    $dataResult = crudDB($sqlStatement);   
    $result = array(     "status" => true    );


 
    getJsonResponse($app, $result); 
}


//#entry point 
function  editJobSuggestion($app , $JobSuggestionId    ){
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;
    // checkAccessAdmin($userInfo  ) ; 
 
 
    $Id  = getKeyVal($reqParam, "Id"); 
    $CampaignId  = getKeyVal($reqParam, "NotificationId"); 
    $NotificationId  = getKeyVal($reqParam, "NotificationCode");
    $FromUserId  = getKeyVal($reqParam, "FromUserId"); 
    $ToUserId  = getKeyVal($reqParam, "ToUserId"); 
    $Description  = getKeyVal($reqParam, "Description"); 
 
    checkEmpty($app,"Id", $Id) ; 
  

    $sqlStatement   = " Update  JobSuggestion  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $JobSuggestionId . "' ;";

             

    $CampaignId = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $setStatement = $setStatement . " CampaignId='" . $CampaignId . "' ,";
    }
    $NotificationId = getKeyVal($reqParam, "NotificationId");
    if (!empty($NotificationId)) {
        $setStatement = $setStatement . " NotificationId='" . $NotificationId . "' ,";
    }
    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $setStatement = $setStatement . " FromUserId='" . $FromUserId . "' ,";
    }
      
    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $setStatement = $setStatement . " ToUserId='" . $ToUserId . "' ,";
    }
    
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $setStatement = $setStatement . " Description='" . $Description . "' ,";
    }

    $CreatedBy = $userInfo["Id"];
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "result" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

//#entry point  
function addJobSuggestion($app  ){
    $reqParam = getJsonRequest($app) ;
   
   $userInfo = getUserSessionInfo($app) ;    
 
    $CampaignId  = getKeyVal($reqParam, "CampaignId"); 
    $NotificationId  = getKeyVal($reqParam, "NotificationId");
    $FromUserId  = getKeyVal($reqParam, "FromUserId"); 
    $ToUserId  = getKeyVal($reqParam, "ToUserId"); 
    $Description  = getKeyVal($reqParam, "Description");  

   
    checkEmpty($app,"CampaignId", $CampaignId) ;
    checkEmpty($app,"ToUserId", $ToUserId) ;    


    $sqlStatement   = "INSERT INTO  JobSuggestion (  
                        `CampaignId`  ,  
                        `NotificationId`  ,   
                        `FromUserId` ,  
                        `ToUserId`  ,       
                        `Description`  ,        
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  


    $CampaignId = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $valueStatement = $valueStatement . "'" . $CampaignId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $NotificationId = getKeyVal($reqParam, "NotificationId");
    if (!empty($NotificationId)) {
        $valueStatement = $valueStatement . "'" . $NotificationId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }



    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $valueStatement = $valueStatement . "'" . $FromUserId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }


    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $valueStatement = $valueStatement . "'" . $ToUserId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
 
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $valueStatement = $valueStatement . "'" . $Description . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
 

//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "JobSuggestionId" =>$mysqli->insert_id  );
    // var_dump( ($sqlStatement . $valueStatement)) ; 
    getJsonResponse($app,$result); 

  

}

function getCommonJobSuggestion($reqParam ){    

    $sqlStatement   = 'SELECT 
                        j.Id  ,  
                        j.CampaignId ,  
                        j.NotificationId  , 
                        j.ToUserId  , 
                        j.FromUserId  ,     
                        j.Description  ,    
                        j.CreatedBy   ,    
                        j.CreatedDate     ,   
                        toUser.Name as ToUserName    , 
                        toUser.Location  as ToLocation    , 
                        fromUser.Name as FromUserName    , 
                        fromUser.Location  as FromLocation    
                        c.Name as CampaignName  , 
                        c.Description as CampaignDescription  , 
                        c.PromoTitle as CampaignTitle  ,  
                        c.IsActive as CampaignIsActive  , 
                        c.FromDate as CampaignFromDate , 
                        c.ToDate as CampaignToDate  , 
                        c.ClosingDate as CampaignClosingDate  
    FROM JobSuggestion j, Campaign c, User toUser, User fromUser';
    $whereStatement = " where 1=1  and j.CampaignId = c.Id and j.FromUserId = fromUser.Id and j.ToUserId = toUser.Id  "; 
                

    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and j.Id = '" . $Id . "'";
    }
    
    $CampaignId = getKeyVal($reqParam, "CampaignId");
    if (!empty($CampaignId)) {
        $whereStatement = $whereStatement . " and j.CampaignId = '" . $CampaignId . "'";
    }
    $NotificationId = getKeyVal($reqParam, "NotificationId");
    if (!empty($NotificationId)) {
        $whereStatement = $whereStatement . " and j.NotificationId = '" . $NotificationId . "'";
    }
    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $whereStatement = $whereStatement . " and j.ToUserId = '" . $ToUserId . "'";
    }
     
    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $whereStatement = $whereStatement . " and j.FromUserId = '" . $FromUserId . "'";
    } 
     
     
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $whereStatement = $whereStatement . " and j.Description = '" . $Description . "'";
    } 

//createddate
    $filterMinCreatedDate = getKeyVal($reqParam, "MinCreatedDate");
    if (!empty($filterMinCreatedDate)) {
        $whereStatement = $whereStatement . " and j.CreatedDate >= '" . $filterMinCreatedDate . "'";
    }
    $filterMaxCreatedDate = getKeyVal($reqParam, "MaxCreatedDate");
    if (!empty($filterMaxCreatedDate)) {
        $whereStatement = $whereStatement . " and j.CreatedDate <= '" . $filterMaxCreatedDate . "'";
    } 
//createdby
    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . " and j.CreatedBy like '%" . $filterCreatedBy . "%'";
    }
 
   
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
     return  queryDB($sqlStatement);

      // var_dump($sqlStatement); 

    
}
 
 
 
 
?>