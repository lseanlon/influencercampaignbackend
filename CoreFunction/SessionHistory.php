<?php  


function editSessionHistory($reqParam, $userId)
{
      
     
    if( empty($reqParam)){
        return false;
    }
    $sqlStatement   = " Update  SessionHistory ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE UserId='" . $userId . "' ;";
          
 
   
    $Password = getKeyVal($reqParam, "Password");
    if (!empty($Password)) {
        $setStatement = $setStatement . " Password='" . $Password . "' ,";
    } 
    $SessionKey = getKeyVal($reqParam, "SessionKey");
    if (!empty($SessionKey)) {
        $setStatement = $setStatement . " SessionKey='" . $SessionKey . "' ,";
    } 

    $Status = getKeyVal($reqParam, "Status");
    if (!empty($Status)) {
        $setStatement = $setStatement . " Status='" . $Status . "' ,";
    } 

    $CreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    } 
    $LoginDate = getKeyVal($reqParam, "LoginDate");
    if (!empty($LoginDate)) {
        $setStatement = $setStatement . " LoginDate='" . $LoginDate . "' ,";
    } 
    $LogoutDate = getKeyVal($reqParam, "LogoutDate");
    if (!empty($LogoutDate)) {
        $setStatement = $setStatement . " LogoutDate='" . $LogoutDate . "' ,";
    }  
    
    $setStatement = $setStatement . " CreatedBy=CreatedBy ";
    return crudDB($sqlStatement . $setStatement . $whereStatement); 
    // var_export( $sqlStatement . $valueStatement. $whereStatement ); 
    
}
function addSessionHistory($reqParam)
{ 
    if(empty( $reqParam)){ 
         $reqParam = new StdClass;
    } 
 
    $sqlStatement   = "INSERT INTO  SessionHistory (  
                `UserId`,
                `Password`,
                `SessionKey`,   
                `Status`  ,
                `CreatedBy`   ,
                `LoginDate`     , 
                `LogoutDate`     
            )  ";


    $valueStatement = "VALUES (";
    $UserId  = getKeyVal($reqParam, "UserId");
    if (!empty($UserId)) {
        $valueStatement = $valueStatement . "'" . $UserId . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $Password = getKeyVal($reqParam, "Password");
    if (!empty($Password)) {
        $valueStatement = $valueStatement . "'" . $Password . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    }
    
    $SessionKey = getKeyVal($reqParam, "SessionKey");
    if (!empty($SessionKey)) {
        $valueStatement = $valueStatement . "'" . $SessionKey . "',";
    }else{ 
        $valueStatement = $valueStatement . "'',";
    } 
    $Status = getKeyVal($reqParam, "Status");
    if (!empty($Status)) {
        $valueStatement = $valueStatement . "'" . $Status . "',";
    }else{ 
        $valueStatement = $valueStatement . "'ACTIVE',";
    }
    $CreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }else{ 
        $valueStatement = $valueStatement . "'APIv1',";
    }
    $LoginDate = getKeyVal($reqParam, "LoginDate");
    if (!empty($LoginDate)) {
        $valueStatement = $valueStatement . "'" . $LoginDate . "',";
    }else{ 
        $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "',";
    }
    $LogoutDate = getKeyVal($reqParam, "LogoutDate");
    if (!empty($LogoutDate)) {
        $valueStatement = $valueStatement . "'" . $LogoutDate . "' ";
    }else{ 
        $valueStatement = $valueStatement . "NULL ";
    } 
     
  
    $valueStatement = $valueStatement . " )";
    // var_export( $sqlStatement . $valueStatement ); 
    $mysqli         = crudDB($sqlStatement . $valueStatement);
    return $mysqli->insert_id;
     
}




  
 
?>