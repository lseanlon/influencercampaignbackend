<?php
 function checkAccessAdmin($userRec  )
{
     //ensure only return this to admin 
    if(empty( $userRec) ||  empty($userRec["UserType"]["Name"] ) || strtoupper($userRec["UserType"]["Name"] )!="ADMIN" ){

         return processErrorMessage($app, "Service denied ", "001");
    }


}
//#entry point  
function getVirtualCurrencyTransaction($app  )
{
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ; 
    checkAccessAdmin($userInfo  ) ;  
    getJsonResponse($app, getCommonVirtualCurrencyTransaction($reqParam )  ); 
}



//#entry point 
function  deleteVirtualCurrencyTransaction($app, $virtualCurrencyTransactionId) 
{

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;    
    checkAccessAdmin($userInfo  ) ;
 

    checkEmpty($app,"virtualCurrencyTransactionId", $virtualCurrencyTransactionId) ;  

    //delete campaign record
    $sqlStatement      = "DELETE FROM VirtualCurrencyTransaction WHERE Id='" . $virtualCurrencyTransactionId . "'";
    $dataResult = crudDB($sqlStatement);   
    $result = array(     "status" => true    );


 
    getJsonResponse($app, $result); 
}


//#entry point 
function  editVirtualCurrencyTransaction($app , $virtualCurrencyTransactionId    ){
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;
    checkAccessAdmin($userInfo  ) ; 
      
 
    
  
    $FromUserId  = getKeyVal($reqParam, "FromUserId"); 
    $ToUserId  = getKeyVal($reqParam, "ToUserId");
    $FromUserPaymentId  = getKeyVal($reqParam, "FromUserPaymentId");
    $ToUserPaymentId  = getKeyVal($reqParam, "ToUserPaymentId"); 
    $TransactionType  = getKeyVal($reqParam, "TransactionType");
    $VirtualAmount  = getKeyVal($reqParam, "VirtualAmount");
    $RealAmount  = getKeyVal($reqParam, "RealAmount");
 
    checkEmpty($app,"FromUserId", $FromUserId) ;
    checkEmpty($app,"ToUserId", $ToUserId) ;
    checkEmpty($app,"VirtualAmount", $VirtualAmount) ;
    checkEmpty($app,"RealAmount", $RealAmount) ;

  

    $sqlStatement   = " Update  VirtualCurrencyTransaction  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $virtualCurrencyTransactionId . "' ;";

             
     
    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $setStatement = $setStatement . " FromUserId='" . $FromUserId . "' ,";
    }
      
    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $setStatement = $setStatement . " ToUserId='" . $ToUserId . "' ,";
    }
   
    $FromUserPaymentId = getKeyVal($reqParam, "FromUserPaymentId");
    if (!empty($FromUserPaymentId)) {
        $setStatement = $setStatement . " FromUserPaymentId='" . $FromUserPaymentId . "' ,";
    } 
    $ToUserPaymentId = getKeyVal($reqParam, "ToUserPaymentId");
    if (!empty($ToUserPaymentId)) {
        $setStatement = $setStatement . " ToUserPaymentId='" . $ToUserPaymentId . "' ,";
    } 

    $TransactionType = getKeyVal($reqParam, "TransactionType");
    if (!empty($TransactionType)) {
        $setStatement = $setStatement . " TransactionType='" . $TransactionType . "' ,";
    }
    $RealAmount = getKeyVal($reqParam, "RealAmount");
    if (!empty($RealAmount)) {
        $setStatement = $setStatement . " RealAmount='" . $RealAmount . "' ,";
    }    
     $VirtualAmount = getKeyVal($reqParam, "VirtualAmount");
    if (!empty($VirtualAmount)) {
        $setStatement = $setStatement . " VirtualAmount='" . $VirtualAmount . "' ,";
    } 

    $CreatedBy = $userInfo["Id"];
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "result" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

//#entry point  
function addVirtualCurrencyTransaction($app  ){
    $reqParam = getJsonRequest($app) ;
   
   $userInfo = getUserSessionInfo($app) ;   
 
    

    $FromUserId  = getKeyVal($reqParam, "FromUserId"); 
    $ToUserId  = getKeyVal($reqParam, "ToUserId");
    $FromUserPaymentId  = getKeyVal($reqParam, "FromUserPaymentId");
    $ToUserPaymentId  = getKeyVal($reqParam, "ToUserPaymentId"); 
    $TransactionType  = getKeyVal($reqParam, "TransactionType");
    $VirtualAmount  = getKeyVal($reqParam, "VirtualAmount");
    $RealAmount  = getKeyVal($reqParam, "RealAmount");
 
    checkEmpty($app,"FromUserId", $FromUserId) ;
    checkEmpty($app,"ToUserId", $ToUserId) ;
    checkEmpty($app,"VirtualAmount", $VirtualAmount) ;
    checkEmpty($app,"RealAmount", $RealAmount) ;




    $sqlStatement   = "INSERT INTO  VirtualCurrencyTransaction (     
                        `FromUserId` ,  
                        `ToUserId`  , 
                        `FromUserPaymentId`  , 
                        `ToUserPaymentId`  ,    
                        `TransactionType`  ,   
                        `VirtualAmount`  ,   
                        `RealAmount`  ,     
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $valueStatement = $valueStatement . "'" . $FromUserId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }


    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $valueStatement = $valueStatement . "'" . $ToUserId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }


    $FromUserPaymentId = getKeyVal($reqParam, "FromUserPaymentId");
    if (!empty($FromUserPaymentId)) {
        $valueStatement = $valueStatement . "'" . $FromUserPaymentId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

    $ToUserPaymentId = getKeyVal($reqParam, "ToUserPaymentId");
    if (!empty($ToUserPaymentId)) {
        $valueStatement = $valueStatement . "'" . $ToUserPaymentId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

    $TransactionType = getKeyVal($reqParam, "TransactionType");
    if (!empty($TransactionType)) {
        $valueStatement = $valueStatement . "'" . $TransactionType . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $VirtualAmount = getKeyVal($reqParam, "VirtualAmount");
    if (!empty($VirtualAmount)) {
        $valueStatement = $valueStatement . "'" . $VirtualAmount . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

    $RealAmount = getKeyVal($reqParam, "RealAmount");
    if (!empty($RealAmount)) {
        $valueStatement = $valueStatement . "'" . $RealAmount . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "virtualCurrencyTransactionId" =>$mysqli->insert_id  );
    // var_dump( ($sqlStatement . $valueStatement)) ; 
    getJsonResponse($app,$result); 

    //update the user profile currency amount
    //

}

function getCommonVirtualCurrencyTransaction($reqParam ){  
 

 
 


    $sqlStatement   = 'SELECT 
                        `Id`  ,  
                        `FromUserId` ,  
                        `ToUserId`  , 
                        `FromUserPaymentId`  , 
                        `ToUserPaymentId`  ,    
                        `TransactionType`  ,   
                        `VirtualAmount`  ,   
                        `RealAmount`    ,  
                        `CreatedBy`   ,    
                        `CreatedDate`         

    FROM VirtualCurrencyTransaction  ';
    $whereStatement = " where 1=1 ";
 
    $filterToPaymentId = getKeyVal($reqParam, "ToPaymentId");
    if (!empty($filterToPaymentId)) {
        $whereStatement = $whereStatement . " and ToPaymentId = '" . $filterToPaymentId . "'";
    }
   
     
    $filterFromPaymentId = getKeyVal($reqParam, "FromPaymentId");
    if (!empty($filterFromPaymentId)) {
        $whereStatement = $whereStatement . " and FromPaymentId = '" . $filterFromPaymentId . "'";
    }
    $filterToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($filterToUserId)) {
        $whereStatement = $whereStatement . " and ToUserId = '" . $filterToUserId . "'";
    }
   
     
    $filterFromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($filterFromUserId)) {
        $whereStatement = $whereStatement . " and FromUserId = '" . $filterFromUserId . "'";
    }
   
     
    $filterTransactionType = getKeyVal($reqParam, "TransactionType");
    if (!empty($filterTransactionType)) {
        $whereStatement = $whereStatement . " and TransactionType like '%" . $filterTransactionType . "%'";
    }  

// Virtual Amount
    $filterVirtualAmountMin = getKeyVal($reqParam, "VirtualAmountMin");
    if (!empty($filterVirtualAmountMin)) {
        $whereStatement = $whereStatement . " and cast( a.VirtualAmount  as decimal)  >= cast( '" . $filterVirtualAmountMin . "' as decimal)  ";
    }
      $filterVirtualAmountMax = getKeyVal($reqParam, "VirtualAmountMax");
    if (!empty($filterVirtualAmountMax)) {
        $whereStatement = $whereStatement . " and  cast( a.VirtualAmount  as decimal)  <=  cast( '" . $filterVirtualAmountMax . "'  as decimal)  ";
    }

// RealAmount
    $filterRealAmountMin = getKeyVal($reqParam, "RealAmountMin");
    if (!empty($filterRealAmountMin)) {
        $whereStatement = $whereStatement . " and cast( a.RealAmount  as decimal)  >= cast( '" . $filterRealAmountMin . "' as decimal)  ";
    }
      $filterRealAmountMax = getKeyVal($reqParam, "RealAmountMax");
    if (!empty($filterRealAmountMax)) {
        $whereStatement = $whereStatement . " and  cast( a.RealAmount  as decimal)  <=  cast( '" . $filterRealAmountMax . "'  as decimal)  ";
    } 

//createddate
    $filterMinCreatedDate = getKeyVal($reqParam, "MinCreatedDate");
    if (!empty($filterMinCreatedDate)) {
        $whereStatement = $whereStatement . " and  CreatedDate >= '" . $filterMinCreatedDate . "'";
    }
    $filterMaxCreatedDate = getKeyVal($reqParam, "MaxCreatedDate");
    if (!empty($filterMaxCreatedDate)) {
        $whereStatement = $whereStatement . " and  CreatedDate <= '" . $filterMaxCreatedDate . "'";
    } 
//createdby
    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . " and CreatedBy like '%" . $filterCreatedBy . "%'";
    }
 
   
    $orderStatement = " order by CreatedDate desc";

    $sqlStatement = $sqlStatement . $whereStatement .$orderStatement. " ; ";
    return  queryDB($sqlStatement);

       // var_dump($sqlStatement); 

    
}
 
 
 
 
?>