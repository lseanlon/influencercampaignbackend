<?php 
//#entry point  
function getCampaignRunningMilestoneListing($app  )
{
  
    $reqParam = getJsonRequest($app); 
   // $userInfo = getUserSessionInfo($app) ;    
    getJsonResponse($app, getCampaignRunningMilestone($reqParam )  ); 
}
 

function getCampaignRunningMilestone($reqParam ){ 

 
    $sqlStatement   = 'SELECT 
                          Id   , 
                          CampaignRunningId    ,   
                          MilestoneDescription  ,  
                          MilestoneQuestion   , 
                          MilestoneAnswers   , 
                          Price   ,  
                          Funded    , 
                          Paid    ,         
                          CreatedDate        
    FROM CampaignRunningMilestone   ';
    $whereStatement = " where 1=1   ";
  

    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and  Id = '" . $Id . "'";
    }  
   
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $whereStatement = $whereStatement . "and  CampaignRunningId = '" . $CampaignRunningId . "'";
    }
    $MilestoneDescription = getKeyVal($reqParam, "MilestoneDescription");
    if (!empty($MilestoneDescription)) {
        $whereStatement = $whereStatement . "and  MilestoneDescription like '%" . $MilestoneDescription . "%'";
      } 
    $MilestoneAnswers = getKeyVal($reqParam, "MilestoneAnswers");
    if (!empty($MilestoneAnswers)) {
        $whereStatement = $whereStatement . "and  MilestoneAnswers like '%" . $MilestoneAnswers . "%'";
    }   
    $MilestoneQuestion = getKeyVal($reqParam, "MilestoneQuestion");
    if (!empty($MilestoneQuestion)) {
        $whereStatement = $whereStatement . "and  MilestoneQuestion like '%" . $MilestoneQuestion . "%'";
    }   

     $Price = getKeyVal($reqParam, "Price");
    if (!empty($Price)) {
        $whereStatement = $whereStatement . "and  Price like '%" . $Price . "%'";
    }
    $Funded = getKeyVal($reqParam, "Funded");
    if (!empty($Funded)) {
        $whereStatement = $whereStatement . "and  Funded like '%" . $Funded . "%'";
    } 
    $Paid = getKeyVal($reqParam, "Paid");
    if (!empty($Paid)) {
    $whereStatement = $whereStatement . "and  Paid like '%" . $Paid . "%'";
    }  

    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . "and  CreatedBy like '%" . $filterCreatedBy . "%'";
    }
 
    $CreatedDate = getKeyVal($reqParam, "CreatedDate");
    if (!empty($CreatedDate)) {
        $whereStatement = $whereStatement . "and  CreatedDate  = '" . $CreatedDate . "'";
    }  
   
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
   return  queryDB($sqlStatement); 

    
}
 


//#entry point 
function  editCampaignRunningMilestone($app , $campaignRunningMilestoneId    ){
  
    $reqParam = getJsonRequest($app);  
    // $userInfo = getUserSessionInfo($app) ; 
 
  
    $CampaignRunningId  = getKeyVal($reqParam, "CampaignRunningId");   
    checkEmpty($app,"CampaignRunningId", $CampaignRunningId) ; 

    $sqlStatement   = " Update  CampaignRunningMilestone  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $campaignRunningMilestoneId . "' ;";

  
     
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $setStatement = $setStatement . " CampaignRunningId='" . $CampaignRunningId . "' ,";
    }

 
   
    $MilestoneDescription = getKeyVal($reqParam, "MilestoneDescription");
    if (!empty($MilestoneDescription)) {
        $setStatement = $setStatement . " MilestoneDescription='" . $MilestoneDescription . "' ,";
    } 
    $MilestoneQuestion = getKeyVal($reqParam, "MilestoneQuestion");
    if (!empty($MilestoneQuestion)) {
        $setStatement = $setStatement . " MilestoneQuestion='" . $MilestoneQuestion . "' ,";
    } 
    $MilestoneAnswers = getKeyVal($reqParam, "MilestoneAnswers");
    if (!empty($MilestoneAnswers)) {
        $setStatement = $setStatement . " MilestoneAnswers='" . $MilestoneAnswers . "' ,";
    } 
    $Price = getKeyVal($reqParam, "Price");
    if (!empty($Price)) {
        $setStatement = $setStatement . " Price='" . $Price . "' ,";
    } 
    $Funded = getKeyVal($reqParam, "Funded");
    if (!empty($Funded)) {
        $setStatement = $setStatement . " Funded='" . $Funded . "' ,";
    } 
    $Paid = getKeyVal($reqParam, "Paid");
    if (!empty($Paid)) {
        $setStatement = $setStatement . " Paid='" . $Paid . "' ,";
    } 
    $CreatedBy = "*";
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
     
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "campaignRunningMilestoneId" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

 
//#entry point 
function  deleteCampaignRunningMilestone($app, $CampaignRunningMilestoneId) 
{

    $reqParam = getJsonRequest($app);  
    // $userInfo = getUserSessionInfo($app) ;    

    checkEmpty($app,"CampaignRunningMilestoneId", $CampaignRunningMilestoneId) ;  

    //delete virtualCurrency record
    $sqlStatement = "DELETE FROM CampaignRunningMilestone WHERE Id='" . $CampaignRunningMilestoneId . "'";
    $dataResult = crudDB($sqlStatement);   
    $result            = array(
        "status" => true,
    );


 
    getJsonResponse($app, $result); 
}
 
//#entry point  
function addCampaignRunningMilestone($app  ){
    $reqParam = getJsonRequest($app) ; 

 
    $CampaignRunningId  = getKeyVal($reqParam, "CampaignRunningId");  
    $MilestoneDescription  = getKeyVal($reqParam, "MilestoneDescription");  
    $MilestoneQuestion  = getKeyVal($reqParam, "MilestoneQuestion");  
    $Price  = getKeyVal($reqParam, "Price");   
 
    checkEmpty($app,"CampaignRunningId", $CampaignRunningId) ;
    checkEmpty($app,"Milestone Description", $MilestoneDescription) ;
    checkEmpty($app,"Milestone Question", $MilestoneQuestion) ;  
    checkEmpty($app,"Price", $Price) ;

 

  
    $sqlStatement   = "INSERT INTO  CampaignRunningMilestone (    
                        `CampaignRunningId` ,  
                        `MilestoneDescription`  , 
                        `MilestoneQuestion`  ,   
                        `MilestoneAnswers`  ,  
                        `Price`  ,  
                        `Funded`  ,   
                        `Paid`  ,   
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
    
    $CampaignRunningId = getKeyVal($reqParam, "CampaignRunningId");
    if (!empty($CampaignRunningId)) {
        $valueStatement = $valueStatement . "'" . $CampaignRunningId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $MilestoneDescription = getKeyVal($reqParam, "MilestoneDescription");
    if (!empty($MilestoneDescription)) {
        $valueStatement = $valueStatement . "'" . $MilestoneDescription . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    $MilestoneQuestion = getKeyVal($reqParam, "MilestoneQuestion");
    if (!empty($MilestoneQuestion)) {
        $valueStatement = $valueStatement . "'" . $MilestoneQuestion . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $MilestoneAnswers = getKeyVal($reqParam, "MilestoneAnswers");
    if (!empty($MilestoneAnswers)) {
        $valueStatement = $valueStatement . "'" . $MilestoneAnswers . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $Price = getKeyVal($reqParam, "Price");
    if (!empty($Price)) {
        $valueStatement = $valueStatement . "'" . $Price . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $Funded = getKeyVal($reqParam, "Funded");
    if (!empty($Funded)) {
        $valueStatement = $valueStatement . "'" . $Funded . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }     
    $Paid = getKeyVal($reqParam, "Paid");
    if (!empty($Paid)) {
        $valueStatement = $valueStatement . "'" . $Paid . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    } 

 
//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "CampaignRunningMilestoneId" =>$mysqli->insert_id  );
    getJsonResponse($app,$result); 

}
 


 
 
 
 
?>