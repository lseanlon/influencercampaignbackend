<?php 
//#entry point  
function getReviewListing($app  )
{
  
    $reqParam = getJsonRequest($app); 
    $userInfo = getUserSessionInfo($app) ;    
    getJsonResponse($app, getReviews($reqParam,$userInfo )  ); 
}
 
function getReviews($reqParam ,$userInfo ){ 

    $sqlStatement   = 'SELECT 
                         r.Id   , 
                         r.FromId    , 
                         toUser.Name as ToUserName    , 
                         toUser.Location  as ToLocation    , 
                         fromUser.Name as FromUserName    , 
                         fromUser.Location  as FromLocation    , 
                         r.ToId  ,  
                         r.Name   , 
                         r.Stars   , 
                         r.Description   ,  
                         r.DescriptionLong    , 
                         r.CreatedBy    ,         
                         r.CreatedDate       
    FROM Reviews r, User fromUser, User toUser ';
    $whereStatement = " where 1=1 and r.FromId= fromUser.Id and r.ToId=toUser.Id ";
  

    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and r.Id = '" . $Id . "'";
    }  
   
    $FromId = getKeyVal($reqParam, "FromId");
    if (!empty($FromId)) {
        $whereStatement = $whereStatement . "and r.FromId like '%" . $FromId . "%'";
    }
    $ToId = getKeyVal($reqParam, "ToId"); 
    if (!empty($ToId)) {
        $whereStatement = $whereStatement . "and r.ToId like '%" . $ToId . "%'";
      } 
    $Name = getKeyVal($reqParam, "Name");
    if (!empty($Name)) {
        $whereStatement = $whereStatement . "and r.Name like '%" . $Name . "%'";
    }   
     $Stars = getKeyVal($reqParam, "Stars");
    if (!empty($Stars)) {
        $whereStatement = $whereStatement . "and r.Stars like '%" . $Stars . "%'";
    }
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $whereStatement = $whereStatement . "and r.Description like '%" . $Description . "%'";
    } 
    $DescriptionLong = getKeyVal($reqParam, "DescriptionLong");
    if (!empty($DescriptionLong)) {
    $whereStatement = $whereStatement . "and r.DescriptionLong like '%" . $DescriptionLong . "%'";
    }  

    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy"); 
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . "and r.CreatedBy like '%" . $filterCreatedBy . "%'";
    }
 
    $filterCreatedDate = getKeyVal($reqParam, "CreatedDate");
    if (!empty($filterCreatedDate)) {
        $whereStatement = $whereStatement . "and r.CreatedDate  = '" . $filterCreatedDate . "'";
    }  
   
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
   return  queryDB($sqlStatement); 
    
}
 

 
//#entry point 
function  deleteReview($app, $reviewId) 
{

    $reqParam = getJsonRequest($app);  
    // $userInfo = getUserSessionInfo($app) ;    

    checkEmpty($app,"reviewId", $reviewId) ;  

    //delete virtualCurrency record
    $sqlStatement = "DELETE FROM Reviews WHERE Id='" . $reviewId . "'";
    $dataResult = crudDB($sqlStatement);  
 
    $result            = array(
        "status" => true,
    );


 
    getJsonResponse($app, $result); 
}
 
//#entry point  
function addReview($app  ){
    $reqParam = getJsonRequest($app) ;
   
    $userInfo = getUserSessionInfo($app) ;   
     $result = getReviews($reqParam,$userInfo );

     if (!empty($result[0]["Id"]) ) {
        return processErrorMessage($app, "DUPLICATE: Review record existed. ", "001");
    }

 
    $FromId = $userInfo['Id']; 
    $ToId  = getKeyVal($reqParam, "ToId");  
    $Name  = getKeyVal($reqParam, "Name");  
    $Stars  = getKeyVal($reqParam, "Stars");  
    $Description  = getKeyVal($reqParam, "Description");  
    $DescriptionLong  = getKeyVal($reqParam, "DescriptionLong"); 
 
    checkEmpty($app,"Description", $Description) ;
    checkEmpty($app,"To User", $ToId) ;
   // checkEmpty($app,"From User", $FromId) ;
    checkEmpty($app,"Stars", $Stars) ;
    checkEmpty($app,"Name", $Name) ;
    checkEmpty($app,"Description", $Description) ;
    checkEmpty($app,"DescriptionLong", $DescriptionLong) ;

 
  
    $sqlStatement   = "INSERT INTO  Reviews (    
                        `FromId` ,  
                        `ToId`  , 
                        `Name`  ,   
                        `Stars`  ,  
                        `Description`  ,  
                        `DescriptionLong`  ,   
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
     
    if (!empty($FromId)) {
        $valueStatement = $valueStatement . "'" . $FromId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $ToId = getKeyVal($reqParam, "ToId");
    if (!empty($ToId)) {
        $valueStatement = $valueStatement . "'" . $ToId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $Name = getKeyVal($reqParam, "Name");
    if (!empty($Name)) {
        $valueStatement = $valueStatement . "'" . $Name . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $Stars = getKeyVal($reqParam, "Stars");
    if (!empty($Stars)) {
        $valueStatement = $valueStatement . "'" . $Stars . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $valueStatement = $valueStatement . "'" . $Description . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }
    $DescriptionLong = getKeyVal($reqParam, "DescriptionLong");
    if (!empty($DescriptionLong)) {
        $valueStatement = $valueStatement . "'" . $DescriptionLong . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    } 

 
//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "reviewId" =>$mysqli->insert_id  );
    getJsonResponse($app,$result); 

}
 


 
 
 
 
?>