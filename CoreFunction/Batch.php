<?php
//REAdME: 
//
// please setup to run cron
//  chmod +x /home/curatedbpc/public_html/API/AUTO_DEPLOY/CleanUpCampaign.sh
// eg sh /home/curatedbpc/public_html/API/AUTO_DEPLOY/CleanUpCampaign.sh
// eg bash /home/curatedbpc/public_html/API/AUTO_DEPLOY/CleanUpCampaign.sh 

function performFetchFacebookUserInfluence($app, $secretKey)
{
    $request  = $app->request(); 
    $reqParam = getJsonRequest($app);
    checkEmpty($app, "secretKey", $secretKey);
    
    if (!isComparedEqual("secret-key", $secretKey)) {
        return processErrorMessage($app, "ILLEGAL ACCESS", "001");
    }
    
    
    $filterCampaign = new StdClass;
    
    // pick record that is last updated from last month to yesterday
    $minDate = date('Y-m-d H:i:s', strtotime('-1 months'));
    $maxDate = date('Y-m-d H:i:s', strtotime('-1 days'));
    
    // iterate all user influence table - filter facebook connected
    $filterUserInfluence                 = new StdClass;
    $filterUserInfluence->MinCreatedDate = $minDate;
    $filterUserInfluence->MaxCreatedDate = $maxDate;
    $filterUserInfluence->IsFbConnected  = "1";
    $userInfList                         = getUserInfluence($filterUserInfluence, null);
    
    // use result userid to get info
    
    foreach ($userInfList as $key => &$rec) {
        $dbUserId = ($rec["UserId"]);
        // var_export($dbUserId  );
        $UserRow  = getUser(null, $dbUserId, null);
        if (!empty($UserRow)) {
            $SessionKey  = $UserRow[0]["SessionKey"];
            $FbLoginName = $UserRow[0]["FbLoginName"];
            // trigger to facebook to fetch totalFollower , totalFriends  update into table userinfluence
            $resp        = updateUserFacebookInfluence($app, $dbUserId, $FbLoginName, $SessionKey);
        }
    }
    
    $result = array(
        "status" => true
    );
    return $result;
    
}

function getSummaryTotalRunningCampaignRec($reqParam)
{
    
    $sqlStatement = "  SELECT COUNT(cr.Id ) AS TotalRunningCampaign    
                        FROM CampaignRunning cr ,CampaignStatus cs  ";
    
    $whereStatement = "  where 1=1 and cr.StatusId=cs.Id    ";
    $orderStatement = "     ";
    
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cr.InfluencerId = '" . $InfluencerId . "'";
    }
    
    $StatusCode = "PROGR";
    if (!empty($StatusCode)) {
        $whereStatement = $whereStatement . " and cs.StatusCode = '" . $StatusCode . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . $orderStatement . " ; ";
    return queryDB($sqlStatement);
    
    
    
}
function getSummaryTotalCampaignRec($reqParam)
{
    //GET TOTAL campaign REC
    $sqlStatement = "  SELECT COUNT(cr.Id ) AS TotalCampaign, 
                        SUM(cr.AgreedTotalCost) AS TotalAmountCollected    
                        FROM CampaignRunning cr   ";
    
    $whereStatement = "  where 1=1     ";
    $orderStatement = "    ";
    
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cr.InfluencerId = '" . $InfluencerId . "'";
    }
    
    
    $sqlStatement = $sqlStatement . $whereStatement . $orderStatement . " ; ";
    return queryDB($sqlStatement);
    
    
}
function getSummaryFbTotalCampaignRec($reqParam)
{
    
    // //GET facebook summary campaign REC  
    $sqlStatement = "  SELECT COUNT(cf.Id ) AS FacebookTotalCampaign    ,
                           SUM(cf.TotalPostComment ) AS FacebookTotalPostComment   , 
                           SUM(cf.TotalPostReaction ) AS FacebookTotalPostReactions   , 
                           SUM(cf.TotalPostShare ) AS FacebookTotalPostShares   , 
                           SUM(cf.TotalLike ) AS FacebookTotalPostLikes    ,
                           AVG(cf.TotalPostComment ) AS FacebookAvgTotalPostComment   , 
                           AVG(cf.TotalPostReaction ) AS FacebookAvgTotalPostReaction   , 
                           AVG(cf.TotalPostShare ) AS FacebookAvgTotalPostShare   , 
                           AVG(cf.TotalLike ) AS FacebookAvgTotalLike    
                        FROM  CampaignRunning cr, CampaignFacebook cf, CampaignStatus cs   ";
    
    
    
    $whereStatement = "  where 1=1 and  cr.StatusId = cs.Id   ";
    $orderStatement = "     ";
    
    
    $InfluencerId = getKeyVal($reqParam, "InfluencerId");
    if (!empty($InfluencerId)) {
        $whereStatement = $whereStatement . " and cf.UserId = '" . $InfluencerId . "'";
    }
    
    $sqlStatement = $sqlStatement . $whereStatement . $orderStatement . " ; ";
    return queryDB($sqlStatement);
    
    
}
function performDashboardPopulation($app, $secretKey)
{
    
    $reqParam = getJsonRequest($app);
    checkEmpty($app, "secretKey", $secretKey);
    
    if (!isComparedEqual("secret-key", $secretKey)) {
        return processErrorMessage($app, "ILLEGAL ACCESS", "001");
    }
    
    // Populate dashboard related data, 
    // 1) Find all related user who is influencer, last updated time is less than yesterday.
    // 2) Traverse list. 
    // 3) Get total campaign stats. Get total campaign running stats.
    // 4) Get facebook summary.
    // 3) update/existing into userinfluence table
    
    // iterate all user influence table - filter yesterday updated  
    $mindate                             = date('Y-m-d H:i:s', strtotime('-2 days'));
    $maxdate                             = date('Y-m-d H:i:s', strtotime('-1 days'));
    $filterUserInfluence                 = new StdClass;
    $filterUserInfluence->MinCreatedDate = $mindate;
    $filterUserInfluence->MaxCreatedDate = $maxdate;
    $userInfList                         = getUserInfluence($filterUserInfluence, null);
    // var_export(  $userInfList  ); 
    foreach ($userInfList as $key => &$rec) {
        $dbUserId = ($rec["UserId"]);
        // var_export($dbUserId  );
        $UserRow  = getUser(null, $dbUserId, null);


        if (!empty($UserRow)) {
            
            $isInfluencer = isUserInfluencer($dbUserId);

            if ($isInfluencer) {
                $filterUserInfluence->InfluencerId = $dbUserId;
                $result                            = getSummaryTotalCampaignRec($filterUserInfluence)[0];
                if (!empty($result)) {
                    $rec["TotalSuccessfulCampaign"] = $result["TotalCampaign"];
                    $rec["TotalAmountCollected"]    = $result["TotalAmountCollected"];
                 }  
                
                $result = getSummaryTotalRunningCampaignRec($filterUserInfluence)[0];
                if (!empty($result)) {
                    $rec["TotalRunningCampaign"] = $result["TotalRunningCampaign"];
                }
                $result = getSummaryFbTotalCampaignRec($filterUserInfluence)[0];
                if (!empty($result)) {
                    $rec["FacebookTotalCampaign"]      = $result["FacebookAvgTotalPostComment"];
                    $rec["FacebookTotalPostLikes"]     = $result["FacebookAvgTotalLike"];
                    $rec["FacebookTotalPostComment"]   = $result["FacebookAvgTotalPostComment"];
                    $rec["FacebookTotalPostShares"]    = $result["FacebookAvgTotalPostShare"];
                    $rec["FacebookTotalPostReactions"] = $result["facebookAvgTotalPostReaction"];
                    
                    
                }
                $rec["CreatedDate"]=date('Y-m-d H:i:s' );
           
            }
            
            //update each row in db 
           var_dump( $rec);
            $rec                        = json_decode(json_encode($rec), FALSE); 
            $userInflRec                = new StdClass;
            $userInflRec->UserInfluence = $rec;
            $updCampaignStatus          = editUserInfluence($userInflRec, $dbUserId);
        }
    }
    
    
   getJsonResponse($app, true);
    
    
    
}

// #entry
function performCleanExpiredCampaign($app, $secretKey)
{
    
    $reqParam = getJsonRequest($app);
    checkEmpty($app, "secretKey", $secretKey);
    
    if (!isComparedEqual("secret-key", $secretKey)) {
        return processErrorMessage($app, "ILLEGAL ACCESS", "001");
    }
    
    
    $filterCampaign = new StdClass;
    $currentDate    = date('Y-m-d H:i:s', strtotime('-0 months'));
    //GET campaign list that is active
    $campaignList   = getCampaign($filterCampaign);
    
    // check each row  
    foreach ($campaignList as $key => &$rec) {
        $FromDate   = strtotime($rec["FromDate"]);
        $ToDate     = strtotime($rec["ToDate"]);
        $CurDate    = strtotime($currentDate);
        $campaignId = $rec["Id"];
        //check all active: whether legally is active
        if (intval($rec["IsActive"]) > 0) {
            //expired date
            if ($CurDate > $ToDate) {
                $rec["IsActive"] = "00";
            }
            
            //not yet start/active date
            if ($CurDate < $FromDate) {
                $rec["IsActive"] = "00";
            }
        }
        
        
        $rec["CreatedDate"] = $currentDate;
        $rec                = json_decode(json_encode($rec), FALSE);
        //update each row in db
        $updCampaignStatus  = editCampaignValue($rec, $campaignId);
        
    }
    
    
    $result = array(
        "updCampaignStatus" => $updCampaignStatus,
        "currentDate" => $currentDate
    );
    
    getJsonResponse($app, $result);
}



function performCleanCampaignRunningStatusAPPRO($app )
{


//     //goal 1: check status 'APPRO' -->  changed ALL to 'NOPAY'
//     //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD, CHANGED TO NOPAY.
    
    $filterCampaign = new StdClass;
    $filterCampaign->StatusCodeList = "APPRO" ;
    $filterCampaign->MinCreatedDate = date('Y-m-d H:i:s', strtotime('-1 months'));  
    $filterCampaign->MaxCreatedDate = date('Y-m-d H:i:s', strtotime('-1 days'));  
 
    $newStatusId =   getCampaignStatusByCode('NOPAY')["Id"]; 
    //GET campaign list that is active
    $campaignRunningList    = getCampaignRunningByInfluencerList(  $filterCampaign  ) ; 
     // var_export( $campaignRunningList  ) ;
    $updRec = null;
    // check each row  
    foreach ($campaignRunningList as $key => &$rec) {
        
        $rec["StatusId"]  = $newStatusId; 
        $rec["CreatedDate"]  =date('Y-m-d H:i:s', strtotime('-0 months'));     
        $rec                = json_decode(json_encode($rec), FALSE);
        // var_export( $rec  ) ;
        //update each row in db 
        $updRec =  editCampaignRunning(  $rec );
        
    }
    
    return $updRec; 

}
function performCleanCampaignRunningStatusNOPAY($app )
{



    //goal 2: check status 'NOPAY' -->  changed to 'PROGR' 
    //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD frm last month, 
    //  CHECK IF CAMPAIGNPAYMENTID NOT EMPTY? CHANGED TO PROGR.
    //  CHECK IF CAMPAIGNPAYMENTID EMPTY AND LAST UPDATE IS MORE THAN 3 DAYS? SEND EMAIL TO MERCHANT REMINDER
    
    $filterCampaign = new StdClass;
    $filterCampaign->StatusCodeList = "NOPAY" ;
    $filterCampaign->MinCreatedDate = date('Y-m-d H:i:s', strtotime('-1 months'));  
    $filterCampaign->MaxCreatedDate = date('Y-m-d H:i:s', strtotime('-1 days'));  
 
    $newStatusId =   getCampaignStatusByCode('PROGR')["Id"]; 
    //GET campaign list that is active
    $campaignRunningList    = getCampaignRunningByInfluencerList(  $filterCampaign  ) ; 
     // var_export( $campaignRunningList  ) ;
    $updRec = null;
    // check each row  
    foreach ($campaignRunningList as $key => &$rec) {
        
        if(!empty($rec["CampaignPaymentId"])){ 
            $rec["StatusId"]  = $newStatusId;  
        } 

         $dateMinAllowed= date('Y-m-d H:i:s', strtotime('-7 days'));
         if($rec["CreatedDate"]  < $dateMinAllowed){
                //send email to merchat reminder.
            $CampaignDescription = $rec["CampaignDescription"]  ;
            $CampaignName = $rec["CampaignName"]  ;

            $OwnerId = $rec["CampaignCreatedBy"]  ;
            $OwnerInfo = getUser( null ,  $OwnerId,null)[0];  

            $OwnerEmail = $OwnerInfo["Email"] ;  
            $OwnerName = $OwnerInfo["Name"] ;  
 
            $InfluencerId = $rec["InfluencerId"]  ;
            $InfluencerInfo = getUser( null ,  $InfluencerId,null)[0]; 
            $InfluencerEmail = $InfluencerInfo["Email"] ;  
            $InfluencerName = $InfluencerInfo["Name"] ;  
  
   
        $bodyText="
             Hi $OwnerName ,\n We notice that you have not make the payment  for your campaign: $CampaignName - $CampaignDescription to influencer $InfluencerName. It has been 7 days since the influencer's approval, we urge you to do so immediately . \n\n If you think you have received this email by mistake, please email to support@domain. 
        ";
        $bodyHtml="
            <p>  Hi $OwnerName , </p> 
            <p> We notice that you have not make the payment  for your campaign: $CampaignName - $CampaignDescription to influencer $InfluencerName.It has been 7 days since the influencer's approval, we urge you to do so immediately.
            </p> 
            <p>  If you think you have received this email by mistake, please email to support@domain. </p>  

        ";
         $emailArr = array(
            "recipientEmail" =>   $OwnerEmail,
            "recipientName" =>   $OwnerName,
            "subject" => "Running Campaign $CampaignName - Payment Reminder to  $InfluencerName",
            "bodyHtml" => $bodyHtml,
            "bodyText" => $bodyText
        );
        $emailResult= sendEMailMessage($emailArr); 

 
         }
         else{

            $rec["CreatedDate"]  =date('Y-m-d H:i:s', strtotime('-0 months'));     
            $rec                = json_decode(json_encode($rec), FALSE);
            // var_export( $rec  ) ;
            //update each row in db 
            $updRec =  editCampaignRunning(  $rec );
         }

        
    }
    
    return $updRec; 

}



function performCleanCampaignRunningStatusPROGR($app )
{

    //goal 3: check status 'PROGR' -->  changed to 'COMPL' 
    //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD from lastmonth, 
    //  CHECK IF InfluencerRemainingCost & InfluencerRemainingPost IS <=0 ? CHANGED TO COMPL. 
    
    $filterCampaign = new StdClass;
    $filterCampaign->StatusCodeList = "PROGR" ;
    $filterCampaign->MinCreatedDate = date('Y-m-d H:i:s', strtotime('-1 months'));  
    $filterCampaign->MaxCreatedDate = date('Y-m-d H:i:s', strtotime('-1 days'));  
 
    $newStatusId =   getCampaignStatusByCode('COMPL')["Id"]; 
    //GET campaign list that is active
    $campaignRunningList    = getCampaignRunningByInfluencerList(  $filterCampaign  ) ; 
     // var_export( $campaignRunningList  ) ;
    $updRec = null;
    // check each row  
    foreach ($campaignRunningList as $key => &$rec) {
     
        if(floatval($rec["InfluencerRemainingCost"])<=0 || intval($rec["InfluencerRemainingPost"] )<=0){

            $rec["StatusId"]  = $newStatusId; 
            $rec["CreatedDate"]  =date('Y-m-d H:i:s', strtotime('-0 months'));     
            $rec                = json_decode(json_encode($rec), FALSE);
            $updRec =  editCampaignRunning(  $rec );
        }
        
    }
    
    return $updRec; 

}

function performCleanCampaignRunningStatusCOMPL($app )
{


    //goal 4: check status 'COMPL' -->  changed to 'INACT'  
    //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD from last month
    //  CHECK IF COMPL IS MORE THAN 3 DAYS  ? CHANGED TO INACT. 
    $filterCampaign = new StdClass;
    $filterCampaign->StatusCodeList = "COMPL" ;
    $filterCampaign->MinCreatedDate = date('Y-m-d H:i:s', strtotime('-1 months'));  
    $filterCampaign->MaxCreatedDate = date('Y-m-d H:i:s', strtotime('-1 days'));  
 
    $newStatusId =   getCampaignStatusByCode('INACT')["Id"]; 
    //GET campaign list that is active
    $campaignRunningList    = getCampaignRunningByInfluencerList(  $filterCampaign  ) ; 
     // var_export( $campaignRunningList  ) ;
    $updRec = null;
    // check each row  
    foreach ($campaignRunningList as $key => &$rec) {

        $dateMinAllowed= date('Y-m-d H:i:s', strtotime('-3 days'));
        
         if($rec["CreatedDate"]  < $dateMinAllowed){
            $rec["StatusId"]  = $newStatusId; 
            $rec["CreatedDate"]  =date('Y-m-d H:i:s', strtotime('-0 months'));     
            $rec                = json_decode(json_encode($rec), FALSE);
            $updRec =  editCampaignRunning(  $rec );
            
         } 

       
        
    }
    
    return $updRec; 

}
// #entry
function performCleanCampaignRunningStatus($app, $secretKey)
{
    
    $reqParam = getJsonRequest($app);
    checkEmpty($app, "secretKey", $secretKey);
    
    if (!isComparedEqual("secret-key", $secretKey)) {
        return processErrorMessage($app, "ILLEGAL ACCESS", "001");
    }
    
     // 'REQUE'  'APPRO'  'NOPAY'  'PROGR'  'COMPL'  'INACT'


    //goal 1: check status 'APPRO' -->  changed ALL to 'NOPAY'
    //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD, CHANGED TO NOPAY.
    
    $approUPD = performCleanCampaignRunningStatusAPPRO($app ) ; 

    //goal 2: check status 'NOPAY' -->  changed to 'PROGR' 
    //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD, 
    //  CHECK IF CAMPAIGNPAYMENTID NOT EMPTY? CHANGED TO PROGR.
    //  CHECK IF CAMPAIGNPAYMENTID EMPTY AND LAST UPDATE IS MORE THAN 3 DAYS? SEND EMAIL TO MERCHANT REMINDER
    $nopayUPD = performCleanCampaignRunningStatusNOPAY($app ) ; 

    //goal 3: check status 'PROGR' -->  changed to 'COMPL' 
    //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD, 
    //  CHECK IF InfluencerRemainingCost & InfluencerRemainingPost IS <=0 ? CHANGED TO COMPL. 
   $progrUPD =  performCleanCampaignRunningStatusPROGR($app ) ; 
 

    //goal 4: check status 'COMPL' -->  changed to 'INACT'  
    //  FETCH ALL LAST UPDATE LESS THAN YESTERDAY RECORD, 
    //  CHECK IF COMPL IS MORE THAN 3 DAYS  ? CHANGED TO INACT. 
    $complUPD =  performCleanCampaignRunningStatusCOMPL($app ) ; 
 

    $result = array(
        "approUPD" => $approUPD ,
        "nopayUPD" => $nopayUPD ,
        "progrUPD" => $progrUPD  ,
        "complUPD" => $complUPD  ,
    );
    
    getJsonResponse($app, $result);
}
?>