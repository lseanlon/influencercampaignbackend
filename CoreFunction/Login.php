<?php

// #ENTRY
function performPreLogin($app  )
{
    //check if cookie on login exist 
    $serializedUserRec = $app->getCookie('UserData');
    $LoginType         = $app->getCookie('LoginType');
    $SessionKey = $app->request->headers->get('INFL-KEY');       
    $isLoggedOut       = true; $action       = "NEW";
    $userRec = ""; $userType = "";                 $redirect="";

    if (!empty($SessionKey) || !empty($serializedUserRec)) {
         //  logged in
        $userRec = unserialize($serializedUserRec);
        $isLoggedOut = false; 


        // if(!isComparedEqual($SessionKey,  $userRec["SessionKey"] )) { 
         //     $isLoggedOut = true; 
          //    return processErrorMessage($app, "Invalid Session! Please ", "999");
         //}


          $userType =   getKeyVal($userRec, "UserType")[ "Code"]; 
        // $userType = $userRec["UserType"][ "Code"]; 
        // $action = "EXISTING";    
        // $redirect =  getRedirectPath($userRec["UserType"]["Code"]);
    } 

    $result = array(
        "userInfo" =>  $userRec   ,
        "userType" =>  $userType ,
        "isLoggedOut"=> $isLoggedOut ,
        "redirect" => $redirect
    );

    getJsonResponse($app, $result);
     
}


// #ENTRY
function performCommonLogin($app)
{
    
    $reqParam = getJsonRequest($app);
    
    $LoginType  = getKeyVal($reqParam, "LoginType");
    $UserName   = getKeyVal($reqParam, "UserName");
    $Password   = getKeyVal($reqParam, "Password");
    $SessionKey = getKeyVal($reqParam, "SessionKey");
    $UserType = getKeyVal($reqParam, "UserType");
    $UserTypeId = getKeyVal($reqParam, "UserTypeId");


    $Name   = getKeyVal($reqParam, "Name"); 
    $Summary   = getKeyVal($reqParam, "Summary");
    $Email   = getKeyVal($reqParam, "Email");
    $ImageUrl   = getKeyVal($reqParam, "ImageUrl"); 
    $UserInfluence   = getKeyVal($reqParam, "UserInfluence"); 
 

    checkEmpty($app, "LoginType", $LoginType);
    checkEmpty($app, "UserName", $UserName);
    checkEmpty($app, "Password", $Password); 
    
    
    $reqParam             = new StdClass;
    $reqParam->LoginType  = $LoginType;
    $reqParam->UserName   = $UserName;
    $reqParam->Password   = $Password;
    $reqParam->SessionKey = $SessionKey;
    $reqParam->UserInfluence = $UserInfluence;
    
    
    //check user   
    // $resultList = getUser($reqParam, $UserName, null);  
    $resultList= getCommonUserProfile($reqParam, $UserName,  null); 
    
    //check logintype 
    if (isComparedEqual("INSTA", $LoginType)) {
        
        checkEmpty($app, "SessionKey", $SessionKey);
        
        //check with fb api ; valid token for user  , extend access token
        $reqParam->lastSuccessfulLogin = date('Y-m-d H:i:s');
        $reqParam->IsLoggedOut = "000";
        $reqParam->FailedLoginAttempt = "000";
        $reqParam->UserTypeId = $UserTypeId;
        $reqParam->UserType = $UserType;
        $reqParam->Name = $Name;
        $reqParam->UserLoginName = $UserName;
        $reqParam->Email = $Email;
        $reqParam->Summary = $Summary;
        
        $result = performSocialLogin($app, $reqParam, $resultList); 
        
        $reqParam->SessionKey = $SessionKey;
        return getSessionJsonResponse($app, $result,  $reqParam->SessionKey    )  ;
        
    } 

    else if (isComparedEqual("FB", $LoginType)) {
        
        checkEmpty($app, "SessionKey", $SessionKey);
        
        //check with fb api ; valid token for user  , extend access token
        $fbUserData                    = checkAccessTokenstatus($app, $reqParam);
        $reqParam->UserName            = $fbUserData["userName"];
        $reqParam->FbLoginName         = $UserName;
        $reqParam->SessionKey          = $fbUserData["accessToken"]->getValue();
        $reqParam->lastSuccessfulLogin = date('Y-m-d H:i:s');
        $reqParam->IsLoggedOut = "000";
        $reqParam->FailedLoginAttempt = "000";
        $reqParam->UserTypeId = $UserTypeId;
        $reqParam->UserType = $UserType;
        $reqParam->Name = $Name;
        $reqParam->Email = $Email;
  
        
        $result = performSocialLogin($app, $reqParam, $resultList);
        performFacebookLogin($app, $reqParam, $resultList, $result["userId"]);
        
        return getSessionJsonResponse($app, $result,  $reqParam->SessionKey    )  ;
        
    } else if (isComparedEqual("GOOGLE", $LoginType)) {
        
        checkEmpty($app, "SessionKey", $SessionKey);
        
        //check with google api ; valid token for user 
        $tokenInfo = checkGoogleAccessTokenstatus($app,   $SessionKey);
 

        $reqParam->lastSuccessfulLogin = date('Y-m-d H:i:s');
        $reqParam->IsLoggedOut = "000";
        $reqParam->FailedLoginAttempt = "000";
        $reqParam->UserTypeId = $UserTypeId;
        $reqParam->UserType = $UserType;
        $reqParam->SessionKey = getKeyVal($tokenInfo,"at_hash") ;


        $reqParam->GoogleLoginName = $UserName;
        $reqParam->Name = $Name;
        $reqParam->Email = $Email;


        $result = performSocialLogin($app, $reqParam, $resultList);
        return getSessionJsonResponse($app, $result,  $reqParam->SessionKey    )  ;  
 
        
    } else { //NORMAL login
        
        performNormalLogin($app, $reqParam, $resultList );
    }
    
    
    
    
}

 
function getRedirectPath($Code){ 
            
         if (empty(  $Code)) { 
            return "LOGIN-UNKNOWN"; 
        }

       if (isComparedEqual("INFLUENCER", $Code)) { 
            return "LOGIN-INFLUENCER"; 
        }
        else{
            return "LOGIN-MERCHANT"; 
        }
}

function createLoginCookie($app,$serializedUserRec, $LoginType){ 
    // var_export( $SerializedUserRec );
    // var_export( $LoginType ); 

    $app->setCookie('UserData', $serializedUserRec, '1 days', '/Influencer'); 
    $app->setCookie('LoginType', $LoginType, '1 days', '/Influencer');
}

function performNormalLogin($app, $reqParam, $userRec)
{
    
    processPreLoginHeaderRequest($app)  ;  
    
    if (empty($userRec)) {
        return processErrorMessage($app, "User does not exist. ", "001");
    }
    
    
    if (!empty($userRec)) {
        $Password = getKeyVal($reqParam, "Password");
        $rawDbPwd = ("pepper" . $userRec["Password"] . "salt");
        
 
        if (!isHashValueEqual($app, $rawDbPwd, $Password)) {
            //do counterfailed logic here 
            $userRec["FailedLoginAttempt"] = empty($userRec["FailedLoginAttempt"]) ? 1 : $userRec["FailedLoginAttempt"];
            $userRec["FailedLoginAttempt"] = intval($userRec["FailedLoginAttempt"]) + 1;
            if (intval($userRec["FailedLoginAttempt"]) >= 5) {
                $userRec["IsTempBlocked"] = "1";
            }
            
            $userRec["LastFailedLogin"] = date('Y-m-d H:i:s');
            $rec                        = json_decode(json_encode($userRec), FALSE);
            $trxStatus                  = editUserCommon($rec, $userRec["Id"], null);
            
            return processErrorMessage($app, "Wrong username or password ", "003");
        }
        if ( empty($userRec["IsActive"])  || intval($userRec["IsActive"]) < 1) {
            
            return processErrorMessage($app, "Account is still under review. Please be patient and wait for approval. ", "003");
        }
        if (!empty($userRec["IsTempBlocked"]) && intval($userRec["IsTempBlocked"]) >= 1) {
            
            return processErrorMessage($app, "Account already Blocked. Please contact admin to unlock. ", "003");
        }
 
        //do counter success logic here  
        $userRec["LastSuccessfulLogin"] = "" . date('Y-m-d H:i:s') . "";
        $userRec["IsLoggedOut"]         = "000";
        $userRec["FailedLoginAttempt"]  = "000";
        // geenerate sessionkey and cookie 
        $userRec["SessionKey"]          = md5(microtime() . $_SERVER['REMOTE_ADDR']);
        
        $rec       = json_decode(json_encode($userRec), FALSE);
        $trxStatus = editUserCommon($rec, $userRec["Id"], null);

        $userRec["Password"]          = ":]";
        $result    = array(
            "trxStatus" => "SUCCESS", 
            "userType" => $userRec["UserType"]["Code"], 
            "userRec" => $userRec , 
            "redirect" => getRedirectPath($userRec["UserType"]["Code"])
        );


        // update Session history accordingly
        $rec ->UserId = $userRec["Id"] ;
        performSessionHistory(  $rec, "LOGIN") ;


        $userRec["Password"]          = ":]";


        $userCookie = new stdClass(); 
        $userCookie->Id =  $userRec[ "Id"]; 
        $userCookie->Name =  $userRec[ "Name"];  
        $userCookie->SessionKey =   $userRec[ "SessionKey"];  
        $userCookie->UserType =   $userRec[ "UserType"];   
        $userCookie->IsFeatureUnlockedInfluencer =   $userRec[ "IsFeatureUnlockedInfluencer"];   
         
        //serialized data for cookie use
        $serializedUserRec = serialize($userCookie);  
         createLoginCookie($app, $serializedUserRec, "NORMAL");
        //    $app->setCookie('UserData', $serializedUserRec, '1 days', '/Influencer'); 
        // $app->setCookie('LoginType', "NORMAL", '1 days', '/Influencer');
        
        
        getSessionJsonResponse($app, $result, $userRec["SessionKey"]  );
         
        
        
    }
    
}

function performFacebookLogin($app, $reqParam, $resultList, $userId)
{ 
    //update user facebook populartiy influence
    updateUserFacebookInfluence($app, $userId, getKeyVal($reqParam, "FbLoginName"), getKeyVal($reqParam, "SessionKey"));
    
} 

function performSocialLogin($app, $reqParam, $resultList)
{
    
    $trxStatus = "";
    $operation = "";
    $userId    = "00";

    if (empty($resultList)) {
        
        //if not exist --> add  user table with request data
        $operation = "ADD";
        $resp      = addUserCommon($reqParam); 

        $userId           = $resp["userId"];
    } else {  
        //if exist --> update  user table with request data
        $operation = "UPD";
        $userId    = $resultList["Id"];   
        editUserInfluence($reqParam , $userId);

         if ( !empty($resultList["IsTempBlocked"])  && intval($resultList["IsTempBlocked"]) >=0 ) { 
            return processErrorMessage($app, "Account is temporarily blocked. Please contact admin or support for help. ", "003");
        }
         if ( empty($resultList["IsActive"])  || intval($resultList["IsActive"]) < 1) { 
            return processErrorMessage($app, "Account is still under review. Please be patient and wait for approval. ", "003");
        }
        $trxStatus = editUserCommon($reqParam, $userId, null); 
    } 


    $reqParam->UserId = $userId;
    performSessionHistory(  $reqParam, "LOGIN");  


    // var_export($userId ); 
    

    $userCookie = new stdClass(); 
    $userCookie->Name =  $reqParam->UserName ;  
    $userCookie->GoogleLoginName =  $reqParam->GoogleLoginName ;  
    $userCookie->FbLoginName =  $reqParam->FbLoginName ;  
    $userCookie->SessionKey =   $reqParam->SessionKey ;  
    $userCookie->UserTypeId = $reqParam->UserTypeId;      
    $userCookie->UserType = $reqParam->UserType;    



    //serialized data for cookie use
    $serializedUserRec = serialize($userCookie);  

    createLoginCookie($app,$serializedUserRec , getKeyVal($reqParam, "LoginType")  ) ;
    
    $result = array(
        "sessionkey" => getKeyVal($reqParam, "SessionKey"),
        "operation" => $operation, 
        "trxStatus" => $trxStatus,
        "userId" => $userId, "userRec" => $resultList , 
        "userType" => $resultList["UserType"]["Code"] ,  
        "redirect" => getRedirectPath($resultList["UserType"]["Code"])
    );
    
    
    return $result;
}
?>