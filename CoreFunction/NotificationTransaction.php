<?php
 function checkAccessAdmin($userRec  )
{
     //ensure only return this to admin 
    if(empty( $userRec) ||  empty($userRec["UserType"]["Name"] ) || strtoupper($userRec["UserType"]["Name"] )!="ADMIN" ){

         return processErrorMessage($app, "Service denied ", "001");
    }


}
//#entry point  
function getNotificationTransaction($app  )
{
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;  
    getJsonResponse($app, getCommonNotificationTransaction($reqParam )  ); 
}



//#entry point 
function  deleteNotificationTransaction($app, $NotificationTransactionId) 
{

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;    
    // checkAccessAdmin($userInfo  ) ;
 

    checkEmpty($app,"NotificationTransactionId", $NotificationTransactionId) ;  

    //delete campaign record
    $sqlStatement      = "DELETE FROM NotificationTransaction WHERE Id='" . $NotificationTransactionId . "'";
    $dataResult = crudDB($sqlStatement);   
    $result = array(     "status" => true    );


 
    getJsonResponse($app, $result); 
}


//#entry point 
function  editNotificationTransaction($app , $NotificationTransactionId    ){
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;
    // checkAccessAdmin($userInfo  ) ; 
 
  
    $Id  = getKeyVal($reqParam, "Id"); 
    $NotificationId  = getKeyVal($reqParam, "NotificationId"); 
    $NotificationCode  = getKeyVal($reqParam, "NotificationCode");
    $IsRead  = getKeyVal($reqParam, "IsRead");
    $ToUserId  = getKeyVal($reqParam, "ToUserId");
    $FromUserId  = getKeyVal($reqParam, "FromUserId"); 
    $FollowUpId  = getKeyVal($reqParam, "FollowUpId");
    $Description  = getKeyVal($reqParam, "Description"); 
 
    checkEmpty($app,"Id", $Id) ; 
  

    $sqlStatement   = " Update  NotificationTransaction  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $NotificationTransactionId . "' ;";

             

    $NotificationId = getKeyVal($reqParam, "NotificationId");
    if (!empty($NotificationId)) {
        $setStatement = $setStatement . " NotificationId='" . $NotificationId . "' ,";
    }
    $NotificationCode = getKeyVal($reqParam, "NotificationCode");
    if (!empty($NotificationCode)) {
        $setStatement = $setStatement . " NotificationCode='" . $NotificationCode . "' ,";
    }
    $IsRead = getKeyVal($reqParam, "IsRead");
    if (!empty($IsRead)) {
        $setStatement = $setStatement . " IsRead='" . $IsRead . "' ,";
    }
    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $setStatement = $setStatement . " FromUserId='" . $FromUserId . "' ,";
    }
      
    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $setStatement = $setStatement . " ToUserId='" . $ToUserId . "' ,";
    }
   
    $FollowUpId = getKeyVal($reqParam, "FollowUpId");
    if (!empty($FollowUpId)) {
        $setStatement = $setStatement . " FollowUpId='" . $FollowUpId . "' ,";
    }
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $setStatement = $setStatement . " Description='" . $Description . "' ,";
    }

    $CreatedBy = $userInfo["Id"];
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "result" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

//#entry point  
function addNotificationTransaction($app  ){
    $reqParam = getJsonRequest($app) ;
   
   $userInfo = getUserSessionInfo($app) ;  
    


 
    $NotificationId  = getKeyVal($reqParam, "NotificationId"); 
    $NotificationCode  = getKeyVal($reqParam, "NotificationCode");
    $IsRead  = getKeyVal($reqParam, "IsRead");
    $ToUserId  = getKeyVal($reqParam, "ToUserId");
    $FromUserId  = getKeyVal($reqParam, "FromUserId"); 
    $FollowUpId  = getKeyVal($reqParam, "FollowUpId");
    $Description  = getKeyVal($reqParam, "Description");  

   
    checkEmpty($app,"NotificationId", $NotificationId) ;
    checkEmpty($app,"ToUserId", $ToUserId) ;
    checkEmpty($app,"FromUserId", $FromUserId) ;   


    $sqlStatement   = "INSERT INTO  NotificationTransaction (     
                        `IsRead`  , 
                        `FromUserId` ,  
                        `ToUserId`  , 
                        `NotificationId`  , 
                        `NotificationCode`  ,    
                        `FollowUpId`  ,   
                        `Description`  ,        
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
 

    $IsRead = getKeyVal($reqParam, "IsRead");
    if (!empty($IsRead)) {
        $valueStatement = $valueStatement . "'" . $IsRead . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $valueStatement = $valueStatement . "'" . $FromUserId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }



    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $valueStatement = $valueStatement . "'" . $ToUserId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }


    $NotificationId = getKeyVal($reqParam, "NotificationId");
    if (!empty($NotificationId)) {
        $valueStatement = $valueStatement . "'" . $NotificationId . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

    $NotificationCode = getKeyVal($reqParam, "NotificationCode");
    if (!empty($NotificationCode)) {
        $valueStatement = $valueStatement . "'" . $NotificationCode . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

    $FollowUpId = getKeyVal($reqParam, "FollowUpId");
    if (!empty($FollowUpId)) {
        $valueStatement = $valueStatement . "'" . $FollowUpId . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }

    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $valueStatement = $valueStatement . "'" . $Description . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }
 

//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "NotificationTransactionId" =>$mysqli->insert_id  );
    // var_dump( ($sqlStatement . $valueStatement)) ; 
    getJsonResponse($app,$result); 

    //update the user profile currency amount
    //

}

function getCommonNotificationTransaction($reqParam ){   
  


    $sqlStatement   = 'SELECT 
                        n.Id  ,  
                        n.IsRead  , 
                        n.FromUserId ,  
                        n.ToUserId  , 
                        n.NotificationId  , 
                        n.NotificationCode  ,    
                        n.FollowUpId  ,   
                        n.Description  ,    
                        n.CreatedBy   ,    
                        n.CreatedDate     ,   
                         toUser.Name as ToUserName    , 
                         toUser.Location  as ToLocation    , 
                         fromUser.Name as FromUserName    , 
                         fromUser.Location  as FromLocation    
    FROM NotificationTransaction  n, User toUser, User fromUser';
    $whereStatement = " where 1=1 and n.FromUserId = fromUser.Id and n.ToUserId = toUser.Id  ";
 
    $Id = getKeyVal($reqParam, "Id");
    if (!empty($Id)) {
        $whereStatement = $whereStatement . " and n.Id = '" . $Id . "'";
    }
    
    $IsRead = getKeyVal($reqParam, "IsRead");
    if (!empty($IsRead)) {
        $whereStatement = $whereStatement . " and n.IsRead = '" . $IsRead . "'";
    }
    $FromUserId = getKeyVal($reqParam, "FromUserId");
    if (!empty($FromUserId)) {
        $whereStatement = $whereStatement . " and n.FromUserId = '" . $FromUserId . "'";
    }
    $ToUserId = getKeyVal($reqParam, "ToUserId");
    if (!empty($ToUserId)) {
        $whereStatement = $whereStatement . " and n.ToUserId = '" . $ToUserId . "'";
    }
    $NotificationId = getKeyVal($reqParam, "NotificationId");
    if (!empty($NotificationId)) {
        $whereStatement = $whereStatement . " and n.NotificationId = '" . $NotificationId . "'";
    }
     
    $NotificationCode = getKeyVal($reqParam, "NotificationCode");
    if (!empty($NotificationCode)) {
        $whereStatement = $whereStatement . " and n.NotificationCode = '" . $NotificationCode . "'";
    }
     
    $FollowUpId = getKeyVal($reqParam, "FollowUpId");
    if (!empty($FollowUpId)) {
        $whereStatement = $whereStatement . " and n.FollowUpId = '" . $FollowUpId . "'";
    }
     
     
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $whereStatement = $whereStatement . " and n.Description = '" . $Description . "'";
    } 

//createddate
    $filterMinCreatedDate = getKeyVal($reqParam, "MinCreatedDate");
    if (!empty($filterMinCreatedDate)) {
        $whereStatement = $whereStatement . " and n.CreatedDate >= '" . $filterMinCreatedDate . "'";
    }
    $filterMaxCreatedDate = getKeyVal($reqParam, "MaxCreatedDate");
    if (!empty($filterMaxCreatedDate)) {
        $whereStatement = $whereStatement . " and n.CreatedDate <= '" . $filterMaxCreatedDate . "'";
    } 
//createdby
    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . " and n.CreatedBy like '%" . $filterCreatedBy . "%'";
    }
 
   
    $sqlStatement = $sqlStatement . $whereStatement . " ; ";
     return  queryDB($sqlStatement);

      // var_dump($sqlStatement); 

    
}
 
 
 
 
?>