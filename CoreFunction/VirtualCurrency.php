<?php
 function checkAccessAdmin($userRec  )
{
     //ensure only return this to admin 
    if(empty( $userRec) ||  empty($userRec["UserType"]["Name"] ) || strtoupper($userRec["UserType"]["Name"] )!="ADMIN" ){

         return processErrorMessage($app, "Service denied ", "001");
    } 

}
//#entry point  
function getVirtualCurrency($app  )
{
  
    $reqParam = getJsonRequest($app); 

    $userInfo = getUserSessionInfo($app) ; 
    checkAccessAdmin($userInfo  ) ;

 
    getJsonResponse($app, getCommonVirtualCurrency($reqParam )  ); 
}



//#entry point 
function  deleteVirtualCurrency($app, $virtualCurrencyId) 
{

    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;    
    checkAccessAdmin($userInfo  ) ;
 

    checkEmpty($app,"virtualCurrencyId", $virtualCurrencyId) ;  

    //delete virtualCurrency record
    $sqlStatement      = "DELETE FROM VirtualCurrency WHERE Id='" . $virtualCurrencyId . "'";
    $dataResult      = crudDB($sqlStatement);  
 
    $result            = array(
        "status" => true,
    );


 
    getJsonResponse($app, $result); 
}


//#entry point 
function  editVirtualCurrency($app , $virtualCurrencyId    ){
  
    $reqParam = getJsonRequest($app);  
    $userInfo = getUserSessionInfo($app) ;
    checkAccessAdmin($userInfo  ) ; 
     

 
  
    $Description  = getKeyVal($reqParam, "Description"); 
    $Unit  = getKeyVal($reqParam, "Unit");
    $Cost  = getKeyVal($reqParam, "Cost");
 
    checkEmpty($app,"Description", $Description) ;
    checkEmpty($app,"Unit", $Unit) ;
    checkEmpty($app,"Cost", $Cost) ;

    $sqlStatement   = " Update  VirtualCurrency  ";
    $setStatement   = " SET ";
    $whereStatement = " WHERE Id='" . $virtualCurrencyId . "' ;";

  
     
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $setStatement = $setStatement . " Description='" . $Description . "' ,";
    }
   
    $Unit = getKeyVal($reqParam, "Unit");
    if (!empty($Unit)) {
        $setStatement = $setStatement . " Unit='" . $Unit . "' ,";
    } 
    $Cost = getKeyVal($reqParam, "Cost");
    if (!empty($Cost)) {
        $setStatement = $setStatement . " Cost='" . $Cost . "' ,";
    } 
    $CreatedBy = $userInfo["Id"];
    if (!empty($CreatedBy)) {
        $setStatement = $setStatement . " CreatedBy='" . $CreatedBy . "' ,";
    }
    $CreatedDate = "'" . date('Y-m-d H:i:s') . "' ";
    if (!empty($CreatedDate)) {
        $setStatement = $setStatement . " CreatedDate=" . $CreatedDate . " ,";
    }
    
    
    $setStatement = $setStatement . " CreatedDate=CreatedDate "; 
 
    $result = array( 
        "virtualCurrencyId" =>  crudDB($sqlStatement . $setStatement . $whereStatement) 
    );
    getJsonResponse($app,$result); 
     
}

//#entry point  
function addVirtualCurrency($app  ){
    $reqParam = getJsonRequest($app) ;
   
    $userInfo = getUserSessionInfo($app) ;  
    checkAccessAdmin($userInfo  ) ; 
  
     $result = getCommonVirtualCurrency(    $reqParam   ) ;  
     if (!empty($result[0]["Id"]) ) {
        return processErrorMessage($app, "DUPLICATE: Virtual Currency existed. ", "001");
    }


    $Description  = getKeyVal($reqParam, "Description"); 
    $Unit  = getKeyVal($reqParam, "Unit");
    $Cost  = getKeyVal($reqParam, "Cost");
 
    checkEmpty($app,"Description", $Description) ;
    checkEmpty($app,"Unit", $Unit) ;
    checkEmpty($app,"Cost", $Cost) ;


    $sqlStatement   = "INSERT INTO  VirtualCurrency (    
                        `Description` ,  
                        `Unit`  , 
                        `Cost`  ,   
                        `CreatedBy`   ,    
                        `CreatedDate`         
            )  ";
    $valueStatement = "VALUES (";
  
    $Description = getKeyVal($reqParam, "Description");
    if (!empty($Description)) {
        $valueStatement = $valueStatement . "'" . $Description . "',";
    }else{
         $valueStatement = $valueStatement . "'-',";
    }


    $Unit = getKeyVal($reqParam, "Unit");
    if (!empty($Unit)) {
        $valueStatement = $valueStatement . "'" . $Unit . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }


    $Cost = getKeyVal($reqParam, "Cost");
    if (!empty($Cost)) {
        $valueStatement = $valueStatement . "'" . $Cost . "',";
    }else{
         $valueStatement = $valueStatement . "'0',";
    }

//createdby
    $CreatedBy = $userInfo['Id'];
    if (!empty($CreatedBy)) {
        $valueStatement = $valueStatement . "'" . $CreatedBy . "',";
    }
    else{ 
        $valueStatement = $valueStatement . "'APIv1' ,";
    }
 
    
    //createddate
    $valueStatement = $valueStatement . "'" . date('Y-m-d H:i:s') . "' ";
    $valueStatement = $valueStatement . " )";

    $mysqli = crudDB($sqlStatement . $valueStatement); 
    $result = array(    "virtualCurrencyId" =>$mysqli->insert_id  );
    getJsonResponse($app,$result); 

}

function getCommonVirtualCurrency($reqParam ){  
 

    $sqlStatement   = 'SELECT 
                        `Id`  ,  
                        `Description` ,  
                        `Unit`  , 
                        `Cost`  ,  
                         DATEDIFF(CreatedDate ,NOW()  ) AS DaysAgo,
                        `CreatedBy`   ,    
                        `CreatedDate`         

    FROM VirtualCurrency ';
    $whereStatement = " where 1=1 ";
 
     
    $filterId = getKeyVal($reqParam, "Id");
    if (!empty($filterId)) {
        $whereStatement = $whereStatement . " and Id = '" . $filterId . "'";
    }
   
     
    $filterDescription = getKeyVal($reqParam, "Description");
    if (!empty($filterDescription)) {
        $whereStatement = $whereStatement . " and Description like '%" . $filterDescription . "%'";
    }  

// Cost
    $filterCostMin = getKeyVal($reqParam, "CostMin");
    if (!empty($filterCostMin)) {
        $whereStatement = $whereStatement . " and cast( a.Cost  as decimal)  >= cast( '" . $filterCostMin . "' as decimal)  ";
    }
      $filterCostMax = getKeyVal($reqParam, "CostMax");
    if (!empty($filterCostMax)) {
        $whereStatement = $whereStatement . " and  cast( a.Cost  as decimal)  <=  cast( '" . $filterCostMax . "'  as decimal)  ";
    }

// Unit
    $filterUnitMin = getKeyVal($reqParam, "UnitMin");
    if (!empty($filterUnitMin)) {
        $whereStatement = $whereStatement . " and cast( a.Unit  as decimal)  >= cast( '" . $filterUnitMin . "' as decimal)  ";
    }
      $filterUnitMax = getKeyVal($reqParam, "UnitMax");
    if (!empty($filterUnitMax)) {
        $whereStatement = $whereStatement . " and  cast( a.Unit  as decimal)  <=  cast( '" . $filterUnitMax . "'  as decimal)  ";
    } 

//createddate
    $filterMinCreatedDate = getKeyVal($reqParam, "MinCreatedDate");
    if (!empty($filterMinCreatedDate)) {
        $whereStatement = $whereStatement . " and  CreatedDate >= '" . $filterMinCreatedDate . "'";
    }
    $filterMaxCreatedDate = getKeyVal($reqParam, "MaxCreatedDate");
    if (!empty($filterMaxCreatedDate)) {
        $whereStatement = $whereStatement . " and  CreatedDate <= '" . $filterMaxCreatedDate . "'";
    } 
//createdby
    $filterCreatedBy = getKeyVal($reqParam, "CreatedBy");
    if (!empty($filterCreatedBy)) {
        $whereStatement = $whereStatement . " and CreatedBy like '%" . $filterCreatedBy . "%'";
    }
   
   
 
    $orderStatement = " order by CreatedDate asc ";
   
    $sqlStatement = $sqlStatement . $whereStatement .  $orderStatement ." ; ";
   return  queryDB($sqlStatement); 
    
}
 
 
 
 
?>