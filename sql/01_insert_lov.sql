
INSERT INTO  `UserType` (
`Id` ,
`Code` ,
`Name` ,
`CreatedBy` ,
`CreatedDate`
)
VALUES (
NULL ,  'INFLUENCER',  'Influencer',  'API',  '2016-06-10:12:12:12'
), (
NULL ,  'MERCHANT',  'Merchant',  'API',  '2016-06-10:12:12:12'
);

 
INSERT INTO `AccountType` (
	`Id`, `Code`, `Name`, `SubscriptionName`, `SubscriptionInfo`, 
	`Price`, `MaxAllowedMonth`,    `MaxAllowedCampaign`, 
	`MaxInfluencer`,  `MaxPosting`, `MaxCost`,  
	 `CreatedBy`, `CreatedDate`)
 VALUES (NULL, 'P10', 'REGULAR ACCOUNT BASIC', 'REGULAR ACCOUNT BASIC',
  'JUST REGULAR ACCOUNT','3',  '10',   '10', '10', '10.60', '10', 'API', '2016-06-10:12:12:12');


 INSERT INTO  `InterestTag` (
`Id` ,
`Name` ,
`Description` ,
`IsActive` ,
`CreatedBy` ,
`CreatedDate`
)
VALUES ( NULL ,  'Tech',  'Tech',  '1',  'API',  '2016-06-10:12:12:12' ), 
( NULL ,  'Beauty',  'Beauty',  '1',  'API',  '2016-06-10:12:12:12' ),
 ( NULL ,  'Fashion',  'Fashion',  '1',  'API',  '2016-06-10:12:12:12' ),
  (
NULL ,  'Sports',  'Sports',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Food',  'Food',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Cars',  'Cars',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Drinks',  'Drinks',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Event',  'Event',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Business',  'Business',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Fitness',  'Fitness',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Movies',  'Movies',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Travel',  'Travel',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Sports',  'Sports',  '1',  'API',  '2016-06-10:12:12:12'
),(
NULL ,  'Game',  'GAME',  '1',  'API',  '2016-06-10:12:12:12'
);
   

Delete from CampaignStatus;
INSERT INTO  `CampaignStatus` ( `Id` , `StatusCode` , `Description` )
VALUES  
( NULL ,  'AVAIL',  'AVAIL' ),
( NULL ,  'REQUE',  'REQUESTED' ),
( NULL ,  'APPRO',  'APPROVED' ), 
( NULL ,  'NOPAY',  'NO PAYMENT YET' ),
( NULL ,  'PROGR',  'IN PROGRESS ' ),
( NULL ,  'COMPL',  'TRX COMPLETED' ),

( NULL ,  'INF-REQUE',  'REQUESTED' ), #INFLUENCER REQUEST CAMPAIGN TO MERCHANT
#( NULL ,  'INF-APPRO',  'APPROVED' ), #INFLUENCER REQUEST IS APPROVED
#( NULL ,  'INF-DISSC',  'IN DISCUSSION ' ), #INFLUENCER REQUEST IS IN DISUCCSION WITH MERCHANT
( NULL ,  'INF-PROGR',  'IN PROGRESS ' ), #INFLUENCER IS FINALLY ABLE TO START AFTER DISCUSSION, WITH MILESTONE CREATED.
( NULL ,  'INF-COMPL',  'INFLUENCER ALL TASKS COMPLETED ' ), #INFLUENCER MARK COMPLETE AFTER FINISHES MIELESTONE
( NULL ,  'INF-VERIF',  'IN COMPL ' ), #INFLUENCER GETS NOTICE WHEN THE MERCHANT VERFIES THE MILESTONE. MONEY WILL AUTO DEBIT NEXT DAY...
#( NULL ,  'INF-FINIS',  'TRX FINISH' ) ; #INFLUENCER GETS THE MONEY FROM THE SYSTEM..
 
#
# 'INF-REQUE', 'INF-APPRO', 'INF-DISSC',  'INF-PROGR','INF-COMPL',  'INF-VERIF',  'INF-FINIS',  