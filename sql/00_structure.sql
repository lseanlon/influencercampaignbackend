 CREATE DATABASE ifluence;
 Drop table User;
 CREATE TABLE  `User` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`UserLoginName` VARCHAR( 100 ) ,
	`GoogleLoginName` VARCHAR( 100 ) ,
	`FbLoginName` VARCHAR( 100 ) ,
	`LoginType` VARCHAR( 10 ) , #* FB, GOOGLE, NORMAL
	`Password` VARCHAR( 100 ) ,
	`Name` VARCHAR( 255 ) ,
	`Email` VARCHAR( 255 ) ,
	`Mobile` VARCHAR( 255 ) ,
	`Dob` VARCHAR( 255 ) ,
	`Profession` VARCHAR( 255 ) ,
	`Gender` VARCHAR( 255 ) ,
	`Languange` VARCHAR( 50 ) ,
	`Location` VARCHAR( 50 ) ,
	`Country` VARCHAR( 50 ) ,
	`Summary` VARCHAR( 255 ) ,
	`PreferredBankNo` VARCHAR( 255 ) ,
	`PreferredBankName` VARCHAR( 255 ) ,
	`PreferredAccNo` VARCHAR( 255 ) ,
	`PreferredAccName` VARCHAR( 255 ) ,
	`Designation` VARCHAR( 255 ) , 
	`CompanyName` VARCHAR( 255 ) ,
	`CompanyNature` VARCHAR( 255 ) ,
	`CompanyNo` VARCHAR( 255 ) ,
	`CompanyLocation` VARCHAR( 255 ) , 
	`FailedLoginAttempt` INT( 5 ) ,
	`IsLoggedOut` TINYINT( 1 ) ,
	`IsTempBlocked` TINYINT( 1 ) ,
	`IsActive` TINYINT( 1 ), 
	`lastFailedLogin` DATETIME( 3 )   , 
	`lastSuccessfulLogin` DATETIME( 3 )   , 
	`sessionKey` VARCHAR( 500 ) , 
	`UserTypeId` INT( 10 ) ,  #*
	`AccountTypeId` INT( 10 ),  #*
	`isFeatureUnlockedInfluencer` TINYINT( 1 )  ,  
	`PhotoId` INT( 10 ) ,  #*
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   , 
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
 
 ALTER TABLE User
ADD  `isLoggedOut` TINYINT( 1 )  ;

ALTER TABLE User
ADD  `VirtualAmount` decimal( 10,2 )  , 
ADD  `VirtualAmountCheckoutDate` DATETIME( 3 )    ;

Drop table PasswordReset;
CREATE TABLE  `PasswordReset` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`UserId` VARCHAR( 100 ) , 
	`Password` VARCHAR( 100 ) , 
	`ActivationCode` VARCHAR( 500 ) , 
	`Status` VARCHAR( 10 ) , #REQ AND USED
	`CreatedBy` VARCHAR( 100 ) ,    
	`CreatedDate`  DATETIME( 3 ) ,  
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


Drop table SessionHistory;
CREATE TABLE  `SessionHistory` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`UserId` VARCHAR( 100 ) , 
	`Password` VARCHAR( 100 ) , 
	`SessionKey` VARCHAR( 500 ) , 
	`Status` VARCHAR( 10 ) , 
	`CreatedBy` VARCHAR( 100 ) ,   
	`LoginDate`  DATETIME( 3 ) , 
	`LogoutDate`  DATETIME( 3 ) ,  
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


#eg - influencer or merchant
CREATE TABLE  `UserType` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`Code` VARCHAR( 10 ) ,
	`Name` VARCHAR( 100 ) ,  
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,  
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
   
 CREATE TABLE  `UserUsage` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`UserId` INT( 10 ) ,#*
	`Price` VARCHAR( 100 ) ,  
	`RemainingAllowedCampaign` INT( 10 ), 
	`RemainingInfluencer` INT( 10 ), 
	`RemainingPosting` INT( 10 ), 
	`MaxCostRemaining` decimal( 10,2 ),  
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   , 
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
    

 #eg - subscription plan name
 DROP TABLE AccountType;
 CREATE TABLE  `AccountType` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`Code` VARCHAR( 10 ) ,
	`Name` VARCHAR( 100 ) , 
	`SubscriptionName` VARCHAR( 100 ) , 
	`SubscriptionInfo` VARCHAR( 300 ) , 
	`Price` VARCHAR( 100 ) , 
	`MaxAllowedCampaign` INT( 10 ), 
	`MaxInfluencer` INT( 10 ), 
	`MaxPosting` INT( 10 ), 
	`MaxAllowedMonth` INT( 10 ), 
	`MaxCost` decimal( 10,2 ),  
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   , 
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
 
CREATE TABLE  `Photo` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`Url` VARCHAR( 255 ) ,
	`Blob` BLOB,  
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   , 
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


#eg - payment info of a user
CREATE TABLE  `UserPayment` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`UserId` INT( 10 ) ,#*
	`PaymentAccNo` VARCHAR( 30 ) ,
	`PaymentHolderName` VARCHAR( 100 ) , 
	`PaymentExpiryDate` VARCHAR( 100 ) , 
	`PaymentSecurityCode` VARCHAR( 100 ) , 
	`PaymentIssuerName` VARCHAR( 100 ) ,  #eg maybank
	`PaymentIssuerCategory` VARCHAR( 100 ) , #eg amex,master,visa
	`PaymentType` VARCHAR( 100 ) ,  #eg. CARD, mol
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,  
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;

#eg -influencer  user
Drop table UserInfluence;
 
CREATE TABLE  `UserInfluence` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`IsBanned` TINYINT( 1 )  ,  
	`IsFbConnected` TINYINT( 1 )  ,  
	`IsFbPage` TINYINT( 1 )  ,  
	`IsInstagramConnected` TINYINT( 1 )  ,  
	`IsTwitterConnected` TINYINT( 1 )  ,  
	`IsGoogleConnected` TINYINT( 1 )  ,  
	`IsVineConnected` TINYINT( 1 )  ,  
	`TotalSuccessfulCampaign` INT( 10 ) , 
	`TotalFailedCampaign`  INT( 10 ) , 
	`TotalRunningCampaign` INT( 10 ) ,  
	`TotalAmountCollected`  decimal( 10,2 ), 

	`TotalFollowerFb` INT( 10 ) , 
	`TotalFollowerHangout` INT( 10 ) , 
	`TotalFollowerInstagram` INT( 10 ) , 
	`TotalFollowerTwitter` INT( 10 ) ,  
	`TotalFollowerVine` INT( 10 ) , 
	`TotalFriendFb` INT( 10 ) , 
	`TotalLikesFb` INT( 10 ) , 
 

	`InstagramTotalMedia` INT( 10 ) , 
	`InstagramTotalSubscribes` INT( 10 ) ,   

	`FacebookTotalCampaign` INT( 10 ) ,  
	`FacebookTotalPostLikes` INT( 10 ) , 
	`FacebookTotalPostComment` INT( 10 ) , 
	`FacebookTotalPostShares` INT( 10 ) , 
	`FacebookTotalPostReactions` INT( 10 ) , 
 

	`PageAuthority`  decimal( 10,2 ),
	`PageRank`  decimal( 10,2 ),
	`ReplyRatio`  decimal( 10,2 ),
	`AverageShared`  decimal( 10,2 ), 
	`UserId` INT( 10 ) ,#*
	`CreatedBy` VARCHAR( 100 ) ,     
	`CreatedDate`  DATETIME( 3 )   ,  
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
 

ALTER TABLE UserInfluence
ADD  `InstagramTotalMedia` TINYINT( 1 )  ;
ALTER TABLE UserInfluence
ADD  `InstagramTotalSubscribes` TINYINT( 1 )  ;


CREATE TABLE  `Reviews` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`FromId` INT( 10 ) UNSIGNED NOT NULL , #* link to id in user influencer
	`ToId` INT( 10 ) UNSIGNED NOT NULL ,  #* link to id in user influencer
	`Name` VARCHAR( 100 ) , 
	`Stars` VARCHAR( 100 ) ,  #star rating
	`Description` VARCHAR( 100 ) ,  #title text
	`DescriptionLong` VARCHAR( 100 ) ,  #body textarea 
	`CreatedBy` VARCHAR( 100 ) ,   #* link to id in user merchant
	`CreatedDate`  DATETIME( 3 )   ,  
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


#eg hash tag
 CREATE TABLE  `InterestTag` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
	`Name` VARCHAR( 100 ) , 
	`Description` VARCHAR( 100 ) ,  
	`IsActive` TINYINT( 1 )  ,  
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   , 
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
 
 #eg. tagId=1 has this user[Influencer/merchant]
CREATE TABLE  `UserTag` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`InterestTagId` INT( 10 )  ,#*
	`UserId` INT( 10 )  , #*
	`IsInfluencer` TINYINT( 1 )  ,     
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
 
 
#-----------------------------
#-----------------------------
##	CAMPAIGN
#
#-------------------------------------
Drop table Campaign; 
CREATE TABLE  `Campaign` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`Name` VARCHAR( 100 ) , 
	`Description` VARCHAR( 255 ) ,  
	`PromoTitle` VARCHAR( 100 ) ,  
	`PromoCaption` VARCHAR( 300 ) ,   
	`PromoDescription` VARCHAR( 300 ) ,  
	`Price` decimal( 10,2 ), 
	`NoOfInfluencer` INT( 10 ), 
	`NoOfPosting` INT( 10 ), 
	`Charges1` decimal( 10,2 ),
	`Charges1Desc` VARCHAR( 100 ) ,
	`Charges2` decimal( 10,2 ),
	`Charges2Desc` VARCHAR( 100 ) ,
	`Charges3` decimal( 10,2 ),
	`Charges3Desc` VARCHAR( 100 ) , 
	`TotalCost` decimal( 10,2 ),     
	`PhotoIdList` VARCHAR( 1000  ) , #* keep all photoId
	`TagIdList` VARCHAR( 1000  ) , #* keep all interest tag
	`IsActive` TINYINT( 1 )  ,   #indicate expiry  
	`FromDate`  DATETIME( 3 )   , 
	`ToDate`  DATETIME( 3 )   , 
	`DueDate`  DATETIME( 3 )   , 
	`ClosingDate`  DATETIME( 3 )   , 
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   , 
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
   
ALTER TABLE Campaign
ADD  `ClosingDate` TINYINT( 1 )  ;

ALTER TABLE Campaign
ADD  `DueDate` DATETIME( 3 )  ;

#eg hash tag
drop table CampaignTag;
--  CREATE TABLE  `CampaignTag` (
-- 	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,
-- 	`CampaignId` INT( 10 )  ,#*
-- 	`InterestTagId` INT( 10 )  , #*
-- PRIMARY KEY (  `id` )
-- ) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;

# REQUEST, ACCEPTED
CREATE TABLE  `CampaignStatus` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`StatusCode` VARCHAR( 10 ) ,   
	`Description`   VARCHAR( 100 ) ,  
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


#eg CampaignSuggestions
 CREATE TABLE  `CampaignSuggestions` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`CampaignId` INT( 10 )  ,#*
	`InfluencerId` INT( 10 )  , #*
	`TopInterestTagId` INT( 10 )  , #*
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;

  
Drop table CampaignRunning; 
#eg CampaignRunning -Influecncer & brands interaction
 CREATE TABLE  `CampaignRunning` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`CampaignId` INT( 10 )  ,#*
	`CampaignRunningGroupId` INT( 10 )  , #* 
	`InfluencerId` INT( 10 )  , #* 
	`MerchantId` INT( 10 )  , #* 
	`MilestoneDescription` VARCHAR( 300 ) ,   
	`AgreedTotalCost` decimal( 10,2 ),
	`AgreedNoOfPosting` INT( 10 )  , #*  
	`InfluencerSinglePostCost`  decimal( 10,2 ),
	`InfluencerRemainingCost`   decimal( 10,2 ),
	`InfluencerRemainingPost` INT( 10 ),  
	`StatusId` INT( 10 )  ,  #*     'REQUE'  'APPRO'  'NOPAY'  'PROGR'  'COMPL'  'INACT'
	`CampaignPaymentId` INT( 10 )  , #* CampaignPaymentHistory   
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1; 
   
 
ALTER TABLE CampaignRunning	 
ADD  `CampaignRunningGroupId` INT( 10 )  ;

 Drop table CampaignRunningGroup; 
#eg CampaignRunningGroup  for a group influencers in a running campaign
 CREATE TABLE  `CampaignRunningGroup` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`CampaignRunningId` INT( 10 )  ,#* 
	`GroupName` VARCHAR( 2000 ) ,       
	`GroupDescription` VARCHAR( 2000 ) ,       
	`InfluencerId` INT( 10 )  , #* 
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1; 
 
ALTER TABLE CampaignRunningGroup	 
ADD  `GroupDescription` VARCHAR( 2000 )  ;

 Drop table CampaignRunningMilestone; 
#eg CampaignRunningMilestone -Influecncer & brands interaction
 CREATE TABLE  `CampaignRunningMilestone` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`CampaignRunningId` INT( 10 )  ,#* 
	`MilestoneDescription` VARCHAR( 2000 ) ,      
	`MilestoneQuestion` VARCHAR( 2000 ) ,   
	`MilestoneAnswers` VARCHAR( 2000 ) ,      
	`Price` decimal( 10,2 ), 
	`Funded` decimal( 10,2 ), 
	`Paid` decimal( 10,2 ), 
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1; 
    

 Drop table CampaignHistory;     
 CREATE TABLE  `CampaignHistory` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`InfluencerId` INT( 10 ) ,#*
	`PostTitle`   VARCHAR( 500 ) ,   
	`PostLink`   VARCHAR( 100 ) ,   
	`PostContent`   VARCHAR( 1000 ) , 
	`PostPhotoIdList` VARCHAR( 100 ) ,#*  
	`CampaignRunningId` INT( 10 ) ,#*  
	`FacebookPostId` INT( 10 ),  
	`TwitterPostId` INT( 10 ),  
	`InstagramPostId` INT( 10 ),  
	`VinePostId` INT( 10 ), 
	`GooglePostId` INT( 10 ),  
	`PostCost` decimal( 10,2 ), 
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   , 
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
    


#eg CampaignPaymentHistory
 Drop table CampaignPaymentHistory; 
 CREATE TABLE  `CampaignPaymentHistory` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`CampaignId` INT( 10 )  ,#* 
	`FromUserId` INT( 10 )  ,#* 
	`ToUserId` INT( 10 )  ,#* 
	`Amount` decimal( 10,2 ), 
	`TrxCharges` decimal( 10,2 ), 
	`MerchantPortal` VARCHAR( 100 ) ,   
	`MerchantName` VARCHAR( 100 ) ,   
	`RecipientBankAccNo` VARCHAR( 100 ) ,   
	`RecipientName` VARCHAR( 100 ) ,   
	`PaymentStatus` VARCHAR( 5 ) ,   
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;

#statistic and report purpose for campaign
#facebook data of influencer post on campaign id
Drop table CampaignFacebook; 
 CREATE TABLE  `CampaignFacebook` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`CampaignId` INT( 10 )  ,#* 
	`UserId` INT( 10 )  ,#* 
	`PostedId`   INT( 10 ),
	`PostContent` VARCHAR( 400 ) ,   
	`PostedDate`  DATETIME( 3 )   ,
	`TotalPostComment` INT( 10 ),
	`TotalPostReaction` INT( 10 ),
	`TotalLike` INT( 10 ),
	`TotalPostShare` INT( 10 ),
	`ProductPageId`   INT( 10 ),
	`TotalProductPageShare` INT( 10 ),
	`TotalProductPageFollow` INT( 10 ),
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


Drop table VirtualCurrency; 
 CREATE TABLE  `VirtualCurrency` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,  
	`Description` VARCHAR( 100 ) , 
	`Unit` decimal( 10,2 ), 
	`Cost` decimal( 10,2 ),  
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


#log down - which user buy/sell and how much and when
Drop table VirtualCurrencyTransaction; 
 CREATE TABLE  `VirtualCurrencyTransaction` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,    
	`FromUserId` INT( 10  )  NOT NULL,   # 0 is admin or ifluence system
	`ToUserId` INT( 10  )  NOT NULL,   
	`FromUserPaymentId` INT( 10  )  NOT NULL,   #user payment account that gives
	`ToUserPaymentId` INT( 10  )  NOT NULL,    #user payment account that receives
	`TransactionType` VARCHAR( 20 ) ,   # MERCHANT-GET / MERCHANT-GIVE / INFLUENCER-GET / INFLUENCER-GIVE
	`VirtualAmount` decimal( 10,2 ), 
	`RealAmount` decimal( 10,2 ),  	
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;

 
Drop table Notification; 
 CREATE TABLE  `Notification` ( 
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT , 
	`Code` VARCHAR(30 ) ,   
	`Description`   VARCHAR( 100 ) ,  
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;
 
Drop table NotificationTransaction; 
 CREATE TABLE  `NotificationTransaction` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,    
	`NotificationId` INT( 10  )  NOT NULL,   #   Notification
	`NotificationCode` VARCHAR( 30  )  ,   
	`ToUserId` INT( 10  )   ,   
	`IsRead` TINYINT( 1 ) ,
	`FromUserId` INT( 10  )   ,    
	`FollowUpId` INT( 10  )   ,    #can be campaign id, campaignrunning id, person id, etc,link
 	`Description`   VARCHAR( 700 ) ,   
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


ALTER TABLE NotificationTransaction
ADD  `IsRead` TINYINT( 1 )  ;

#favorite feature for both merchant and influencer id
Drop table Bookmark; 
 CREATE TABLE  `Bookmark` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,    
	`InfluencerId` INT( 10  )   ,    
	`MerchantId` INT( 10  )   ,   
	`CampaignId` INT( 10  )   ,   
	`Priority` VARCHAR( 100 ) ,   #index to sort
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;


Drop table JobSuggestion; 
 CREATE TABLE  `JobSuggestion` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,    
	`CampaignId` INT( 10  )  NOT NULL,   
	`NotificationId` INT( 10  )  NOT NULL,    
	`ToUserId` INT( 10  )   ,   
	`FromUserId` INT( 10  )   ,    
  	`Description`   VARCHAR( 700 ) ,   
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;




 # CampaignRunningChat
Drop table CampaignRunningChat; 
 CREATE TABLE  `CampaignRunningChat` (
	`Id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT ,    
	`CampaignRunningId` INT( 10  )  NOT NULL,       
	`SenderId` INT( 10  )  NOT NULL,       
	`ReceiverId` INT( 10  )  NOT NULL,     
	`Message` VARCHAR( 300 ) ,   #index to sort
	`CreatedBy` VARCHAR( 100 ) ,   
	`CreatedDate`  DATETIME( 3 )   ,
PRIMARY KEY (  `id` )
) ENGINE = MYISAM DEFAULT CHARSET = UTF8 AUTO_INCREMENT =1;