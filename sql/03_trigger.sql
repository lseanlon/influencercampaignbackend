#adding a record into VirtualCurrencyTransaction 
#	--> update user amount
#	--> update user balance
DELIMITER $$ 

DROP TRIGGER  adjustUserBalance_after_insert;
 


CREATE
	TRIGGER `adjustUserBalance_after_insert` AFTER INSERT 
	ON `VirtualCurrencyTransaction` 
	FOR EACH ROW BEGIN   
		SET @newVirtualAmount =(SELECT COALESCE(VirtualAmount, 0) FROM User  WHERE Id = NEW.ToUserId)  ;
	    SET @newVirtualAmount = @newVirtualAmount + NEW.VirtualAmount ; 

	   	UPDATE `User` SET VirtualAmount = @newVirtualAmount  WHERE Id = NEW.ToUserId ;
    END ;


DELIMITER ;
